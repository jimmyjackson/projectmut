/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.21 : Database - projectmut
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`projectmut` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `projectmut`;

/*Table structure for table `access` */

DROP TABLE IF EXISTS `access`;

CREATE TABLE `access` (
  `access_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `access_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `access_url` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `id_mainmenu` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`access_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `access` */

insert  into `access`(`access_id`,`access_name`,`access_url`,`access_status`,`id_mainmenu`) values ('ACC00001','จัดการข้อมูลพนักงาน','emp_controller','1','1'),('ACC00002','จัดการข้อมูลสิทธ์ตำแหน่ง','position_controller','1','1'),('ACC00003','จัดการข้อมูลสมาชิก','member_controller','1','1'),('ACC00004','จัดการข้อมูลหนังสือ','book_controller','1','1'),('ACC00005','จัดการข้อมูลสำนักพิมพ์','publishing_controller','1','1'),('ACC00006','จัดการข้อมูลหมู่หนังสือ','mubook_controller','1','1'),('ACC00007','จัดการข้อมูลหมู่ย่อยหนังสือ','submubook_controller','1','1'),('ACC00008','จัดการข้อมูลหัวเรื่องหนังสือ','title_controller','1','1'),('ACC00009','จัดการข้อมูลผู้แต่ง/ผู้แปล','author_controller','1','1'),('ACC00010','จัดการข้อมูลสำเนาหนังสือ','bookcopy_controller','1','1'),('ACC00011','การเสนอซื้อหนังสือ','offerorder_controller','1','2'),('ACC00012','การอนุมัติเสนอซื้อหนังสือ','approveorder_controller','1','2'),('ACC00013','การสั่งซื้อหนังสือ','purchaseorder_controller','1','2'),('ACC00014','การรับหนังสือจากการสั่งซื้อ','receiptorder_controller','1','2'),('ACC00015','การจองหนังสือ','booking_controller','1','3'),('ACC00016','การยืมหนังสือ','borrowing_controller','1','3'),('ACC00017','การคืนหนังสือ','returning_controller','1','3'),('ACC00018','การค้นหาข้อมูลสำเนาหนังสือ','searching_controller','1','4'),('ACC00019','การออกรายงานการสั่งซื้อหนังสือ','reportbuying_controller','1','5'),('ACC00020','การออกรายงานการยืมหนังสือ','reportborrowing_controller','1','5');

/*Table structure for table `accessposition` */

DROP TABLE IF EXISTS `accessposition`;

CREATE TABLE `accessposition` (
  `id_position` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_access` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_position`,`id_access`),
  KEY `fk_accessposition_access` (`id_access`),
  CONSTRAINT `fk_accessposition_access` FOREIGN KEY (`id_access`) REFERENCES `access` (`access_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_accessposition_position` FOREIGN KEY (`id_position`) REFERENCES `position` (`position_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `accessposition` */

insert  into `accessposition`(`id_position`,`id_access`) values ('POSIT0001','ACC00001'),('POSIT0002','ACC00001'),('POSIT0001','ACC00002'),('POSIT0003','ACC00002'),('POSIT0001','ACC00003'),('POSIT0001','ACC00004'),('POSIT0003','ACC00004'),('POSIT0001','ACC00005'),('POSIT0001','ACC00006'),('POSIT0001','ACC00007'),('POSIT0001','ACC00008'),('POSIT0001','ACC00009'),('POSIT0001','ACC00010'),('POSIT0001','ACC00011'),('POSIT0001','ACC00012'),('POSIT0001','ACC00013'),('POSIT0001','ACC00014'),('POSIT0001','ACC00015'),('POSIT0001','ACC00016'),('POSIT0001','ACC00017'),('POSIT0001','ACC00018'),('POSIT0001','ACC00019'),('POSIT0001','ACC00020');

/*Table structure for table `author` */

DROP TABLE IF EXISTS `author`;

CREATE TABLE `author` (
  `author_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `author_lname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `author_pname` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `author` */

insert  into `author`(`author_id`,`author_name`,`author_lname`,`author_pname`,`author_status`) values ('AUTH000001','ศุภชัย','สมพานิช','ศุภชัย สมพานิช','1'),('AUTH000002','ดร.ธันวา','ศรีประมง','คนไทยในลอนดอน','1'),('AUTH000003','ธันยพัฒน์','วงศ์รัตน์','ธันยพัฒน์ วงศ์รัตน์','1'),('AUTH000004','ผศ.ดร. วฤษาย์',' ร่มสายหยุด','ผศ.ดร. วฤษาย์ ร่มสายหยุด','1'),('AUTH000005','Jonathan','Chaffer','Jonathan Chaffer','1'),('AUTH000006','Karl','Swedberg','Karl Swedberg','1'),('AUTH000007','โกศล','โสฬสรุ่งเรือง','โกศล โสฬสรุ่งเรือง','1'),('AUTH000008','กุลวดี','โภคสวัสดิ์','กุลวดี โภคสวัสดิ์','1'),('AUTH000009','Davey','Shafik','Davey Shafik','1'),('AUTH000010','Matthew','Weier O\'Phinney','Matthew Weier O\'Phinney','1'),('AUTH000011','Ligaya','Turmelle','Ligaya Turmelle','1'),('AUTH000012','ทวิร','พานิชสมบัติ','ทวิร พานิชสมบัติ','1');

/*Table structure for table `authorbook` */

DROP TABLE IF EXISTS `authorbook`;

CREATE TABLE `authorbook` (
  `id_author` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_book` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_author`,`id_book`),
  KEY `fk_authorbook_book` (`id_book`),
  CONSTRAINT `fk_authorbook_author` FOREIGN KEY (`id_author`) REFERENCES `author` (`author_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_authorbook_book` FOREIGN KEY (`id_book`) REFERENCES `book` (`book_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `authorbook` */

insert  into `authorbook`(`id_author`,`id_book`) values ('AUTH000005','BOOK000001'),('AUTH000006','BOOK000001'),('AUTH000005','BOOK000002');

/*Table structure for table `book` */

DROP TABLE IF EXISTS `book`;

CREATE TABLE `book` (
  `book_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `book_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `book_size` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `book_page` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `book_image` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `book_price` double NOT NULL,
  `book_isbn` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `book_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `no_mubook` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `no_submubook` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `id_publishing` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`book_id`),
  KEY `fk_mubook_submubook` (`no_mubook`,`no_submubook`),
  KEY `fk_publishing` (`id_publishing`),
  CONSTRAINT `fk_mubook_submubook` FOREIGN KEY (`no_mubook`, `no_submubook`) REFERENCES `submubook` (`no_mubook`, `submubook_no`) ON UPDATE CASCADE,
  CONSTRAINT `fk_publishing` FOREIGN KEY (`id_publishing`) REFERENCES `publishing` (`publishing_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `book` */

insert  into `book`(`book_id`,`book_name`,`book_size`,`book_page`,`book_image`,`book_price`,`book_isbn`,`book_status`,`no_mubook`,`no_submubook`,`id_publishing`) values ('BOOK000001','nodejs','123*123*123','123','http://localhost/CI/upload/demoUpload.jpg',150,'1234567890df','1','000','000','PUBLI00001'),('BOOK000002','bas','123*123*123','320','http://localhost/CI/upload/20160312173901_228691_218619748166350_8213149_n.jpg',350,'12445555555555assad','1','100','100','PUBLI00001');

/*Table structure for table `bookcopy` */

DROP TABLE IF EXISTS `bookcopy`;

CREATE TABLE `bookcopy` (
  `id_book` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `bookcopy_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `bookcopy_callno` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `bookcopy_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_book`,`bookcopy_id`),
  CONSTRAINT `fk_book` FOREIGN KEY (`id_book`) REFERENCES `book` (`book_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `bookcopy` */

/*Table structure for table `booking` */

DROP TABLE IF EXISTS `booking`;

CREATE TABLE `booking` (
  `booking_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `booking_date` date NOT NULL,
  `id_booktype` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_book` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_member` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`booking_id`),
  KEY `fk_booking_booktype` (`id_booktype`),
  KEY `fk_booking_book` (`id_book`),
  KEY `fk_booking_member` (`id_member`),
  CONSTRAINT `fk_booking_book` FOREIGN KEY (`id_book`) REFERENCES `book` (`book_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_booking_booktype` FOREIGN KEY (`id_booktype`) REFERENCES `bookingtype` (`bookingtype_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_booking_member` FOREIGN KEY (`id_member`) REFERENCES `member` (`member_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `booking` */

/*Table structure for table `bookingcounter` */

DROP TABLE IF EXISTS `bookingcounter`;

CREATE TABLE `bookingcounter` (
  `id_booking` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_employee` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_booking`),
  KEY `fk_bookingcounter_employee` (`id_employee`),
  CONSTRAINT `fk_bookingcounter_booking` FOREIGN KEY (`id_booking`) REFERENCES `booking` (`booking_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_bookingcounter_employee` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `bookingcounter` */

/*Table structure for table `bookingdetial` */

DROP TABLE IF EXISTS `bookingdetial`;

CREATE TABLE `bookingdetial` (
  `id_booking` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `bookingdetial_no` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `bookingdetial_datereceiptbook` date NOT NULL,
  `bookingdetial_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_booking`,`bookingdetial_no`),
  CONSTRAINT `fk_bookingdetial_booking` FOREIGN KEY (`id_booking`) REFERENCES `booking` (`booking_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `bookingdetial` */

/*Table structure for table `bookingtype` */

DROP TABLE IF EXISTS `bookingtype`;

CREATE TABLE `bookingtype` (
  `bookingtype_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `bookingtype_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `bookingtype_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`bookingtype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `bookingtype` */

/*Table structure for table `borrowing` */

DROP TABLE IF EXISTS `borrowing`;

CREATE TABLE `borrowing` (
  `borrowing_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `borrowing_date` date NOT NULL,
  `id_employee` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_member` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`borrowing_id`),
  KEY `fk_borrowing_emp` (`id_employee`),
  KEY `fk_borrowing_mem` (`id_member`),
  CONSTRAINT `fk_borrowing_emp` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_borrowing_mem` FOREIGN KEY (`id_member`) REFERENCES `member` (`member_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `borrowing` */

/*Table structure for table `borrowingbooking` */

DROP TABLE IF EXISTS `borrowingbooking`;

CREATE TABLE `borrowingbooking` (
  `id_borrowing` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `no_borrowingdetial` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_booking` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `no_bookingdetial` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_borrowing`,`no_borrowingdetial`),
  KEY `fk_borrowingbooking_booking` (`id_booking`,`no_bookingdetial`),
  CONSTRAINT `fk_borrowingbooking_booking` FOREIGN KEY (`id_booking`, `no_bookingdetial`) REFERENCES `bookingdetial` (`id_booking`, `bookingdetial_no`) ON UPDATE CASCADE,
  CONSTRAINT `fk_borrowingbooking_borrowing` FOREIGN KEY (`id_borrowing`, `no_borrowingdetial`) REFERENCES `borrowingdetial` (`id_borrowing`, `borrowingdetial_no`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `borrowingbooking` */

/*Table structure for table `borrowingdetial` */

DROP TABLE IF EXISTS `borrowingdetial`;

CREATE TABLE `borrowingdetial` (
  `id_borrowing` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `borrowingdetial_no` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `borrowingdetial_datereturn` date NOT NULL,
  `borrowingdetial_status` char(30) COLLATE utf8_unicode_ci NOT NULL,
  `id_bookcopy` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_borrowing`,`borrowingdetial_no`),
  KEY `fk_borrowingdetial_bookcopy` (`id_bookcopy`),
  CONSTRAINT `fk_borrowingdetial_bookcopy` FOREIGN KEY (`id_bookcopy`) REFERENCES `bookcopy` (`id_book`) ON UPDATE CASCADE,
  CONSTRAINT `fk_borrowingdetial_borrowing` FOREIGN KEY (`id_borrowing`) REFERENCES `borrowing` (`borrowing_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `borrowingdetial` */

/*Table structure for table `ci_sessions` */

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ci_sessions` */

insert  into `ci_sessions`(`session_id`,`ip_address`,`user_agent`,`last_activity`,`user_data`) values ('6a16bc1d7f5014431ce90d4970bbf1f1','::1','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36',1460527956,'a:14:{s:9:\"user_data\";s:0:\"\";s:5:\"idemp\";s:8:\"EMP00001\";s:6:\"imgemp\";s:50:\"http://localhost/CI/upload/20151129162213_logo.jpg\";s:8:\"username\";s:7:\"admin01\";s:19:\"permission_constant\";a:10:{s:14:\"emp_controller\";s:14:\"emp_controller\";s:19:\"position_controller\";s:19:\"position_controller\";s:17:\"member_controller\";s:17:\"member_controller\";s:15:\"book_controller\";s:15:\"book_controller\";s:21:\"publishing_controller\";s:21:\"publishing_controller\";s:17:\"mubook_controller\";s:17:\"mubook_controller\";s:20:\"submubook_controller\";s:20:\"submubook_controller\";s:16:\"title_controller\";s:16:\"title_controller\";s:17:\"author_controller\";s:17:\"author_controller\";s:19:\"bookcopy_controller\";s:19:\"bookcopy_controller\";}s:24:\"permission_constant_name\";a:10:{s:14:\"emp_controller\";s:57:\"จัดการข้อมูลพนักงาน\";s:19:\"position_controller\";s:72:\"จัดการข้อมูลสิทธ์ตำแหน่ง\";s:17:\"member_controller\";s:54:\"จัดการข้อมูลสมาชิก\";s:15:\"book_controller\";s:57:\"จัดการข้อมูลหนังสือ\";s:21:\"publishing_controller\";s:66:\"จัดการข้อมูลสำนักพิมพ์\";s:17:\"mubook_controller\";s:69:\"จัดการข้อมูลหมู่หนังสือ\";s:20:\"submubook_controller\";s:81:\"จัดการข้อมูลหมู่ย่อยหนังสือ\";s:16:\"title_controller\";s:84:\"จัดการข้อมูลหัวเรื่องหนังสือ\";s:17:\"author_controller\";s:76:\"จัดการข้อมูลผู้แต่ง/ผู้แปล\";s:19:\"bookcopy_controller\";s:72:\"จัดการข้อมูลสำเนาหนังสือ\";}s:19:\"permission_purchase\";a:4:{s:21:\"offerorder_controller\";s:21:\"offerorder_controller\";s:23:\"approveorder_controller\";s:23:\"approveorder_controller\";s:24:\"purchaseorder_controller\";s:24:\"purchaseorder_controller\";s:23:\"receiptorder_controller\";s:23:\"receiptorder_controller\";}s:24:\"permission_purchase_name\";a:4:{s:21:\"offerorder_controller\";s:54:\"การเสนอซื้อหนังสือ\";s:23:\"approveorder_controller\";s:75:\"การอนุมัติเสนอซื้อหนังสือ\";s:24:\"purchaseorder_controller\";s:54:\"การสั่งซื้อหนังสือ\";s:23:\"receiptorder_controller\";s:81:\"การรับหนังสือจากการสั่งซื้อ\";}s:17:\"permission_borrow\";a:3:{s:18:\"booking_controller\";s:18:\"booking_controller\";s:20:\"borrowing_controller\";s:20:\"borrowing_controller\";s:20:\"returning_controller\";s:20:\"returning_controller\";}s:22:\"permission_borrow_name\";a:3:{s:18:\"booking_controller\";s:39:\"การจองหนังสือ\";s:20:\"borrowing_controller\";s:39:\"การยืมหนังสือ\";s:20:\"returning_controller\";s:39:\"การคืนหนังสือ\";}s:17:\"permission_search\";a:1:{s:20:\"searching_controller\";s:20:\"searching_controller\";}s:22:\"permission_search_name\";a:1:{s:20:\"searching_controller\";s:78:\"การค้นหาข้อมูลสำเนาหนังสือ\";}s:17:\"permission_report\";a:2:{s:23:\"reportbuying_controller\";s:23:\"reportbuying_controller\";s:26:\"reportborrowing_controller\";s:26:\"reportborrowing_controller\";}s:22:\"permission_report_name\";a:2:{s:23:\"reportbuying_controller\";s:90:\"การออกรายงานการสั่งซื้อหนังสือ\";s:26:\"reportborrowing_controller\";s:75:\"การออกรายงานการยืมหนังสือ\";}}');

/*Table structure for table `district` */

DROP TABLE IF EXISTS `district`;

CREATE TABLE `district` (
  `id_province` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_zone` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `district_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `district_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `district_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`district_id`,`id_zone`,`id_province`),
  KEY `pk_fk_district` (`id_zone`,`id_province`),
  CONSTRAINT `pk_fk_district` FOREIGN KEY (`id_zone`, `id_province`) REFERENCES `zone` (`zone_id`, `id_province`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `district` */

insert  into `district`(`id_province`,`id_zone`,`district_id`,`district_name`,`district_status`) values ('PROV0001','ZONE0001','DIST0001','บางแก้ว','1'),('PROV0002','ZONE0001','DIST0001','ทุ่งสองห้อง','1'),('PROV0001','ZONE0002','DIST0002','บางด้วน','1'),('PROV0002','ZONE0002','DIST0002','กระทุ่มราย','1'),('PROV0002','ZONE0002','DIST0003','ลำผักชี','1');

/*Table structure for table `donation` */

DROP TABLE IF EXISTS `donation`;

CREATE TABLE `donation` (
  `id_book` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_bookcopy` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_donation` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_donation` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_book`,`id_bookcopy`),
  KEY `fk_donation_donation` (`id_donation`,`no_donation`),
  CONSTRAINT `fk_donation_book` FOREIGN KEY (`id_book`, `id_bookcopy`) REFERENCES `bookcopy` (`id_book`, `bookcopy_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_donation_donation` FOREIGN KEY (`id_donation`, `no_donation`) REFERENCES `donationorderdetial` (`id_donationorder`, `donationorderdetial_no`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `donation` */

/*Table structure for table `donationorder` */

DROP TABLE IF EXISTS `donationorder`;

CREATE TABLE `donationorder` (
  `donationorder_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `donationorder_date` date NOT NULL,
  `id_emp` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_dona` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`donationorder_id`),
  KEY `fk_donationorder_emp` (`id_emp`),
  KEY `fk_donationorder_dona` (`id_dona`),
  CONSTRAINT `fk_donationorder_dona` FOREIGN KEY (`id_dona`) REFERENCES `donator` (`donator_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_donationorder_emp` FOREIGN KEY (`id_emp`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `donationorder` */

/*Table structure for table `donationorderdetial` */

DROP TABLE IF EXISTS `donationorderdetial`;

CREATE TABLE `donationorderdetial` (
  `id_donationorder` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `donationorderdetial_no` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `donationorderdetial_unit` int(11) NOT NULL,
  `donationorderdetial_detial` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_donationorder`,`donationorderdetial_no`),
  CONSTRAINT `fk_donationorderdetial_iddona` FOREIGN KEY (`id_donationorder`) REFERENCES `donationorder` (`donationorder_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `donationorderdetial` */

/*Table structure for table `donator` */

DROP TABLE IF EXISTS `donator`;

CREATE TABLE `donator` (
  `donator_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `donator_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `donator_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `id_donoragencies` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`donator_id`),
  KEY `fk_id_donoragencies` (`id_donoragencies`),
  CONSTRAINT `fk_id_donoragencies` FOREIGN KEY (`id_donoragencies`) REFERENCES `donoragencies` (`donoragencies_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `donator` */

/*Table structure for table `donoragencies` */

DROP TABLE IF EXISTS `donoragencies`;

CREATE TABLE `donoragencies` (
  `donoragencies_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `donoragencies_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `donoragencies_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`donoragencies_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `donoragencies` */

/*Table structure for table `employee` */

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `employee_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `employee_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `employee_lname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `employee_sex` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `employee_address` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `employee_brithday` date NOT NULL,
  `employee_image` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `employee_idcard` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `employee_username` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `employee_password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `employee_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `id_postcode` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `id_province` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_zone` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_district` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_position` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `fk_employee_address` (`id_province`,`id_zone`,`id_district`,`id_postcode`),
  KEY `fk_employee_addres` (`id_postcode`,`id_province`,`id_zone`,`id_district`),
  KEY `fk_employee_positio` (`id_position`),
  CONSTRAINT `fk_employee_addres` FOREIGN KEY (`id_postcode`, `id_province`, `id_zone`, `id_district`) REFERENCES `postcode` (`postcode_id`, `id_province`, `id_zone`, `id_district`) ON UPDATE CASCADE,
  CONSTRAINT `fk_employee_positio` FOREIGN KEY (`id_position`) REFERENCES `position` (`position_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `employee` */

insert  into `employee`(`employee_id`,`employee_name`,`employee_lname`,`employee_sex`,`employee_address`,`employee_brithday`,`employee_image`,`employee_email`,`employee_idcard`,`employee_username`,`employee_password`,`employee_status`,`id_postcode`,`id_province`,`id_zone`,`id_district`,`id_position`) values ('EMP00001','bas','adultindy','ชาย','14/3 M.3','1991-01-01','http://localhost/CI/upload/20151129162213_logo.jpg','bas_adultindy07@hotmail.com','1102001626808','admin01','01b307acba4f54f55aafc33bb06bbbf6ca803e9a','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00002','rewiepe','ผผผผผผผผผผผ','ชาย','33','2015-06-04','http://localhost/CI/upload/20150608184003_5f375f6a.jpg','bas_adultindy07@hotmail.com','1102001626808','basadultindy3','01b307acba4f54f55aafc33bb06bbbf6ca803e9a','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00003','a','a','ชาย','aaa','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','aaasss','aaaaaaaaaaaaaaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00004','a','a2','ชาย','aaa','0000-00-00','http://localhost/CI/assets/images/demoUpload.jpg','bas_adultindy07@hotmail.com','1102001626808','aaaaaaaaaaaaaa','3499c60eea227453c779de50fc84e217e9a53a18','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00005','xxxx','xxxx','ชาย','xxxx','2015-03-19','./upload/20150319_Luffy.jpg','xxxx07@hotmail.com','1102001626808','xxxxxxxxx','xxxxxxxxxxxxxxxx','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00006','aaa','aaaa','ชาย','qqqqqqq','2015-03-20','./upload/20150320_Luffy.jpg','xxxx07@hotmail.com','1102001626808','hhhhhhhhh','qqqqqqqqqqqqqqqq','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00007','a','aaa','หญิง','qqqq','2015-03-29',NULL,'aaaa','1111111111111','aaaa','aaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00008','เธเธเธซเธซ','sdfsdf','เธเธฒเธข','1111','2015-03-15',NULL,'bas_adultindy07@hotmail.com','1102001626808','basadultindy','1111111111111111','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00009','เธเธเธเธเธ','เธเธเธเธเธเธเธเธเธเธ','เธเธฒเธข','3333','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','basadultindy3','3333333333333333','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00011','เธเธเธเธเธ','เธเธเธเธเธเธเธเธเธเธ','เธเธฒเธข','3333','0000-00-00','http://localhost/CI/upload/20150505195043_1509316_1603614116542811_6226532874515504688_n.jpg','bas_adultindy07@hotmail.com','1102001626808','basadultindy3','3333333333333333','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00012','a','a','เธเธฒเธข','aaa','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','aaasss','aaaaaaaaaaaaaaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00013','a','a2','เธเธฒเธข','aaa','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','aaaaaaaaaaaaaa','aaaaaaaaaaaaaaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00014','xxxx','xxxx','เธเธฒเธข','xxxx','2015-03-19','./upload/20150319_Luffy.jpg','xxxx07@hotmail.com','1102001626808','xxxxxxxxx','xxxxxxxxxxxxxxxx','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00015','aaa','aaaa','เธเธฒเธข','qqqqqqq','2015-03-20','./upload/20150320_Luffy.jpg','xxxx07@hotmail.com','1102001626808','hhhhhhhhh','qqqqqqqqqqqqqqqq','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00016','a','aaa','เธซเธเธดเ','qqqq','2015-03-29',NULL,'aaaa','1111111111111','aaaa','aaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00017','เน€เธยเน€เธยเน€เธเธเน€เธ','sdfsdf','เน€เธยเน','1111','2015-03-15',NULL,'bas_adultindy07@hotmail.com','1102001626808','basadultindy','1111111111111111','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00018','เน€เธยเน€เธยเน€เธยเน€เธ','เน€เธยเน€เธยเน€เธยเน€เธ','เน€เธยเน','3333','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','basadultindy3','3333333333333333','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00019','a','a','เน€เธยเน','aaa','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','aaasss','aaaaaaaaaaaaaaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00020','aaa','aaaa','aaaaaa','aaasd','2015-05-16',NULL,'dasdasdad','asdasdasdas','sdasdas','asdas','1','10210','PROV0002','ZONE0001','DIST0001','POSIT0001'),('EMP00021','เธเธเธเธเธ','เธเธเธเธเธเธเธเธเธเธ','เธเธฒเธข','3333','0000-00-00','http://localhost/CI/upload/20150505195043_1509316_1603614116542811_6226532874515504688_n.jpg','bas_adultindy07@hotmail.com','1102001626808','basadultindy3','3333333333333333','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00022','a','a','เธเธฒเธข','aaa','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','aaasss','aaaaaaaaaaaaaaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00023','a','a2','เธเธฒเธข','aaa','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','aaaaaaaaaaaaaa','aaaaaaaaaaaaaaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00024','xxxx','xxxx','เธเธฒเธข','xxxx','2015-03-19','./upload/20150319_Luffy.jpg','xxxx07@hotmail.com','1102001626808','xxxxxxxxx','xxxxxxxxxxxxxxxx','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00025','aaa','aaaa','เธเธฒเธข','qqqqqqq','2015-03-20','./upload/20150320_Luffy.jpg','xxxx07@hotmail.com','1102001626808','hhhhhhhhh','qqqqqqqqqqqqqqqq','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00026','a','aaa','เธซเธเธดเ','qqqq','2015-03-29',NULL,'aaaa','1111111111111','aaaa','aaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00027','เน€เธยเน€เธยเน€เธเธเน€เธ','sdfsdf','เน€เธยเน','1111','2015-03-15',NULL,'bas_adultindy07@hotmail.com','1102001626808','basadultindy','1111111111111111','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00028','เน€เธยเน€เธยเน€เธยเน€เธ','เน€เธยเน€เธยเน€เธยเน€เธ','เน€เธยเน','3333','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','basadultindy3','3333333333333333','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00029','a','a','เน€เธยเน','aaa','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','aaasss','aaaaaaaaaaaaaaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00030','aaa','aaaa','aaaaaa','aaasd','2015-05-16',NULL,'dasdasdad','asdasdasdas','sdasdas','asdas','1','10210','PROV0002','ZONE0001','DIST0001','POSIT0001'),('EMP00031','เธเธเธเธเธ','เธเธเธเธเธเธเธเธเธเธ','เธเธฒเธข','3333','0000-00-00','http://localhost/CI/upload/20150505195043_1509316_1603614116542811_6226532874515504688_n.jpg','bas_adultindy07@hotmail.com','1102001626808','basadultindy3','3333333333333333','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00032','a','a','เธเธฒเธข','aaa','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','aaasss','aaaaaaaaaaaaaaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00033','a','a2','เธเธฒเธข','aaa','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','aaaaaaaaaaaaaa','aaaaaaaaaaaaaaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00034','xxxx','xxxx','เธเธฒเธข','xxxx','2015-03-19','./upload/20150319_Luffy.jpg','xxxx07@hotmail.com','1102001626808','xxxxxxxxx','xxxxxxxxxxxxxxxx','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00035','aaa','aaaa','เธเธฒเธข','qqqqqqq','2015-03-20','./upload/20150320_Luffy.jpg','xxxx07@hotmail.com','1102001626808','hhhhhhhhh','qqqqqqqqqqqqqqqq','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00036','a','aaa','เธซเธเธดเ','qqqq','2015-03-29',NULL,'aaaa','1111111111111','aaaa','aaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00037','เน€เธยเน€เธยเน€เธเธเน€เธ','sdfsdf','เน€เธยเน','1111','2015-03-15',NULL,'bas_adultindy07@hotmail.com','1102001626808','basadultindy','1111111111111111','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00038','เน€เธยเน€เธยเน€เธยเน€เธ','เน€เธยเน€เธยเน€เธยเน€เธ','เน€เธยเน','3333','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','basadultindy3','3333333333333333','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00039','a','a','เน€เธยเน','aaa','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','aaasss','aaaaaaaaaaaaaaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00040','aaa','aaaa','aaaaaa','aaasd','2015-05-16',NULL,'dasdasdad','asdasdasdas','sdasdas','asdas','1','10210','PROV0002','ZONE0001','DIST0001','POSIT0001'),('EMP00041','เธเธเธเธเธ','เธเธเธเธเธเธเธเธเธเธ','เธเธฒเธข','3333','0000-00-00','http://localhost/CI/upload/20150505195043_1509316_1603614116542811_6226532874515504688_n.jpg','bas_adultindy07@hotmail.com','1102001626808','basadultindy3','3333333333333333','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00042','a','a','เธเธฒเธข','aaa','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','aaasss','aaaaaaaaaaaaaaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00043','a','a2','เธเธฒเธข','aaa','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','aaaaaaaaaaaaaa','aaaaaaaaaaaaaaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00044','xxxx','xxxx','เธเธฒเธข','xxxx','2015-03-19','./upload/20150319_Luffy.jpg','xxxx07@hotmail.com','1102001626808','xxxxxxxxx','xxxxxxxxxxxxxxxx','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00045','aaa','aaaa','เธเธฒเธข','qqqqqqq','2015-03-20','./upload/20150320_Luffy.jpg','xxxx07@hotmail.com','1102001626808','hhhhhhhhh','qqqqqqqqqqqqqqqq','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00046','a','aaa','เธซเธเธดเ','qqqq','2015-03-29',NULL,'aaaa','1111111111111','aaaa','aaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00047','เน€เธยเน€เธยเน€เธเธเน€เธ','sdfsdf','เน€เธยเน','1111','2015-03-15',NULL,'bas_adultindy07@hotmail.com','1102001626808','basadultindy','1111111111111111','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00048','เน€เธยเน€เธยเน€เธยเน€เธ','เน€เธยเน€เธยเน€เธยเน€เธ','เน€เธยเน','3333','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','basadultindy3','3333333333333333','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00049','a','a','เน€เธยเน','aaa','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','aaasss','aaaaaaaaaaaaaaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00050','aaa','aaaa','aaaaaa','aaasd','2015-05-16',NULL,'dasdasdad','asdasdasdas','sdasdas','asdas','1','10210','PROV0002','ZONE0001','DIST0001','POSIT0001'),('EMP00051','เธเธเธเธเธ','เธเธเธเธเธเธเธเธเธเธ','เธเธฒเธข','3333','0000-00-00','http://localhost/CI/upload/20150505195043_1509316_1603614116542811_6226532874515504688_n.jpg','bas_adultindy07@hotmail.com','1102001626808','basadultindy3','3333333333333333','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00052','a','a','เธเธฒเธข','aaa','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','aaasss','aaaaaaaaaaaaaaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00053','a','a2','เธเธฒเธข','aaa','2015-03-18',NULL,'bas_adultindy07@hotmail.com','1102001626808','aaaaaaaaaaaaaa','aaaaaaaaaaaaaaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00054','xxxx','xxxx','เธเธฒเธข','xxxx','2015-03-19','./upload/20150319_Luffy.jpg','xxxx07@hotmail.com','1102001626808','xxxxxxxxx','xxxxxxxxxxxxxxxx','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00055','aaa','aaaa','เธเธฒเธข','qqqqqqq','2015-03-20','./upload/20150320_Luffy.jpg','xxxx07@hotmail.com','1102001626808','hhhhhhhhh','qqqqqqqqqqqqqqqq','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00056','a','aaa','เธซเธเธดเ','qqqq','2015-03-29',NULL,'aaaa','1111111111111','aaaa','aaaa','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00057','เน€เธยเน€เธยเน€เธเธเน€เธ','sdfsdf','เน€เธยเน','1111','2015-03-15',NULL,'bas_adultindy07@hotmail.com','1102001626808','basadultindy','1111111111111111','1','10540','PROV0001','ZONE0001','DIST0001','POSIT0001'),('EMP00058','aaaaaaaas','asd','ชาย','1','0000-00-00','http://localhost/CI/upload/20150607185105_NoSQL-vs-Sql.jpg','bas_adultindy07@hotmail.com','1102001626808','1111233','747417f2206148a3118d02f3adf20b5e4139baac','1','10210','PROV0002','ZONE0001','DIST0001','POSIT0001'),('EMP00059','sadsa','sadas','ชาย','aaaaa','2015-04-06','http://localhost/CI/upload/demoUpload.jpg','bas_adultindy07@hotmail.com','1102001626808','admin01aa','3499c60eea227453c779de50fc84e217e9a53a18','1','10270','PROV0001','ZONE0002','DIST0002','POSIT0001'),('EMP00060','ฟฟฟฟ','ฟฟฟฟฟฟ','ชาย','22','0000-00-00','http://localhost/CI/upload/20151129173512_images.jpg','baq@gggg.vvv','1102001626808','2222222222222222','7a243de0f4889417c88415da382e96fce3a4653d','1','10210','PROV0002','ZONE0001','DIST0001','POSIT0002');

/*Table structure for table `groupofferorder` */

DROP TABLE IF EXISTS `groupofferorder`;

CREATE TABLE `groupofferorder` (
  `groupofferorder_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `groupofferorder_unittotal` int(11) NOT NULL,
  `groupofferorder_pricetotal` double NOT NULL,
  `id_book` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`groupofferorder_id`),
  KEY `fk_groupofferorder_book` (`id_book`),
  CONSTRAINT `fk_groupofferorder_book` FOREIGN KEY (`id_book`) REFERENCES `book` (`book_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `groupofferorder` */

insert  into `groupofferorder`(`groupofferorder_id`,`groupofferorder_unittotal`,`groupofferorder_pricetotal`,`id_book`) values ('GOF0000001',6,720,'BOOK000001'),('GOF0000002',7,2240,'BOOK000002'),('GOF0000003',6,720,'BOOK000001'),('GOF0000004',7,2240,'BOOK000002'),('GOF0000005',6,720,'BOOK000001'),('GOF0000006',7,2240,'BOOK000002'),('GOF0000007',6,720,'BOOK000001'),('GOF0000008',7,2240,'BOOK000002'),('GOF0000009',6,720,'BOOK000001'),('GOF0000010',7,2240,'BOOK000002');

/*Table structure for table `groupofferorderdetial` */

DROP TABLE IF EXISTS `groupofferorderdetial`;

CREATE TABLE `groupofferorderdetial` (
  `id_offerorder` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `no_offerorderdetial` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_groupofferorder` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `groupofferorderdetial_unit` int(11) NOT NULL,
  `groupofferorderdetial_price` double NOT NULL,
  PRIMARY KEY (`id_offerorder`,`no_offerorderdetial`,`id_groupofferorder`),
  KEY `fk_groupofferorderdetial_groupofferorder` (`id_groupofferorder`),
  CONSTRAINT `fk_groupofferorderdetial_groupofferorder` FOREIGN KEY (`id_groupofferorder`) REFERENCES `groupofferorder` (`groupofferorder_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_groupofferorderdetial_offerorde` FOREIGN KEY (`id_offerorder`, `no_offerorderdetial`) REFERENCES `offerorderdetial` (`id_offerorder`, `offerorderdetial_no`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `groupofferorderdetial` */

insert  into `groupofferorderdetial`(`id_offerorder`,`no_offerorderdetial`,`id_groupofferorder`,`groupofferorderdetial_unit`,`groupofferorderdetial_price`) values ('OFF0000001','1','GOF0000001',5,600),('OFF0000001','1','GOF0000003',5,600),('OFF0000001','1','GOF0000005',5,600),('OFF0000001','1','GOF0000007',5,600),('OFF0000001','1','GOF0000009',5,600),('OFF0000001','2','GOF0000002',6,1920),('OFF0000001','2','GOF0000004',6,1920),('OFF0000001','2','GOF0000006',6,1920),('OFF0000001','2','GOF0000008',6,1920),('OFF0000001','2','GOF0000010',6,1920),('OFF0000002','1','GOF0000001',1,120),('OFF0000002','1','GOF0000003',1,120),('OFF0000002','1','GOF0000005',1,120),('OFF0000002','1','GOF0000007',1,120),('OFF0000002','1','GOF0000009',1,120),('OFF0000002','2','GOF0000002',1,320),('OFF0000002','2','GOF0000004',1,320),('OFF0000002','2','GOF0000006',1,320),('OFF0000002','2','GOF0000008',1,320),('OFF0000002','2','GOF0000010',1,320);

/*Table structure for table `invoice` */

DROP TABLE IF EXISTS `invoice`;

CREATE TABLE `invoice` (
  `invoice_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `invoice_date` date NOT NULL,
  `invoice_totalprice` double NOT NULL,
  `id_employee` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`invoice_id`),
  KEY `fk_invoice_employee` (`id_employee`),
  CONSTRAINT `fk_invoice_employee` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `invoice` */

insert  into `invoice`(`invoice_id`,`invoice_date`,`invoice_totalprice`,`id_employee`) values ('INV0000000','2015-04-29',100,'EMP00001');

/*Table structure for table `invoicedetial` */

DROP TABLE IF EXISTS `invoicedetial`;

CREATE TABLE `invoicedetial` (
  `id_invoice` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `invoicedetial_no` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `invoicedetial_price` double NOT NULL,
  `id_returning` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `no_returning` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_invoice`,`invoicedetial_no`),
  KEY `fk_invoicedetial_returning` (`id_returning`,`no_returning`),
  CONSTRAINT `fk_invoicedetial_invoice` FOREIGN KEY (`id_invoice`) REFERENCES `invoice` (`invoice_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_invoicedetial_returning` FOREIGN KEY (`id_returning`, `no_returning`) REFERENCES `returningdetial` (`id_returning`, `returningdetial_no`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `invoicedetial` */

/*Table structure for table `member` */

DROP TABLE IF EXISTS `member`;

CREATE TABLE `member` (
  `member_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `member_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `member_lname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `member_brithday` date NOT NULL,
  `member_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `member_image` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `member_email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `member_sex` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `member_username` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `member_password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `member_registerdate` date NOT NULL,
  `member_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `id_membertype` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_postcode` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `id_province` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_zone` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_district` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`member_id`),
  KEY `fk_member_address` (`id_postcode`,`id_province`,`id_zone`,`id_district`),
  KEY `fk_member_type` (`id_membertype`),
  CONSTRAINT `fk_member_address` FOREIGN KEY (`id_postcode`, `id_province`, `id_zone`, `id_district`) REFERENCES `postcode` (`postcode_id`, `id_province`, `id_zone`, `id_district`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_member_type` FOREIGN KEY (`id_membertype`) REFERENCES `membertype` (`membertype_id`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `member` */

insert  into `member`(`member_id`,`member_name`,`member_lname`,`member_brithday`,`member_address`,`member_image`,`member_email`,`member_sex`,`member_username`,`member_password`,`member_registerdate`,`member_status`,`id_membertype`,`id_postcode`,`id_province`,`id_zone`,`id_district`) values ('MEM00001','tete','tetessss','2015-02-12','11s','http://localhost/CI/upload/20151213171901_12310694_1224371067579966_7019700724355912569_n.jpg','sess.homaru@hotmail.com','หญิง','student11','8e8062409c1877e0309b75d68eabda2644406262','2015-11-30','1','MEMTY00001','10530','PROV0002','ZONE0002','DIST0002'),('MEM00002','teteddd','tete','2015-11-03','11 5555','http://localhost/CI/upload/20151213171444_12373448_1224369227580150_2089053334143456149_n.jpg','sess.homaru@hotmail.com','ชาย','personnel','747417f2206148a3118d02f3adf20b5e4139baac','2015-11-30','1','MEMTY00002','10540','PROV0001','ZONE0001','DIST0001'),('MEM00003','asas','asdd','2016-02-01','11','http://localhost/CI/upload/demoUpload.jpg','aasds@fds.com','ชาย','admin01aaaa','3499c60eea227453c779de50fc84e217e9a53a18','2016-02-06','1','MEMTY00001','10210','PROV0002','ZONE0001','DIST0001'),('MEM00004','ธนศักดิ์ ศรีอจมบุญกุล','ศรีอจมบุญกุล','2016-03-21','14/3หมู่ 3\r\nต.บางแก้ว','http://localhost/CI/upload/demoUpload.jpg','bas_adultindy07@hotmail.com','ชาย','admin01111','747417f2206148a3118d02f3adf20b5e4139baac','2016-03-21','1','MEMTY00001','10540','PROV0001','ZONE0001','DIST0001');

/*Table structure for table `membertype` */

DROP TABLE IF EXISTS `membertype`;

CREATE TABLE `membertype` (
  `membertype_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `membertype_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `membertype_date` int(11) NOT NULL,
  `membertype_unit` int(11) NOT NULL,
  `membertype_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `membertype_dateborrow` int(11) NOT NULL,
  `membertype_price` double NOT NULL,
  PRIMARY KEY (`membertype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `membertype` */

insert  into `membertype`(`membertype_id`,`membertype_name`,`membertype_date`,`membertype_unit`,`membertype_status`,`membertype_dateborrow`,`membertype_price`) values ('MEMTY00001','นักเรียน',3,3,'1',0,0),('MEMTY00002','บุคลากร',7,5,'1',0,0);

/*Table structure for table `mubook` */

DROP TABLE IF EXISTS `mubook`;

CREATE TABLE `mubook` (
  `mubook_no` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `mubook_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`mubook_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `mubook` */

insert  into `mubook`(`mubook_no`,`mubook_name`) values ('000','เบ็ตเตล็ดหรือความรู้ทั่วไป (Generalities'),('100','ปรัชญา (Philosophy) '),('200','ศาสนา (Religion) '),('300','สังคมศาสตร์ (Social sciences) '),('400','ภาษาศาสตร์ (Language) '),('500','วิทยาศาสตร์ (Science) '),('600','วิทยาศาสตร์ประยุกต์ หรือเทคโนโลยี (Techn'),('700','ศิลปกรรมและการบันเทิง (Arts and recreati'),('800','วรรณคดี (Literature) '),('900','ประวัติศาสตร์และภูมิศาสตร์ (History and ');

/*Table structure for table `offerorder` */

DROP TABLE IF EXISTS `offerorder`;

CREATE TABLE `offerorder` (
  `offerorder_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `offerorder_offerdate` date NOT NULL,
  `offerorder_offerpricetotal` double NOT NULL,
  `offerorder_approvedate` date DEFAULT NULL,
  `offerorder_approvepricetotal` double DEFAULT NULL,
  `offerorder_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `id_employeeoffer` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_employeeapprove` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offerorder_offervattotal` double NOT NULL,
  `offerorder_offersubtotal` double NOT NULL,
  `offerorder_approvesubtotal` double DEFAULT NULL,
  `offerorder_offervat` double NOT NULL,
  `id_publishing` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `offerorder_year` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `offerorder_semester` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `offerorder_approvevattotal` double DEFAULT NULL,
  PRIMARY KEY (`offerorder_id`),
  KEY `fk_offerorder_employeeoffer` (`id_employeeoffer`),
  KEY `fk_offerorder_employeeapprove` (`id_employeeapprove`),
  KEY `fk_offerorder_publishing` (`id_publishing`),
  KEY `fk_offerorder_yearsemester` (`offerorder_year`,`offerorder_semester`),
  CONSTRAINT `fk_offerorder_employeeapprove` FOREIGN KEY (`id_employeeapprove`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_offerorder_employeeoffer` FOREIGN KEY (`id_employeeoffer`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_offerorder_publishing` FOREIGN KEY (`id_publishing`) REFERENCES `publishing` (`publishing_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_offerorder_yearsemester` FOREIGN KEY (`offerorder_year`, `offerorder_semester`) REFERENCES `yearstudy` (`yearstudy`, `semester`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `offerorder` */

insert  into `offerorder`(`offerorder_id`,`offerorder_offerdate`,`offerorder_offerpricetotal`,`offerorder_approvedate`,`offerorder_approvepricetotal`,`offerorder_status`,`id_employeeoffer`,`id_employeeapprove`,`offerorder_offervattotal`,`offerorder_offersubtotal`,`offerorder_approvesubtotal`,`offerorder_offervat`,`id_publishing`,`offerorder_year`,`offerorder_semester`,`offerorder_approvevattotal`) values ('OFF0000001','2016-04-11',1520,'2016-04-11',2520,'สั่งซื้อแล้ว','EMP00001','EMP00001',106.4,1626.4,2696.4,7,'PUBLI00001','2556','1',176.4),('OFF0000002','2016-04-11',440,'2016-04-11',440,'สั่งซื้อแล้ว','EMP00001','EMP00001',30.8,470.8,470.8,7,'PUBLI00001','2556','1',30.8);

/*Table structure for table `offerorderdetial` */

DROP TABLE IF EXISTS `offerorderdetial`;

CREATE TABLE `offerorderdetial` (
  `id_offerorder` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `offerorderdetial_no` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `offerorderdetial_offerunit` int(11) NOT NULL,
  `offerorderdetial_approveunit` int(11) DEFAULT NULL,
  `offerorderdetial_offerprice` double NOT NULL,
  `offerorderdetial_approveprice` double DEFAULT NULL,
  `pricebook` double NOT NULL,
  `id_book` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_offerorder`,`offerorderdetial_no`),
  KEY `fk_pricebook` (`pricebook`,`id_book`),
  KEY `fk_offerorderdetial_book` (`id_book`),
  CONSTRAINT `fk_offerorderdetial_book` FOREIGN KEY (`id_book`) REFERENCES `book` (`book_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_offerorderdetial_offer` FOREIGN KEY (`id_offerorder`) REFERENCES `offerorder` (`offerorder_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `offerorderdetial` */

insert  into `offerorderdetial`(`id_offerorder`,`offerorderdetial_no`,`offerorderdetial_offerunit`,`offerorderdetial_approveunit`,`offerorderdetial_offerprice`,`offerorderdetial_approveprice`,`pricebook`,`id_book`) values ('OFF0000001','1',2,5,240,600,120,'BOOK000001'),('OFF0000001','2',4,6,1280,1920,320,'BOOK000002'),('OFF0000002','1',1,1,120,120,120,'BOOK000001'),('OFF0000002','2',1,1,320,320,320,'BOOK000002');

/*Table structure for table `origin` */

DROP TABLE IF EXISTS `origin`;

CREATE TABLE `origin` (
  `origin_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `origin_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `origin_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`origin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `origin` */

/*Table structure for table `personnel` */

DROP TABLE IF EXISTS `personnel`;

CREATE TABLE `personnel` (
  `id_member` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `personnel_idcard` char(13) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_member`),
  CONSTRAINT `fk_personnel` FOREIGN KEY (`id_member`) REFERENCES `member` (`member_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `personnel` */

insert  into `personnel`(`id_member`,`personnel_idcard`) values ('MEM00002','1102001626808');

/*Table structure for table `position` */

DROP TABLE IF EXISTS `position`;

CREATE TABLE `position` (
  `position_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `position_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `position_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `position` */

insert  into `position`(`position_id`,`position_name`,`position_status`) values ('POSIT0001','หัวหน้าห้องสมุด','1'),('POSIT0002','บรรณารักษ์','1'),('POSIT0003','เจ้าหน้าที่ห้องสมุด','1');

/*Table structure for table `positionbook` */

DROP TABLE IF EXISTS `positionbook`;

CREATE TABLE `positionbook` (
  `positionbook_tutee` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `positionbook_chantee` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `positionbook_chongtee` char(3) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`positionbook_tutee`,`positionbook_chantee`,`positionbook_chongtee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `positionbook` */

/*Table structure for table `postcode` */

DROP TABLE IF EXISTS `postcode`;

CREATE TABLE `postcode` (
  `id_province` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_zone` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_district` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `postcode_id` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `postcode_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`postcode_id`,`id_province`,`id_zone`,`id_district`),
  KEY `pk_fk_postcode` (`id_district`,`id_zone`,`id_province`),
  CONSTRAINT `pk_fk_postcod` FOREIGN KEY (`id_district`, `id_zone`, `id_province`) REFERENCES `district` (`district_id`, `id_zone`, `id_province`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `postcode` */

insert  into `postcode`(`id_province`,`id_zone`,`id_district`,`postcode_id`,`postcode_status`) values ('PROV0002','ZONE0001','DIST0001','10210','1'),('PROV0001','ZONE0002','DIST0002','10270','1'),('PROV0002','ZONE0002','DIST0002','10530','1'),('PROV0001','ZONE0001','DIST0001','10540','1');

/*Table structure for table `pricebook` */

DROP TABLE IF EXISTS `pricebook`;

CREATE TABLE `pricebook` (
  `pricebook_date` datetime NOT NULL,
  `pricebook_price` double NOT NULL,
  `id_book` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_publishing` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pricebook_date`,`id_book`,`id_publishing`),
  KEY `fk_pricebook_book` (`id_book`),
  KEY `fk_pricebook_publishing` (`id_publishing`),
  CONSTRAINT `fk_pricebook_book` FOREIGN KEY (`id_book`) REFERENCES `book` (`book_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pricebook_publishing` FOREIGN KEY (`id_publishing`) REFERENCES `publishing` (`publishing_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `pricebook` */

insert  into `pricebook`(`pricebook_date`,`pricebook_price`,`id_book`,`id_publishing`) values ('2016-02-13 15:44:26',120,'BOOK000001','PUBLI00001'),('2016-03-12 23:39:02',350,'BOOK000002','PUBLI00001'),('2016-03-12 23:56:55',320,'BOOK000002','PUBLI00001');

/*Table structure for table `province` */

DROP TABLE IF EXISTS `province`;

CREATE TABLE `province` (
  `province_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `province_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `province_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`province_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `province` */

insert  into `province`(`province_id`,`province_name`,`province_status`) values ('PROV0001','สมุทรปราการ','1'),('PROV0002','กรุงเทพมหานคร','1'),('PROV0003','ชลบุรี','1'),('PROV0004','ระยอง','1'),('PROV0005','จันทบุรี','1');

/*Table structure for table `publicationyear` */

DROP TABLE IF EXISTS `publicationyear`;

CREATE TABLE `publicationyear` (
  `publicationyear_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `publicationyear_edition` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`publicationyear_id`,`publicationyear_edition`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `publicationyear` */

/*Table structure for table `publicationyearbook` */

DROP TABLE IF EXISTS `publicationyearbook`;

CREATE TABLE `publicationyearbook` (
  `id_publicationyear` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `edition_publicationyear` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `id_book` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_publicationyear`,`edition_publicationyear`,`id_book`),
  KEY `fk_publicationyearbook_book` (`id_book`),
  CONSTRAINT `fk_publicationyearbook_book` FOREIGN KEY (`id_book`) REFERENCES `book` (`book_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_publicationyearbook_publicationyear` FOREIGN KEY (`id_publicationyear`, `edition_publicationyear`) REFERENCES `publicationyear` (`publicationyear_id`, `publicationyear_edition`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `publicationyearbook` */

/*Table structure for table `publishing` */

DROP TABLE IF EXISTS `publishing`;

CREATE TABLE `publishing` (
  `publishing_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `publishing_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `publishing_address` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `publishing_image` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `publishing_email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `publishing_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `id_province` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_zone` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_district` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_postcode` char(5) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`publishing_id`),
  KEY `fk_address_publishing` (`id_province`,`id_zone`,`id_district`,`id_postcode`),
  KEY `fk_address_publis` (`id_postcode`,`id_province`,`id_zone`,`id_district`),
  CONSTRAINT `fk_address_publis` FOREIGN KEY (`id_postcode`, `id_province`, `id_zone`, `id_district`) REFERENCES `postcode` (`postcode_id`, `id_province`, `id_zone`, `id_district`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `publishing` */

insert  into `publishing`(`publishing_id`,`publishing_name`,`publishing_address`,`publishing_image`,`publishing_email`,`publishing_status`,`id_province`,`id_zone`,`id_district`,`id_postcode`) values ('PUBLI00001','บริษัท โปรวิชั่น จำกัด','408/33 ชั้น 9 อาคารพหลโยธินเพลส','','editor@provision.co.th','1','PROV0002','ZONE0001','DIST0001','10210'),('PUBLI00002','บริษัท ซีเอ็ดยูเคชั่น จำกัด','1858/87-90 ชั้น 19 อาคารทีซีไอเอฟ ทาวเวอร์','','textbook@se-ed.com','1','PROV0001','ZONE0002','DIST0002','10270'),('PUBLI00003','บริษัท จรัลสนิทวงศ์การพิมพ์ จำกัด','219 ซอยเพชรเกษม 102/2','','','1','PROV0002','ZONE0001','DIST0001','10210');

/*Table structure for table `purchase` */

DROP TABLE IF EXISTS `purchase`;

CREATE TABLE `purchase` (
  `id_book` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_bookcopy` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_receipt` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `no_receipt` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_book`,`id_bookcopy`),
  KEY `fk_purchase_receipt` (`id_receipt`,`no_receipt`),
  CONSTRAINT `fk_purchase_bookcopy` FOREIGN KEY (`id_book`, `id_bookcopy`) REFERENCES `bookcopy` (`id_book`, `bookcopy_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_purchase_receipt` FOREIGN KEY (`id_receipt`, `no_receipt`) REFERENCES `receiptorderdetial` (`id_receiptorder`, `receiptorderdetial_no`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `purchase` */

/*Table structure for table `purchaseorder` */

DROP TABLE IF EXISTS `purchaseorder`;

CREATE TABLE `purchaseorder` (
  `purchaseorder_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `purchaseorder_date` date NOT NULL,
  `purchaseorder_pricetotal` double NOT NULL,
  `purchaseorder_vat` int(11) NOT NULL,
  `purchaseorder_vattotal` double NOT NULL,
  `purchaseorder_subpricetotal` double NOT NULL,
  `purchaseorder_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `id_employee` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_publishing` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`purchaseorder_id`),
  KEY `fk_purchaseorder_employee` (`id_employee`),
  KEY `fk_purchaseorder_publishing` (`id_publishing`),
  CONSTRAINT `fk_purchaseorder_employee` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_purchaseorder_publishing` FOREIGN KEY (`id_publishing`) REFERENCES `publishing` (`publishing_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `purchaseorder` */

insert  into `purchaseorder`(`purchaseorder_id`,`purchaseorder_date`,`purchaseorder_pricetotal`,`purchaseorder_vat`,`purchaseorder_vattotal`,`purchaseorder_subpricetotal`,`purchaseorder_status`,`id_employee`,`id_publishing`) values ('PUR0000001','2016-04-12',2960,7,207.20000000000002,3167.2000000000003,'สั่งซื้อ','EMP00001','PUBLI00001'),('PUR0000002','2016-04-13',2960,7,207.20000000000002,3167.2000000000003,'สั่งซื้อ','EMP00001','PUBLI00001'),('PUR0000003','2016-04-13',2960,7,207.20000000000002,3167.2000000000003,'สั่งซื้อ','EMP00001','PUBLI00001'),('PUR0000004','2016-04-13',2960,7,207.20000000000002,3167.2000000000003,'สั่งซื้อ','EMP00001','PUBLI00001'),('PUR0000005','2016-04-13',2960,7,207.20000000000002,3167.2000000000003,'สั่งซื้อ','EMP00001','PUBLI00001');

/*Table structure for table `purchaseorderdetial` */

DROP TABLE IF EXISTS `purchaseorderdetial`;

CREATE TABLE `purchaseorderdetial` (
  `id_purchaseorder` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `purchaseorderdetial_no` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `purchaseorderdetial_unit` int(11) NOT NULL,
  `purchaseorderdetial_bookprice` double NOT NULL,
  `purchaseorderdetial_sumprice` double NOT NULL,
  `purchaseorderdetial_balanceunit` int(11) DEFAULT NULL,
  `id_book` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_groupofferorder` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_purchaseorder`,`purchaseorderdetial_no`),
  KEY `fk_purchaseorderdetial_book` (`id_book`),
  KEY `fk_purchaseorderdetial_groupofferorder` (`id_groupofferorder`),
  CONSTRAINT `fk_purchaseorderdetial_book` FOREIGN KEY (`id_book`) REFERENCES `book` (`book_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_purchaseorderdetial_groupofferorder` FOREIGN KEY (`id_groupofferorder`) REFERENCES `groupofferorder` (`groupofferorder_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_purchaseorderdetial_purchase` FOREIGN KEY (`id_purchaseorder`) REFERENCES `purchaseorder` (`purchaseorder_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `purchaseorderdetial` */

insert  into `purchaseorderdetial`(`id_purchaseorder`,`purchaseorderdetial_no`,`purchaseorderdetial_unit`,`purchaseorderdetial_bookprice`,`purchaseorderdetial_sumprice`,`purchaseorderdetial_balanceunit`,`id_book`,`id_groupofferorder`) values ('PUR0000001','1',6,120,720,6,'BOOK000001','GOF0000001'),('PUR0000001','2',7,320,2240,7,'BOOK000002','GOF0000002'),('PUR0000002','1',6,120,720,6,'BOOK000001','GOF0000003'),('PUR0000002','2',7,320,2240,7,'BOOK000002','GOF0000004'),('PUR0000003','1',6,120,720,6,'BOOK000001','GOF0000005'),('PUR0000003','2',7,320,2240,7,'BOOK000002','GOF0000006'),('PUR0000004','1',6,120,720,6,'BOOK000001','GOF0000007'),('PUR0000004','2',7,320,2240,7,'BOOK000002','GOF0000008'),('PUR0000005','1',6,120,720,6,'BOOK000001','GOF0000009'),('PUR0000005','2',7,320,2240,7,'BOOK000002','GOF0000010');

/*Table structure for table `receiptorder` */

DROP TABLE IF EXISTS `receiptorder`;

CREATE TABLE `receiptorder` (
  `receiptorder_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `receiptorder_date` date NOT NULL,
  `receiptorder_totalprice` double NOT NULL,
  `receiptorder_vat` int(11) NOT NULL,
  `receiptorder_vattotal` double NOT NULL,
  `receiptorder_subtotal` double NOT NULL,
  `id_employee` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_publishing` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`receiptorder_id`),
  KEY `fk_receiptorder_employee` (`id_employee`),
  KEY `fk_receiptorder_publishing` (`id_publishing`),
  CONSTRAINT `fk_receiptorder_employee` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_receiptorder_publishing` FOREIGN KEY (`id_publishing`) REFERENCES `publishing` (`publishing_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `receiptorder` */

/*Table structure for table `receiptorderdetial` */

DROP TABLE IF EXISTS `receiptorderdetial`;

CREATE TABLE `receiptorderdetial` (
  `id_receiptorder` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `receiptorderdetial_no` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `receiptorderdetial_unit` int(11) NOT NULL,
  `receiptorderdetial_sumprice` double NOT NULL,
  `id_purchase` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `no_purchase` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_receiptorder`,`receiptorderdetial_no`),
  KEY `fk_receiptorderdetial_purchase` (`id_purchase`,`no_purchase`),
  CONSTRAINT `fk_receiptorderdetial_purchase` FOREIGN KEY (`id_purchase`, `no_purchase`) REFERENCES `purchaseorderdetial` (`id_purchaseorder`, `purchaseorderdetial_no`) ON UPDATE CASCADE,
  CONSTRAINT `fk_receiptorderdetial_receip` FOREIGN KEY (`id_receiptorder`) REFERENCES `receiptorder` (`receiptorder_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `receiptorderdetial` */

/*Table structure for table `repairoder` */

DROP TABLE IF EXISTS `repairoder`;

CREATE TABLE `repairoder` (
  `repairoder_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `repairoder_date` date NOT NULL,
  `id_employee` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`repairoder_id`),
  KEY `fk_repairoder_emp` (`id_employee`),
  CONSTRAINT `fk_repairoder_emp` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `repairoder` */

/*Table structure for table `repairoderdetial` */

DROP TABLE IF EXISTS `repairoderdetial`;

CREATE TABLE `repairoderdetial` (
  `id_repairoder` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `repairoderdetial_no` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `repairoderdetial_detial` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `repairoderdetial_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `id_book` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_bookcopy` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_repairoder`,`repairoderdetial_no`),
  KEY `fk_repairoderdetial_bookcopy` (`id_book`,`id_bookcopy`),
  CONSTRAINT `fk_repairoderdetial_bookcopy` FOREIGN KEY (`id_book`, `id_bookcopy`) REFERENCES `bookcopy` (`id_book`, `bookcopy_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_repairoderdetial_repair` FOREIGN KEY (`id_repairoder`) REFERENCES `repairoder` (`repairoder_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `repairoderdetial` */

/*Table structure for table `returning` */

DROP TABLE IF EXISTS `returning`;

CREATE TABLE `returning` (
  `returning_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `returning_date` date NOT NULL,
  `id_employee` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`returning_id`),
  KEY `fk_returning_emp` (`id_employee`),
  CONSTRAINT `fk_returning_emp` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `returning` */

/*Table structure for table `returningdetial` */

DROP TABLE IF EXISTS `returningdetial`;

CREATE TABLE `returningdetial` (
  `id_returning` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `returningdetial_no` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `returningdetial_dateover` int(11) NOT NULL,
  `returningdetial_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `id_borrowing` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `no_borrowingdetial` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_returning`,`returningdetial_no`),
  KEY `fk_returningdetial_borrowing` (`id_borrowing`,`no_borrowingdetial`),
  CONSTRAINT `fk_returningdetial_borrowing` FOREIGN KEY (`id_borrowing`, `no_borrowingdetial`) REFERENCES `borrowingdetial` (`id_borrowing`, `borrowingdetial_no`) ON UPDATE CASCADE,
  CONSTRAINT `fk_returningdetial_returning` FOREIGN KEY (`id_returning`) REFERENCES `returning` (`returning_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `returningdetial` */

/*Table structure for table `soldout` */

DROP TABLE IF EXISTS `soldout`;

CREATE TABLE `soldout` (
  `id_book` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_bookcopy` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `soldout_detial` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `soldout_date` date NOT NULL,
  `id_employee` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_book`,`id_bookcopy`),
  KEY `fk_soldout_emp` (`id_employee`),
  CONSTRAINT `fk_soldout_book` FOREIGN KEY (`id_book`, `id_bookcopy`) REFERENCES `bookcopy` (`id_book`, `bookcopy_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_soldout_emp` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `soldout` */

/*Table structure for table `student` */

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `id_student` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `student_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_student`),
  CONSTRAINT `st_fk` FOREIGN KEY (`id_student`) REFERENCES `member` (`member_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `student` */

insert  into `student`(`id_student`,`student_id`) values ('MEM00001','1234567890'),('MEM00003','1111111111'),('MEM00004','1111111111');

/*Table structure for table `submubook` */

DROP TABLE IF EXISTS `submubook`;

CREATE TABLE `submubook` (
  `no_mubook` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `submubook_no` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `submubook_name` char(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`no_mubook`,`submubook_no`),
  CONSTRAINT `fk_mubook` FOREIGN KEY (`no_mubook`) REFERENCES `mubook` (`mubook_no`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `submubook` */

insert  into `submubook`(`no_mubook`,`submubook_no`,`submubook_name`) values ('000','000','คอมพิวเตอร์ ความรู้ทั่วไป'),('000','010','บรรณานุกรม แคตตาล็อก'),('000','020','บรรณารักษศาสตร์และสารนิเทศศาสต'),('100','100','ปรัชญา'),('100','110','อภิปรัชญา'),('100','120','ญาณวิทยา ความเป็นเหตุผล ความเป'),('200','200','ศาสนา'),('200','210','ศาสนาธรรมชาติ'),('300','300','สังคมศาสตร์'),('400','400','ภาษา'),('400','420','ภาษาอังกฤษ'),('500','500','วิทยาศาสตร์'),('500','510','คณิตศาสตร์'),('600','600','วิทยาศาสตร์ประยุกต์ เทคโนโลยี'),('600','610','แพทยศาสตร์'),('700','700','ศิลปกรรม การบันเทิง'),('800','800','วรรณกรรม วรรณคดี'),('900','900','ประวัติศาสตร์ ภูมิศาสตร์'),('900','910','ภูมิศาสตร์ การท่องเที่ยว');

/*Table structure for table `subscriber` */

DROP TABLE IF EXISTS `subscriber`;

CREATE TABLE `subscriber` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first` varchar(100) DEFAULT NULL,
  `last` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `subscriber` */

insert  into `subscriber`(`id`,`first`,`last`,`email`,`date_created`) values (23,'Ahmedd','samy','ahmed.samy.cs@gmail.com','2013-03-11 23:11:40'),(24,'John','Carter','test@test.com','2013-03-11 23:19:05'),(25,'Lina','Khaled','lina@hotmail.com','2013-03-17 20:54:48'),(26,'aaa','aaa','aaaa','2015-03-22 23:55:37'),(27,'vbbb','bbbb','bbb','2015-03-22 23:55:43'),(28,'qqqqqqqqq','qqqqqqqqqqq','qqqqqqqqqqqqq','2015-03-22 23:55:49'),(29,'eeeeeeeeee','eeeeeeeeeeeeee','eeeeeeeeeeeeeee','2015-03-22 23:55:55'),(30,'wwwwwww','wwwwwwwwww','wwwwwwwwwww','2015-03-22 23:55:59'),(31,'44444444444','444444444444','44444444444444','2015-03-22 23:56:05'),(32,'22222','2','222','2015-03-22 23:56:10'),(33,'Ahmedd','samy','ahmed.samy.cs@gmail.com','2013-03-11 23:11:40'),(34,'John','Carter','test@test.com','2013-03-11 23:19:05'),(35,'Lina','Khaled','lina@hotmail.com','2013-03-17 20:54:48');

/*Table structure for table `telemployee` */

DROP TABLE IF EXISTS `telemployee`;

CREATE TABLE `telemployee` (
  `id_employee` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `telemployee_tel` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_employee`,`telemployee_tel`),
  CONSTRAINT `fk_employee` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `telemployee` */

insert  into `telemployee`(`id_employee`,`telemployee_tel`) values ('EMP00001','023865125'),('EMP00001','0958747575'),('EMP00006','0333333333');

/*Table structure for table `telpersonnel` */

DROP TABLE IF EXISTS `telpersonnel`;

CREATE TABLE `telpersonnel` (
  `id_member` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `telpersonnel_tel` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_member`,`telpersonnel_tel`),
  CONSTRAINT `fk_telpersonnel` FOREIGN KEY (`id_member`) REFERENCES `personnel` (`id_member`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `telpersonnel` */

insert  into `telpersonnel`(`id_member`,`telpersonnel_tel`) values ('MEM00002','0988888888'),('MEM00002','0999999999');

/*Table structure for table `telpublishing` */

DROP TABLE IF EXISTS `telpublishing`;

CREATE TABLE `telpublishing` (
  `id_publishing` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `telpublishing_tel` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_publishing`,`telpublishing_tel`),
  CONSTRAINT `fk_publishing_id` FOREIGN KEY (`id_publishing`) REFERENCES `publishing` (`publishing_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `telpublishing` */

insert  into `telpublishing`(`id_publishing`,`telpublishing_tel`) values ('PUBLI00001','098765432'),('PUBLI00002','089900001'),('PUBLI00003','081112223');

/*Table structure for table `title` */

DROP TABLE IF EXISTS `title`;

CREATE TABLE `title` (
  `title_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `title_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`title_id`),
  UNIQUE KEY `ID_NAME` (`title_id`,`title_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `title` */

insert  into `title`(`title_id`,`title_name`) values ('TIT0000001','Programming'),('TIT0000003','NodeJS'),('TIT0000004','Big Data'),('TIT0000005','Database'),('TIT0000006','Android'),('TIT0000007','PHP'),('TIT0000008','Oracle'),('TIT0000009','Docker'),('TIT0000010','Spring Framework'),('TIT0000011','Game');

/*Table structure for table `titlebook` */

DROP TABLE IF EXISTS `titlebook`;

CREATE TABLE `titlebook` (
  `id_title` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_book` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_title`,`id_book`),
  KEY `fk_titlebook_book` (`id_book`),
  CONSTRAINT `fk_titlebook_book` FOREIGN KEY (`id_book`) REFERENCES `book` (`book_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_titlebook_title` FOREIGN KEY (`id_title`) REFERENCES `title` (`title_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `titlebook` */

insert  into `titlebook`(`id_title`,`id_book`) values ('TIT0000001','BOOK000001'),('TIT0000003','BOOK000001'),('TIT0000005','BOOK000002');

/*Table structure for table `translatorbook` */

DROP TABLE IF EXISTS `translatorbook`;

CREATE TABLE `translatorbook` (
  `id_translator` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_book` char(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_translator`,`id_book`),
  KEY `fk_translatorbook_book` (`id_book`),
  CONSTRAINT `fk_translatorbook_book` FOREIGN KEY (`id_book`) REFERENCES `book` (`book_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_translatorbook_translator` FOREIGN KEY (`id_translator`) REFERENCES `author` (`author_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `translatorbook` */

insert  into `translatorbook`(`id_translator`,`id_book`) values ('AUTH000012','BOOK000001');

/*Table structure for table `yearmemberstudent` */

DROP TABLE IF EXISTS `yearmemberstudent`;

CREATE TABLE `yearmemberstudent` (
  `id_memberstudent` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_yearstudy` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `semester` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_memberstudent`,`id_yearstudy`,`semester`),
  KEY `fk_yearmember_year` (`id_yearstudy`,`semester`),
  CONSTRAINT `fk_agemember_memberstudent` FOREIGN KEY (`id_memberstudent`) REFERENCES `student` (`id_student`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yearmember_year` FOREIGN KEY (`id_yearstudy`, `semester`) REFERENCES `yearstudy` (`yearstudy`, `semester`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `yearmemberstudent` */

insert  into `yearmemberstudent`(`id_memberstudent`,`id_yearstudy`,`semester`) values ('MEM00001','2556','1'),('MEM00003','2556','1'),('MEM00004','2556','1');

/*Table structure for table `yearstudy` */

DROP TABLE IF EXISTS `yearstudy`;

CREATE TABLE `yearstudy` (
  `yearstudy` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `semester` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `expenditure` double DEFAULT NULL,
  `balance` double DEFAULT NULL,
  `expiration_date` date NOT NULL,
  PRIMARY KEY (`yearstudy`,`semester`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `yearstudy` */

insert  into `yearstudy`(`yearstudy`,`semester`,`expenditure`,`balance`,`expiration_date`) values ('2554','1',100000,20000,'2016-01-31'),('2554','2',200000,30000,'2015-11-16'),('2554','3',NULL,NULL,'2015-11-16'),('2555','1',NULL,NULL,'2015-11-24'),('2556','1',1000000,1000000,'2016-04-01');

/*Table structure for table `zone` */

DROP TABLE IF EXISTS `zone`;

CREATE TABLE `zone` (
  `id_province` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `zone_id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `zone_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `zone_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`zone_id`,`id_province`),
  KEY `pk_fk_zone` (`id_province`),
  CONSTRAINT `pk_fk_zone` FOREIGN KEY (`id_province`) REFERENCES `province` (`province_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `zone` */

insert  into `zone`(`id_province`,`zone_id`,`zone_name`,`zone_status`) values ('PROV0001','ZONE0001','บางพลี','1'),('PROV0002','ZONE0001','หลักสี่','1'),('PROV0004','ZONE0001','แกลง','1'),('PROV0001','ZONE0002','เมืองสมุทรปราการ','1'),('PROV0002','ZONE0002','หนองจอก','1'),('PROV0001','ZONE0003','พระประแดง','1'),('PROV0002','ZONE0003','บางนา','1'),('PROV0005','ZONE0003','นายายอาม','1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
