<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class District_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 
    }
    
    public function index()
    {
        $this->load->view("header");
        $this->load->view("district");
        $this->load->view("footter");
    }
    public function adddistrict()
    {
      
    }
    public function editdistrict()
    {
        
    }
    public function deletedistrict()
    {
        
    }
    public function selectalldistrict()
    {
        $this->load->model("district_model");
        $json = json_encode($this->input->post('objdata'));
        list($obj) = json_decode($json); 
        $data['district']=$this->district_model->selectalldistrict_model(array(
                'idprovince'=>$obj->idprovince,'idzone'=>$obj->idzone));    
        echo json_encode($data);
    }
    public function selectdistrict()
    {
        
    }
}
