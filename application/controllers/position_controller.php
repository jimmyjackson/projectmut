<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Position_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 
        $this->load->model("position_model");
        $this->load->model("access_model");
    }
    
    public function index()
    {
        $this->load->view("header");
        $this->load->view("positionlist");
        $this->load->view("footter");
    }
    
    public function position($idposition='0')
    {
        $data['access_data']=$this->access_model->selectallaccess_model();

        if($idposition!='0'){
            
            $this->load->view("header");
            if($this->position_model->selectposition_where_id_model($idposition)){
                $data['idposition']=$idposition;
                $data['nameposition']=
                    $this->position_model->select_name_position_where_id_model($idposition);
                $this->load->view("positioned",$data);
            }
            else{
                $this->load->view("nodata");
            }
            
            $this->load->view("footter");
        }
        else{
            $this->load->view("header");
            $this->load->view("position",$data);
            $this->load->view("footter");
        }
    }
    
    public function addposition()
    {
        $autoid['autoid']=$this->position_model->autoidposi_model();    
            
        foreach ($autoid as $value)
            foreach ($value as $subvalue)
                foreach ($subvalue as $subvalue2)
                    $autoidposi = $subvalue2;
            
        $json = json_encode($this->input->post('objposi'));
        list($obj) = json_decode($json);
                
        $status=$this->position_model->addposition_model(array('idposi'=>$autoidposi,
        'nameposi'=>$obj->name_position,'accessposi'=>$obj->access_position));
             
        header('Location:/CI/position_controller');
        exit(0);

    }
    
    public function editposition()
    {
        $json = json_encode($this->input->post('objposi'));
        list($obj) = json_decode($json);
                
        $status=$this->position_model->editposition_model(array(
            'nameposi'=>$obj->name_position,
            'idposi'=>$obj->id_position,
            'accessposi'=>$obj->access_position));
             
        header('Location:/CI/position_controller');
        exit(0);
    }
    
    public function deleteposition()
    {
        $positionid=$this->input->post('idposition');
        $data=$this->position_model->deleteposition_model($positionid);
    }
    
    public function selectallposition()
    {
        $offset=$this->input->post('offsetsend');
        $data['positionselect']=$this->position_model->selectallposition_model($offset);
        echo json_encode($data);
    }
    
    public function countallposition()
    {
        echo json_encode($this->position_model->countallposition_model());
    }
    
    public function selectposition()
    {
        $data['position']=$this->position_model->selectposition_model();
        echo json_encode($data);
    }
    
    public function searchposition()
    {
        $json = json_encode($this->input->post('datasearch'));
        $obj= json_decode($json);
        
        $datasearch=$this->position_model->searchposition_model(array(
        'inputsearch'=>$obj->inputsearch,'offset'=>$obj->offset,
        'selectmessage'=>$obj->selectmessage));
        
        echo json_encode($datasearch);
    }
    
    public function countsearchposition()
    {
        $json = json_encode($this->input->post('datasearch'));
        $obj= json_decode($json);
        
        $datasearch=$this->position_model->countsearchposition_model(array(
        'inputsearch'=>$obj->inputsearch,'selectmessage'=>$obj->selectmessage));
        
        echo json_encode($datasearch);
    }
    
    public function select_access_id_where_id_position($idposition='0')
    {
        $datasearch=$this->position_model->select_access_id_where_id_position_model($idposition);
        
        echo json_encode($datasearch);
    }
    
}
