<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Returning_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("returning_model");
        $this->load->library("mpdf/mpdf");
    }
    public function index()
    {
        $dataBooking = '';
        $dataMember = '';
        $status = '';
        $idinvoice = '';

        if(!empty($_POST)) {

          $status_a = false;
          $status_b = false;
          $status_c = false;
          if(empty($_POST['return']) && !empty($_POST['member_hidder'])){
            $status = 'no';
            var_dump('no 1');
          } else {
            if(!empty($_POST['fray']) && !empty($_POST['lost'])) {
              foreach ($_POST['fray'] as $key => $value) {
                $c = array_search($value,$_POST['lost']);
                if($c >= 0)
                {
                  $status_c = false;
                }
                elseif($c == false)
                {
                  $status_c = true;
                  break;
                }
              }
            }

            if(!empty($_POST['lost'])) {
              foreach ($_POST['lost'] as $key => $value) {
                $a = array_search($value,$_POST['return']);

                if($a >= 0)
                {
                  $status_a = false;
                }
                elseif($a == false)
                {
                  $status_a = true;
                  break;
                }
              }
            }

            if(!empty($_POST['fray'])) {
              foreach ($_POST['fray'] as $key => $value) {
                $b = array_search($value,$_POST['return']);
                if($b >= 0)
                {
                  $status_b = false;
                }
                elseif($b == false || $b < 0)
                {
                  $status_b = true;
                  break;
                }
              }
            }

            if($status_c || $status_a || $status_b){
              if($status_c){
                $status = 'not';
              } else {
                $status = 'no';
                var_dump('no 2');
              }
            }
            else {
                if(!empty($_POST['member'])){
                    $dataBooking = $this->returning_model->getborrowing($_POST);
                    $dataMember = $this->returning_model->member($_POST['member']);
                }elseif(!empty($_POST['return'])){
                    $countcheckreturnbook = $this->returning_model->checkreturn($_POST);
                    if($countcheckreturnbook){
                      $status = 'countcheckreturnbook';
                    } else {
                      $idinvoice = $this->returning_model->save($_POST);
                      $status = 'save';
                    }
                }else{
                    $status = 'nomember';
                }
            }
          }
        }

        $this->load->view("header");
        $this->load->view("returning",[
            'dataBooking'=>$dataBooking,
            'dataMember'=>$dataMember,
            'status'=>$status,
            'invoice'=>$idinvoice
        ]);
        $this->load->view("footter");
    }

    public function invoicepdf($invoice_id)
    {
      $data=$this->returning_model->selectdatainvoicereport_model($invoice_id);

      $html .= "<link rel='stylesheet' href='./assets/plugins/bootstrap/css/bootstrap.css' />";
      $html .= "<div class='section'><div class='container'><div class='row'><div class='col-md-12 text-center'><h1 contenteditable='true'>ใบเสร็จค่าปรับ</h1>";
      $html .= "<table class='table table-bordered'><thead><tr><td height='28' class='col-md-2'>รหัสใบเสร็จ :</td><td height='28' class='col-md-4'>".$data[0]->invoice_id."</td><td height='28' class='col-md-2'>วันที่ :</td><td height='28' class='col-md-4'>".$data[0]->invoice_date."</td></tr></thead>";
      $html .= "<tbody><tr><td height='28' class='col-md-2'>รหัสพนักงาน :</td><td height='28' class='col-md-4'>".$data[0]->id_employee."</td></tr></tbody></table>";
      $html .= "<table class='table table-bordered'>";
      $html .= "<thead><tr><th height='28'><center>ลำดับ</center></th><th height='28'><center>รหัสสำเนา</center></th><th height='28'><center>ชื่อหนังสือ</center></th><th height='28'><center>ค่าปรับ</center></th></tr></thead>";
      $html .= "<tbody>";
      foreach ($data as $value) {
        $html .= "<tr><td height='28'><center>".$value->invoicedetial_no."</center></td><td height='28'><center>".$value->id_bookcopy."</center></td><td height='28'><center>".$value->book_name."</center></td><td height='28'><center>".$value->invoicedetial_price."</center></td></tr>";
      }
      $html .= "</tbody><tfoot>";
      $html .= "<tr><th height='28' colspan='3' class='text-right'>ราคารวมสุทธิ</th><th height='28' colspan='1'><center>".number_format($data[0]->invoice_totalprice,2,'.','')."</center></th></tr>";
      $html .= "</tfoot></table></div></div><br><br><br><br><br><br><br>";
      $html .= "</div></div>";

      $this->mpdf = new mPDF('th', 'A4');
      $this->mpdf->WriteHTML($html);
      $this->mpdf->Output();
    }
}
