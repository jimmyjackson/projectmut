<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bookcopy_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("bookcopy_model");
    }
    public function index()
    {
        $this->load->view("header");
        $this->load->view("bookcopylist");
        $this->load->view("footter");
    }

    public function bookcopy($value='')
    {

        if($value == ''){
          $statusSave = '';
          if(!empty($_POST['receiptorder_id'])){
              $this->bookcopy_model->save($_POST);
              $statusSave = 'true';
          }
          $dataReceiptorderdetial = $this->bookcopy_model->selectreceiptorderdetial('');

          $this->load->view("header");
          $this->load->view("bookcopy",[
              'dataReceiptorderdetial' => $dataReceiptorderdetial,
              'statusSave' => $statusSave
          ]);
          $this->load->view("footter");
        } else {
          $data = $this->bookcopy_model->selectdatabookcopy($value);

          $author = array();
          $translator = array();
          $title = array();

          if(count($data) > 0)
          {
            foreach ($data as $value) {
              if(!in_array($value->title_name, $title))
                array_push($title, $value->title_name);

              if(!in_array($value->author, $author))
                array_push($author, $value->author);

              if(!in_array($value->translator, $translator))
                array_push($translator, $value->translator);
            }

            $this->load->view("header");
            $this->load->view("bookcopyed",[
              'book_id'=>$data[0]->book_id,
              'book_name'=>$data[0]->book_name,
              'book_size'=>$data[0]->book_size,
              'book_page'=>$data[0]->book_page,
              'book_image'=>$data[0]->book_image,
              'book_price'=>$data[0]->book_price,
              'book_isbn'=>$data[0]->book_isbn,
              'publishing_name'=>$data[0]->publishing_name,
              'mubook_name'=>$data[0]->mubook_name,
              'submubook_name'=>$data[0]->submubook_name,
              'bookcopy_id'=>$data[0]->bookcopy_id,
              'bookcopy_callno'=>$data[0]->bookcopy_callno,
              'bookcopy_status'=>$data[0]->bookcopy_status,
              'positionbook'=>$data[0]->positionbook,
              'edition'=>$data[0]->edition,
              'author'=>$author,
              'translator'=>$translator,
              'title'=>$title,
            ]);
            $this->load->view("footter");
          } else {
            $this->load->view("header");
            $this->load->view("nodata");
            $this->load->view("footter");
          }
        }
    }

    public function form()
    {

        $dataReceiptorderdetial = $this->bookcopy_model->selectreceiptorderdetial($_POST);

        $sqlEmployee = "SELECT *
                        FROM employee
                        WHERE employee_id='".$dataReceiptorderdetial[0]->id_employee."'
                        ";
        $dataEmployee = $this->db->query($sqlEmployee)->result();

        $this->load->view("header");
        $this->load->view("bookcopyform",[
            'dataReceiptorderdetial'=>$dataReceiptorderdetial,
            'dataEmployee'=>$dataEmployee,
            'receiptorder_id'=>$_POST['receiptorder_id']
        ]);
        $this->load->view("footter");
    }

    public function selectbookcopy($offset='0')
    {
        $offset=$this->input->post('offsetsend');

        $data['book']=$this->bookcopy_model->selectbookcopy_model($offset);
        $bookcopy_id_old = $data['book'][0]->bookcopy_id;

        $databookselect[] = $data['book'][0];

        $databookcopy = array();

        foreach ($data['book'] as $value) {
          if($value->bookcopy_id != $bookcopy_id_old){
            $bookcopy_id_old = $value->bookcopy_id;
            $databookselect[]= $value;
          }
        }

        foreach ($databookselect as $val) {
          $author=$this->bookcopy_model->selectauthorbookcopybyid_model($val->bookcopy_id);
          $translator=$this->bookcopy_model->selecttranslatorbookcopybyid_model($val->bookcopy_id);
          $title=$this->bookcopy_model->selecttitlebookcopybyid_model($val->bookcopy_id);

          $databookcopy[] = [
            'bookcopy' => $val,
            'author' => $author,
            'translator' => $translator,
            'title' => $title
          ];
        }
        echo json_encode($databookcopy);
    }

    public function countbookcopy()
    {
        echo json_encode($this->bookcopy_model->countbookcopy_model());
    }

    public function searchbookcopy()
    {
        $json = json_encode($this->input->post('datasearch'));
        $obj= json_decode($json);

        $data['book']=$this->bookcopy_model->searchbookcopy_model(array(
            'inputsearch'=>$obj->inputsearch,'offset'=>$obj->offset,
            'selectmessage'=>$obj->selectmessage,'selectshowtype'=>$obj->selectshowtype
        ));

        $bookcopy_id_old = $data['book'][0]->bookcopy_id;

        $databookselect[] = $data['book'][0];

        $databookcopy = array();

        foreach ($data['book'] as $value) {
          if($value->bookcopy_id != $bookcopy_id_old){
            $bookcopy_id_old = $value->bookcopy_id;
            $databookselect[]= $value;
          }
        }

        foreach ($databookselect as $val) {
          $author=$this->bookcopy_model->selectauthorbookcopybyid_model($val->bookcopy_id);
          $translator=$this->bookcopy_model->selecttranslatorbookcopybyid_model($val->bookcopy_id);
          $title=$this->bookcopy_model->selecttitlebookcopybyid_model($val->bookcopy_id);

          $databookcopy[] = [
            'bookcopy' => $val,
            'author' => $author,
            'translator' => $translator,
            'title' => $title
          ];
        }
        echo json_encode($databookcopy);
    }

    public function countsearchbookcopy()
    {
        $json = json_encode($this->input->post('datasearch'));
        $obj= json_decode($json);

        $datasearch=$this->bookcopy_model->countsearchbookcopy_model(array(
            'inputsearch'=>$obj->inputsearch,'selectmessage'=>$obj->selectmessage,
            'selectshowtype'=>$obj->selectshowtype
        ));

        echo json_encode($datasearch);
    }
}
