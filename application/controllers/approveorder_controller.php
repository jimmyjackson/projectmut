<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Approveorder_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("offerorder_model");
        $this->load->library("mpdf/mpdf");
    }

    public function index()
    {
        $this->load->view("header");
        $this->load->view("approveorderlist");
        $this->load->view("footter");
    }

    public function approveorder($idapproveorder='0')
    {
      $this->load->view("header");
      if($this->offerorder_model->selectapproveorder_where_id_model($idapproveorder)){
        $data['idapproveorder']=$idapproveorder;
        $this->load->view("approveordered",$data);
      }
      else{
        $this->load->view("nodata");
      }
      $this->load->view("footter");
    }

    private function substr_date($str)
    {
        return substr($str,6).'-'.substr($str,0,2).'-'.substr($str,3,2);
    }

    public function save_approveorder()
    {
        $json = json_encode($this->input->post('objapproveorder'));
        list($obj) = json_decode($json);

        $status="";
        $id = $obj->formdata->id;
        $status = $this->editapproveorder($id,$obj);

        echo json_encode($status);
    }

    private function editapproveorder($idofferorder,$obj)
    {
      $this->db->trans_begin();

      $status = "อนุมัติ";
      $date = $this->substr_date($obj->formdata->dateapprove);

      if($obj->status=="save")
      {
        $status = "รออนุมัติ";
      }

      $this->offerorder_model->editapproveorder_model(array(
            'date'=>$date,
            'total'=>$obj->total,
            'vattotal'=>$obj->vattotal,
            'subtotal'=>$obj->subtotal,
            'totalapprove'=>$obj->totalapprove,
            'vattotalapprove'=>$obj->vattotalapprove,
            'subtotalapprove'=>$obj->subtotalapprove,
            'idemp'=>$obj->formdata->empidapprove,
            'status'=>$status,
            'id'=>$idofferorder
      ));

      $this->offerorder_model->delete_offerorder_detial_model($idofferorder);

      if($obj->offerorderlist[0]!="none")
        $this->offerorder_model->add_approveorder_detial_model(
          $idofferorder,
          $obj->offerorderlist
        );

      if ($this->db->trans_status() === FALSE)
      {
          $this->db->trans_rollback();
          return false;
      }
      else
      {
          $this->db->trans_commit();
          return ['idofferorder'=>$idofferorder,'status'=>$status];
      }
    }

    public function deleteapproveorder()
    {
        $offerorderid=$this->input->post('idofferorder');
        $this->offerorder_model->deleteapproveorder_model($offerorderid);
    }

    public function selectdataapproveorder($idofferorder)
    {
      $data=$this->offerorder_model->selectdataofferorder_model($idofferorder);
      echo json_encode($data);
    }

    public function selectapproveorder()
    {
        $offset=$this->input->post('offsetsend');
        $data['offerorderselect']=$this->offerorder_model->selectapproveorder_model($offset);
        echo json_encode($data);
    }

    public function countapproveorder()
    {
        echo json_encode($this->offerorder_model->countapproveorder_model());
    }

    public function searchapproveorder()
    {
      $json = json_encode($this->input->post('datasearch'));
      $obj= json_decode($json);

      $datasearch=$this->offerorder_model->searchapproveorder_model(array(
          'inputsearch'=>$obj->inputsearch,'offset'=>$obj->offset,
          'selectmessage'=>$obj->selectmessage,'selectshowtype'=>$obj->selectshowtype
      ));

      echo json_encode($datasearch);
    }
    public function countsearchapproveorder()
    {
      $json = json_encode($this->input->post('datasearch'));
      $obj= json_decode($json);

      $datasearch=$this->offerorder_model->countsearchapproveorder_model(array(
          'inputsearch'=>$obj->inputsearch,'selectmessage'=>$obj->selectmessage,
          'selectshowtype'=>$obj->selectshowtype,'offset'=>$obj->offsetsend
      ));

      echo json_encode($datasearch);
    }

    public function approveorderpdf($idapproveorder)
    {
      $i = 1;
      $data=$this->offerorder_model->selectdataofferorderreport_model($idapproveorder);

      $html .= "<link rel='stylesheet' href='./assets/plugins/bootstrap/css/bootstrap.css' />";
      $html .= "<div class='section'><div class='container'><div class='row'><div class='col-md-12 text-center'><h1 contenteditable='true'>ใบอนุมัติซื้อหนังสือ</h1>";
      $html .= "<table class='table table-bordered'><thead><tr><td height='28' class='col-md-2'>รหัสใบเสนอ :</td><td height='28' class='col-md-4'>".$data[0]->offerorder_id."</td><td height='28' class='col-md-2'>วันที่อนุมัติ :</td><td height='28' class='col-md-4'>".$data[0]->offerorder_approvedate."</td></tr></thead>";
      $html .= "<tbody><tr><td height='28' class='col-md-2'>ปีการศึกษา :&nbsp;</td><td height='28' class='col-md-4'>".$data[0]->offerorder_year."/".$data[0]->offerorder_semester."</td><td height='28' class='col-md-2'>สำนักพิมพ์ :</td><td height='28' class='col-md-4'>".$data[0]->publishing_name."</td></tr>";
      $html .= "<tr><td height='28' class='col-md-2'>รหัสผู้เสนอ :</td><td height='28' class='col-md-4'>".$data[0]->id_employeeoffer."</td><td height='28' class='col-md-2'>ชื่อผู้เสนอ :</td><td height='28' class='col-md-4'>".$data[0]->offer_name."  ".$data[0]->offer_lname."</td></tr>";
      $html .= "<tr><td height='28' class='col-md-2'>รหัสผู้อนุมัติ :</td><td height='28' class='col-md-4'>".$data[0]->id_employeeapprove."</td><td height='28' class='col-md-2'>ชื่อผู้อนุมัติ :</td><td height='28' class='col-md-4'>".$data[0]->approve_name."  ".$data[0]->approve_lname."</td></tr></tbody></table>";
      $html .= "<table class='table table-bordered'>";
      $html .= "<thead><tr><th height='28'><center>ลำดับ</center></th><th height='28'><center>ชื่อหนังสือ</center></th><th height='28'><center>ราคา</center></th><th height='28'><center>จำนวนเสนอ</center></th><th height='28'><center>ราคารวมเสนอ</center></th><th height='28'><center>จำนวนอนุมัติ</center></th><th height='28'><center>ราคารวมอนุมัติ</center></th></tr></thead>";
      $html .= "<tbody>";
      foreach ($data as $value) {
        $html .= "<tr><td height='28'><center>".$i."</center></td><td height='28'><center>".$value->book_name."</center></td><td height='28'><center>".$value->pricebook."</center></td><td height='28'><center>".$value->offerorderdetial_offerunit."</center></td><td height='28'><center>".$value->offerorderdetial_offerprice."</center></td><td height='28'><center>".$value->offerorderdetial_approveunit."</center></td><td height='28'><center>".$value->offerorderdetial_approveprice."</center></td></tr>";
        $i++;
      }
      $html .= "</tbody><tfoot>";
      $html .= "<tr><th height='28' colspan='6' class='text-right'>ราคารวมเสนอทั้งหมด</th><th height='28' colspan='1'><center>".$data[0]->offerorder_offerpricetotal."</center></th></tr>";
      $html .= "<tr><th height='28' colspan='6' class='text-right'>ภาษีมูลค่าเพิ่มเสนอ ".$data[0]->offerorder_offervat." %</th><th height='28' colspan='1'><center>".$data[0]->offerorder_offervattotal."</center></th></tr>";
      $html .= "<tr><th height='28' colspan='6' class='text-right'>ราคารวมเสนอสุทธิ</th><th height='28' colspan='1'><center>".$data[0]->offerorder_offersubtotal."</center></th></tr>";
      $html .= "<tr><th height='28' colspan='7' class='text-right'></th></tr>";
      $html .= "<tr><th height='28' colspan='6' class='text-right'>ราคารวมอนุมัติทั้งหมด</th><th height='28' colspan='1'><center>".$data[0]->offerorder_approvepricetotal."</center></th></tr>";
      $html .= "<tr><th height='28' colspan='6' class='text-right'>ภาษีมูลค่าเพิ่มอนุมัติ ".$data[0]->offerorder_offervat." %</th><th height='28' colspan='1'><center>".(($data[0]->offerorder_approvepricetotal * $data[0]->offerorder_offervat)/100)."</center></th></tr>";
      $html .= "<tr><th height='28' colspan='6' class='text-right'>ราคารวมอนุมัติสุทธิ</th><th height='28' colspan='1'><center>".$data[0]->offerorder_approvesubtotal."</center></th></tr>";
      $html .= "</tfoot></table></div></div><br><br><br><br><br><br><br>";
      $html .= "<table border='0'>";
      $html .= "<tr><td width='550'></td><td><center>ชื่อผู้เสนอ</center></td></tr><tr><td width='560'></td><td><center>&nbsp;</center></td></tr><tr><td width='560'></td><td><center>&nbsp;</center></td></tr>";
      $html .= "<tr><td width='550'></td><td><center>......................................................</center></td></tr><tr><td width='560'></td><td><center>( ".$data[0]->offer_name."  ".$data[0]->offer_lname." )</center></td></tr></table>";
      $html .= "<br><table border='0'>";
      $html .= "<tr><td width='550'></td><td><center>ชื่อผู้อนุมัติ</center></td></tr><tr><td width='560'></td><td><center>&nbsp;</center></td></tr><tr><td width='560'></td><td><center>&nbsp;</center></td></tr>";
      $html .= "<tr><td width='550'></td><td><center>......................................................</center></td></tr><tr><td width='560'></td><td><center>( ".$data[0]->approve_name."  ".$data[0]->approve_lname." )</center></td></tr></table>";
      $html .= "</div></div>";

      $this->mpdf = new mPDF('th', 'A4');
      $this->mpdf->WriteHTML($html);
      $this->mpdf->Output();
    }
}
