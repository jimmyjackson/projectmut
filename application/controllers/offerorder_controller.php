<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offerorder_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("offerorder_model");
        $this->load->library("mpdf/mpdf");
    }

    public function index()
    {
        $this->load->view("header");
        $this->load->view("offerorderlist");
        $this->load->view("footter");
    }

    public function offerorder($idofferorder='0')
    {
        if($idofferorder!='0'){
            $this->load->view("header");
            if($this->offerorder_model->selectofferorder_where_id_model($idofferorder)){
                $data['idofferorder']=$idofferorder;
                $this->load->view("offerordered",$data);
            }
            else{
                $this->load->view("nodata");
            }
            $this->load->view("footter");
        }
        else{
            $this->load->view("header");
            $this->load->view("offerorder");
            $this->load->view("footter");
        }
    }

    private function loop_auto_id($autoid)
    {
        $autoidofferorder = "";

        foreach ($autoid as $value)
            foreach ($value as $subvalue)
                foreach ($subvalue as $subvalue2)
                    $autoidofferorder = $subvalue2;

        return $autoidofferorder;
    }

    private function substr_date($str)
    {
        return substr($str,6).'-'.substr($str,0,2).'-'.substr($str,3,2);
    }

    public function save_offerorder()
    {
        $json = json_encode($this->input->post('objofferorder'));
        list($obj) = json_decode($json);

        $status="";

        if($obj->formdata->id == 'Auto_Id'){
            $status = $this->addofferorder($obj);
        }else{
            $id = $obj->formdata->id;
            $status = $this->editofferorder($id,$obj);
        }

        echo json_encode($status);
    }

    private function addofferorder($obj)
    {
        $this->db->trans_begin();

        $status = "รออนุมัติ";
        $date = $this->substr_date($obj->formdata->dateoffer);
        $autoid['autoid'] = $this->offerorder_model->autoidofferorder_model();
        list($year, $semester) = explode("/", $obj->formdata->year);
        $autoidofferorder = $this->loop_auto_id($autoid);

        if($obj->status=="save")
        {
          $status = "รอเสนอ";
        }

        $result = $this->offerorder_model->addofferorder_model(array(
            'id'=>$autoidofferorder,
            'date'=>$date,
            'total'=>$obj->total,
            'vattotal'=>$obj->vattotal,
            'vat'=>$obj->formdata->vat,
            'subtotal'=>$obj->subtotal,
            'idemp'=>$obj->formdata->empid,
            'idpublishing'=>$obj->formdata->publishing,
            'year'=>$year,
            'semester'=>$semester,
            'status'=>$status
        ));

        if($obj->offerorderlist[0]!="none")
            $this->offerorder_model->add_offerorder_detial_model(
              $autoidofferorder,
              $obj->offerorderlist
            );

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $this->db->trans_commit();
            return ['idofferorder'=>$autoidofferorder,'status'=>$status];
        }
    }

    private function editofferorder($idofferorder,$obj)
    {
      $this->db->trans_begin();

      $status = "รออนุมัติ";
      $date = $this->substr_date($obj->formdata->dateoffer);
      list($year, $semester) = explode("/", $obj->formdata->year);
      $idofferorder = $obj->formdata->id;

      if($obj->status=="save")
      {
        $status = "รอเสนอ";
      }

      $this->offerorder_model->editofferorder_model(array(
            'date'=>$date,
            'total'=>$obj->total,
            'vattotal'=>$obj->vattotal,
            'vat'=>$obj->formdata->vat,
            'subtotal'=>$obj->subtotal,
            'idemp'=>$obj->formdata->empid,
            'idpublishing'=>$obj->formdata->publishing,
            'year'=>$year,
            'semester'=>$semester,
            'status'=>$status,
            'id'=>$idofferorder
      ));

      $this->offerorder_model->delete_offerorder_detial_model($idofferorder);

      if($obj->offerorderlist[0]!="none")
        $this->offerorder_model->add_offerorder_detial_model(
          $idofferorder,
          $obj->offerorderlist
        );

      if ($this->db->trans_status() === FALSE)
      {
          $this->db->trans_rollback();
          return false;
      }
      else
      {
          $this->db->trans_commit();
          return ['idofferorder'=>$idofferorder,'status'=>$status];
      }
    }

    public function selectapproveorderpurchaseorder()
    {
        $id_publishing=$this->input->post('idpublishing');
        $data=$this->offerorder_model->selectapproveorderpurchaseorder_model($id_publishing);
        echo json_encode($data);
    }

    public function deleteofferorder()
    {
        $offerorderid=$this->input->post('idofferorder');
        $this->offerorder_model->deleteofferorder_model($offerorderid);
    }

    public function selectdataofferorder($idofferorder)
    {
      $data=$this->offerorder_model->selectdataofferorder_model($idofferorder);
      echo json_encode($data);
    }

    public function selectofferorder()
    {
        $offset=$this->input->post('offsetsend');
        $data['offerorderselect']=$this->offerorder_model->selectofferorder_model($offset);
        echo json_encode($data);
    }

    public function countofferorder()
    {
        echo json_encode($this->offerorder_model->countofferorder_model());
    }

    public function searchofferorder()
    {
      $json = json_encode($this->input->post('datasearch'));
      $obj= json_decode($json);

      $datasearch=$this->offerorder_model->searchofferorder_model(array(
          'inputsearch'=>$obj->inputsearch,'offset'=>$obj->offset,
          'selectmessage'=>$obj->selectmessage,'selectshowtype'=>$obj->selectshowtype
      ));

      echo json_encode($datasearch);
    }
    public function countsearchofferorder()
    {
      $json = json_encode($this->input->post('datasearch'));
      $obj= json_decode($json);

      $datasearch=$this->offerorder_model->countsearchofferorder_model(array(
          'inputsearch'=>$obj->inputsearch,'selectmessage'=>$obj->selectmessage,
          'selectshowtype'=>$obj->selectshowtype,'offset'=>$obj->offsetsend
      ));

      echo json_encode($datasearch);
    }

    public function offerorderpdf($idofferorder)
    {
      $i = 1;
      $data=$this->offerorder_model->selectdataofferorderreport1_model($idofferorder);

      $html .= "<link rel='stylesheet' href='./assets/plugins/bootstrap/css/bootstrap.css' />";
      $html .= "<div class='section'><div class='container'><div class='row'><div class='col-md-12 text-center'><h1 contenteditable='true'>ใบเสนอซื้อหนังสือ</h1>";
      $html .= "<table class='table table-bordered'><thead><tr><td height='28' class='col-md-2'>รหัสใบเสนอ :</td><td height='28' class='col-md-4'>".$data[0]->offerorder_id."</td><td height='28' class='col-md-2'>วันที่เสนอ :</td><td height='28' class='col-md-4'>".$data[0]->offerorder_offerdate."</td></tr></thead>";
      $html .= "<tbody><tr><td height='28' class='col-md-2'>ปีการศึกษา :&nbsp;</td><td height='28' class='col-md-4'>".$data[0]->offerorder_year."/".$data[0]->offerorder_semester."</td><td height='28' class='col-md-2'>สำนักพิมพ์ :</td><td height='28' class='col-md-4'>".$data[0]->publishing_name."</td></tr>";
      $html .= "<tr><td height='28' class='col-md-2'>รหัสผู้เสนอ :</td><td height='28' class='col-md-4'>".$data[0]->id_employeeoffer."</td><td height='28' class='col-md-2'>ชื่อผู้เสนอ :</td><td height='28' class='col-md-4'>".$data[0]->offer_name."  ".$data[0]->offer_lname."</td></tr></tbody></table>";
      $html .= "<table class='table table-bordered'>";
      $html .= "<thead><tr><th height='28'><center>ลำดับ</center></th><th height='28'><center>ชื่อหนังสือ</center></th><th height='28'><center>ราคา</center></th><th height='28'><center>จำนวน</center></th><th height='28'><center>ราคารวม</center></th></tr></thead>";
      $html .= "<tbody>";
      foreach ($data as $value) {
        $html .= "<tr><td height='28'><center>".$i."</center></td><td height='28'><center>".$value->book_name."</center></td><td height='28'><center>".$value->pricebook."</center></td><td height='28'><center>".$value->offerorderdetial_offerunit."</center></td><td height='28'><center>".$value->offerorderdetial_offerprice."</center></td></tr>";
        $i++;
      }
      $html .= "</tbody><tfoot>";
      $html .= "<tr><th height='28' colspan='4' class='text-right'>ราคารวมทั้งหมด</th><th height='28' colspan='1'><center>".$data[0]->offerorder_offerpricetotal."</center></th></tr>";
      $html .= "<tr><th height='28' colspan='4' class='text-right'>ภาษีมูลค่าเพิ่ม ".$data[0]->offerorder_offervat." %</th><th height='28' colspan='1'><center>".$data[0]->offerorder_offervattotal."</center></th></tr>";
      $html .= "<tr><th height='28' colspan='4' class='text-right'>ราคารวมสุทธิ</th><th height='28' colspan='1'><center>".$data[0]->offerorder_offersubtotal."</center></th></tr>";
      $html .= "</tfoot></table></div></div><br><br><br><br><br><br><br>";
      $html .= "<table border='0'>";
      $html .= "<tr><td width='550'></td><td><center>ชื่อผู้เสนอ</center></td></tr><tr><td width='560'></td><td><center>&nbsp;</center></td></tr><tr><td width='560'></td><td><center>&nbsp;</center></td></tr>";
      $html .= "<tr><td width='550'></td><td><center>......................................................</center></td></tr><tr><td width='560'></td><td><center>( ".$data[0]->offer_name."  ".$data[0]->offer_lname." )</center></td></tr></table>";
      $html .= "</div></div>";

      $this->mpdf = new mPDF('th', 'A4');
      $this->mpdf->WriteHTML($html);
      $this->mpdf->Output();
    }
}
