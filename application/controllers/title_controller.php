<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Title_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 
        $this->load->model("title_model");
    }

    public function selectalltitle()
    {
        $data['title']=$this->title_model->selectalltitle_model();    
        echo json_encode($data['title']);
    }
    
}
