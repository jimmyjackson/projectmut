<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("receiptorder_model");
        $this->load->model("booking_model");
    }
    public function index()
    {

        $dataPublishing = $this->receiptorder_model->selectpublishing();
        $dataBooking = '';
        $status = '';
        if(isset($_POST['type'])){
            $dataBooking = $this->booking_model->updatebooking($_POST);
            $status = ($dataBooking == 'unit' ? $dataBooking : $_POST['type']);
        }elseif(isset($_POST['btadddetial'])){
            $dataBooking = $this->booking_model->getbooking($_POST);
        }

        $this->load->view("header");
        $this->load->view("booking",[
            'dataPublishing'=>$dataPublishing,
            'dataBooking'=>$dataBooking,
            'status'=>$status
        ]);
        $this->load->view("footter");
    }

    public function newbooking()
    {
        $dataPublishing = $this->receiptorder_model->selectpublishing();
        $status = '';
        $dataBooking = '';
        $error = '';

        if(isset($_POST['btadddetial'])){
            $dataBooking = $this->booking_model->getbook($_POST);
        }elseif(isset($_POST['add_id'])){
          $checkBook = $this->booking_model->checkBook($_POST);
          if($checkBook->checkbook > 0){
            $error = "checkBook";
          }
          else {
            $memberUnit = $this->booking_model->memberUnit($_POST);
            $checkBorrow = $this->booking_model->checkBorrow($_POST);

            if($checkBorrow->countborrow >= $memberUnit->membertype_unit){
              $error = "checkBorrow";
            } else {
              $dataMember = $this->booking_model->member($_POST);
              if(count($dataMember)>0){
                  $checkcountbooking = $this->booking_model->checkbooking($_POST);

                  if($checkcountbooking->countbooking > 0){
                    $status = 'countbooking';
                  } else {
                    $this->booking_model->save($_POST);
                    $status = 'save';
                  }
              }else{
                  $status = 'member';
              }
            }
          }
        }

        $this->load->view("header");
        $this->load->view("bookingnew",[
            'dataPublishing'=>$dataPublishing,
            'dataBooking'=>$dataBooking,
            'status'=>$status,
            'error'=>$error
        ]);
        $this->load->view("footter");
    }

}
