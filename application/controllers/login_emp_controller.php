<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_Emp_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    public function index()
    {
        $this->load->view("loginemp");
    }
    public function checklogin()
    {
        $this->load->model("login_emp_model");

        $json = json_encode($this->input->post('ob'));
        list($obj) = json_decode($json);

        $data=$this->login_emp_model->checklogin_model(array(
            'username'=>$obj->username,'password'=>sha1($obj->password)));

        $permission_constant=$this->login_emp_model->checkpermission_model(array(
            'username'=>$obj->username,'password'=>sha1($obj->password),'mainmenu'=>'1'));

        $permission_purchase=$this->login_emp_model->checkpermission_model(array(
            'username'=>$obj->username,'password'=>sha1($obj->password),'mainmenu'=>'2'));

        $permission_borrow=$this->login_emp_model->checkpermission_model(array(
            'username'=>$obj->username,'password'=>sha1($obj->password),'mainmenu'=>'3'));

        $permission_search=$this->login_emp_model->checkpermission_model(array(
            'username'=>$obj->username,'password'=>sha1($obj->password),'mainmenu'=>'4'));

        $permission_report=$this->login_emp_model->checkpermission_model(array(
            'username'=>$obj->username,'password'=>sha1($obj->password),'mainmenu'=>'5'));

        if($data != null){
            $this->session->set_userdata(array(
                'idemp'=>$data[0]->employee_id,
                'imgemp'=>$data[0]->employee_image,
                'username'=>$data[0]->employee_username
            ));
            echo json_encode($data);
        }
        else
        {
            echo json_encode($data);
        }

        foreach ($permission_constant as $value1){
            $session_permission['access_url1'][$value1->access_url]=$value1->access_url;
            $session_permission['access_name1'][$value1->access_url]=$value1->access_name;
            $this->session->set_userdata(
                array("permission_constant"=>$session_permission['access_url1']));
            $this->session->set_userdata(
                array("permission_constant_name"=>$session_permission['access_name1']));
        }

        foreach ($permission_purchase as $value2){
            $session_permission['access_url2'][$value2->access_url]=$value2->access_url;
            $session_permission['access_name2'][$value2->access_url]=$value2->access_name;
            $this->session->set_userdata(
                array("permission_purchase"=>$session_permission['access_url2']));
            $this->session->set_userdata(
                array("permission_purchase_name"=>$session_permission['access_name2']));
        }

        foreach ($permission_borrow as $value3){
            $session_permission['access_url3'][$value3->access_url]=$value3->access_url;
            $session_permission['access_name3'][$value3->access_url]=$value3->access_name;
            $this->session->set_userdata(
                array("permission_borrow"=>$session_permission['access_url3']));
            $this->session->set_userdata(
                array("permission_borrow_name"=>$session_permission['access_name3']));
        }

        foreach ($permission_search as $value4){
            $session_permission['access_url4'][$value4->access_url]=$value4->access_url;
            $session_permission['access_name4'][$value4->access_url]=$value4->access_name;
            $this->session->set_userdata(
                array("permission_search"=>$session_permission['access_url4']));
            $this->session->set_userdata(
                array("permission_search_name"=>$session_permission['access_name4']));
        }

        foreach ($permission_report as $value5){
            $session_permission['access_url5'][$value5->access_url]=$value5->access_url;
            $session_permission['access_name5'][$value5->access_url]=$value5->access_name;
            $this->session->set_userdata(
                array("permission_report"=>$session_permission['access_url5']));
            $this->session->set_userdata(
                array("permission_report_name"=>$session_permission['access_name5']));
        }

    }
}
