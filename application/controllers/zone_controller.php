<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Zone_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 
    }
    
    public function index()
    {
        $this->load->view("header");
        $this->load->view("zone");
        $this->load->view("footter");
    }
    public function addzone()
    {
      
    }
    public function editzone()
    {
        
    }
    public function deletezone()
    {
        
    }
    public function selectallzone()
    {
        $this->load->model("zone_model");
        $json = json_encode($this->input->post('idprovince'));
        $obj = json_decode($json); 
        $data['zone']=$this->zone_model->selectallzone_model(array('idprovince'=>$obj));    
        echo json_encode($data);
    }
    public function selectzone()
    {
        
    }
}
