<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Receiptorder_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("receiptorder_model");
        $this->load->library("mpdf/mpdf");
    }

    public function index()
    {
      $this->load->view("header");
      $this->load->view("receiptorderlist");
      $this->load->view("footter");
    }

    public function receiptorder($value='')
    {
      $idreceiptorder = '';

      if($value == ''){
        $dataPublishing = $this->receiptorder_model->selectpublishing();
        $dataPurchase = '';
        $statusReceipt = '';
        if(!empty($_POST['purchaseId']) || !empty($_POST['publishing'])){
            $dataPurchase = $this->receiptorder_model->selectpurchase($_POST);
        }elseif(!empty($_POST['receipt'])){
            $idreceiptorder = $this->receiptorder_model->savereceiptorder($_POST);
            $statusReceipt = 'receipt';
        }
        $this->load->view("header");
        $this->load->view("receiptorder",[
            'dataPublishing'=>$dataPublishing,
            'dataPurchase'=>$dataPurchase,
            'statusReceipt'=>$statusReceipt,
            'id'=>$idreceiptorder
        ]);
        $this->load->view("footter");
      } else {
        $data = $this->receiptorder_model->selectdatareceiptorder($value);

        if(count($data) > 0)
        {
          $this->load->view("header");
          $this->load->view("receiptordered",[
            'data'=>$data,
          ]);
          $this->load->view("footter");
        } else {
          $this->load->view("header");
          $this->load->view("nodata");
          $this->load->view("footter");
        }
      }
    }

    public function selectreceiptorder()
    {
        $offset=$this->input->post('offsetsend');
        $data['purchaseorderselect']=$this->receiptorder_model->selectreceiptorder_model($offset);
        echo json_encode($data);
    }

    public function countreceiptorder()
    {
        echo json_encode($this->receiptorder_model->countreceiptorder_model());
    }

    public function searchreceiptorder()
    {
      $json = json_encode($this->input->post('datasearch'));
      $obj= json_decode($json);

      $datasearch=$this->receiptorder_model->searchreceiptorder_model(array(
          'inputsearch'=>$obj->inputsearch,'offset'=>$obj->offset,
          'selectmessage'=>$obj->selectmessage,'selectshowtype'=>$obj->selectshowtype
      ));

      echo json_encode($datasearch);
    }
    public function countsearchreceiptorder()
    {
      $json = json_encode($this->input->post('datasearch'));
      $obj= json_decode($json);

      $datasearch=$this->receiptorder_model->countsearchreceiptorder_model(array(
          'inputsearch'=>$obj->inputsearch,'selectmessage'=>$obj->selectmessage,
          'selectshowtype'=>$obj->selectshowtype,'offset'=>$obj->offsetsend
      ));

      echo json_encode($datasearch);
    }

    public function receiptorderpdf($idreceiptorder)
    {
      $i = 1;
      $data=$this->receiptorder_model->selectdatareceiptorderreport_model($idreceiptorder);
      // var_dump($data);
      $html .= "<link rel='stylesheet' href='./assets/plugins/bootstrap/css/bootstrap.css' />";
      $html .= "<div class='section'><div class='container'><div class='row'><div class='col-md-12 text-center'><h1 contenteditable='true'>ใบรับหนังสือ</h1>";
      $html .= "<table class='table table-bordered'><thead><tr><td height='28' class='col-md-2'>รหัสใบรับ :</td><td height='28' class='col-md-4'>".$data[0]->receiptorder_id."</td><td height='28' class='col-md-2'>วันที่รับ :</td><td height='28' class='col-md-4'>".$data[0]->receiptorder_date."</td></tr></thead>";
      $html .= "<tbody><tr><td height='28' class='col-md-2'>สำนักพิมพ์ :</td><td height='28' class='col-md-4'>".$data[0]->publishing_name."</td><td height='28' class='col-md-2'>รหัสผู้สั่งซื้อ :</td><td height='28' class='col-md-4'>".$data[0]->id_employee."</td></tr>";
      $html .= "<tr><td height='28' class='col-md-2'>ชื่อผู้สั่งซื้อ :</td><td height='28' class='col-md-4'>".$data[0]->employee_name."  ".$data[0]->employee_lname."</td></tr></tbody></table>";
      $html .= "<table class='table table-bordered'>";
      $html .= "<thead><tr><th height='28'><center>ลำดับ</center></th><th height='28'><center>ชื่อหนังสือ</center></th><th height='28'><center>ราคา</center></th><th height='28'><center>จำนวน</center></th><th height='28'><center>ราคารวม</center></th></tr></thead>";
      $html .= "<tbody>";
      foreach ($data as $value) {
        $html .= "<tr><td height='28'><center>".$i."</center></td><td height='28'><center>".$value->book_name."</center></td><td height='28'><center>".$value->price."</center></td><td height='28'><center>".$value->unit."</center></td><td height='28'><center>".$value->sumprice."</center></td></tr>";
        $i++;
      }
      $html .= "</tbody><tfoot>";
      $html .= "<tr><th height='28' colspan='4' class='text-right'>ราคารวมทั้งหมด</th><th height='28' colspan='1'><center>".$data[0]->receiptorder_totalprice."</center></th></tr>";
      $html .= "<tr><th height='28' colspan='4' class='text-right'>ภาษีมูลค่าเพิ่ม</th><th height='28' colspan='1'><center>".number_format($data[0]->receiptorder_vat,2,'.','')."</center></th></tr>";
      $html .= "<tr><th height='28' colspan='4' class='text-right'>ราคารวมสุทธิ</th><th height='28' colspan='1'><center>".number_format($data[0]->receiptorder_vattotal,2,'.','')."</center></th></tr>";
      $html .= "</tfoot></table></div></div><br><br><br><br><br><br><br>";
      $html .= "<table border='0'>";
      $html .= "<tr><td width='550'></td><td><center>ชื่อผู้รับ</center></td></tr><tr><td width='560'></td><td><center>&nbsp;</center></td></tr><tr><td width='560'></td><td><center>&nbsp;</center></td></tr>";
      $html .= "<tr><td width='550'></td><td><center>......................................................</center></td></tr><tr><td width='560'></td><td><center>( ".$data[0]->employee_name."  ".$data[0]->employee_lname." )</center></td></tr></table>";
      $html .= "</div></div>";

      $this->mpdf = new mPDF('th', 'A4');
      $this->mpdf->WriteHTML($html);
      $this->mpdf->Output();
    }

}
