<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller
{
  function __construct() {
    parent:: __construct();
    $this->load->library("MPDF57/mpdf");
  }

  function pdf() {
    $html = "<h1>Hello</h1>";

    $this->mpdf = new mPDF();
    $this->mpdf->WriteHTML($html);
    $this->mpdf->Output();
  }
}
