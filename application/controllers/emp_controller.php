<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emp_Controller extends CI_Controller
{ 
    public function __construct()
    {
        parent::__construct(); 
        $this->load->model("emp_model");
        $this->load->helper('file');
    }    
    public function index()
    {
        $this->load->view("header");
        $this->load->view("emplist");
        $this->load->view("footter");
    }
    public function emp($idemp='0')
    {
        if($idemp!='0'){
            $this->load->view("header");
            if($this->emp_model->selectemp_where_id_model($idemp)){
                $data['idemp']=$idemp;
                $this->load->view("emped",$data);
            }
            else{
                $this->load->view("nodata");
            }
            $this->load->view("footter");
            //$data['e']=$this->selectdataemp($idemp);
            //echo json_encode($data);
        }
        else{
            $this->load->view("header");
            $this->load->view("emp");
            $this->load->view("footter");
            //echo json_encode("no");
        }
    }
    public function doupload()
    {
        // an array of allowed extensions
		  $allowedExts = array("gif", "jpeg", "jpg", "png","GIF","JPEG","JPG","PNG");
		  $temp = explode(".", $_FILES["file"]["name"]);
		  $extension = end($temp);
		  //check if the file type is image and then extension
		  // store the files to upload folder
		  //echo '0' if there is an error
		  if ((($_FILES["file"]["type"] == "image/gif")
		  || ($_FILES["file"]["type"] == "image/jpeg")
		  || ($_FILES["file"]["type"] == "image/jpg")
		  || ($_FILES["file"]["type"] == "image/pjpeg")
		  || ($_FILES["file"]["type"] == "image/x-png")
		  || ($_FILES["file"]["type"] == "image/png"))
		  && in_array($extension, $allowedExts)) {
  			if ($_FILES["file"]["error"] > 0) {
    			/*echo "0";*/
                header('Location: /CI/emp_controller');
  			} else {
    			$target = "./upload/".date("YmdHis")."_".$_FILES["file"]["name"];
    			move_uploaded_file($_FILES["file"]["tmp_name"],$target);
    			header('Location: /CI/emp_controller');
                /*echo "./upload/".$_FILES["file"]["name"];*/
  			}
		  }else {
  			/*echo "0";*/
             header('Location: /CI/emp_controller');
		  }
    }
    public function addemp()
    {
            $autoid['autoid']=$this->emp_model->autoidemp_model();    
            
            foreach ($autoid as $value)
                foreach ($value as $subvalue)
                    foreach ($subvalue as $subvalue2)
                      $autoidemp = $subvalue2;
        
            $json = json_encode($this->input->post('objemp'));
            list($obj) = json_decode($json);
            if($obj->imagestatus=='1')
                $filenameup = base_url()."upload/".date("YmdHis").'_'.$obj->filename;
            else
                $filenameup = base_url()."upload/".$obj->filename;
            //$this->emp_model->addemp_model(array('idemp'=>$autoidemp));
            
            $data['emp']=$this->emp_model->addemp_model(array('idemp'=>$autoidemp,'nameemp'=>$obj->nameemp,
            'lnameemp'=>$obj->lnameemp,'sexemp'=>$obj->sexemp,
            'birthdayemp'=>$obj->birthdayemp,'imgemp'=>$filenameup,'addressemp'=>$obj->addressemp,
            'emailemp'=>$obj->emailemp,'idcardemp'=>$obj->idcardemp,'useremp'=>$obj->useremp,
            'passemp'=>$obj->passemp,'postcemp'=>$obj->postcemp,'provemp'=>$obj->provemp,
            'zoneemp'=>$obj->zoneemp,'distemp'=>$obj->distemp,'positemp'=>$obj->positemp,'tel'=>$obj->tel));
             
            echo json_encode($autoidemp);
            header('Location: /CI/emp_controller');
    }
    public function deleteemp()
    {
        $empid=$this->input->post('idemp');
        $data=$this->emp_model->deleteemp_model($empid);
        
        if($data['status'] == false) 
            $this->delete_image_emp(0,$data['image']);
    }
    public function editemp()
    {
        $tel = "";
        $deltel = "";
        
        $json = json_encode($this->input->post('objemp'));
        list($obj) = json_decode($json);
        
        if($obj->imagestatus=='1'){  
            $filenameup = base_url()."upload/".date("YmdHis").'_'.$obj->filename;
            $this->delete_image_emp($obj->idemp,null);
        }else
            $filenameup = $obj->filename;
        
        if($obj->tel != "")
            $tel = $obj->tel;
        
        if($obj->teldel != "")
            $deltel = $obj->teldel;
            
        $data['emp']=$this->emp_model->editemp_model(array(
        'nameemp'=>$obj->nameemp,'lnameemp'=>$obj->lnameemp,'sexemp'=>$obj->sexemp,
        'birthdayemp'=>$obj->birthdayemp,'imgemp'=>$filenameup,
        'addressemp'=>$obj->addressemp,'emailemp'=>$obj->emailemp,
        'idcardemp'=>$obj->idcardemp,'useremp'=>$obj->useremp,'postcemp'=>$obj->postcemp,
        'provemp'=>$obj->provemp,'zoneemp'=>$obj->zoneemp,'distemp'=>$obj->distemp,
        'positemp'=>$obj->positemp,'idemp'=>$obj->idemp,'tel'=>$tel,'teldel'=>$deltel));
        
        echo json_encode($data);
        //header('Location: /CI/emp_controller');
    }
    public function selectdataemp($idemp='')
    {
        $data=$this->emp_model->selectdataemp_model($idemp);
        echo json_encode($data);
    }
    public function selectemp()
    {
        $offset=$this->input->post('offsetsend');
        $data['empselect']=$this->emp_model->selectemp_model($offset);
        echo json_encode($data);
    }
    public function countemp()
    {
        echo json_encode($this->emp_model->countemp_model());
    }
    public function selectusernameemp()
    {
        $json = json_encode($this->input->post('usernamecheck'));
        $obj= json_decode($json);
        $usernamecheck['uc']=$this->emp_model->selectusernameemp_model($obj);
        
        echo json_encode($usernamecheck);
    }
    public function searchemp()
    {
        $json = json_encode($this->input->post('datasearch'));
        $obj= json_decode($json);
        
        $datasearch=$this->emp_model->searchemp_model(array(
        'inputsearch'=>$obj->inputsearch,'offset'=>$obj->offset,
        'selectmessage'=>$obj->selectmessage));
        
        echo json_encode($datasearch);
    }
    public function countsearchemp()
    {
        $json = json_encode($this->input->post('datasearch'));
        $obj= json_decode($json);
        
        $datasearch=$this->emp_model->countsearchemp_model(array(
        'inputsearch'=>$obj->inputsearch,'selectmessage'=>$obj->selectmessage));
        
        echo json_encode($datasearch);
    }
    
    public function delete_image_emp($id,$image)
    {
        if($id !== 0 && $image === null)
            $oldnameimage = $this->emp_model->get_image_emp_model($id)->emp_image;
        else
            $oldnameimage = $image;
        
        if($oldnameimage !== "demoUpload.jpg")
            unlink('../CI/upload/'.$oldnameimage);
        
        echo json_encode($oldnameimage);
        
    }
    
    public function edit_password_emp()
    {
        $json = json_encode($this->input->post('objemp'));
        list($obj) = json_decode($json);

        $result = $this->emp_model->edit_password_emp_model(array
        ('password_edit'=>$obj->password_edit,'id_emp'=>$obj->id_emp));
        
        echo json_encode($result);
    }
}
