<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Submubook_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 
    }

    public function selectallsubmubook()
    {
        $this->load->model("submubook_model");
        $json = json_encode($this->input->post('mubook'));
        $obj = json_decode($json); 
        $data['zone']=$this->submubook_model->selectallsubmubook_model(array('mubook'=>$obj));    
        echo json_encode($data);
    }

}
