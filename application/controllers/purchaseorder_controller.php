<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchaseorder_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("purchaseorder_model");
        $this->load->library("mpdf/mpdf");
    }

    public function index()
    {
        $this->load->view("header");
        $this->load->view("purchaseorderlist");
        $this->load->view("footter");
    }

    public function purchaseorder($idpurchaseorder='0')
    {
        if($idpurchaseorder!='0'){
            $this->load->view("header");
            if($this->purchaseorder_model->selectpurchaseorder_where_id_model($idpurchaseorder)){
                $data['idpurchaseorder']=$idpurchaseorder;
                $this->load->view("purchaseordered",$data);
            }
            else{
                $this->load->view("nodata");
            }
            $this->load->view("footter");
        }
        else{
            $this->load->view("header");
            $this->load->view("purchaseorder");
            $this->load->view("footter");
        }
    }

    private function loop_auto_id($autoid)
    {
        $autoidofferorder = "";

        foreach ($autoid as $value)
            foreach ($value as $subvalue)
                foreach ($subvalue as $subvalue2)
                    $autoidofferorder = $subvalue2;

        return $autoidofferorder;
    }

    private function substr_date($str)
    {
        return substr($str,6).'-'.substr($str,0,2).'-'.substr($str,3,2);
    }

    public function save_purchaseorder()
    {
        $json = json_encode($this->input->post('objpurchaseorder'));
        list($obj) = json_decode($json);

        $status = $this->addpurchaseorder($obj);

        echo json_encode($status);
    }

    private function addpurchaseorder($obj)
    {
        $this->db->trans_begin();
        $autoidgroup = array();

        $selectsumapproveorder = $this->purchaseorder_model->selectsumapproveordergroupofferorder_model(
          array(
            'purchaseorderlist' => $obj->purchaseorderlist,
            'publishing' => $obj->publishing
          )
        );

        foreach ($selectsumapproveorder as $value)
        {
          if($value->groupapproveunit != '0')
          {
            $autoidgroupofferorder['autoidgroupofferorder'] = $this->purchaseorder_model->autoidgroupofferorder_model();
            $autoidgroupoffer = $this->loop_auto_id($autoidgroupofferorder);
            array_push($autoidgroup,$autoidgroupoffer);

            $this->purchaseorder_model->addgroupofferorder_model(array(
                'id'=>$autoidgroupoffer,
                'unittotal'=>$value->groupapproveunit,
                'pricetotal'=>$value->groupapproveprice,
                'idbook'=>$value->id_book,
            ));

            $selectlistapproveorder = $this->purchaseorder_model->selectlistapproveordergroupofferorder_model(
             array(
               'purchaseorderlist' => $obj->purchaseorderlist,
               'publishing' => $obj->publishing,
               'bookid' => $value->id_book
             )
            );

            foreach ($selectlistapproveorder as $val)
            {
              $this->purchaseorder_model->adddetialgroupofferorder_model(array(
                  'idoffer'=>$val->offerorder_id,
                  'nooffer'=>$val->offerorderdetial_no,
                  'idgroupoffer'=>$autoidgroupoffer,
                  'unit'=>$val->offerorderdetial_approveunit,
                  'price'=>$val->offerorderdetial_approveprice
              ));
            }
          }
        }
        $autoid['autoidpurchaseorder'] = $this->purchaseorder_model->autoidpurchaseorder_model();
        $autoidpurchaseorder = $this->loop_auto_id($autoid);

        $selectsumpurchaseorder = $this->purchaseorder_model->selectsumapproveorderpurchaseorder_model(
          array(
            'purchaseorderlist' => $obj->purchaseorderlist
          )
        );

        $this->purchaseorder_model->addpurchaseorder_model(
          array(
            'id'=>$autoidpurchaseorder,
            'total'=>$selectsumpurchaseorder[0]->pricetotal,
            'vat'=>$selectsumpurchaseorder[0]->vat,
            'vattotal'=>$selectsumpurchaseorder[0]->vattotal,
            'subtotal'=>$selectsumpurchaseorder[0]->subtotal,
            'idemp'=>$this->session->userdata('idemp'),
            'idpublishing'=>$obj->publishing
          )
        );

        $i = 1;
        foreach ($autoidgroup as $v)
        {
          $ofg = $this->purchaseorder_model->selectsumapproveorderpurchaseorderdetial_model($v);

          $this->purchaseorder_model->adddetialpurchaseorder_model(
            array(
                'id'=>$autoidpurchaseorder,
                'no'=>$i,
                'unit'=>$ofg[0]->groupofferorder_unittotal,
                'bookprice'=>$ofg[0]->pricebook,
                'sumprice'=>$ofg[0]->groupofferorder_pricetotal,
                'balanceunit'=>$ofg[0]->groupofferorder_unittotal,
                'id_book'=>$ofg[0]->id_book,
                'id_groupofferorder'=>$ofg[0]->groupofferorder_id
            )
          );
          $i++;
        }

        foreach ($obj->purchaseorderlist as $purchaseorder)
        {
          $this->purchaseorder_model->updatestatusofferorder_model($purchaseorder);
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $this->db->trans_commit();
            return ['idpurchaseorder'=>$autoidpurchaseorder];
        }
    }

    public function selectvatapproveorder()
    {
      $status = true;

      $json = json_encode($this->input->post('objpurchaseorder'));
      list($obj) = json_decode($json);

      $data=$this->purchaseorder_model->selectvatapproveorder_model(
        array(
          'purchaseorderlist' => $obj->purchaseorderlist
        )
      );

      $tmp = $data[0]->offerorder_offervat;

      foreach ($data as $value) {
        if($tmp == $value->offerorder_offervat)
          $status = true;
        else
          $status = false;
      }

      echo $status;
    }

    public function selectdatapurchaseorder($idpurchaseorder)
    {
      $data=$this->purchaseorder_model->selectdatapurchaseorder_model($idpurchaseorder);
      echo json_encode($data);
    }

    public function selectpurchaseorder()
    {
        $offset=$this->input->post('offsetsend');
        $data['purchaseorderselect']=$this->purchaseorder_model->selectpurchaseorder_model($offset);
        echo json_encode($data);
    }

    public function countpurchaseorder()
    {
        echo json_encode($this->purchaseorder_model->countpurchaseorder_model());
    }

    public function searchpurchaseorder()
    {
      $json = json_encode($this->input->post('datasearch'));
      $obj= json_decode($json);

      $datasearch=$this->purchaseorder_model->searchpurchaseorder_model(array(
          'inputsearch'=>$obj->inputsearch,'offset'=>$obj->offset,
          'selectmessage'=>$obj->selectmessage,'selectshowtype'=>$obj->selectshowtype
      ));

      echo json_encode($datasearch);
    }
    public function countsearchpurchaseorder()
    {
      $json = json_encode($this->input->post('datasearch'));
      $obj= json_decode($json);

      $datasearch=$this->purchaseorder_model->countsearchpurchaseorder_model(array(
          'inputsearch'=>$obj->inputsearch,'selectmessage'=>$obj->selectmessage,
          'selectshowtype'=>$obj->selectshowtype,'offset'=>$obj->offsetsend
      ));

      echo json_encode($datasearch);
    }

    public function purchaseorderpdf($idpurchaseorder)
    {
      $i = 1;
      $data=$this->purchaseorder_model->selectdatapurchaseorderreport_model($idpurchaseorder);
      // var_dump($data);
      $html .= "<link rel='stylesheet' href='./assets/plugins/bootstrap/css/bootstrap.css' />";
      $html .= "<div class='section'><div class='container'><div class='row'><div class='col-md-12 text-center'><h1 contenteditable='true'>ใบสั่งซื้อหนังสือ</h1>";
      $html .= "<table class='table table-bordered'><thead><tr><td height='28' class='col-md-2'>รหัสใบสั่งซื้อ :</td><td height='28' class='col-md-4'>".$data[0]->purchaseorder_id."</td><td height='28' class='col-md-2'>วันที่สั่งซื้อ :</td><td height='28' class='col-md-4'>".$data[0]->purchaseorder_date."</td></tr></thead>";
      $html .= "<tbody><tr><td height='28' class='col-md-2'>สำนักพิมพ์ :</td><td height='28' class='col-md-4'>".$data[0]->publishing_name."</td><td height='28' class='col-md-2'>รหัสผู้สั่งซื้อ :</td><td height='28' class='col-md-4'>".$data[0]->id_employee."</td></tr>";
      $html .= "<tr><td height='28' class='col-md-2'>ชื่อผู้สั่งซื้อ :</td><td height='28' class='col-md-4'>".$data[0]->employee_name."  ".$data[0]->employee_lname."</td></tr></tbody></table>";
      $html .= "<table class='table table-bordered'>";
      $html .= "<thead><tr><th height='28'><center>ลำดับ</center></th><th height='28'><center>ชื่อหนังสือ</center></th><th height='28'><center>ราคา</center></th><th height='28'><center>จำนวน</center></th><th height='28'><center>ราคารวม</center></th></tr></thead>";
      $html .= "<tbody>";
      foreach ($data as $value) {
        $html .= "<tr><td height='28'><center>".$value->purchaseorderdetial_no."</center></td><td height='28'><center>".$value->book_name."</center></td><td height='28'><center>".$value->purchaseorderdetial_bookprice."</center></td><td height='28'><center>".$value->purchaseorderdetial_unit."</center></td><td height='28'><center>".$value->purchaseorderdetial_sumprice."</center></td></tr>";
        $i++;
      }
      $html .= "</tbody><tfoot>";
      $html .= "<tr><th height='28' colspan='4' class='text-right'>ราคารวมทั้งหมด</th><th height='28' colspan='1'><center>".$data[0]->purchaseorder_pricetotal."</center></th></tr>";
      $html .= "<tr><th height='28' colspan='4' class='text-right'>ภาษีมูลค่าเพิ่ม ".$data[0]->purchaseorder_vat." %</th><th height='28' colspan='1'><center>".number_format($data[0]->purchaseorder_vattotal,2,'.','')."</center></th></tr>";
      $html .= "<tr><th height='28' colspan='4' class='text-right'>ราคารวมสุทธิ</th><th height='28' colspan='1'><center>".number_format($data[0]->purchaseorder_subpricetotal,2,'.','')."</center></th></tr>";
      $html .= "</tfoot></table></div></div><br><br><br><br><br><br><br>";
      $html .= "<table border='0'>";
      $html .= "<tr><td width='550'></td><td><center>ชื่อผู้สั่งซื้อ</center></td></tr><tr><td width='560'></td><td><center>&nbsp;</center></td></tr><tr><td width='560'></td><td><center>&nbsp;</center></td></tr>";
      $html .= "<tr><td width='550'></td><td><center>......................................................</center></td></tr><tr><td width='560'></td><td><center>( ".$data[0]->employee_name."  ".$data[0]->employee_lname." )</center></td></tr></table>";
      $html .= "</div></div>";

      $this->mpdf = new mPDF('th', 'A4');
      $this->mpdf->WriteHTML($html);
      $this->mpdf->Output();
    }
}
