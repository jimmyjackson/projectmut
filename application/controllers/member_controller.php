<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("member_model");
        /*$this->load->helper('file');
        $this->load->library('upload');*/
    }

    public function index()
    {
        $this->load->view("header");
        $this->load->view("memberlist");
        $this->load->view("footter");
    }

    public function member($idmember='0')
    {

        if($idmember!='0'){
            $this->load->view("header");
            if($this->member_model->selectmember_where_id_model($idmember)){
                $data['idmember']=$idmember;
                $this->load->view("membered",$data);
            }
            else{
                $this->load->view("nodata");
            }
            $this->load->view("footter");
        }
        else{
            $this->load->view("header");
            $this->load->view("member");
            $this->load->view("footter");
        }

    }

    private function base64_to_jpeg($base64_string, $output_file)
    {
        $ifp = fopen("../CI/".$output_file, "wb");

        $data = explode(',', $base64_string);

        fwrite($ifp, base64_decode($data[1]));
        fclose($ifp);

        /*return $output_file;*/
    }

    public function save_member()
    {
        $json = json_encode($this->input->post('objmem'));
        list($obj) = json_decode($json);
        /*$tels = [];
        if($obj->tels[0]!="none")
            $tels=$obj->tels;
        else
            $tels=["none"];*/

        /*$output = $this->base64_to_jpeg($obj->fileimage,
                                 '../CI/upload/pig_'.$obj->formdata->id.'.jpg');*/

        $status="";

        $birthdate = $this->substr_date($obj->formdata->birthdate);
        $regisdate = $this->substr_date($obj->formdata->regisdate);

        $type_member = $this->return_type_member($obj->formdata->typemember);
        $filenameup = $this->change_filename($obj->filename);

        $idcardmember = $this->add_sub_class_member($type_member,$obj);

        if($obj->formdata->id == 'Auto_Id'){
            $status = $this->addmember($idcardmember,$type_member,
                                        $filenameup,$birthdate,$regisdate,$obj);

            if($filenameup != "upload/demoUpload.jpg")
                $this->base64_to_jpeg($obj->fileimage,$filenameup);
                /*$this->do_upload($filenameup);*/

        }else{
            $id = $obj->formdata->id;
            $status = $this->editmember($id,$idcardmember,$type_member,
                                        $filenameup,$birthdate,$regisdate,$obj);

            if($filenameup != "upload/demoUpload.jpg")
                $this->base64_to_jpeg($obj->fileimage,$filenameup);
                /*$this->do_upload($filenameup);*/


        }
        echo $status;
    }

    private function addmember($idcardmember,$type_member,
                              $filenameup,$birthdate,$regisdate,$obj)
    {
        $autoid['autoid'] = $this->member_model->autoidmember_model();
        $autoidmember = $this->loop_auto_id($autoid);

        echo $this->member_model->addmember_model(array(
            'id'=>$autoidmember,
            'name'=>$obj->formdata->name,
            'lname'=>$obj->formdata->lname,
            'birthdate'=>$birthdate,
            'address'=>$obj->formdata->address,
            'image'=>base_url().$filenameup,
            'email'=>$obj->formdata->email,
            'sex'=>$obj->formdata->sex,
            'user'=>$obj->formdata->user,
            'pass'=>$obj->formdata->pass,
            'regisdate'=>$regisdate,
            'status'=>'1',
            'typemember'=>$type_member,
            'postcode'=>$obj->formdata->postcode,
            'province'=>$obj->formdata->province,
            'zone'=>$obj->formdata->zone,
            'district'=>$obj->formdata->district,
            'idcardmember'=>$idcardmember,
            'tels'=>$obj->tels
        ));
    }

    private function editmember($idmember,$idcardmember,$type_member,
                                $filenameup,$birthdate,$regisdate,$obj)
    {
        $nameimage = "";

        if($filenameup == "upload/demoUpload.jpg")
        {
            $nameimage = $this->member_model->get_image_member_model($idmember)->member_image;
        }
        else{
            $this->delete_image_member(
                $idmember,
                $this->member_model->get_image_member_model($idmember)->member_image
            );
            $nameimage = substr($filenameup,7);
        }

        echo $this->member_model->editmember_model(array(
            'name'=>$obj->formdata->name,
            'lname'=>$obj->formdata->lname,
            'birthdate'=>$birthdate,
            'address'=>$obj->formdata->address,
            'image'=>base_url().'upload/'.$nameimage,
            'email'=>$obj->formdata->email,
            'sex'=>$obj->formdata->sex,
            'regisdate'=>$regisdate,
            'status'=>'1',
            'typemember'=>$type_member,
            'postcode'=>$obj->formdata->postcode,
            'province'=>$obj->formdata->province,
            'zone'=>$obj->formdata->zone,
            'district'=>$obj->formdata->district,
            'id'=>$idmember,
            'idcardmember'=>$idcardmember,
            'tels'=>$obj->tels
        ));

    }

    public function deletemember()
    {
        $memberid=$this->input->post('idmember');
        $data=$this->member_model->deletemember_model($memberid);

        if($data['status'] == false)
            $this->delete_image_member(0,$data['image']);
    }

    public function selectdatamember($idmember='')
    {
        $data=$this->member_model->selectdatamember_model($idmember);
        echo json_encode($data);
    }
    public function selectmember()
    {
        $offset=$this->input->post('offsetsend');
        $data['memberselect']=$this->member_model->selectmember_model($offset);
        echo json_encode($data);
    }
    public function countmember()
    {
        echo json_encode($this->member_model->countmember_model());
    }
    public function selectusernamemember()
    {
        $json = json_encode($this->input->post('usernamecheck'));
        $obj= json_decode($json);
        $usernamecheck['uc']=$this->member_model->selectusernamemember_model($obj);

        echo json_encode($usernamecheck);
    }
    public function searchmember()
    {
        $json = json_encode($this->input->post('datasearch'));
        $obj= json_decode($json);

        $datasearch=$this->member_model->searchmember_model(array(
            'inputsearch'=>$obj->inputsearch,'offset'=>$obj->offset,
            'selectmessage'=>$obj->selectmessage,'selectshowtype'=>$obj->selectshowtype
        ));

        echo json_encode($datasearch);
    }
    public function countsearchmember()
    {
        $json = json_encode($this->input->post('datasearch'));
        $obj= json_decode($json);

        $datasearch=$this->member_model->countsearchmember_model(array(
            'inputsearch'=>$obj->inputsearch,'selectmessage'=>$obj->selectmessage,
            'selectshowtype'=>$obj->selectshowtype
        ));

        echo json_encode($datasearch);
    }

    public function delete_image_member($id,$image)
    {
        if($id !== 0 && $image === null)
            $oldnameimage = $this->member_model->get_image_member_model($id)->member_image;
        else
            $oldnameimage = $image;

        if($oldnameimage != "demoUpload.jpg")
            unlink('../CI/upload/'.$oldnameimage);

        /*echo $oldnameimage;*/

    }

    public function edit_password_member()
    {
        $json = json_encode($this->input->post('objmember'));
        list($obj) = json_decode($json);

        $result = $this->member_model->edit_password_member_model(array(
            'password_edit'=>$obj->password_edit,'id_member'=>$obj->id_member
        ));

        echo json_encode($result);
    }

    public function edit_expiration_date_member(){

        $id = $this->input->post('idmember');
        $idtype = $this->member_model->get_member_type_model($id)->id_membertype;
        if($idtype=='MEMTY00001')
        {
            $result = $this->member_model->manage_yearmemberstudent_model($id);
            if($result == false)
                echo "false";
            else
                echo $result;
        }
        else
            echo "false";

    }

    public function manage_tel_personal(){

        $json = json_encode($this->input->post('obj'));
        $obj= json_decode($json);

        $data["tel"] = $this->get_tel_personal($obj[0]->id);

        if($data["tel"] != null)
            $this->del_tel_personal($obj[0]->id);

        $this->add_tel_personal($obj[0]->id,$obj[0]->tels);
    }

    private function add_tel_personal($id,$ar=array()){
        $this->member_model->add_tel_personal_model($id,$ar);
    }

    private function del_tel_personal($idmember){
        $this->member_model->del_tel_personal_model($idmember);
    }

    private function get_tel_personal($idmember){
        return $this->member_model->get_tel_personal_model($idmember);
    }

    private function add_sub_class_member($type_member,$obj){

        if($type_member == "MEMTY00001")
            return $obj->formdata->idstudentcard;
        else
            return $obj->formdata->idcard;
    }

    private function change_filename($filename)
    {
        if($filename == null || $filename == "")
            return "upload/demoUpload.jpg";
        else
            return "upload/".date("YmdHis").'_'.$filename;

    }

    private function do_upload($filename)
    {
        $allowedExts = array("gif", "jpeg", "jpg", "png","GIF","JPEG","JPG","PNG");
        $temp = explode(".", $_FILES["file"]["name"]);
        $extension = end($temp);
		  //check if the file type is image and then extension
		  // store the files to upload folder
		  //echo '0' if there is an error
        if ((($_FILES["file"]["type"] == "image/gif")
		  || ($_FILES["file"]["type"] == "image/jpeg")
		  || ($_FILES["file"]["type"] == "image/jpg")
		  || ($_FILES["file"]["type"] == "image/pjpeg")
		  || ($_FILES["file"]["type"] == "image/x-png")
		  || ($_FILES["file"]["type"] == "image/png"))
		  && in_array($extension, $allowedExts)) {
  			if ($_FILES["file"]["error"] > 0) {
    			echo "0";
  			} else {
    			$target = "./".$filename;
    			move_uploaded_file($_FILES["file"]["tmp_name"],$target);
  			}
		  }else {
  			echo "0";
		  }
    }

    private function substr_date($str)
    {
        return substr($str,6).'-'.substr($str,3,2).'-'.substr($str,0,2);
    }

    private function loop_auto_id($autoid)
    {
        $autoidmember = "";

        foreach ($autoid as $value)
            foreach ($value as $subvalue)
                foreach ($subvalue as $subvalue2)
                    $autoidmember = $subvalue2;

        return $autoidmember;
    }

    private function return_type_member($type_member)
    {
        if($type_member=='1')
            return "MEMTY00001";
        else
            return "MEMTY00002";
    }

}
