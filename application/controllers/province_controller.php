<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Province_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 
    }
    
    public function index()
    {
        $this->load->view("header");
        $this->load->view("emp");
        $this->load->view("footter");
    }
    public function addprovince()
    {
      
    }
    public function editprovince()
    {
        
    }
    public function deleteprovince()
    {
        
    }
    public function selectallprovince()
    {
        $this->load->model("province_model");
        $data['province']=$this->province_model->selectallprovince_model();    
        echo json_encode($data);
    }
    public function selectprovince()
    {
        
    }
}
