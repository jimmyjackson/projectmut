<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportsoldout_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("reportsoldout_model");
        $this->load->library("mpdf/mpdf");
    }

    public function index()
    {
        $this->load->view("header");
        $this->load->view("reportsoldout");
        $this->load->view("footter");
    }

    public function selectreport($date = 0)
    {
      if($date != 0) {
        $arrDate = explode(':', $date);

        $data = $this->reportsoldout_model->selectreport_model($arrDate[0], $arrDate[1]);

        $html .= "<link rel='stylesheet' href='./assets/plugins/bootstrap/css/bootstrap.css' />";
        $html .= "<div class='section'><div class='container'><div class='row'><div class='col-md-12 text-center'><h1 contenteditable='true'>รายงานการรับจากสั่งซื้อ</h1>";
        $html .= "<table class='table table-bordered'><thead><tr><td height='28' class='col-md-2'>วันที่ออกรายงาน :</td><td height='28' class='col-md-4'>".$data[0]->cur_date."</td><td height='28' class='col-md-2'>รหัสพนักงาน :</td><td height='28' class='col-md-4'>".$this->session->userdata('idemp')."</td></tr></thead>";
        $html .= "<tbody><tr><td height='28' class='col-md-2'>วันที่เริ่มต้น :</td><td height='28' class='col-md-4'>".$arrDate[0]."</td><td height='28' class='col-md-2'>วันที่สิ้นสุด :</td><td height='28' class='col-md-4'>".$arrDate[1]."</td></tr></tbody></table>";
        $html .= "<table class='table table-bordered'>";
        $html .= "<thead><tr><th height='28'><center>รหัสหนังสือ</center></th><th height='28'><center>ชื่อหนังสือ</center></th><th height='28'><center>จำนวนชำรุด</center></th><th height='28'><center>จำนวนสูญหาย</center></th></tr></thead>";
        $html .= "<tbody>";
        foreach ($data as $value) {
          $html .= "<tr><td height='28'><center>".$value->book_id."</center></td><td height='28'><center>".$value->book_name."</center></td><td height='28'><center>".$value->book_fixed."</center></td><td height='28'><center>".$value->book_lost."</center></td></tr>";
        }
        $html .= "</tbody></table></div></div>";
        $html .= "</div></div>";

        $this->mpdf = new mPDF('th', 'A4');
        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
      } else {
        echo "กรุณากรอกวันที่เริ่มต้นและวันที่สิ้นสุด";
      }
    }

}
