<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Yearstudy_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("yearstudy_model");
    }

    public function get_year()
    {
        $data['year']=$this->yearstudy_model->get_year_model();
        echo json_encode($data);
    }
}
