<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportbuying_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("reportbuying_model");
        $this->load->library("mpdf/mpdf");
    }

    public function index()
    {
        $this->load->view("header");
        $this->load->view("reportbuying");
        $this->load->view("footter");
    }

    public function selectreport($date = 0)
    {
      if($date != 0) {
        $arrDate = explode(':', $date);

        $data = $this->reportbuying_model->selectreport_model($arrDate[0], $arrDate[1]);
        $sumtotal = $this->reportbuying_model->selectreporttotal_model($arrDate[0], $arrDate[1]);

        $html .= "<link rel='stylesheet' href='./assets/plugins/bootstrap/css/bootstrap.css' />";
        $html .= "<div class='section'><div class='container'><div class='row'><div class='col-md-12 text-center'><h1 contenteditable='true'>รายงานการสั่งซื้อ</h1>";
        $html .= "<table class='table table-bordered'><thead><tr><td height='28' class='col-md-2'>วันที่ออกรายงาน :</td><td height='28' class='col-md-4'>".$data[0]->cur_date."</td><td height='28' class='col-md-2'>รหัสพนักงาน :</td><td height='28' class='col-md-4'>".$this->session->userdata('idemp')."</td></tr></thead>";
        $html .= "<tbody><tr><td height='28' class='col-md-2'>วันที่เริ่มต้น :</td><td height='28' class='col-md-4'>".$arrDate[0]."</td><td height='28' class='col-md-2'>วันที่สิ้นสุด :</td><td height='28' class='col-md-4'>".$arrDate[1]."</td></tr></tbody></table>";
        $html .= "<table class='table table-bordered'>";
        $html .= "<thead><tr><th height='28'><center>รหัสหนังสือ</center></th><th height='28'><center>ชื่อหนังสือ</center></th><th height='28'><center>ราคาปก</center></th><th height='28'><center>ราคาทุน</center></th><th height='28'><center>จำนวน</center></th><th height='28'><center>ราคารวม</center></th></tr></thead>";
        $html .= "<tbody>";
        foreach ($data as $value) {
          $html .= "<tr><td height='28'><center>".$value->id_book."</center></td><td height='28'><center>".$value->book_name."</center></td><td height='28'><center>".$value->book_price."</center></td><td height='28'><center>".$value->purchaseorderdetial_bookprice."</center></td><td height='28'><center>".$value->sum_unit."</center></td><td height='28'><center>".$value->sum_price."</center></td></tr>";
        }
        $html .= "</tbody><tfoot>";
        $html .= "<tr><td height='28' colspan='5' class='text-right'>ราคารวม</td><td height='28' colspan='1'><center>".$sumtotal[0]->total."</center></td></tr>";
        $html .= "<tr><td height='28' colspan='5' class='text-right'>ภาษีมูลค่าเพิ่ม</td><td height='28' colspan='1'><center>".$sumtotal[0]->vat."</center></td></tr>";
        $html .= "<tr><td height='28' colspan='5' class='text-right'>ราคารวมสุทธิ</td><td height='28' colspan='1'><center>".$sumtotal[0]->subtotal."</center></td></tr>";
        $html .= "</tfoot></table></div></div>";
        $html .= "</div></div>";

        $this->mpdf = new mPDF('th', 'A4');
        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
      } else {
        echo "กรุณากรอกวันที่เริ่มต้นและวันที่สิ้นสุด";
      }
    }

}
