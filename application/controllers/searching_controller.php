<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Searching_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("searching_model");
        $this->load->model("receiptorder_model");
        $this->load->model("bookcopy_model");
    }
    public function index()
    {
        $dataPublishing = $this->receiptorder_model->selectpublishing();

        $dataBook;
        $databookcopy;

        if(isset($_POST['btadddetial'])){
          $dataBook = $this->searching_model->getbook($_POST);

          if(count($dataBook) > 0){
            $bookcopy_id_old = $dataBook[0]->bookcopy_id;

            $databookselect[] = $dataBook[0];

            $author = array();
            $translator = array();
            $title = array();

            foreach ($dataBook as $value) {
              if($value->bookcopy_id != $bookcopy_id_old){
                $bookcopy_id_old = $value->bookcopy_id;
                $databookselect[]= $value;
              }
            }

            foreach ($databookselect as $val) {
              $author=$this->bookcopy_model->selectauthorbookcopybyid_model($val->bookcopy_id);
              $translator=$this->bookcopy_model->selecttranslatorbookcopybyid_model($val->bookcopy_id);
              $title=$this->bookcopy_model->selecttitlebookcopybyid_model($val->bookcopy_id);

              $databookcopy[] = [
                'dataBook'=>$val,
                'author'=>$author,
                'translator'=>$translator,
                'title'=>$title
              ];
            }
          } else {
            $dataBook = "";
            $databookselect = "";
            $databookcopy = "";
          }
        } else {
          $dataBook = "";
          $databookselect = "";
          $databookcopy = "";
        }
        // var_dump($databookcopy);
        $this->load->view("header");
        $this->load->view("searching",[
            'dataPublishing'=>$dataPublishing,
            'databookcopy'=>$databookcopy
        ]);
        $this->load->view("footter");
    }


}
