<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Author_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 
        $this->load->model("author_model");
    }
    
    public function selectallauthor()
    {
        $data['author']=$this->author_model->selectallauthor_model();    
        echo json_encode($data['author']);
    }

    public function selectallauthorbypname()
    {
        $data['author']=$this->author_model->selectallauthorbypname_model();    
        echo json_encode($data['author']);
    }
}
