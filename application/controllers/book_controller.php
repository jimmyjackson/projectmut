<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Book_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("book_model");
    }
    public function index()
    {
        $this->load->view("header");
        $this->load->view("booklist");
        $this->load->view("footter");
    }
    public function book($idbook='0')
    {
        if($idbook!='0'){
            $this->load->view("header");
            if($this->book_model->selectbook_where_id_model($idbook)){
                $data['idbook']=$idbook;
                $this->load->view("booked",$data);
            }
            else{
                $this->load->view("nodata");
            }
            $this->load->view("footter");

        }
        else{
            $this->load->view("header");
            $this->load->view("book");
            $this->load->view("footter");
        }
    }

    private function base64_to_jpeg($base64_string, $output_file)
    {
        $ifp = fopen("../CI/".$output_file, "wb");

        $data = explode(',', $base64_string);

        fwrite($ifp, base64_decode($data[1]));
        fclose($ifp);
    }

    private function change_filename($filename)
    {
        if($filename == null || $filename == "")
            return "upload/demoUpload.jpg";
        else
            return "upload/".date("YmdHis").'_'.$filename;

    }

    public function delete_image_book($id,$image)
    {
        if($id !== 0 && $image === null)
            $oldnameimage = $this->book_model->get_image_book_model($id)->book_image;
        else
            $oldnameimage = $image;

        if($oldnameimage != "demoUpload.jpg")
            unlink('../CI/upload/'.$oldnameimage);
    }

    private function loop_auto_id($autoid)
    {
        $autoidbook = "";

        foreach ($autoid as $value)
            foreach ($value as $subvalue)
                foreach ($subvalue as $subvalue2)
                    $autoidbook = $subvalue2;

        return $autoidbook;
    }

    public function save_book()
    {
        $json = json_encode($this->input->post('objbook'));
        list($obj) = json_decode($json);

        $status="";

        $filenameup = $this->change_filename($obj->filename);


        if($obj->formdata->id == 'Auto_Id'){
            $status = $this->addbook($filenameup,$obj);

            if($filenameup != "upload/demoUpload.jpg")
                $this->base64_to_jpeg($obj->fileimage,$filenameup);

        }else{
            $id = $obj->formdata->id;
            $status = $this->editbook($id,$filenameup,$obj);

            if($filenameup != "upload/demoUpload.jpg")
                $this->base64_to_jpeg($obj->fileimage,$filenameup);

        }
        echo $status;
    }

    private function addbook($filenameup,$obj)
    {
        $this->db->trans_begin();

        $autoid['autoid'] = $this->book_model->autoidbook_model();

        $autoidbook = $this->loop_auto_id($autoid);

        $result = $this->book_model->addbook_model(array(
            'id'=>$autoidbook,
            'name'=>$obj->formdata->name,
            'size'=>$obj->formdata->size,
            'page'=>$obj->formdata->page,
            'image'=>"./../".$filenameup,
            'price'=>$obj->formdata->price,
            'isbn'=>$obj->formdata->isbn,
            'status'=>'1',
            'mubook'=>$obj->formdata->category,
            'submubook'=>$obj->formdata->subcategory,
            'publishing'=>$obj->formdata->publishing
        ));

        $this->book_model->add_pricebook_model($obj->formdata->priceupdate,
                                               $autoidbook,
                                               $obj->formdata->publishing);

        if($obj->translators[0]!="none")
            $this->book_model->add_translatorbook_model($autoidbook,$obj->translators);

        $this->book_model->add_titlebook_model($autoidbook,$obj->titles);

        $this->book_model->add_authorbook_model($autoidbook,$obj->authors);


        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            echo false;
        }
        else
        {
            $this->db->trans_commit();
            echo true;
        }
    }

    private function editbook($idbook,$filenameup,$obj)
    {
        $this->db->trans_begin();

        $nameimage = "";

        if($filenameup == "upload/demoUpload.jpg")
        {
            $nameimage = $this->book_model->get_image_book_model($idbook)->book_image;
        }
        else{
            $this->delete_image_book(
                $idbook,
                $this->book_model->get_image_book_model($idbook)->book_image
            );
            $nameimage = substr($filenameup,7);
        }

        $this->book_model->editbook_model(array(
            'name'=>$obj->formdata->name,
            'size'=>$obj->formdata->size,
            'page'=>$obj->formdata->page,
            'image'=>'./../upload/'.$nameimage,
            'price'=>$obj->formdata->price,
            'isbn'=>$obj->formdata->isbn,
            'status'=>'1',
            'mubook'=>$obj->formdata->category,
            'submubook'=>$obj->formdata->subcategory,
            'publishing'=>$obj->formdata->publishing,
            'id'=>$idbook
        ));

        $this->book_model->add_pricebook_model($obj->formdata->priceupdate,
                                               $idbook,
                                               $obj->formdata->publishing);

        $this->book_model->del_translatorbook_model($idbook);

        $this->book_model->del_titlebook_model($idbook);

        $this->book_model->del_authorbook_model($idbook);

        if($obj->translators[0]!="none")
            $this->book_model->add_translatorbook_model($idbook,$obj->translators);

        $this->book_model->add_titlebook_model($idbook,$obj->titles);

        $this->book_model->add_authorbook_model($idbook,$obj->authors);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            echo false;
        }
        else
        {
            $this->db->trans_commit();
            echo true;
        }
    }

    public function deletebook()
    {
        $bookid=$this->input->post('idbook');
        $data=$this->book_model->deletebook_model($bookid);

        if($data['status'] == false)
            $this->delete_image_book(0,$data['image']);
    }

    public function selectdatabook($idbook='')
    {
        $data=$this->book_model->selectdatabook_model($idbook);
        echo json_encode($data);
    }

    public function selectbook()
    {
        $offset=$this->input->post('offsetsend');
        $data['bookselect']=$this->book_model->selectbook_model($offset);
        echo json_encode($data);
    }

    public function getbook()
    {
        $id_publishing=$this->input->post('id_publishing');
        $data['books']=$this->book_model->get_book_model($id_publishing);
        echo json_encode($data);
    }

    public function get_book_and_price()
    {
        $json = json_encode($this->input->post('obj'));
        list($obj) = json_decode($json);
        $data['books']=$this->book_model->get_book_and_price_model($obj->id_book,$obj->id_publishing);
        echo json_encode($data);
    }

    public function countbook()
    {
        echo json_encode($this->book_model->countbook_model());
    }

    public function searchbook()
    {
        $json = json_encode($this->input->post('datasearch'));
        $obj= json_decode($json);

        $datasearch=$this->book_model->searchbook_model(array(
            'inputsearch'=>$obj->inputsearch,'offset'=>$obj->offset,
            'selectmessage'=>$obj->selectmessage,'selectshowtype'=>$obj->selectshowtype
        ));

        echo json_encode($datasearch);
    }

    public function countsearchbook()
    {
        $json = json_encode($this->input->post('datasearch'));
        $obj= json_decode($json);

        $datasearch=$this->book_model->countsearchbook_model(array(
            'inputsearch'=>$obj->inputsearch,'selectmessage'=>$obj->selectmessage,
            'selectshowtype'=>$obj->selectshowtype
        ));

        echo json_encode($datasearch);
    }
}
