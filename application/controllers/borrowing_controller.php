<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Borrowing_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("receiptorder_model");
        $this->load->model("borrowing_model");
        $this->load->library('session');
        $this->load->helper('url');
    }
    public function index()
    {
        $dataPublishing = $this->receiptorder_model->selectpublishing();

        $dataBooking = '';
        $dataBorrowing = '';
        $dataMember = '';
        $sendError = '';

        if(!empty($_POST['databookcopy_id']))
          $data_idbookcopy = $_POST['databookcopy_id'];
        else
          $data_idbookcopy = array();

          if(!empty($_POST['type']) && $_POST['type']=='save'){
            $arrDataBorrowing = $this->session->userdata('borrowing');
            $checkdatastatus = $this->borrowing_model->checkstatusbookcopy($arrDataBorrowing[$_POST['member']]);

            if($checkdatastatus->countbookcopy != 0)
              $sendError = 'datastatus';
            else {
              $this->borrowing_model->save($_POST);
              $sendError = 'save';
              $this->session->set_userdata(array('borrowing'=>''));
              $this->session->set_userdata(array('borrowingMember'=>''));
            }
          }elseif(isset($_POST['delete_id'])){
              $dataBooking = $this->borrowing_model->getbook($_POST);
              $dataMember = $this->borrowing_model->member($_POST);
              $dataBorrowing = $this->borrowing_model->borrowing($_POST,$dataMember[0]->membertype_date);

              $arrDataBorrowing = $this->session->userdata('borrowing');
              $arrBorrowing = "";

              if(!empty($arrDataBorrowing[$_POST['member']])){
                  foreach ($arrDataBorrowing[$_POST['member']] as $val){
                      if($_POST['delete_id'] != $val['bookcopy_id']){
                          $arrBorrowing[$_POST['member']][] = [
                              'bookcopy_id'=>$val['bookcopy_id'],
                              'bookcopy_callno'=>$val['bookcopy_callno'],
                              'book_name'=>$val['book_name'],
                              'date'=>$val['date']
                          ];
                      }
                  }
              }
              $this->session->set_userdata(array('borrowing'=>$arrBorrowing));
              $this->session->set_userdata(array('borrowingMember'=>$_POST['member']));

          }elseif(!empty($_POST['bookcopy'])){
              $arrBorrowing = array();
              $dataBooking = $this->borrowing_model->getbook($_POST);
              $dataMember = $this->borrowing_model->member($_POST);
              $dataBorrowing = $this->borrowing_model->borrowing($_POST,$dataMember[0]->membertype_date);

              foreach ($dataBorrowing as $value) {
                $data_idbookcopy[] = $value->id_bookcopy;
              }

              foreach ($_POST['bookcopy'] as $val) {
                $data_idbookcopy[] = $val;
              }

              $dataduplicate = $this->borrowing_model->checkduplicate($data_idbookcopy);

                if(count($dataduplicate) < count($data_idbookcopy))
                  $sendError = 'duplicate';
                else {
                  $countBorrowing = count($dataBorrowing);

                  $arrDataBorrowing = $this->session->userdata('borrowing');
                  if(!empty($arrDataBorrowing[$_POST['member']])){
                      foreach ($arrDataBorrowing[$_POST['member']] as $val){
                          $arrBorrowing[$_POST['member']][] = [
                              'bookcopy_id'=>$val['bookcopy_id'],
                              'bookcopy_callno'=>$val['bookcopy_callno'],
                              'book_name'=>$val['book_name'],
                              'date'=>$val['date']
                          ];
                      }
                  }

                  foreach ($_POST['bookcopy'] as $val){
                      $dataBook = $this->borrowing_model->getdatabook($val,$dataMember[0]->membertype_date);
                      $arrBorrowing[$_POST['member']][] = [
                          'bookcopy_id'=>$dataBook[0]->bookcopy_id,
                          'bookcopy_callno'=>$dataBook[0]->bookcopy_callno,
                          'book_name'=>$dataBook[0]->book_name,
                          'date'=>$dataBook[0]->datereturn
                      ];
                  }

                  $checkTotal = $countBorrowing + count($arrBorrowing[$_POST['member']]);

                  if($checkTotal > $dataMember[0]->membertype_unit){
                      $sendError = 'unit';
                  }else{
                      $this->session->set_userdata(array('borrowing'=>$arrBorrowing));
                      $this->session->set_userdata(array('borrowingMember'=>$_POST['member']));
                  }
                }
          }elseif(isset($_POST['btadddetial'])){
              $arrDataBorrowingMember = $this->session->userdata('borrowingMember');
              if(!empty($arrDataBorrowingMember)){
                  if($arrDataBorrowingMember != $_POST['member']){
                      $this->session->set_userdata(array('borrowing'=>''));
                      $this->session->set_userdata(array('borrowingMember'=>$_POST['member']));
                  }
              }else{
                  $this->session->set_userdata(array('borrowingMember'=>$_POST['member']));
              }

              $dataBooking = $this->borrowing_model->getbook($_POST);
              $dataMember = $this->borrowing_model->member($_POST);
              $dataBorrowing = $this->borrowing_model->borrowing($_POST,$dataMember[0]->membertype_date);
          }else{
              $this->session->set_userdata(array('borrowing'=>''));
              $this->session->set_userdata(array('borrowingMember'=>''));
          }
        $this->load->view("header");
        $this->load->view("borrowing",[
            'dataPublishing'=>$dataPublishing,
            'dataBooking'=>$dataBooking,
            'dataBorrowing'=>$dataBorrowing,
            'dataMember'=>$dataMember,
            'sendError'=>$sendError
        ]);
        $this->load->view("footter");
    }
//     redirect('/login/form/', 'refresh');



}
