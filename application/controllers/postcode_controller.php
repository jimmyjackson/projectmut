<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Postcode_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 
    }
    
    public function index()
    {
        $this->load->view("header");
        $this->load->view("postcode");
        $this->load->view("footter");
    }
    public function addpostcode()
    {
      
    }
    public function editpostcode()
    {
        
    }
    public function deletepostcode()
    {
        
    }
    public function selectallpostcode()
    {
        $this->load->model("postcode_model");
        $json = json_encode($this->input->post('objdata'));
        list($obj) = json_decode($json); 
        $data['postcode']=$this->postcode_model->selectallpostcode_model(array(
        'idprovince'=>$obj->idprovince,'idzone'=>$obj->idzone,'iddistrict'=>$obj->iddistrict)); 
        echo json_encode($data);
    }
    public function selectpostcode()
    {
        
    }
}
