<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mubook_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 
    }
    
    public function selectmubook()
    {
        $this->load->model("mubook_model");
        $data['mubook']=$this->mubook_model->selectallmubook_model();    
        echo json_encode($data);
    }

}
