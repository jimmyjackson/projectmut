<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout_Emp_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 
        $this->load->library('session');
    }
    
    public function index()
    {
        
    }
    public function logout()
    {
        $this->session->sess_destroy();
       
        redirect('/');
    }
}
