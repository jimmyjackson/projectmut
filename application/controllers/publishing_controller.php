<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Publishing_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 
    }
    
    public function selectpublishing()
    {
        $this->load->model("publishing_model");
        $data['publishing']=$this->publishing_model->selectallpublishing_model();    
        echo json_encode($data);
    }

}
