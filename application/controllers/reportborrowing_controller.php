<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportborrowing_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("reportborrowing_model");
        $this->load->library("mpdf/mpdf");
    }

    public function index()
    {
        $data = $this->reportborrowing_model->getYear();

        $this->load->view("header");
        $this->load->view("reportborrowing",['year'=>$data]);
        $this->load->view("footter");
    }

    public function selectreport($year = 0)
    {
      if($year != 0) {
        $vYear = $year-543;

        $data = $this->reportborrowing_model->selectreport_model($vYear);

        $html .= "<link rel='stylesheet' href='./assets/plugins/bootstrap/css/bootstrap.css' />";
        $html .= "<div class='section'><div class='container'><div class='row'><div class='col-md-12 text-center'><h1 contenteditable='true'>รายงานการยืมหนังสือรายปี</h1>";
        $html .= "<table class='table table-bordered'><thead><tr><td height='28' class='col-md-2'>วันที่ออกรายงาน :</td><td height='28' class='col-md-4'>".$data[0]->cur_date."</td><td height='28' class='col-md-2'>รหัสพนักงาน :</td><td height='28' class='col-md-4'>".$this->session->userdata('idemp')."</td></tr></thead>";
        $html .= "<tbody><tr><td height='28' class='col-md-2'>ปีการศึกษา(พ.ศ) :</td><td height='28' class='col-md-4'>".$year."</td><td height='28' class='col-md-2'>ปีการศึกษา(ค.ศ) :</td><td height='28' class='col-md-4'>".$vYear."</td></tr></tbody></table>";
        $html .= "<table class='table table-bordered'>";
        $html .= "<thead><tr><th height='28'><center>รหัสหนังสือ</center></th><th height='28'><center>ชื่อหนังสือ</center></th><th height='28'><center>ม.ค.</center></th><th height='28'><center>ก.พ.</center></th><th height='28'><center>มี.ค.</center></th><th height='28'><center>เม.ย.</center></th>";
        $html .= "<th height='28'><center>พ.ค.</center></th><th height='28'><center>ม.ย.</center></th><th height='28'><center>ก.ค.</center></th><th height='28'><center>ส.ค.</center></th><th height='28'><center>ก.ย.</center></th><th height='28'><center>ต.ค.</center></th><th height='28'><center>พ.ย.</center></th><th height='28'><center>ธ.ค.</center></th>";
        $html .= "</tr></thead>";
        $html .= "<tbody>";
        foreach ($data as $value) {
          $html .= "<tr><td height='28'><center>".$value->book_id."</center></td><td height='28'><center>".$value->book_name."</center></td>";
          $html .= "<td height='28'><center>".$value->count_m01."</center></td><td height='28'><center>".$value->count_m02."</center></td>";
          $html .= "<td height='28'><center>".$value->count_m03."</center></td><td height='28'><center>".$value->count_m04."</center></td>";
          $html .= "<td height='28'><center>".$value->count_m05."</center></td><td height='28'><center>".$value->count_m06."</center></td>";
          $html .= "<td height='28'><center>".$value->count_m07."</center></td><td height='28'><center>".$value->count_m08."</center></td>";
          $html .= "<td height='28'><center>".$value->count_m09."</center></td><td height='28'><center>".$value->count_m10."</center></td>";
          $html .= "<td height='28'><center>".$value->count_m11."</center></td><td height='28'><center>".$value->count_m12."</center></td></tr>";
        }
        $html .= "</tbody></table></div></div>";
        $html .= "</div></div>";

        $this->mpdf = new mPDF('th', 'A4');
        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
      } else {
        echo "กรุณากรอกวันที่เริ่มต้นและวันที่สิ้นสุด";
      }
    }

}
