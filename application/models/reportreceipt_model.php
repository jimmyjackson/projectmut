<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportreceipt_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();
    }

    public function selectreport_model($date_from, $date_to)
    {
        $sql = "SELECT CURDATE() AS cur_date,
                b.book_id,
                b.book_name,
                b.book_price,
                pcd.purchaseorderdetial_bookprice,
                SUM(rcd.receiptorderdetial_unit) AS sum_unit,
                SUM(rcd.receiptorderdetial_sumprice) AS sum_price
                FROM receiptorderdetial rcd
                INNER JOIN receiptorder rc ON rcd.id_receiptorder = rc.receiptorder_id
                INNER JOIN purchaseorderdetial pcd ON rcd.id_purchase = pcd.id_purchaseorder AND rcd.no_purchase = pcd.purchaseorderdetial_no
                INNER JOIN book b ON rcd.id_book = b.book_id
                WHERE rc.receiptorder_date BETWEEN '".$date_from."' AND '".$date_to."'
                GROUP BY b.book_id,pcd.purchaseorderdetial_bookprice";

        return $this->db->query($sql)->result();
    }

    public function selectreporttotal_model($date_from, $date_to)
    {
        $sql = "SELECT
                  SUM(rcd.receiptorderdetial_sumprice) AS total,
                  ((SUM(rcd.receiptorderdetial_sumprice)*7)/100) AS vat,
                  (SUM(rcd.receiptorderdetial_sumprice)+((SUM(rcd.receiptorderdetial_sumprice)*7)/100)) AS subtotal
                FROM receiptorderdetial rcd
                INNER JOIN receiptorder rc ON rcd.id_receiptorder = rc.receiptorder_id
                INNER JOIN purchaseorderdetial pcd ON rcd.id_purchase = pcd.id_purchaseorder AND rcd.no_purchase = pcd.purchaseorderdetial_no
                INNER JOIN book b ON rcd.id_book = b.book_id
                WHERE rc.receiptorder_date BETWEEN '".$date_from."' AND '".$date_to."'";

        return $this->db->query($sql)->result();
    }
}
