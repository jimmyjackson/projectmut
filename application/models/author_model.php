<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Author_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();   
    }

    public function selectallauthor_model()
    {
        $sql = "SELECT author_id AS id,CONCAT(author_name,' ',author_lname) AS name
        FROM author
        WHERE author_status='1'"; 
        
        return $this->db->query($sql)->result();
    }
    
    public function selectallauthorbypname_model()
    {
        $sql = "SELECT author_id AS id,author_pname AS name
        FROM author
        WHERE author_status='1'"; 
        
        return $this->db->query($sql)->result();
    }

}
