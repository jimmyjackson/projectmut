<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Yearstudy_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_year_model()
    {
        $sqlselect="SELECT CONCAT(yearstudy,'/',semester) AS year_semester
        FROM yearstudy
        ORDER BY yearstudy DESC,semester DESC";

        $queryselect = $this->db->query($sqlselect)->result();

        return $queryselect;
    }

    public function get_last_semester_and_year_model()
    {
        $sqlselect="SELECT yearstudy,MAX(semester) AS semester,expiration_date
        FROM yearstudy
        WHERE yearstudy IN (SELECT MAX(yearstudy) FROM yearstudy)
        AND expiration_date > NOW()";

        $queryselect = $this->db->query($sqlselect)->row();

        return $queryselect;
    }

}
