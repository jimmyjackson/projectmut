<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Book_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();
    }

    public function autoidbook_model()
    {
        $sqlautoid = "SELECT CONVERT(IFNULL(CONCAT('BOOK',LPAD(SUBSTRING(MAX(book_id),5,6)+1,6,'0')),'BOOK000001') USING utf8) AS autoid FROM book";

        return $this->db->query($sqlautoid)->result();
    }

    public function get_image_book_model($id)
    {
        $sql = "SELECT SUBSTRING(book_image,13) AS book_image
        FROM book
        WHERE book_id = ?";

        return $this->db->query($sql,$id)->row();
    }

    public function deletebook_model($idbook='')
    {
        $result['status']=false;
        $result['image']=$this->get_image_book_model($idbook)->book_image;
        $sqldelete = "DELETE FROM book WHERE book_id=?";
        $result['data']=$this->db->query($sqldelete,$idbook);
        if(!$result['data'])
        {
            $sqldelete = "UPDATE book SET book_status = '0' WHERE book_id=?";
            $this->db->query($sqldelete,$idbook);
            $result['status']=true;
        }

        return $result;
    }

    public function editbook_model($ar=array())
    {
        $sqledit = "UPDATE book SET book_name=?,
        book_size=?,book_page=?,book_image=?,book_price=?, book_isbn=?,book_status=?,no_mubook=?,no_submubook=?,
        id_publishing=?
        WHERE book_id=?";

        $this->db->query($sqledit,$ar);
    }

    public function addbook_model($ar=array())
    {
        $sqladd = "INSERT INTO book
                (book_id, book_name, book_size, book_page,
                book_image, book_price, book_isbn,
                book_status, no_mubook, no_submubook, id_publishing)
                VALUES(?,?,?,?,?,?,?,?,?,?,?)";

        $this->db->query($sqladd,$ar);
    }

    public function selectbook_where_id_model($idbook)
    {
        $sqlselect="SELECT book_id
                    FROM book
                    WHERE book_id=? AND book_status='1'";
        $queryselect = $this->db->query($sqlselect,$idbook)->result();

        return $queryselect!=null?true:false;
    }

    public function selectdatabook_model($idbook)
    {
        $sqlselectdatabook = "SELECT b.book_id,b.book_name,b.book_size,b.book_page,
                b.book_image,b.book_price,b.book_isbn,pb.pricebook_price,
                b.id_publishing,b.no_mubook,b.no_submubook,ab.id_author,
                author_.name AS author_name,tb.id_title,title_.name AS title_name,
                ttb.id_translator,translator_.name AS translator_name
                FROM book b
                INNER JOIN pricebook pb ON b.book_id = pb.id_book
                INNER JOIN authorbook ab ON b.book_id = ab.id_book
                INNER JOIN titlebook  tb ON b.book_id = tb.id_book
                LEFT JOIN translatorbook ttb ON b.book_id = ttb.id_book
                INNER JOIN (SELECT title_id AS id,title_name AS NAME
                FROM title) title_ ON tb.id_title = title_.id
                INNER JOIN (SELECT author_id AS id,CONCAT(author_name,' ',author_lname) AS NAME
                FROM author
                WHERE author_status='1') author_ ON ab.id_author = author_.id
                LEFT JOIN (SELECT author_id AS id,CONCAT(author_name,' ',author_lname) AS NAME
                FROM author
                WHERE author_status='1') translator_ ON ttb.id_translator = translator_.id
                WHERE b.book_id =? AND pricebook_date = (
                SELECT MAX(pricebook_date)
                FROM pricebook
                WHERE id_book =? AND id_publishing = b.id_publishing
                ) AND b.book_status = '1'";
        return $this->db->query($sqlselectdatabook,array($idbook,$idbook))->result();
    }

    public function add_translatorbook_model($id,$ar=array()){
        foreach($ar as $value){
            $sql="insert into translatorbook (id_book, id_translator) values(?,?)";
            $this->db->query($sql,array('id'=>$id,'translator'=>$value));
        }
    }

    public function del_translatorbook_model($idbook){
        $sql="DELETE FROM translatorbook WHERE id_book = ?";
        $this->db->query($sql,$idbook);
    }

    public function get_translatorbook_model($idbook){
        $sql="SELECT id_translator FROM translatorbook WHERE id_book = ?";
        $this->db->query($sql,$idbook);
    }

    public function add_titlebook_model($id,$ar=array()){
        foreach($ar as $value){
            $sql="insert into titlebook (id_book, id_title) values(?,?)";
            $this->db->query($sql,array('id'=>$id,'title'=>$value));
        }
    }

    public function del_titlebook_model($idbook){
        $sql="DELETE FROM titlebook WHERE id_book = ?";
        $this->db->query($sql,$idbook);
    }

    public function get_titlebook_model($idbook){
        $sql="SELECT id_title FROM titlebook WHERE id_book = ?";
        $this->db->query($sql,$idbook);
    }

    public function add_authorbook_model($id,$ar=array()){
        foreach($ar as $value){
            $sql="insert into authorbook (id_book, id_author) values(?,?)";
            $this->db->query($sql,array('id'=>$id,'author'=>$value));
        }
    }

    public function del_authorbook_model($idbook){
        $sql="DELETE FROM authorbook WHERE id_book = ?";
        $this->db->query($sql,$idbook);
    }

    public function get_authorbook_model($idbook){
        $sql="SELECT id_author FROM authorbook WHERE id_book = ?";
        $this->db->query($sql,$idbook);
    }

    public function add_pricebook_model($price,$id_book,$id_publishing){
        $sql="insert into pricebook
        (pricebook_date, pricebook_price, id_book, id_publishing) values(NOW(),?,?,?)";
        $this->db->query($sql,array('price'=>$price,
                                    'id_book'=>$id_book,
                                    'id_publishing'=>$id_publishing));
    }

    public function get_pricebook_model($idbook,$idpublishing){
        $sql="SELECT MAX(pricebook_date) AS max_date,pricebook_price
            FROM pricebook
            WHERE id_book = ? AND id_publishing = ?";
        $this->db->query($sql,array('idbook'=>$idbook,'idpublishing'=>$idpublishing));
    }

    public function selectbook_model($offset=0)
    {
        $sqlselect = "SELECT b.book_id,b.book_name,b.book_isbn,p.publishing_name,
                    b.no_mubook,b.no_submubook,t.title_name,
                    CONCAT(a.author_name,' ',a.author_lname) AS author_name_n_lname
                    FROM book b
                    INNER JOIN publishing p ON b.id_publishing = p.publishing_id
                    INNER JOIN titlebook tb ON b.book_id = tb.id_book
                    INNER JOIN title t ON tb.id_title = t.title_id
                    INNER JOIN authorbook ab ON b.book_id = ab.id_book
                    INNER JOIN author a ON ab.id_author = a.author_id
                    WHERE b.book_status = '1'
                    GROUP BY b.book_id ORDER BY b.book_id
                    LIMIT ".$offset.",10";
        return $this->db->query($sqlselect)->result();
    }

    public function countbook_model()
    {
        $sqlcount = "SELECT COUNT(1) AS count_book
        FROM book
        WHERE book_status='1'";
        return $this->db->query($sqlcount)->row();
    }

    public function get_book_model($id_publishing)
    {
        $sqlcount = "SELECT book_id,book_name
        FROM book
        WHERE book_status='1' AND id_publishing = ?";
        return $this->db->query($sqlcount,$id_publishing)->result();
    }

    public function get_book_and_price_model($id_book,$id_publishing)
    {
        $sqlcount = "SELECT b.book_id,b.book_name,pb.pricebook_price,pb.pricebook_date
        FROM book b INNER JOIN pricebook pb ON  b.book_id=pb.id_book
        WHERE b.book_status='1' AND pb.pricebook_date = (
        SELECT MAX(pricebook_date)
        FROM pricebook
        WHERE id_book=? AND id_publishing = ?)";
        return $this->db->query($sqlcount,array($id_book,$id_publishing))->result();
    }

    public function searchbook_model($arr=array())
    {
        $i=0;

        $sqlselect = "SELECT b.book_id,b.book_name,b.book_isbn,p.publishing_name,
                    b.no_mubook,b.no_submubook,t.title_name,
                    CONCAT(a.author_name,' ',a.author_lname) AS author_name_n_lname
                    FROM book b
                    INNER JOIN publishing p ON b.id_publishing = p.publishing_id
                    INNER JOIN titlebook tb ON b.book_id = tb.id_book
                    INNER JOIN title t ON tb.id_title = t.title_id
                    INNER JOIN authorbook ab ON b.book_id = ab.id_book
                    INNER JOIN author a ON ab.id_author = a.author_id
                    WHERE ";

        foreach ($arr['selectmessage'] as $value)
        {
            if($value == 'id')
                $sqlselect .= "b.book_id LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'name')
                $sqlselect .= "b.book_name LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'isbn')
                $sqlselect .= "b.book_isbn LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'publishing')
                $sqlselect .= "p.publishing_name LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'mubook')
                $sqlselect .= "b.no_mubook LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'submubook')
                $sqlselect .= "b.no_submubook LIKE '%".$arr['inputsearch']."%'";
            else
                $sqlselect .= "b.book_id LIKE '%".$arr['inputsearch']."%' OR b.book_name LIKE '%".$arr['inputsearch']."%' OR b.book_isbn LIKE '%".$arr['inputsearch']."%'";

            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= ' AND ';
            else
                $sqlselect .= ' OR ';

            $i++;
        }

        $sqlselect .= "b.book_status = '1' GROUP BY b.book_id ORDER BY b.book_id LIMIT ".$arr['offset'].",10";

        return $this->db->query($sqlselect)->result();
    }

    public function countsearchbook_model($arr=array())
    {
        $i=0;

        $sqlselect = "SELECT COUNT(1) AS count
                    FROM book b
                    INNER JOIN publishing p ON b.id_publishing = p.publishing_id
                    WHERE ";

        foreach ($arr['selectmessage'] as $value)
        {
            if($value == 'id')
                $sqlselect .= "b.book_id LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'name')
                $sqlselect .= "b.book_name LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'isbn')
                $sqlselect .= "b.book_isbn LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'publishing')
                $sqlselect .= "p.publishing_name LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'mubook')
                $sqlselect .= "b.no_mubook LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'submubook')
                $sqlselect .= "b.no_submubook LIKE '%".$arr['inputsearch']."%'";
            else
                $sqlselect .= "b.book_id LIKE '%".$arr['inputsearch']."%' OR b.book_name LIKE '%".$arr['inputsearch']."%' OR b.book_isbn LIKE '%".$arr['inputsearch']."%'";

            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= ' AND ';
            else
                $sqlselect .= ' OR ';

            $i++;
        }

        $sqlselect .= "b.book_status = '1' GROUP BY b.book_id ORDER BY b.book_id";

        return $this->db->query($sqlselect)->result();
    }
}
