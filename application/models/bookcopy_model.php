<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bookcopy_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();
    }

    public function selectreceiptorderdetial($param)
    {
        $sqlselect = "SELECT
                        a.*,b.book_name,c.publishing_name,d.*
                        FROM receiptorderdetial a
                        LEFT JOIN book b ON a.id_book=b.book_id
                        LEFT JOIN publishing c ON b.id_publishing=c.publishing_id
                        LEFT JOIN receiptorder d ON a.id_receiptorder=d.receiptorder_id
                        WHERE a.is_bookcopy='0'
                        ";
        if(!empty($param['receiptorder_id'])){
            $arr = explode('_', $param['receiptorder_id']);
            $sqlselect .= 'AND id_receiptorder="'.$arr[0].'" AND receiptorderdetial_no="'.$arr[1].'"';
        }

        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }

    public function selectdatabookcopy($idbookcopy)
    {
        $sqlselectdatabook = "SELECT b.book_id,b.book_name,b.book_size,b.book_page,
        b.book_image,b.book_price,b.book_isbn,
        p.publishing_name,mb.mubook_name,smb.submubook_name,
        bc.bookcopy_id,bc.bookcopy_callno,bc.bookcopy_status,
        CONCAT(ptb.positionbook_tutee,':',ptb.positionbook_chantee,':',ptb.positionbook_chongtee) AS positionbook,
        t.title_name,CONCAT(pyb.id_publicationyear,'/',pyb.edition_publicationyear) AS edition,
        CONCAT(a.author_name,' ',a.author_lname) AS author,CONCAT(aa.author_name,' ',aa.author_lname) AS translator
        FROM book b
        INNER JOIN bookcopy bc ON b.book_id = bc.id_book
        INNER JOIN publishing p ON b.id_publishing = p.publishing_id
        LEFT JOIN mubook mb ON b.no_mubook = mb.mubook_no
        LEFT JOIN submubook smb ON b.no_submubook = smb.submubook_no
        INNER JOIN positionbook ptb ON bc.bookcopy_id = ptb.bookcopy_id
        INNER JOIN titlebook tb ON b.book_id = tb.id_book
        INNER JOIN title t ON tb.id_title = t.title_id
        INNER JOIN publicationyearbook pyb ON bc.bookcopy_id = pyb.id_bookcopy
        LEFT JOIN authorbook ab ON b.book_id = ab.id_book
        LEFT JOIN author a ON ab.id_author = a.author_id
        LEFT JOIN translatorbook ttb ON b.book_id = ttb.id_book
        LEFT JOIN author aa ON ttb.id_translator = aa.author_id
        WHERE bc.bookcopy_id = ?";
        return $this->db->query($sqlselectdatabook,$idbookcopy)->result();
    }

    public function selectbookcopy_model($offset=0)
    {
        $sqlselectdatabook = "SELECT b.book_id,b.book_name,b.book_size,b.book_page,
        b.book_image,b.book_price,b.book_isbn,
        p.publishing_name,b.no_mubook,b.no_submubook,
        bc.bookcopy_id,bc.bookcopy_callno,bc.bookcopy_status,
        CONCAT(ptb.positionbook_tutee,':',ptb.positionbook_chantee,':',ptb.positionbook_chongtee) AS positionbook,
        t.title_name,CONCAT(pyb.id_publicationyear,'/',pyb.edition_publicationyear) AS edition,
        CONCAT(a.author_name,' ',a.author_lname) AS author,CONCAT(aa.author_name,' ',aa.author_lname) AS translator
        FROM book b
        INNER JOIN bookcopy bc ON b.book_id = bc.id_book
        INNER JOIN publishing p ON b.id_publishing = p.publishing_id
        INNER JOIN positionbook ptb ON bc.bookcopy_id = ptb.bookcopy_id
        INNER JOIN titlebook tb ON b.book_id = tb.id_book
        INNER JOIN title t ON tb.id_title = t.title_id
        INNER JOIN publicationyearbook pyb ON bc.bookcopy_id = pyb.id_bookcopy
        LEFT JOIN authorbook ab ON b.book_id = ab.id_book
        LEFT JOIN author a ON ab.id_author = a.author_id
        LEFT JOIN translatorbook ttb ON b.book_id = ttb.id_book
        LEFT JOIN author aa ON ttb.id_translator = aa.author_id
        LIMIT ".$offset.",10";

        return $this->db->query($sqlselectdatabook)->result();
    }

    public function countbookcopy_model()
    {
        $sqlselectdatabook = "SELECT COUNT(1) AS count FROM bookcopy";
        return $this->db->query($sqlselectdatabook)->row();
    }

    public function searchbookcopy_model($arr=array())
    {
        $i=0;

        $sqlselect = "SELECT b.book_id,b.book_name,b.book_size,b.book_page,
                      b.book_image,b.book_price,b.book_isbn,
                      p.publishing_name,b.no_mubook,b.no_submubook,
                      bc.bookcopy_id,bc.bookcopy_callno,bc.bookcopy_status,
                      CONCAT(ptb.positionbook_tutee,':',ptb.positionbook_chantee,':',ptb.positionbook_chongtee) AS positionbook,
                      t.title_name,CONCAT(pyb.id_publicationyear,'/',pyb.edition_publicationyear) AS edition,
                      CONCAT(a.author_name,' ',a.author_lname) AS author,CONCAT(aa.author_name,' ',aa.author_lname) AS translator
                      FROM book b
                      INNER JOIN bookcopy bc ON b.book_id = bc.id_book
                      INNER JOIN publishing p ON b.id_publishing = p.publishing_id
                      INNER JOIN positionbook ptb ON bc.bookcopy_id = ptb.bookcopy_id
                      INNER JOIN titlebook tb ON b.book_id = tb.id_book
                      INNER JOIN title t ON tb.id_title = t.title_id
                      INNER JOIN publicationyearbook pyb ON bc.bookcopy_id = pyb.id_bookcopy
                      LEFT JOIN authorbook ab ON b.book_id = ab.id_book
                      LEFT JOIN author a ON ab.id_author = a.author_id
                      LEFT JOIN translatorbook ttb ON b.book_id = ttb.id_book
                      LEFT JOIN author aa ON ttb.id_translator = aa.author_id
                      WHERE ";

        foreach ($arr['selectmessage'] as $value)
        {
            if($value == 'id')
                $sqlselect .= "b.book_id LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'name')
                $sqlselect .= "b.book_name LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'isbn')
                $sqlselect .= "b.book_isbn LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'publishing')
                $sqlselect .= "p.publishing_name LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'idcopy')
                $sqlselect .= "bc.bookcopy_id LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'nocopy')
                $sqlselect .= "bc.bookcopy_callno LIKE '%".$arr['inputsearch']."%'";
            else
                $sqlselect .= "bc.bookcopy_id LIKE '%".$arr['inputsearch']."%' OR bc.bookcopy_callno LIKE '%".$arr['inputsearch']."%' OR b.book_id LIKE '%".$arr['inputsearch']."%' OR b.book_name LIKE '%".$arr['inputsearch']."%' OR b.book_isbn LIKE '%".$arr['inputsearch']."%'";

            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= ' AND ';
            else
                $sqlselect .= ' OR ';

            $i++;
        }

        $sqlselect .= "b.book_status = '1' LIMIT ".$arr['offset'].",10";

        return $this->db->query($sqlselect)->result();
    }

    public function countsearchbookcopy_model($arr=array())
    {
        $i=0;

        $sqlselect = "SELECT COUNT(1) AS count
                    FROM book b
                    INNER JOIN bookcopy bc ON b.book_id = bc.id_book
                    INNER JOIN publishing p ON b.id_publishing = p.publishing_id
                    WHERE ";

        foreach ($arr['selectmessage'] as $value)
        {
          if($value == 'id')
              $sqlselect .= "b.book_id LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'name')
              $sqlselect .= "b.book_name LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'isbn')
              $sqlselect .= "b.book_isbn LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'publishing')
              $sqlselect .= "p.publishing_name LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'idcopy')
              $sqlselect .= "bc.bookcopy_id LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'nocopy')
              $sqlselect .= "bc.bookcopy_callno LIKE '%".$arr['inputsearch']."%'";
          else
              $sqlselect .= "bc.bookcopy_id LIKE '%".$arr['inputsearch']."%' OR bc.bookcopy_callno LIKE '%".$arr['inputsearch']."%' OR b.book_id LIKE '%".$arr['inputsearch']."%' OR b.book_name LIKE '%".$arr['inputsearch']."%' OR b.book_isbn LIKE '%".$arr['inputsearch']."%'";

          if(($i==sizeof($arr['selectmessage'])-1))
              $sqlselect .= ' AND ';
          else
              $sqlselect .= ' OR ';

            $i++;
        }

        $sqlselect .= "b.book_status = '1'";

        return $this->db->query($sqlselect)->result();
    }

    public function selecttitlebookcopybyid_model($id)
    {
        $sqlselectdatabook = "SELECT t.title_name
                              FROM book b
                              INNER JOIN bookcopy bc ON b.book_id = bc.id_book
                              INNER JOIN titlebook tb ON b.book_id = tb.id_book
                              INNER JOIN title t ON tb.id_title = t.title_id
                              WHERE bc.bookcopy_id = ?";

        return $this->db->query($sqlselectdatabook,$id)->result();
    }

    public function selecttranslatorbookcopybyid_model($id)
    {
        $sqlselectdatabook = "SELECT CONCAT(aa.author_name,' ',aa.author_lname) AS translator
                              FROM book b
                              INNER JOIN bookcopy bc ON b.book_id = bc.id_book
                              LEFT JOIN translatorbook ttb ON b.book_id = ttb.id_book
                              LEFT JOIN author aa ON ttb.id_translator = aa.author_id
                               WHERE bc.bookcopy_id = ?";

        return $this->db->query($sqlselectdatabook,$id)->result();
    }

    public function selectauthorbookcopybyid_model($id)
    {
        $sqlselectdatabook = "SELECT CONCAT(a.author_name,' ',a.author_lname) AS author
                              FROM book b
                              INNER JOIN bookcopy bc ON b.book_id = bc.id_book
                              LEFT JOIN authorbook ab ON b.book_id = ab.id_book
                              LEFT JOIN author a ON ab.id_author = a.author_id
                              WHERE bc.bookcopy_id = ?";

        return $this->db->query($sqlselectdatabook,$id)->result();
    }

    public function save($param)
    {

        $arr = explode('_', $param['receiptorder_id']);
        $sqlselect = "SELECT a.*, b.*, c.*
                        FROM receiptorderdetial a
                        LEFT JOIN receiptorder b ON a.id_receiptorder=b.receiptorder_id
                        LEFT JOIN publishing c ON b.id_publishing=c.publishing_id
                    ";
        $sqlselect .= 'WHERE a.id_receiptorder="'.$arr[0].'" AND a.receiptorderdetial_no="'.$arr[1].'"';
        $queryselect = $this->db->query($sqlselect)->result();

        for($i=0;$i<$queryselect[0]->receiptorderdetial_unit;$i++){
            $sqlautoid = "SELECT CONVERT(IFNULL(CONCAT('BCP',LPAD(SUBSTRING(MAX(bookcopy_id),4,7)+1,7,'0')),'BCP0000001') USING utf8) AS autoid FROM bookcopy";
            $autoId = $this->db->query($sqlautoid)->result();
            $sqlBookcopy = "INSERT INTO bookcopy
                  (id_book, bookcopy_id, bookcopy_callno, bookcopy_status,
                    id_receiptorder, receiptorderdetial_no)
                VALUES('".$queryselect[0]->id_book."', '".$autoId[0]->autoid."', '".$autoId[0]->autoid.'_'.$param['bookcopy_callno']."', 'ว่าง',
                '".$arr[0]."', '".$arr[1]."')";
            $this->db->query($sqlBookcopy);

            $sqlautoid2 = "SELECT CONVERT(IFNULL(CONCAT('ORI',LPAD(SUBSTRING(MAX(origin_id),4,7)+1,7,'0')),'ORI0000001') USING utf8) AS autoid FROM origin";
            $autoId2 = $this->db->query($sqlautoid2)->result();
            $sqlOrigin = "INSERT INTO origin
                    (origin_id, bookcopy_id, origin_name, origin_status)
                    VALUES('".$autoId2[0]->autoid."', '".$autoId[0]->autoid."', '".$queryselect[0]->publishing_name."', 'สั่งซื้อ')";
            $this->db->query($sqlOrigin);

            $sqlPositionbook = "INSERT INTO positionbook
                    (bookcopy_id, positionbook_tutee, positionbook_chantee, positionbook_chongtee)
                    VALUES('".$autoId[0]->autoid."', '".$param['positionbook_tutee']."', '".$param['positionbook_chantee']."', '".$param['positionbook_chongtee']."')";
            $this->db->query($sqlPositionbook);

           $sqlPublicationyearbook = "INSERT INTO publicationyearbook
                   (id_book,id_bookcopy, id_publicationyear, edition_publicationyear)
                   VALUES('".$queryselect[0]->id_book."', '".$autoId[0]->autoid."', '".$param['bookcopy_year']."', '".$param['bookcopy_number']."')";
           $this->db->query($sqlPublicationyearbook);
        }

        $sqlUpdate = "UPDATE receiptorderdetial SET
        is_bookcopy = 1
        WHERE id_receiptorder='".$arr[0]."' AND receiptorderdetial_no='".$arr[1]."'";
        $this->db->query($sqlUpdate);

    }

}
