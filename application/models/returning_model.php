<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Returning_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getborrowing($param)
    {
        $sqlselect = "SELECT
                        *
                        FROM borrowing a
                        INNER JOIN borrowingdetial b ON a.borrowing_id=b.id_borrowing
                        INNER JOIN bookcopy c ON b.id_bookcopy=c.bookcopy_id
                        INNER JOIN book d ON d.book_id=c.id_book
                        WHERE b.is_return='0'
                        ";
        if(!empty($param['member'])){
            $sqlselect .= 'AND a.id_member="'.$param['member'].'" ';
        }
        if(!empty($param['bookname'])){
            $sqlselect .= 'AND d.book_name="'.$param['bookname'].'" ';
        }
        if(!empty($param['bookcall'])){
            $sqlselect .= 'AND c.bookcopy_callno="'.$param['bookcall'].'" ';
        }

        $sqlselect .= 'ORDER BY a.borrowing_date ASC';

        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }

    public function member($param)
    {
        $sqlselect = "SELECT
                        *
                        FROM member a
                        INNER JOIN membertype b ON a.id_membertype=b.membertype_id
                        WHERE a.member_status='1' AND a.member_id='".$param."'
                        ";
        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }


    public function save($param)
    {
        $total = 0;

        $sqlautoid = "SELECT CONVERT(IFNULL(CONCAT('RTN',LPAD(SUBSTRING(MAX(returning_id),4,7)+1,7,'0')),'RTN0000001') USING utf8) AS autoid FROM returnings";
        $autoId = $this->db->query($sqlautoid)->result();

        $sqlreturning = "INSERT INTO returnings
              (returning_id, returning_date, id_employee)
            VALUES('".$autoId[0]->autoid."', '".date('Y-m-d')."', '".$this->session->userdata('idemp')."')";
        $this->db->query($sqlreturning);

        $arrayVal  = array();
        $queryselectpricebook;
        $status_bookcopy = '';
        $id_invoice = '';

        foreach ($param['return'] as $key=>$val){

            $arrVal = explode('_', $val);
            $sqlselect = "SELECT
                            *
                            FROM borrowing a
                            INNER JOIN borrowingdetial b ON a.borrowing_id=b.id_borrowing
                            INNER JOIN bookcopy c ON b.id_bookcopy=c.bookcopy_id
                            INNER JOIN book d ON d.book_id=c.id_book
                            WHERE b.id_borrowing='".$arrVal[0]."' AND borrowingdetial_no='".$arrVal[1]."'
                            ";
            $queryselect = $this->db->query($sqlselect)->result();

            $sqlselectMember = "SELECT
                            *
                            FROM member a
                            INNER JOIN membertype b ON a.id_membertype=b.membertype_id
                            WHERE a.member_status='1' AND a.member_id='".$queryselect[0]->id_member."'";
            $queryselectMember = $this->db->query($sqlselectMember)->result();
            $price_book = 0;

            if(!empty($param['lost'][$key]) || !empty($param['fray'][$key]))
            {
              $sqlselectpricebook = "SELECT id_book,bookcopy_id,book_price
                                  FROM book
                                  INNER JOIN bookcopy ON book_id = id_book
                                  WHERE bookcopy_id = '".$queryselect[0]->bookcopy_id."'";
              $queryselectpricebook = $this->db->query($sqlselectpricebook)->row();
              $price_book = $queryselectpricebook->book_price;
            }

            $dStart = new DateTime($queryselect[0]->borrowing_date);
            $dEnd  = new DateTime(date("Y-m-d"));
            $dDiff = $dStart->diff($dEnd);

            $cal = $dDiff->days - $queryselectMember[0]->membertype_date;
            $cal2 = '';
            if($cal > 0 || !empty($param['lost'][$key]) || !empty($param['fray'][$key])){
              if($cal < 0)
              {
                $cal = 0;
              }
                $cal2 = ($queryselectMember[0]->membertype_price * $cal) + $price_book;
                $total = $total + $cal2;
                $arrayVal[] = [$autoId[0]->autoid,($key+1),$cal2];
            }

            $sqlreturning = "INSERT INTO returningdetial
                  (id_returning, returningdetial_no, returningdetial_dateover, returningdetial_status, id_borrowing, no_borrowingdetial)
                VALUES('".$autoId[0]->autoid."', '".($key+1)."', '".($cal < 0 ? 0 : $cal)."', '', '".$arrVal[0]."', '".$arrVal[1]."')";
            $this->db->query($sqlreturning);

            $sqlUpdate = "UPDATE borrowingdetial SET
            borrowingdetial_status = 'คืน', is_return='1'
            WHERE id_borrowing='".$arrVal[0]."' AND borrowingdetial_no='".$arrVal[1]."'";
            $this->db->query($sqlUpdate);

            $status_bookcopy = "ว่าง";

            if(!empty($param['fray'][$key]))
            {
              $status_bookcopy = "ชำรุด";
              $sqlsoldout = "INSERT INTO soldout (id_book,id_bookcopy,soldout_detial,soldout_date,id_employee)
                             VALUES ('".$queryselectpricebook->id_book."','".$queryselectpricebook->bookcopy_id."','".$status_bookcopy."',CURDATE(),'".$this->session->userdata('idemp')."')";
              $this->db->query($sqlsoldout);
            }

            if(!empty($param['lost'][$key]))
            {
              $status_bookcopy = "สูญหาย";
              $sqlsoldout = "INSERT INTO soldout (id_book,id_bookcopy,soldout_detial,soldout_date,id_employee)
                             VALUES ('".$queryselectpricebook->id_book."','".$queryselectpricebook->bookcopy_id."','".$status_bookcopy."',CURDATE(),'".$this->session->userdata('idemp')."')";
              $this->db->query($sqlsoldout);
            }

            $sqlUpdate = "UPDATE bookcopy SET
            bookcopy_status = '".$status_bookcopy."'
            WHERE bookcopy_id='".$queryselect[0]->bookcopy_id."'";
            $this->db->query($sqlUpdate);

            $sqlselectbooking = "SELECT
                            *
                            FROM borrowingbooking
                            WHERE id_borrowing='".$arrVal[0]."' AND no_borrowingdetial='".$arrVal[1]."'
                            ";
            $queryselectbooking = $this->db->query($sqlselectbooking)->result();
            if(count($queryselectbooking) > 0){

                $sqlUpdate = "UPDATE borrowingdetial SET
                borrowingdetial_status = 'คืน', is_return='1'
                WHERE id_borrowing='".$arrVal[0]."' AND borrowingdetial_no='".$arrVal[1]."'";
                $this->db->query($sqlUpdate);
            }
        }

        if(!empty($total)){
            $sqlautoid = "SELECT CONVERT(IFNULL(CONCAT('INV',LPAD(SUBSTRING(MAX(invoice_id),4,7)+1,7,'0')),'INV0000001') USING utf8) AS autoid FROM invoice";
            $autoId2 = $this->db->query($sqlautoid)->result();

            $sqlinvoice = "INSERT INTO invoice
                  (invoice_id, invoice_date, invoice_totalprice, id_employee)
                VALUES('".$autoId2[0]->autoid."', '".date('Y-m-d')."', '".$total."', '".$this->session->userdata('idemp')."')";
            $this->db->query($sqlinvoice);

            foreach ($arrayVal as $key=>$value) {
                $sqlinvoicedetial = "INSERT INTO invoicedetial
                      (id_invoice, invoicedetial_no, invoicedetial_price, id_returning, no_returning)
                    VALUES('".$autoId2[0]->autoid."', '".($key+1)."', '".$value[2]."', '".$value[0]."', '".$value[1]."')";
                $this->db->query($sqlinvoicedetial);
            }

            $id_invoice = $autoId2[0]->autoid;
        }

        return $id_invoice;
    }

    public function selectdatainvoicereport_model($value)
    {
      $sqlselect="SELECT
                  iv.invoice_id,
                  iv.invoice_date,
                  iv.invoice_totalprice,
                  iv.id_employee,
                  ivd.invoicedetial_no,
                  ivd.invoicedetial_price,
                  bid.id_bookcopy,
                  b.book_name
                FROM invoice iv
                INNER JOIN invoicedetial ivd ON iv.invoice_id = ivd.id_invoice
                INNER JOIN returningdetial rtd ON ivd.id_returning = rtd.id_returning AND ivd.no_returning = rtd.returningdetial_no
                INNER JOIN borrowingdetial bid ON rtd.id_borrowing = bid.id_borrowing AND rtd.no_borrowingdetial = bid.borrowingdetial_no
                INNER JOIN book b ON bid.id_book = b.book_id
                WHERE iv.invoice_id = ?";

      return $this->db->query($sqlselect,$value)->result();
    }

    public function checkreturn($id_borrowing)
    {
      $status = false;

      foreach ($id_borrowing['return'] as $key => $value) {
        $arrVal = explode('_', $value);

        $sqlselect="SELECT COUNT(1) AS countreturn
                    FROM borrowingdetial
                    WHERE id_borrowing = '".$arrVal[0]."' AND borrowingdetial_no = '".$arrVal[1]."' AND is_return = '1'";

        $countreturn = $this->db->query($sqlselect)->row();

        if($countreturn->countreturn > 0){
          $status = true;
          break;
        }
      }
      
      return $status;
    }
}
