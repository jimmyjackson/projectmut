<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Borrowing_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getbook($param)
    {
        $sqlselect = "SELECT
                        *
                        FROM bookcopy a
                        LEFT JOIN book b ON a.id_book=b.book_id
                        LEFT JOIN publishing c ON b.id_publishing=c.publishing_id
                        LEFT JOIN receiptorder d ON a.id_receiptorder=d.receiptorder_id
                        WHERE (a.bookcopy_status='ว่าง' OR a.bookcopy_status='ยืม')
                        ";
        if(!empty($param['bookcall'])){
            $sqlselect .= 'AND a.bookcopy_callno LIKE "%'.$param['bookcall'].'%" ';
        }
        if(!empty($param['bookname'])){
            $sqlselect .= 'AND b.book_name LIKE "%'.$param['bookname'].'%" ';
        }
        if(!empty($param['publishing'])){
            $sqlselect .= 'AND b.id_publishing="'.$param['publishing'].'" ';
        }
        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }

    public function getdatabook($param,$dateunit)
    {
        $sqlselect = "SELECT *, DATE_ADD(CURDATE(), INTERVAL ".$dateunit." DAY) AS datereturn
                        FROM bookcopy a
                        LEFT JOIN book b ON a.id_book=b.book_id
                        WHERE a.bookcopy_id='".$param."'
                        ";
        $queryselect = $this->db->query($sqlselect)->result();

        return $queryselect;
    }

    public function borrowing($param, $dateunit)
    {
        $sqlselect = "SELECT *, DATE_ADD(a.borrowing_date, INTERVAL ".$dateunit." DAY) AS datereturn
                        FROM borrowing a
                        INNER JOIN borrowingdetial b ON a.borrowing_id=b.id_borrowing
                        INNER JOIN bookcopy c ON b.id_bookcopy=c.bookcopy_id
                        INNER JOIN book d ON d.book_id=c.id_book
                        WHERE b.is_return='0' AND a.id_member='".$param['member']."'
                        ";
        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }

    public function member($param)
    {
        $sqlselect = "SELECT
                        *
                        FROM member a
                        INNER JOIN membertype b ON a.id_membertype=b.membertype_id
                        WHERE a.member_status='1' AND a.member_id='".$param['member']."'
                        ";
        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }

    public function checkduplicate($param=array())
    {
        $i = 0;
        $sqlselect = "SELECT COUNT(1) AS count FROM bookcopy WHERE bookcopy_id IN (";

        foreach ($param as $va) {
          if($i < (count($param)-1))
            $sqlselect .= "'".$va."',";
          else
            $sqlselect .= "'".$va."')";
          $i++;
        }

        $sqlselect .= " GROUP BY id_book";

        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }

    public function save($param)
    {

        $arrDataBorrowing = $this->session->userdata('borrowing');
        $dataMember = $this->borrowing_model->member($_POST);
        $sqlautoid = "SELECT CONVERT(IFNULL(CONCAT('BOR',LPAD(SUBSTRING(MAX(borrowing_id),4,7)+1,7,'0')),'BOR0000001') USING utf8) AS autoid FROM borrowing";
        $autoId = $this->db->query($sqlautoid)->result();

        $sqlBorrowing = "INSERT INTO borrowing
              (borrowing_id, borrowing_date, id_employee, id_member)
            VALUES('".$autoId[0]->autoid."', CURDATE(), '".$this->session->userdata('idemp')."', '".$param['member']."')";
        $this->db->query($sqlBorrowing);

        $date = $dataMember[0]->membertype_date;
        $dayReturn = date('Y-m-d', strtotime("+$date days", strtotime(date("Y-m-d"))));

        $a = 0;
        foreach ($arrDataBorrowing[$param['member']] as $value) {
            $a++;

            $getdatabook = $this->borrowing_model->getdatabook($value['bookcopy_id'],$dataMember[0]->membertype_date);
            $sqlBorrowingDetail = "INSERT INTO borrowingdetial
              (id_borrowing, borrowingdetial_no, borrowingdetial_datereturn, borrowingdetial_status, id_bookcopy, id_book, is_return)
            VALUES ('".$autoId[0]->autoid."', '".$a."', '".$dayReturn."', 'ยืม', '".$value['bookcopy_id']."', '".$getdatabook[0]->id_book."', '0')";
            $this->db->query($sqlBorrowingDetail);

            $sqlUpdate = "UPDATE bookcopy SET
            bookcopy_status = 'ยืม'
            WHERE bookcopy_id='".$value['bookcopy_id']."'";
            $this->db->query($sqlUpdate);
        }

    }

    public function checkstatusbookcopy($param=array())
    {
      $i = 0;
      $sqlselect = "SELECT COUNT(1) AS countbookcopy
                    FROM bookcopy
                    WHERE bookcopy_id IN (";

      foreach ($param as $va) {
        if($i < (count($param)-1))
          $sqlselect .= "'".$va['bookcopy_id']."',";
        else
          $sqlselect .= "'".$va['bookcopy_id']."')";
        $i++;
      }

      $sqlselect .= " AND bookcopy_status = 'ยืม'";

      return $this->db->query($sqlselect)->row();
    }
}
