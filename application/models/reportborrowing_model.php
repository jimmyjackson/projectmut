<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportborrowing_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getYear()
    {
      $sql = "SELECT yearstudy FROM yearstudy GROUP BY yearstudy ORDER BY yearstudy DESC";
      return $this->db->query($sql)->result();
    }

    public function selectreport_model($year)
    {
        $sql = "SELECT CURDATE() AS cur_date,abc.*
                FROM
                (SELECT b.book_id,b.book_name,
                IFNULL(m1.count_book,0) AS count_m01,
                IFNULL(m2.count_book,0) AS count_m02,
                IFNULL(m3.count_book,0) AS count_m03,
                IFNULL(m4.count_book,0) AS count_m04,
                IFNULL(m5.count_book,0) AS count_m05,
                IFNULL(m6.count_book,0) AS count_m06,
                IFNULL(m7.count_book,0) AS count_m07,
                IFNULL(m8.count_book,0) AS count_m08,
                IFNULL(m9.count_book,0) AS count_m09,
                IFNULL(m10.count_book,0) AS count_m10,
                IFNULL(m11.count_book,0) AS count_m11,
                IFNULL(m12.count_book,0) AS count_m12
                FROM book b
                LEFT JOIN
                (SELECT
                  book_id,
                  book_name,
                  COUNT(id_book) AS count_book
                FROM borrowingdetial
                INNER JOIN borrowing ON id_borrowing = borrowing_id
                INNER JOIN book ON id_book = book_id
                WHERE DATE_FORMAT(borrowing_date,'%m-%Y') = '01-".$year."'
                GROUP BY id_book) m1
                ON b.book_id = m1.book_id
                LEFT JOIN
                (SELECT
                  book_id,
                  book_name,
                  COUNT(id_book) AS count_book
                FROM borrowingdetial
                INNER JOIN borrowing ON id_borrowing = borrowing_id
                INNER JOIN book ON id_book = book_id
                WHERE DATE_FORMAT(borrowing_date,'%m-%Y') = '02-".$year."'
                GROUP BY id_book) m2
                ON b.book_id = m2.book_id
                LEFT JOIN
                (SELECT
                  book_id,
                  book_name,
                  COUNT(id_book) AS count_book
                FROM borrowingdetial
                INNER JOIN borrowing ON id_borrowing = borrowing_id
                INNER JOIN book ON id_book = book_id
                WHERE DATE_FORMAT(borrowing_date,'%m-%Y') = '03-".$year."'
                GROUP BY id_book) m3
                ON b.book_id = m3.book_id
                LEFT JOIN
                (SELECT
                  book_id,
                  book_name,
                  COUNT(id_book) AS count_book
                FROM borrowingdetial
                INNER JOIN borrowing ON id_borrowing = borrowing_id
                INNER JOIN book ON id_book = book_id
                WHERE DATE_FORMAT(borrowing_date,'%m-%Y') = '04-".$year."'
                GROUP BY id_book) m4
                ON b.book_id = m4.book_id
                LEFT JOIN
                (SELECT
                  book_id,
                  book_name,
                  COUNT(id_book) AS count_book
                FROM borrowingdetial
                INNER JOIN borrowing ON id_borrowing = borrowing_id
                INNER JOIN book ON id_book = book_id
                WHERE DATE_FORMAT(borrowing_date,'%m-%Y') = '05-".$year."'
                GROUP BY id_book) m5
                ON b.book_id = m5.book_id
                LEFT JOIN
                (SELECT
                  book_id,
                  book_name,
                  COUNT(id_book) AS count_book
                FROM borrowingdetial
                INNER JOIN borrowing ON id_borrowing = borrowing_id
                INNER JOIN book ON id_book = book_id
                WHERE DATE_FORMAT(borrowing_date,'%m-%Y') = '06-".$year."'
                GROUP BY id_book) m6
                ON b.book_id = m6.book_id
                LEFT JOIN
                (SELECT
                  book_id,
                  book_name,
                  COUNT(id_book) AS count_book
                FROM borrowingdetial
                INNER JOIN borrowing ON id_borrowing = borrowing_id
                INNER JOIN book ON id_book = book_id
                WHERE DATE_FORMAT(borrowing_date,'%m-%Y') = '07-".$year."'
                GROUP BY id_book) m7
                ON b.book_id = m7.book_id
                LEFT JOIN
                (SELECT
                  book_id,
                  book_name,
                  COUNT(id_book) AS count_book
                FROM borrowingdetial
                INNER JOIN borrowing ON id_borrowing = borrowing_id
                INNER JOIN book ON id_book = book_id
                WHERE DATE_FORMAT(borrowing_date,'%m-%Y') = '08-".$year."'
                GROUP BY id_book) m8
                ON b.book_id = m8.book_id
                LEFT JOIN
                (SELECT
                  book_id,
                  book_name,
                  COUNT(id_book) AS count_book
                FROM borrowingdetial
                INNER JOIN borrowing ON id_borrowing = borrowing_id
                INNER JOIN book ON id_book = book_id
                WHERE DATE_FORMAT(borrowing_date,'%m-%Y') = '09-".$year."'
                GROUP BY id_book) m9
                ON b.book_id = m9.book_id
                LEFT JOIN
                (SELECT
                  book_id,
                  book_name,
                  COUNT(id_book) AS count_book
                FROM borrowingdetial
                INNER JOIN borrowing ON id_borrowing = borrowing_id
                INNER JOIN book ON id_book = book_id
                WHERE DATE_FORMAT(borrowing_date,'%m-%Y') = '10-".$year."'
                GROUP BY id_book) m10
                ON b.book_id = m10.book_id
                LEFT JOIN
                (SELECT
                  book_id,
                  book_name,
                  COUNT(id_book) AS count_book
                FROM borrowingdetial
                INNER JOIN borrowing ON id_borrowing = borrowing_id
                INNER JOIN book ON id_book = book_id
                WHERE DATE_FORMAT(borrowing_date,'%m-%Y') = '11-".$year."'
                GROUP BY id_book) m11
                ON b.book_id = m11.book_id
                LEFT JOIN
                (SELECT
                  book_id,
                  book_name,
                  COUNT(id_book) AS count_book
                FROM borrowingdetial
                INNER JOIN borrowing ON id_borrowing = borrowing_id
                INNER JOIN book ON id_book = book_id
                WHERE DATE_FORMAT(borrowing_date,'%m-%Y') = '12-".$year."'
                GROUP BY id_book) m12
                ON b.book_id = m12.book_id) abc
                WHERE abc.count_m01 != 0
                OR abc.count_m02 != 0
                OR abc.count_m03 != 0
                OR abc.count_m04 != 0
                OR abc.count_m05 != 0
                OR abc.count_m06 != 0
                OR abc.count_m07 != 0
                OR abc.count_m08 != 0
                OR abc.count_m09 != 0
                OR abc.count_m10 != 0
                OR abc.count_m11 != 0
                OR abc.count_m12 != 0";

        return $this->db->query($sql)->result();
    }

}
