<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offerorder_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();
    }

    public function autoidofferorder_model()
    {
        $sqlautoid = "SELECT CONVERT(IFNULL(CONCAT('OFF',LPAD(SUBSTRING(MAX(offerorder_id),4,7)+1,7,'0')),'OFF0000001') USING utf8) AS autoid FROM offerorder";

        return $this->db->query($sqlautoid)->result();
    }

    public function deleteofferorder_model($id)
    {
        $sqldelete = "UPDATE offerorder SET
        offerorder_status='0'
        WHERE offerorder_id=?";

        $this->db->query($sqldelete,$id);
    }

    public function deleteapproveorder_model($id)
    {
        $sqldelete = "UPDATE offerorder SET
        offerorder_status='ไม่อนุมัติ'
        WHERE offerorder_id=?";

        $this->db->query($sqldelete,$id);
    }

    public function editofferorder_model($ar=array())
    {
        $sqledit = "UPDATE offerorder SET
        offerorder_offerdate=?,
        offerorder_offerpricetotal=?,
        offerorder_offervattotal=?,
        offerorder_offervat=?,
        offerorder_offersubtotal=?,
        id_employeeoffer=?,
        id_publishing=?,
        offerorder_year=?,
        offerorder_semester=?,
        offerorder_status=?
        WHERE offerorder_id=?";

        $this->db->query($sqledit,$ar);
    }

    public function editapproveorder_model($ar=array())
    {
        $sqledit = "UPDATE offerorder SET
        offerorder_approvedate=?,
        offerorder_offerpricetotal=?,
        offerorder_offervattotal=?,
        offerorder_offersubtotal=?,
        offerorder_approvepricetotal=?,
        offerorder_approvevattotal=?,
        offerorder_approvesubtotal=?,
        id_employeeapprove=?,
        offerorder_status=?
        WHERE offerorder_id=?";

        $this->db->query($sqledit,$ar);
    }

    public function add_offerorder_detial_model($id,$ar= array())
    {
      $i=0;
      foreach($ar as $value){
        $sqladd = "INSERT INTO offerorderdetial
              (id_offerorder, offerorderdetial_no,
              offerorderdetial_offerunit, offerorderdetial_offerprice,
              pricebook, id_book)
              VALUES(?,?,?,?,?,?)";

        $i++;

        $this->db->query($sqladd,array(
          'id'=>$id,
          'no'=>$i,
          'unit'=>$value->unit,
          'total'=>$value->total,
          'price'=>$value->price,
          'idbook'=>$value->id_book
        ));
      }
    }

    public function add_approveorder_detial_model($id,$ar= array())
    {
      $i=0;
      foreach($ar as $value){
        $sqladd = "INSERT INTO offerorderdetial
              (id_offerorder, offerorderdetial_no,
              offerorderdetial_offerunit, offerorderdetial_offerprice,
              offerorderdetial_approveunit,offerorderdetial_approveprice,
              pricebook, id_book)
              VALUES(?,?,?,?,?,?,?,?)";

        $i++;

        $this->db->query($sqladd,array(
          'id'=>$id,
          'no'=>$i,
          'unit'=>$value->unit,
          'total'=>$value->total,
          'unitapprove'=>$value->unitapprove,
          'totalapprove'=>$value->totalapprove,
          'price'=>$value->price,
          'idbook'=>$value->id_book
        ));
      }
    }

    public function delete_offerorder_detial_model($id)
    {
      $sql="DELETE FROM offerorderdetial WHERE id_offerorder = ?";
      $this->db->query($sql,$id);
    }

    public function addofferorder_model($ar=array())
    {
        $sqladd = "INSERT INTO offerorder
                (offerorder_id, offerorder_offerdate,
                offerorder_offerpricetotal, offerorder_offervattotal,
                offerorder_offervat, offerorder_offersubtotal,
                id_employeeoffer, id_publishing, offerorder_year,
                offerorder_semester, offerorder_status)
                VALUES(?,?,?,?,?,?,?,?,?,?,?)";

        $this->db->query($sqladd,$ar);
    }

    public function selectofferorder_where_id_model($id)
    {
        $sqlselect="SELECT offerorder_id
                    FROM offerorder
                    WHERE offerorder_id=?
                    AND (offerorder_status = 'รอเสนอ'
                    OR offerorder_status = 'รออนุมัติ'
                    OR offerorder_status = 'ไม่อนุมัติ')";
        $queryselect = $this->db->query($sqlselect,$id)->result();

        return $queryselect!=null?true:false;
    }

    public function selectapproveorder_where_id_model($id)
    {
        $sqlselect="SELECT offerorder_id
                    FROM offerorder
                    WHERE offerorder_id=?
                    AND (offerorder_status = 'อนุมัติ'
                    OR offerorder_status = 'รออนุมัติ'
                    OR offerorder_status = 'ไม่อนุมัติ')";
        $queryselect = $this->db->query($sqlselect,$id)->result();

        return $queryselect!=null?true:false;
    }

    public function selectdataofferorder_model($id)
    {
        $sqlselectdata = "SELECT
                          f.offerorder_id,
                          f.offerorder_offerdate,
                          f.offerorder_offerpricetotal,
                          f.offerorder_approvedate,
                          f.offerorder_approvepricetotal,
                          f.offerorder_status,
                          f.id_employeeoffer,
                          f.id_employeeapprove,
                          f.offerorder_offervattotal,
                          f.offerorder_offersubtotal,
                          f.offerorder_approvesubtotal,
                          f.offerorder_offervat,
                          f.id_publishing,
                          f.offerorder_year,
                          f.offerorder_semester,
                          fd.offerorderdetial_no,
                          fd.offerorderdetial_offerunit,
                          fd.offerorderdetial_approveunit,
                          fd.offerorderdetial_offerprice,
                          fd.offerorderdetial_approveprice,
                          fd.pricebook,
                          fd.id_book,
                          b.book_name
                        FROM offerorder f
                        INNER JOIN offerorderdetial fd ON f.offerorder_id=fd.id_offerorder
                        INNER JOIN book b ON fd.id_book=b.book_id
                        WHERE f.offerorder_id = ? AND f.offerorder_status != '0'";
        return $this->db->query($sqlselectdata,array($id,$id))->result();
    }

    public function selectdataofferorderreport_model($id)
    {
        $sqlselectdata = "SELECT
                          f.offerorder_id,
                          f.offerorder_offerdate,
                          f.offerorder_offerpricetotal,
                          f.offerorder_approvedate,
                          f.offerorder_approvepricetotal,
                          f.offerorder_status,
                          f.id_employeeoffer,
                          f.id_employeeapprove,
                          f.offerorder_offervattotal,
                          f.offerorder_offersubtotal,
                          f.offerorder_approvesubtotal,
                          f.offerorder_offervat,
                          f.id_publishing,
                          f.offerorder_year,
                          f.offerorder_semester,
                          fd.offerorderdetial_no,
                          fd.offerorderdetial_offerunit,
                          fd.offerorderdetial_approveunit,
                          fd.offerorderdetial_offerprice,
                          fd.offerorderdetial_approveprice,
                          fd.pricebook,
                          fd.id_book,
                          b.book_name,
                          e.employee_name AS offer_name,
                          e.employee_lname AS offer_lname,
                          ee.employee_name AS approve_name,
                          ee.employee_lname AS approve_lname,
                          p.publishing_name
                        FROM offerorder f
                        INNER JOIN offerorderdetial fd ON f.offerorder_id=fd.id_offerorder
                        INNER JOIN book b ON fd.id_book=b.book_id
                        INNER JOIN employee e ON f.id_employeeoffer=e.employee_id
                        INNER JOIN employee ee ON f.id_employeeapprove=ee.employee_id
                        INNER JOIN publishing p ON f.id_publishing=p.publishing_id
                        WHERE f.offerorder_id = ? AND f.offerorder_status != '0'";
        return $this->db->query($sqlselectdata,array($id,$id))->result();
    }

    public function selectdataofferorderreport1_model($id)
    {
        $sqlselectdata = "SELECT
                          f.offerorder_id,
                          f.offerorder_offerdate,
                          f.offerorder_offerpricetotal,
                          f.offerorder_approvedate,
                          f.offerorder_approvepricetotal,
                          f.offerorder_status,
                          f.id_employeeoffer,
                          f.id_employeeapprove,
                          f.offerorder_offervattotal,
                          f.offerorder_offersubtotal,
                          f.offerorder_approvesubtotal,
                          f.offerorder_offervat,
                          f.id_publishing,
                          f.offerorder_year,
                          f.offerorder_semester,
                          fd.offerorderdetial_no,
                          fd.offerorderdetial_offerunit,
                          fd.offerorderdetial_approveunit,
                          fd.offerorderdetial_offerprice,
                          fd.offerorderdetial_approveprice,
                          fd.pricebook,
                          fd.id_book,
                          b.book_name,
                          e.employee_name AS offer_name,
                          e.employee_lname AS offer_lname,
                          p.publishing_name
                        FROM offerorder f
                        INNER JOIN offerorderdetial fd ON f.offerorder_id=fd.id_offerorder
                        INNER JOIN book b ON fd.id_book=b.book_id
                        INNER JOIN employee e ON f.id_employeeoffer=e.employee_id
                        INNER JOIN publishing p ON f.id_publishing=p.publishing_id
                        WHERE f.offerorder_id = ? AND f.offerorder_status != '0'";
        return $this->db->query($sqlselectdata,array($id,$id))->result();
    }

    public function selectapproveorderpurchaseorder_model($id_publishing)
    {
        $sql = "SELECT offerorder_id,offerorder_approvedate,offerorder_approvesubtotal,offerorder_offervat
                FROM offerorder
                WHERE offerorder_status = 'อนุมัติ' AND id_publishing = ?";

        return $this->db->query($sql,$id_publishing)->result();
    }

    public function selectofferorder_model($offset=0)
    {
        $sqlselect = "SELECT offerorder_id,offerorder_offerdate,
        id_employeeoffer,id_publishing,publishing_name,offerorder_status
        FROM offerorder
        INNER JOIN publishing ON id_publishing=publishing_id
        WHERE (offerorder_status = 'รอเสนอ' OR offerorder_status = 'รออนุมัติ'
        OR offerorder_status = 'ไม่อนุมัติ')
        LIMIT ".$offset.",10";

        return $this->db->query($sqlselect)->result();
    }

    public function selectapproveorder_model($offset=0)
    {
        $sqlselect = "SELECT offerorder_id,offerorder_offerdate,
        offerorder_approvedate,id_employeeapprove,
        id_employeeoffer,id_publishing,publishing_name,offerorder_status
        FROM offerorder
        INNER JOIN publishing ON id_publishing=publishing_id
        WHERE (offerorder_status = 'อนุมัติ' OR offerorder_status = 'รออนุมัติ'
        OR offerorder_status = 'ไม่อนุมัติ')
        LIMIT ".$offset.",10";

        return $this->db->query($sqlselect)->result();
    }

    public function countofferorder_model()
    {
        $sqlcount = "SELECT COUNT(1) AS count_offerorder
        FROM offerorder
        WHERE offerorder_status = (offerorder_status = 'รอเสนอ'
        OR offerorder_status = 'รออนุมัติ' OR offerorder_status = 'ไม่อนุมัติ')";

        return $this->db->query($sqlcount)->row();
    }

    public function countapproveorder_model()
    {
        $sqlcount = "SELECT COUNT(1) AS count_offerorder
        FROM offerorder
        WHERE offerorder_status = 'อนุมัติ' OR offerorder_status = 'รออนุมัติ' OR offerorder_status = 'ไม่อนุมัติ'";

        return $this->db->query($sqlcount)->row();
    }

    public function searchofferorder_model($arr=array())
    {
        $i=0;

        $sqlselect = "SELECT offerorder_id,offerorder_offerdate,
                      id_employeeoffer,id_publishing,
                      publishing_name,offerorder_status
                      FROM offerorder
                      INNER JOIN publishing ON id_publishing=publishing_id
                      WHERE ";

        foreach ($arr['selectmessage'] as $value)
        {
          if($value == 'id')
              $sqlselect .= "offerorder_id LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'dateoffer')
              $sqlselect .= "offerorder_offerdate = '".$arr['inputsearch']."'";
          else if($value == 'idempoffer')
              $sqlselect .= "id_employeeoffer LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'publishing')
              $sqlselect .= "publishing_name LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'รอเสนอ')
              $sqlselect .= "offerorder_status = 'รอเสนอ'";
          else if($value == 'รออนุมัติ')
              $sqlselect .= "offerorder_status = 'รออนุมัติ'";
          else if($value == 'ไม่อนุมัติ')
              $sqlselect .= "offerorder_status = 'ไม่อนุมัติ'";
          else
              $sqlselect .= "offerorder_id LIKE '%".$arr['inputsearch']."%' OR offerorder_status = 'รอเสนอ' OR offerorder_status = 'รออนุมัติ'";

            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= '';
            else
                $sqlselect .= ' OR ';

            $i++;
        }

        $sqlselect .= " LIMIT ".$arr['offset'].",10";

        return $this->db->query($sqlselect)->result();
    }

    public function searchapproveorder_model($arr=array())
    {
        $i=0;

        $sqlselect = "SELECT offerorder_id,offerorder_offerdate,
        offerorder_approvedate,id_employeeapprove,
        id_employeeoffer,id_publishing,publishing_name,offerorder_status
        FROM offerorder
        INNER JOIN publishing ON id_publishing=publishing_id
        WHERE ";

        foreach ($arr['selectmessage'] as $value)
        {
          if($value == 'id')
              $sqlselect .= "offerorder_id LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'dateoffer')
              $sqlselect .= "offerorder_offerdate = '".$arr['inputsearch']."'";
          else if($value == 'idempoffer')
              $sqlselect .= "id_employeeoffer LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'dateapprove')
              $sqlselect .= "offerorder_approvedate = '".$arr['inputsearch']."'";
          else if($value == 'idempapprove')
              $sqlselect .= "id_employeeapprove LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'publishing')
              $sqlselect .= "publishing_name LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'อนุมัติ')
              $sqlselect .= "offerorder_status = 'อนุมัติ'";
          else if($value == 'รออนุมัติ')
              $sqlselect .= "offerorder_status = 'รออนุมัติ'";
          else if($value == 'ไม่อนุมัติ')
              $sqlselect .= "offerorder_status = 'ไม่อนุมัติ'";
          else
              $sqlselect .= "offerorder_id LIKE '%".$arr['inputsearch']."%' OR offerorder_status = 'อนุมัติ' OR offerorder_status = 'รออนุมัติ'";

            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= '';
            else
                $sqlselect .= ' OR ';

            $i++;
        }

        $sqlselect .= " LIMIT ".$arr['offset'].",10";

        return $this->db->query($sqlselect)->result();
    }

    public function countsearchofferorder_model($arr=array())
    {
        $i=0;

        $sqlselect = "SELECT COUNT(1) AS count_offerorder
                      FROM offerorder
                      INNER JOIN publishing ON id_publishing=publishing_id
                      WHERE ";

        foreach ($arr['selectmessage'] as $value)
        {
          if($value == 'id')
              $sqlselect .= "offerorder_id LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'dateoffer')
              $sqlselect .= "offerorder_offerdate = '".$arr['inputsearch']."'";
          else if($value == 'idempoffer')
              $sqlselect .= "id_employeeoffer LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'dateapprove')
              $sqlselect .= "offerorder_approvedate = '".$arr['inputsearch']."'";
          else if($value == 'idempapprove')
              $sqlselect .= "id_employeeapprove LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'publishing')
              $sqlselect .= "publishing_name LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'อนุมัติ')
              $sqlselect .= "offerorder_status = 'รอเสนอ'";
          else if($value == 'รออนุมัติ')
              $sqlselect .= "offerorder_status = 'รออนุมัติ'";
          else if($value == 'ไม่อนุมัติ')
              $sqlselect .= "offerorder_status = 'ไม่อนุมัติ'";
          else
              $sqlselect .= "offerorder_id LIKE '%".$arr['inputsearch']."%' OR offerorder_status = 'รอเสนอ' OR offerorder_status = 'รออนุมัติ'";

            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= '';
            else
                $sqlselect .= ' OR ';

            $i++;
        }

        $sqlselect .= " LIMIT ".$arr['offset'].",10";

        return $this->db->query($sqlselect)->result();
    }

    public function countsearchapproveorder_model($arr=array())
    {
        $i=0;

        $sqlselect = "SELECT COUNT(1) AS count_offerorder
                      FROM offerorder
                      INNER JOIN publishing ON id_publishing=publishing_id
                      WHERE ";

        foreach ($arr['selectmessage'] as $value)
        {
          if($value == 'id')
              $sqlselect .= "offerorder_id LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'dateoffer')
              $sqlselect .= "offerorder_offerdate = '".$arr['inputsearch']."'";
          else if($value == 'idempoffer')
              $sqlselect .= "id_employeeoffer LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'dateapprove')
              $sqlselect .= "offerorder_approvedate = '".$arr['inputsearch']."'";
          else if($value == 'idempapprove')
              $sqlselect .= "id_employeeapprove LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'publishing')
              $sqlselect .= "publishing_name LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'อนุมัติ')
              $sqlselect .= "offerorder_status = 'อนุมัติ'";
          else if($value == 'รออนุมัติ')
              $sqlselect .= "offerorder_status = 'รออนุมัติ'";
          else if($value == 'ไม่อนุมัติ')
              $sqlselect .= "offerorder_status = 'ไม่อนุมัติ'";
          else
              $sqlselect .= "offerorder_id LIKE '%".$arr['inputsearch']."%' OR offerorder_status = 'อนุมัติ' OR offerorder_status = 'รออนุมัติ'";

            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= '';
            else
                $sqlselect .= ' OR ';

            $i++;
        }

        $sqlselect .= " LIMIT ".$arr['offset'].",10";

        return $this->db->query($sqlselect)->result();
    }

}
