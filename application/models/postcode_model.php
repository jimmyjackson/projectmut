<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Postcode_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();   
    }
    public function addpostcode_model($ar=array())
    {
    
    }
    public function editpostcode_model()
    {
        
    }
    public function deletepostcode_model()
    {
        
    }
    public function selectallpostcode_model($ar=array())
    {
        $sql = "SELECT postcode_id
        FROM postcode
        WHERE id_province = ? AND id_zone = ? AND id_district = ? AND postcode_status='1'"; 
        $query = $this->db->query($sql,$ar)->result();
        return $query;
    }
    public function selectpostcode_model()
    {
        
    }
}
