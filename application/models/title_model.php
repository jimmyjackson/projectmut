<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Title_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();   
    }
    
    public function selectalltitle_model()
    {
        $sql = "SELECT title_id AS id,title_name AS name
        FROM title"; 

        return $this->db->query($sql)->result();
    }

}
