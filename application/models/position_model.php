<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Position_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();   
    }
    
    public function autoidposi_model()
    {
        $sqlautoid = "SELECT                    
                    CONVERT(IFNULL(CONCAT('POSIT',
                    LPAD(SUBSTRING(MAX(position_id),6,4)+1,4,'0')),'POSIT0001') USING utf8) 
                    AS autoid FROM position"; 
        $autoid = $this->db->query($sqlautoid)->result();
        return $autoid;
    }
    
    public function addposition_model($ar=array())
    {
        $this->db->trans_begin();
        
        $sqlinsert ="INSERT INTO position (position_id,position_name,position_status)
                    VALUES(?,?,'1')";
        
        $this->db->query($sqlinsert,$ar);
        
        if($ar['accessposi']!=""){
            
            foreach($ar['accessposi'] as $accessposi){
                
                $this->add_accessposition_model(array(
                'id'=>$ar['idposi'],'access'=>$accessposi));
                
            }
            
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else 
        {
            $this->db->trans_commit();
            return true;
        }
    }
    
    public function editposition_model($ar=array())
    {
        $this->db->trans_begin();
        
        $sqlupdate ="UPDATE position SET
                    position_name=?
                    WHERE position_id=?";
        
        $this->db->query($sqlupdate,$ar);
        
        if($ar['accessposi']!=""){
            
            $this->delete_accessposition_model($ar['idposi']);
            
            foreach($ar['accessposi'] as $accessposi){
                
                $this->add_accessposition_model(array(
                'id'=>$ar['idposi'],'access'=>$accessposi));
                
            }
            
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else 
        {
            $this->db->trans_commit();
            return true;
        }
    }
    
    public function deleteposition_model($idposi='')
    {
        $result['status']=false;
        $sqldelete = "DELETE FROM position WHERE position_id=?";
        $result['data']=$this->db->query($sqldelete,$idposi); 
        
        if(!$result['data'])
        {         
            $sqldelete = "UPDATE position SET position_status = '0' WHERE position_id=?";
            $this->db->query($sqldelete,$idposi);
        }

        return $result;
    }
    
    public function select_access_id_where_id_position_model($idposition)
    {
        
        $sql = "SELECT id_access
                FROM accessposition
                WHERE id_position=?"; 
        $query = $this->db->query($sql,$idposition)->result();
        return $query;
    }
    
    private function delete_accessposition_model($idposition)
    {
        $sql = "DELETE FROM accessposition
                WHERE id_position = ?"; 
        $query = $this->db->query($sql,$idposition);
    }
    
    private function add_accessposition_model($ar=array())
    {
        $sql = "INSERT INTO accessposition (id_position,id_access) VALUES(?,?)"; 
        $query = $this->db->query($sql,$ar);
    }
    
    public function selectallposition_model($offset=0)
    {
        $sql = "SELECT position_id,position_name
        FROM position
        WHERE position_status='1'
        LIMIT ".$offset.",10"; 
        $query = $this->db->query($sql)->result();
        return $query;
    }
    
    public function countallposition_model()
    {
        $sqlcount = "SELECT COUNT(1)
        FROM position
        WHERE position_status='1'";
        $querycount = $this->db->query($sqlcount)->row();
        return $querycount;
    }
    
    public function searchposition_model($arr=array())
    {
        $i=0;
        $sqlselect = "SELECT position_id,position_name
        FROM position
        WHERE ";
        
        foreach ($arr['selectmessage'] as $value)
        {
            if($value == 'id')
                $sqlselect .= "position_id LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'name')
                $sqlselect .= "position_name LIKE '%".$arr['inputsearch']."%'";
            else
                $sqlselect .= "position_id LIKE '%".$arr['inputsearch']."%' OR position_name LIKE '%".$arr['inputsearch']."%'";
                
            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= ' AND ';
            else
                $sqlselect .= ' OR ';
                
            $i++;
        }
        
        $sqlselect .= "position_status='1'
        LIMIT ".$arr['offset'].",10";
        
        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }
    
    public function countsearchposition_model($arr=array())
    {
        $i=0;
        $sqlselect = "SELECT COUNT(1) as countsearchposition
        FROM position 
        WHERE ";
        
        foreach ($arr['selectmessage'] as $value)
        {
            if($value == 'id')
                $sqlselect .= "position_id LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'name')
                $sqlselect .= "position_name LIKE '%".$arr['inputsearch']."%'";
            else
                $sqlselect .= "position_id LIKE '%".$arr['inputsearch']."%' OR position_name LIKE '%".$arr['inputsearch']."%'";
                
            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= ' AND ';
            else
                $sqlselect .= ' OR ';
                
            $i++;
        }
        
        $sqlselect .= "position_status='1'";
        
        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }
    
    public function selectposition_model()
    {
        $sql = "SELECT position_id,position_name
        FROM position
        WHERE position_status='1'"; 
        $query = $this->db->query($sql)->result();
        return $query;
    }
    
    public function select_name_position_where_id_model($idposition)
    {
        $sql = "SELECT position_name
        FROM position
        WHERE position_id=?"; 
        $query = $this->db->query($sql,$idposition)->result();
        return $query;
    }
    
    public function selectposition_where_id_model($idposition)
    {
        $sqlselect="SELECT position_id
                    FROM position
                    WHERE position_id=?";
        $queryselect = $this->db->query($sqlselect,$idposition)->result();
        
        return $queryselect!=null?true:false;
    }
}
