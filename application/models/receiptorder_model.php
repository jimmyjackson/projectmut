<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Receiptorder_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();
    }

    public function selectpublishing()
    {
        $sqlselect="SELECT publishing_id, publishing_name FROM publishing";
        $queryselect = $this->db->query($sqlselect)->result();

        return $queryselect;
    }

    public function selectdatareceiptorder($value)
    {
      $sqlselect="SELECT
      r.receiptorder_id,
      r.receiptorder_date,
      r.receiptorder_totalprice,
      r.receiptorder_vat,
      r.receiptorder_vattotal,
      r.id_employee,
      p.publishing_name,
      SUM(rd.receiptorderdetial_unit) AS unit,
      SUM(rd.receiptorderdetial_sumprice) AS sumprice,
      b.book_name,
      (SUM(rd.receiptorderdetial_sumprice)/SUM(rd.receiptorderdetial_unit)) AS price
      FROM receiptorder r
      INNER JOIN receiptorderdetial rd
      ON receiptorder_id = id_receiptorder
      INNER JOIN book b ON id_book = b.book_id
      INNER JOIN publishing p ON r.id_publishing = p.publishing_id
      WHERE r.receiptorder_id = ?
      GROUP BY rd.id_book";

      return $this->db->query($sqlselect,$value)->result();
    }

    public function selectdatareceiptorderreport_model($value)
    {
      $sqlselect="SELECT
      r.receiptorder_id,
      r.receiptorder_date,
      r.receiptorder_totalprice,
      r.receiptorder_vat,
      r.receiptorder_vattotal,
      r.id_employee,
      p.publishing_name,
      SUM(rd.receiptorderdetial_unit) AS unit,
      SUM(rd.receiptorderdetial_sumprice) AS sumprice,
      b.book_name,
      e.employee_name,
      e.employee_lname,
      (SUM(rd.receiptorderdetial_sumprice)/SUM(rd.receiptorderdetial_unit)) AS price
      FROM receiptorder r
      INNER JOIN receiptorderdetial rd
      ON receiptorder_id = id_receiptorder
      INNER JOIN book b ON id_book = b.book_id
      INNER JOIN publishing p ON r.id_publishing = p.publishing_id
      INNER JOIN employee e ON r.id_employee=e.employee_id
      WHERE r.receiptorder_id = ?
      GROUP BY rd.id_book";

      return $this->db->query($sqlselect,$value)->result();
    }

    public function selectreceiptorder_model($offset=0)
    {
      $sqlselect="SELECT
                    r.receiptorder_id,
                    r.receiptorder_date,
                    r.id_employee,
                    p.publishing_name
                  FROM receiptorder r
                  INNER JOIN publishing p ON r.id_publishing = p.publishing_id
                  LIMIT ".$offset.",10";

      return $this->db->query($sqlselect)->result();
    }

    public function countreceiptorder_model()
    {
        $sqlselectdatabook = "SELECT COUNT(1) AS count FROM receiptorder";
        return $this->db->query($sqlselectdatabook)->row();
    }

    public function selectpurchase($param)
    {
        $sqlselectdata = "SELECT
                        a.*,b.*,c.book_name
                        FROM purchaseorder a
                        LEFT JOIN purchaseorderdetial b ON a.purchaseorder_id=b.id_purchaseorder
                        LEFT JOIN book c ON c.book_id=b.id_book
                        WHERE b.purchaseorderdetial_balanceunit > 0
                        ";
        if(!empty($param['publishing'])){
            $sqlselectdata .= " AND a.id_publishing = '".$param['publishing']."' ";
        }
        if(!empty($param['purchaseId'])){
            $sqlselectdata .= " AND a.purchaseorder_id = '".$param['purchaseId']."' ";
        }
        return $this->db->query($sqlselectdata)->result();
    }

    public function savereceiptorder($param)
    {
        $sqlautoid = "SELECT CONVERT(IFNULL(CONCAT('REC',LPAD(SUBSTRING(MAX(receiptorder_id),4,7)+1,7,'0')),'REC0000001') USING utf8) AS autoid FROM receiptorder";
        $autoId = $this->db->query($sqlautoid)->result();

        $totalpriceAll = '';
        $vattotalAll = '';
        $subtotalAll = '';
        $vat = '';
        $publishing = '';
        foreach ($param['receipt'] AS $val){
            $arr = explode('-', $val);

            $sqlselectdata = "SELECT
                            a.*,b.*,c.book_name
                            FROM purchaseorder a
                            LEFT JOIN purchaseorderdetial b ON a.purchaseorder_id=b.id_purchaseorder
                            LEFT JOIN book c ON c.book_id=b.id_book
                            WHERE b.purchaseorderdetial_balanceunit > 0
                            AND a.purchaseorder_id='".$arr[0]."'
                            AND b.purchaseorderdetial_no='".$arr[1]."'
                            ";
            $dataPurchase = $this->db->query($sqlselectdata)->result();

            $receiptAmount = $param['receipt_'.str_replace('-', '', $val)];

            $totalprice = $receiptAmount * $dataPurchase[0]->purchaseorderdetial_bookprice;
            $vattotal = $totalprice * ($dataPurchase[0]->purchaseorder_vat / 100);
            $subtotal = $totalprice + $vattotal;

            $publishing = $dataPurchase[0]->id_publishing;
            $vat = $vattotal;
            $totalpriceAll = $totalpriceAll + $totalprice;
            $vattotalAll = $totalpriceAll + $vattotal;
            $subtotalAll = $subtotalAll + $subtotal;
        }

        $sqlReceiptorder = "INSERT INTO receiptorder
              (receiptorder_id, receiptorder_date, receiptorder_totalprice, receiptorder_vat,
                receiptorder_vattotal, receiptorder_subtotal, id_employee, id_publishing)
            VALUES('".$autoId[0]->autoid."', '".date('Y-m-d')."', '".$totalpriceAll."', '".$vat."',
            '".$vattotalAll."', '".$subtotalAll."', '".$this->session->userdata('idemp')."', '".$publishing."')";
        $this->db->query($sqlReceiptorder);

        $a = 0;
        foreach ($param['receipt'] AS $val){
            $a++;
            $arr = explode('-', $val);

            $sqlselectdata = "SELECT
                            a.*,b.*,c.book_name
                            FROM purchaseorder a
                            LEFT JOIN purchaseorderdetial b ON a.purchaseorder_id=b.id_purchaseorder
                            LEFT JOIN book c ON c.book_id=b.id_book
                            WHERE b.purchaseorderdetial_balanceunit > 0
                            AND a.purchaseorder_id='".$arr[0]."'
                            AND b.purchaseorderdetial_no='".$arr[1]."'
                            ";
            $dataPurchase = $this->db->query($sqlselectdata)->result();

            $receiptAmount = $param['receipt_'.str_replace('-', '', $val)];

            $totalprice = $receiptAmount * $dataPurchase[0]->purchaseorderdetial_bookprice;
            $vattotal = $totalprice * ($dataPurchase[0]->purchaseorder_vat / 100);
            $subtotal = $totalprice + $vattotal;

            $sqlReceiptorderDetail = "INSERT INTO receiptorderdetial
                  (id_receiptorder, receiptorderdetial_no, receiptorderdetial_unit, receiptorderdetial_sumprice,
                    id_purchase, no_purchase, id_book)
                VALUES ('".$autoId[0]->autoid."', '".$a."', ".$receiptAmount.", ".$totalprice.",
                '".$arr[0]."', '".$arr[1]."', '".$dataPurchase[0]->id_book."')";
            $this->db->query($sqlReceiptorderDetail);

            $sqlUpdate = "UPDATE purchaseorderdetial SET
            purchaseorderdetial_balanceunit= purchaseorderdetial_balanceunit-$receiptAmount
            WHERE id_purchaseorder='".$arr[0]."' AND purchaseorderdetial_no='".$arr[1]."'";
            $this->db->query($sqlUpdate);
        }

        return $autoId[0]->autoid;
    }

    public function searchreceiptorder_model($arr=array())
    {
        $i=0;

        $sqlselect = "SELECT
                      r.receiptorder_id,
                      r.receiptorder_date,
                      r.id_employee,
                      p.publishing_name
                      FROM receiptorder r
                      INNER JOIN publishing p ON r.id_publishing = p.publishing_id
                      WHERE ";

        foreach ($arr['selectmessage'] as $value)
        {
          if($value == 'id')
              $sqlselect .= "r.receiptorder_id LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'date')
              $sqlselect .= "r.receiptorder_date = '".$arr['inputsearch']."'";
          else if($value == 'idemp')
              $sqlselect .= "r.id_employee LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'publishing')
              $sqlselect .= "p.publishing_name LIKE '%".$arr['inputsearch']."%'";
          else
              $sqlselect .= "r.purchaseorder_id LIKE '%".$arr['inputsearch']."%'";

            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= '';
            else
                $sqlselect .= ' OR ';

            $i++;
        }

        $sqlselect .= " LIMIT ".$arr['offset'].",10";

        return $this->db->query($sqlselect)->result();
    }

    public function countsearchreceiptorder_model($arr=array())
    {
        $i=0;

        $sqlselect = "SELECT COUNT(1) AS count_purchaseorder
                      FROM receiptorder r
                      INNER JOIN publishing p ON id_publishing = publishing_id
                      WHERE ";

        foreach ($arr['selectmessage'] as $value)
        {
          if($value == 'id')
              $sqlselect .= "r.receiptorder_id LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'date')
              $sqlselect .= "r.receiptorder_date = '".$arr['inputsearch']."'";
          else if($value == 'idemp')
              $sqlselect .= "r.id_employee LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'publishing')
              $sqlselect .= "p.publishing_name LIKE '%".$arr['inputsearch']."%'";
          else
              $sqlselect .= "r.purchaseorder_id LIKE '%".$arr['inputsearch']."%'";

            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= '';
            else
                $sqlselect .= ' OR ';

            $i++;
        }

        return $this->db->query($sqlselect)->result();
    }

}
