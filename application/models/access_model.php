<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Access_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();   
    }
    
    public function selectallaccess_model()
    {
        $sql = "SELECT access_id,access_name,access_url
        FROM access 
        WHERE access_status ='1'"; 
        $query = $this->db->query($sql)->result();
        return $query;
    }
    
}
