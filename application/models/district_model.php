<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class District_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();   
    }
    public function adddistrict_model($ar=array())
    {
    
    }
    public function editdistrict_model()
    {
        
    }
    public function deletedistrict_model()
    {
        
    }
    public function selectalldistrict_model($ar=array())
    {
        $sql = "SELECT district_id,district_name
        FROM district
        WHERE id_province = ? AND id_zone = ? AND district_status='1'"; 
        $query = $this->db->query($sql,$ar)->result();
        return $query;
    }
    public function selectdistrict_model()
    {
        
    }
}
