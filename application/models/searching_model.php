<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Searching_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();
    }


    public function getbook($param)
    {
        $sqlselectdata = "SELECT b.book_id,b.book_name,
                          b.book_image,b.book_price,b.book_isbn,
                          p.publishing_name,bc.bookcopy_id,bc.bookcopy_callno,bc.bookcopy_status,
                          CONCAT(ptb.positionbook_tutee,':',ptb.positionbook_chantee,':',ptb.positionbook_chongtee) AS positionbook,
                          t.title_name,CONCAT(pyb.id_publicationyear,'/',pyb.edition_publicationyear) AS edition,
                          CONCAT(a.author_name,' ',a.author_lname) AS author,CONCAT(aa.author_name,' ',aa.author_lname) AS translator
                          FROM book b
                          INNER JOIN bookcopy bc ON b.book_id = bc.id_book
                          INNER JOIN publishing p ON b.id_publishing = p.publishing_id
                          INNER JOIN positionbook ptb ON bc.bookcopy_id = ptb.bookcopy_id
                          INNER JOIN titlebook tb ON b.book_id = tb.id_book
                          INNER JOIN title t ON tb.id_title = t.title_id
                          INNER JOIN publicationyearbook pyb ON bc.bookcopy_id = pyb.id_bookcopy
                          LEFT JOIN authorbook ab ON b.book_id = ab.id_book
                          LEFT JOIN author a ON ab.id_author = a.author_id
                          LEFT JOIN translatorbook ttb ON b.book_id = ttb.id_book
                          LEFT JOIN author aa ON ttb.id_translator = aa.author_id
                          WHERE b.book_status = '1'";

        if(!empty($param['bookcopy_callno'])){
            $sqlselectdata .= " AND bc.bookcopy_callno LIKE '%".$param['bookcopy_callno']."%' ";
        }
        if(!empty($param['book_name'])){
            $sqlselectdata .= " AND b.book_name LIKE '%".$param['book_name']."%' ";
        }
        if(!empty($param['author_name'])){
            $sqlselectdata .= " AND (a.author_name LIKE '%".$param['author_name']."%' || a.author_lname LIKE '%".$param['author_name']."%' || a.author_pname LIKE '%".$param['author_name']."%') ";
        }
        if(!empty($param['book_isbn'])){
            $sqlselectdata .= " AND b.book_isbn LIKE '%".$param['book_isbn']."%' ";
        }
        if(!empty($param['id_publishing'])){
            $sqlselectdata .= " AND p.publishing_id LIKE '%".$param['id_publishing']."%' ";
        }
        if(!empty($param['title_name'])){
            $sqlselectdata .= " AND t.title_name LIKE '%".$param['title_name']."%' ";
        }
        if(!empty($param['bookcopy_id'])){
            $sqlselectdata .= " AND bc.bookcopy_id LIKE '%".$param['bookcopy_id']."%' ";
        }
        if(!empty($param['book_id'])){
            $sqlselectdata .= " AND b.book_id LIKE '%".$param['book_id']."%' ";
        }

        return $this->db->query($sqlselectdata)->result();
    }

}
