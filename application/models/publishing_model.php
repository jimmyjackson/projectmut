<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Publishing_Model extends CI_Model
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function selectallpublishing_model()
    {
        $sqlselect="SELECT publishing_id,publishing_name
                    FROM publishing
                    WHERE publishing_status='1'";
        $query = $this->db->query($sqlselect)->result();
        return $query;
    }
}