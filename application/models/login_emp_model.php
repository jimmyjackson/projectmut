<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_Emp_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();   
    }
    
    public function checklogin_model($ar=array())
    {
        $sql = 'SELECT employee_id,employee_username,employee_image
		FROM employee
		WHERE employee_username = ? AND employee_password = ?'; 
        $query = $this->db->query($sql,$ar)->result();
        return $query;
    }
    
    public function checkpermission_model($ar=array())
    {
        $sql = "SELECT e.employee_id,e.employee_username,e.id_position,
                p.position_name,ap.id_access,a.access_name,a.access_url,
                a.id_mainmenu
                FROM employee e
                INNER JOIN POSITION p ON e.id_position=p.position_id
                INNER JOIN accessposition ap ON e.id_position=ap.id_position
                INNER JOIN access a ON ap.id_access=a.access_id
                WHERE e.employee_username=? AND e.employee_password=?
                AND a.access_status!='0' AND p.position_status!='0' 
                AND e.employee_status!='0' AND id_mainmenu=?"; 
        
        $query = $this->db->query($sql,$ar)->result();
        
        return $query;
    }
}
