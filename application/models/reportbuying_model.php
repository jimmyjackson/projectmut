<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportbuying_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();
    }

    public function selectreport_model($date_from, $date_to)
    {
        $sql = "SELECT CURDATE() AS cur_date,
                  pd.id_book,
                  b.book_name,
                  b.book_price,
                  pd.purchaseorderdetial_bookprice,
                  SUM(pd.purchaseorderdetial_unit) AS sum_unit,
                  SUM(pd.purchaseorderdetial_sumprice) AS sum_price
                FROM purchaseorderdetial pd
                INNER JOIN purchaseorder p ON pd.id_purchaseorder = p.purchaseorder_id
                INNER JOIN book b ON pd.id_book = b.book_id
                WHERE p.purchaseorder_date BETWEEN '".$date_from."' AND '".$date_to."'
                GROUP BY pd.id_book,pd.purchaseorderdetial_bookprice";

        return $this->db->query($sql)->result();
    }

    public function selectreporttotal_model($date_from, $date_to)
    {
        $sql = "SELECT
                  SUM(pd.purchaseorderdetial_sumprice) AS total,
                  ((SUM(pd.purchaseorderdetial_sumprice)*7)/100) AS vat,
                  (SUM(pd.purchaseorderdetial_sumprice)+((SUM(pd.purchaseorderdetial_sumprice)*7)/100)) AS subtotal
                FROM purchaseorderdetial pd
                INNER JOIN purchaseorder p ON pd.id_purchaseorder = p.purchaseorder_id
                INNER JOIN book b ON pd.id_book = b.book_id
                WHERE p.purchaseorder_date BETWEEN '".$date_from."' AND '".$date_to."'";

        return $this->db->query($sql)->result();
    }
}
