<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emp_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();   
    }
    
    private function addtelemp_model($ar_emp_tel=array())
    {
        $this->db->trans_begin();
        
        $sqltel = "INSERT INTO telemployee(id_employee,telemployee_tel) VALUES(?,?)";
        $this->db->query($sqltel,$ar_emp_tel);
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else 
        {
            $this->db->trans_commit();
            return true;
        }
    }
    
    private function deltelemp_model($ar_emp_tel=array())
    {
        $this->db->trans_begin();
        
        $sqltel = "DELETE FROM telemployee
        WHERE id_employee=? AND telemployee_tel=?";
        $this->db->query($sqltel,$ar_emp_tel);
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else 
        {
            $this->db->trans_commit();
            return true;
        }
    }
    
    public function editemp_model($ar=array())
    {
        $this->db->trans_begin();
        
        $sqledit = "UPDATE employee SET employee_name=?,employee_lname=?,employee_sex=?,
        employee_brithday=?,employee_image=?,employee_address=?,employee_email=?,
        employee_idcard=?,employee_username=?,id_postcode=?,id_province=?,id_zone=?,
        id_district=?,id_position=?
        WHERE employee_id=?";
        $this->db->query($sqledit,$ar);
        
        if($ar['teldel']!="")
            foreach($ar['teldel'] as $deltels)
                $this->deltelemp_model(array('id'=>$ar['idemp'],'tel'=>$deltels));
            
        if($ar['tel']!="")
            foreach($ar['tel'] as $tels)
                $this->addtelemp_model(array('id'=>$ar['idemp'],'tel'=>$tels));
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else 
        {
            $this->db->trans_commit();
            return true;
        }
    }
    
    public function addemp_model($ar=array())
    {
        $this->db->trans_begin();
        
        $sqladd = "INSERT  INTO employee
        (employee_id,employee_name,employee_lname,employee_sex,employee_brithday,employee_image,           employee_address,employee_email,employee_idcard,employee_username,employee_password,
        id_postcode,id_province,id_zone,id_district,id_position,employee_status) 
        VALUES (?,?,?,?,?,?,?,?,?,?,SHA1(?),?,?,?,?,?,'1')";
        $this->db->query($sqladd,$ar);
        
        //$sqltel = "INSERT INTO telemployee(id_employee,telemployee_tel) VALUES(?,?)";
        
        foreach($ar['tel'] as $tels)
            //$this->db->query($sqltel,array('id'=>$ar['idemp'],'tel'=>$tels));
            $this->addtelemp_model(array('id'=>$ar['idemp'],'tel'=>$tels));
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else 
        {
            $this->db->trans_commit();
            return true;
        }
    }
    
    public function deleteemp_model($idemp='')
    {
        $result['status']=false;
        $result['image']=$this->get_image_emp_model($idemp)->emp_image;
        $sqldelete = "DELETE FROM employee WHERE employee_id=?";
        $result['data']=$this->db->query($sqldelete,$idemp); 
        if(!$result['data'])
        {
            $sqldelete = "UPDATE employee SET employee_status = '0' WHERE employee_id=?";
            $this->db->query($sqldelete,$idemp);
        }

        return $result;
    }
    
    public function selectemp_model($offset=0)
    {
        $sqlselect = "SELECT                    
	    employee_id,employee_name,employee_lname,employee_email,employee_idcard,
        employee_username,position_name,telemployee_tel
        FROM employee 
        INNER JOIN POSITION ON id_position=position_id
        LEFT JOIN telemployee ON employee_id=id_employee
        WHERE employee_status='1'
        GROUP BY employee_id LIMIT ".$offset.",10";
        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }
    
    public function selectdataemp_model($idemp)
    {
        $sqlselectdataemp = "SELECT        `employee`.*,`position`.`position_name`,`province`.`province_name`,zone.`zone_name`,
        `district`.`district_name`,`postcode`.`postcode_id`,`telemployee`.`telemployee_tel`
        FROM employee INNER JOIN `position` ON employee.`id_position` = `position`.`position_id`
        INNER JOIN `province` ON `employee`.`id_province`=`province`.`province_id`
        INNER JOIN zone ON `employee`.`id_zone`=zone.`zone_id` AND
        `employee`.`id_province`=`zone`.`id_province`
        INNER JOIN `district` ON `employee`.`id_district`=`district`.`district_id` AND
        `employee`.`id_zone`=`district`.`id_zone`
        AND `employee`.`id_province`=`district`.`id_province` 
        INNER JOIN `postcode` ON `employee`.`id_postcode`=`postcode`.`postcode_id` AND
        `employee`.`id_district`=`postcode`.`id_district`
        AND `employee`.`id_zone`=`postcode`.`id_zone` AND
        `employee`.`id_province`=`postcode`.`id_province`
        LEFT JOIN `telemployee` ON `employee`.`employee_id`=`telemployee`.`id_employee` 
        WHERE employee_id =? AND employee_status='1'";
        $dataselectemp = $this->db->query($sqlselectdataemp,$idemp)->result();
        return $dataselectemp;
    }
    
    public function countemp_model()
    {
        $sqlcount = "SELECT COUNT(1)
        FROM employee
        WHERE employee_status='1'";
        $querycount = $this->db->query($sqlcount)->row();
        return $querycount;
    }
    
    public function selectusernameemp_model($user)
    {
        $sqlusernane = "SELECT member_username
        FROM member
        WHERE member_username = ?
        UNION ALL
        SELECT employee_username
        FROM employee
        WHERE employee_username = ?"; 
        $username_check = $this->db->query($sqlusernane,array($user,$user))->result();
        return $username_check;
    }
    
    public function autoidemp_model()
    {
        $sqlautoid = "SELECT                   CONVERT(IFNULL(CONCAT('EMP',LPAD(SUBSTRING(MAX(employee_id),4,5)+1,5,'0')),'EMP00001') USING utf8) 
        AS autoid FROM employee"; 
        $autoid = $this->db->query($sqlautoid)->result();
        return $autoid;
    }
    
    public function searchemp_model($arr=array())
    {
        $i=0;
        $sqlselect = "SELECT                     
	    employee_id,employee_name,employee_lname,employee_email,employee_idcard,
        employee_username,position_name,telemployee_tel
        FROM employee 
        INNER JOIN POSITION ON id_position=position_id
        LEFT JOIN telemployee ON employee_id=id_employee
        WHERE ";
        
        foreach ($arr['selectmessage'] as $value)
        {
            if($value == 'id')
                $sqlselect .= "employee_id LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'name')
                $sqlselect .= "employee_name LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'lname')
                $sqlselect .= "employee_lname LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'username')
                $sqlselect .= "employee_username LIKE '%".$arr['inputsearch']."%'";
            else
                $sqlselect .= "employee_name LIKE '%".$arr['inputsearch']."%' OR employee_username LIKE '%".$arr['inputsearch']."%'";
                
            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= ' AND ';
            else
                $sqlselect .= ' OR ';
                
            $i++;
        }
        
        $sqlselect .= "employee_status='1'
        GROUP BY employee_id LIMIT ".$arr['offset'].",10";
        
        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }
    
    public function get_image_emp_model($id)
    {
        $sql = "SELECT SUBSTRING(employee_image,28) AS emp_image
        FROM employee
        WHERE employee_id = ?";
        
        $query = $this->db->query($sql,$id)->row();
        return $query;
    }
    
    public function countsearchemp_model($arr=array())
    {
        $i=0;
        $sqlselect = "SELECT COUNT(1) as countsearchemp
        FROM employee 
        INNER JOIN POSITION ON id_position=position_id
        WHERE ";
        
        foreach ($arr['selectmessage'] as $value)
        {
            if($value == 'id')
                $sqlselect .= "employee_id LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'name')
                $sqlselect .= "employee_name LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'lname')
                $sqlselect .= "employee_lname LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'username')
                $sqlselect .= "employee_username LIKE '%".$arr['inputsearch']."%'";
            else
                $sqlselect .= "employee_name LIKE '%".$arr['inputsearch']."%' OR employee_username LIKE '%".$arr['inputsearch']."%' OR employee_lname LIKE '%".$arr['inputsearch']."%'";
                
            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= ' AND ';
            else
                $sqlselect .= ' OR ';
                
            $i++;
        }
        
        $sqlselect .= "employee_status='1'";
        
        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }
    
    public function edit_password_emp_model($data = array())
    {
        $this->db->trans_begin();
        
        $sql = "UPDATE employee SET
                employee_password = SHA1(?)
                WHERE employee_id = ?";
        
        $this->db->query($sql,$data);
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else 
        {
            $this->db->trans_commit();
            return true;
        }
    }
    
    public function selectemp_where_id_model($idemp)
    {
        $sqlselect="SELECT employee_id
                    FROM employee
                    WHERE employee_id=?";
        $queryselect = $this->db->query($sqlselect,$idemp)->result();
        
        return $queryselect!=null?true:false;
    }
}
