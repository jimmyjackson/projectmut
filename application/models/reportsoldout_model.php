<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportsoldout_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();
    }

    public function selectreport_model($date_from, $date_to)
    {
        $sql = "SELECT CURDATE() AS cur_date,bo.book_id,bo.book_name,cc.book_fixed,cc.book_lost
                FROM book bo
                INNER JOIN
                (SELECT IFNULL(a.id_book,b.id_book) AS book_id,
                IFNULL(a.book_name,b.book_name) AS book_name,
                IFNULL(a.count_fixed,0) AS book_fixed,
                IFNULL(b.count_lost,0) AS book_lost
                FROM
                (SELECT
                  s1.id_book,
                  b1.book_name,
                  COUNT(s1.id_book) AS count_fixed
                FROM soldout s1
                INNER JOIN book b1 ON s1.id_book = b1.book_id
                WHERE s1.soldout_detial = 'ชำรุด' AND s1.soldout_date BETWEEN '".$date_from."' AND '".$date_to."'
                GROUP BY s1.id_book) a
                LEFT JOIN
                (SELECT
                  s2.id_book,
                  b2.book_name,
                  COUNT(s2.id_book) AS count_lost
                FROM soldout s2
                INNER JOIN book b2 ON s2.id_book = b2.book_id
                WHERE s2.soldout_detial = 'สูญหาย' AND s2.soldout_date BETWEEN '".$date_from."' AND '".$date_to."'
                GROUP BY s2.id_book) b
                ON a.id_book = b.id_book) cc
                ON bo.book_id = cc.book_id";

        return $this->db->query($sql)->result();
    }

}
