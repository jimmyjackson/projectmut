<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchaseorder_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();
    }

    public function autoidgroupofferorder_model()
    {
        $sqlautoid = "SELECT CONVERT(IFNULL(CONCAT('GOF',LPAD(SUBSTRING(MAX(groupofferorder_id),4,7)+1,7,'0')),'GOF0000001') USING utf8) AS autoid FROM groupofferorder";

        return $this->db->query($sqlautoid)->result();
    }

    public function autoidpurchaseorder_model()
    {
        $sqlautoid = "SELECT CONVERT(IFNULL(CONCAT('PUR',LPAD(SUBSTRING(MAX(purchaseorder_id),4,7)+1,7,'0')),'PUR0000001') USING utf8) AS autoid FROM purchaseorder";

        return $this->db->query($sqlautoid)->result();
    }

    public function selectlistapproveordergroupofferorder_model($ar = array())
    {
        $sql = "SELECT of.offerorder_id,ofd.offerorderdetial_no,ofd.id_book,
                      ofd.offerorderdetial_approveunit,ofd.offerorderdetial_approveprice
                      FROM offerorder of INNER JOIN offerorderdetial ofd
                      ON of.offerorder_id=ofd.id_offerorder
                      WHERE of.offerorder_status='อนุมัติ' AND of.id_publishing = ?
                      AND ofd.id_book = ?
                      AND of.offerorder_id IN (";
        $sql .= $this->loop_substr_id_model($ar['purchaseorderlist']);
        $sql .= ")";

        return $this->db->query($sql,array(
          'publishing'=>$ar['publishing'],
          'bookid'=>$ar['bookid']
        ))->result();
    }

    public function selectvatapproveorder_model($ar = array())
    {
        $sql = "SELECT offerorder_offervat
                FROM offerorder
                WHERE offerorder_id IN (";
        $sql .= $this->loop_substr_id_model($ar['purchaseorderlist']);
        $sql .= ")";

        return $this->db->query($sql)->result();
    }

    public function selectsumapproveordergroupofferorder_model($ar = array())
    {
        $sql = "SELECT ofd.id_book,
                SUM(ofd.offerorderdetial_approveunit) AS groupapproveunit ,
                SUM(ofd.offerorderdetial_approveprice) AS groupapproveprice
                FROM offerorder of INNER JOIN offerorderdetial ofd
                ON of.offerorder_id=ofd.id_offerorder
                WHERE of.offerorder_status='อนุมัติ' AND of.id_publishing = ?
                AND of.offerorder_id IN (";
        $sql .= $this->loop_substr_id_model($ar['purchaseorderlist']);
        $sql .= ") GROUP BY ofd.id_book";

        return $this->db->query($sql,$ar['publishing'])->result();
    }

    public function loop_substr_id_model($ar = array())
    {
      $str = "";
      $i = 0;

      foreach ($ar as $value)
      {
        $str .= "'".$value."'";

        if(($i==sizeof($ar)-1))
            $str .= '';
        else
            $str .= ',';

        $i++;
      }

      return $str;
    }

    public function addgroupofferorder_model($ar = array())
    {
        $sql = "INSERT INTO groupofferorder
                      (groupofferorder_id,
                      groupofferorder_unittotal,
                      groupofferorder_pricetotal,
                      id_book)
                      VALUES (?,?,?,?)";

        $this->db->query($sql,$ar);
    }

    public function adddetialgroupofferorder_model($ar = array())
    {
        $sql = "INSERT INTO groupofferorderdetial
                (id_offerorder,
                 no_offerorderdetial,
                 id_groupofferorder,
                 groupofferorderdetial_unit,
                 groupofferorderdetial_price)
                VALUES (?,?,?,?,?)";

        $this->db->query($sql,$ar);
    }

    public function addpurchaseorder_model($ar = array())
    {
        $sql = "INSERT INTO purchaseorder
                (purchaseorder_id,
                purchaseorder_date,
                purchaseorder_pricetotal,
                purchaseorder_vat,
                purchaseorder_vattotal,
                purchaseorder_subpricetotal,
                purchaseorder_status,
                id_employee,
                id_publishing)
                VALUES (?,CURDATE(),?,?,?,?,'สั่งซื้อ',?,?)";

        $this->db->query($sql,$ar);
    }

    public function adddetialpurchaseorder_model($ar = array())
    {
        $sql = "INSERT INTO purchaseorderdetial
                (id_purchaseorder,
                purchaseorderdetial_no,
                purchaseorderdetial_unit,
                purchaseorderdetial_bookprice,
                purchaseorderdetial_sumprice,
                purchaseorderdetial_balanceunit,
                id_book,
                id_groupofferorder)
                VALUES (?,?,?,?,?,?,?,?)";

        $this->db->query($sql,$ar);
    }

    public function selectsumapproveorderpurchaseorder_model($ar = array())
    {
        $sql = "SELECT offerorder_id,SUM(offerorder_approvepricetotal) as pricetotal,
                SUM(offerorder_approvesubtotal) as subtotal,offerorder_offervat as vat,
                SUM(offerorder_approvevattotal) as vattotal
                FROM offerorder
                WHERE offerorder_id IN (";
        $sql .= $this->loop_substr_id_model($ar['purchaseorderlist']);
        $sql .= ")";

        return $this->db->query($sql)->result();
    }

    public function selectsumapproveorderpurchaseorderdetial_model($groupofferorder_id)
    {
        $sql = "SELECT DISTINCT gof.groupofferorder_id,
                gof.groupofferorder_unittotal,
                gof.groupofferorder_pricetotal,
                gof.id_book,ofd.pricebook
                FROM groupofferorder gof
                INNER JOIN groupofferorderdetial gofd ON groupofferorder_id = gofd.id_groupofferorder
                INNER JOIN offerorderdetial ofd ON gofd.id_offerorder = ofd.id_offerorder AND gofd.no_offerorderdetial = ofd.offerorderdetial_no
                WHERE gof.groupofferorder_id = ? AND gof.groupofferorder_unittotal != 0";

        return $this->db->query($sql,$groupofferorder_id)->result();
    }

    public function selectpurchaseorder_where_id_model($id)
    {
        $sqlselect="SELECT purchaseorder_id
                    FROM purchaseorder
                    WHERE purchaseorder_id=?";
        $queryselect = $this->db->query($sqlselect,$id)->result();

        return $queryselect!=null?true:false;
    }

    public function updatestatusofferorder_model($id)
    {
        $sql = "UPDATE offerorder
                SET offerorder_status = 'สั่งซื้อแล้ว'
                WHERE offerorder_id =?";

        $this->db->query($sql,$id);
    }

    public function selectdatapurchaseorder_model($id)
    {
        $sqlselectdata = "SELECT
                          p.purchaseorder_id,
                          p.purchaseorder_date,
                          p.purchaseorder_pricetotal,
                          p.purchaseorder_vat,
                          p.purchaseorder_vattotal,
                          p.purchaseorder_subpricetotal,
                          p.id_employee,
                          p.id_publishing,
                          pd.purchaseorderdetial_no,
                          pd.purchaseorderdetial_unit,
                          pd.purchaseorderdetial_bookprice,
                          pd.purchaseorderdetial_sumprice,
                          pd.purchaseorderdetial_balanceunit,
                          b.book_name
                          FROM purchaseorder p
                          INNER JOIN purchaseorderdetial pd ON purchaseorder_id = id_purchaseorder
                          INNER JOIN book b ON pd.id_book = b.book_id
                          WHERE purchaseorder_id = ?";
        return $this->db->query($sqlselectdata,$id)->result();
    }

    public function selectdatapurchaseorderreport_model($id)
    {
        $sqlselectdata = "SELECT
                          p.purchaseorder_id,
                          p.purchaseorder_date,
                          p.purchaseorder_pricetotal,
                          p.purchaseorder_vat,
                          p.purchaseorder_vattotal,
                          p.purchaseorder_subpricetotal,
                          p.id_employee,
                          p.id_publishing,
                          pd.purchaseorderdetial_no,
                          pd.purchaseorderdetial_unit,
                          pd.purchaseorderdetial_bookprice,
                          pd.purchaseorderdetial_sumprice,
                          pd.purchaseorderdetial_balanceunit,
                          b.book_name,
                          e.employee_name,
                          e.employee_lname,
                          pb.publishing_name
                          FROM purchaseorder p
                          INNER JOIN purchaseorderdetial pd ON p.purchaseorder_id = pd.id_purchaseorder
                          INNER JOIN book b ON pd.id_book = b.book_id
                          INNER JOIN employee e ON p.id_employee=e.employee_id
                          INNER JOIN publishing pb ON p.id_publishing=pb.publishing_id
                          WHERE purchaseorder_id = ?";
        return $this->db->query($sqlselectdata,$id)->result();
    }

    public function selectpurchaseorder_model($offset=0)
    {
        $sqlselect = "SELECT purchaseorder_id,purchaseorder_date,
                      id_employee,publishing_name,purchaseorder_status
                      FROM purchaseorder
                      INNER JOIN publishing ON id_publishing = publishing_id
                      LIMIT ".$offset.",10";

        return $this->db->query($sqlselect)->result();
    }

    public function countpurchaseorder_model()
    {
        $sqlcount = "SELECT COUNT(1) AS count_purchaseorder
        FROM purchaseorder";

        return $this->db->query($sqlcount)->row();
    }

    public function searchpurchaseorder_model($arr=array())
    {
        $i=0;

        $sqlselect = "SELECT purchaseorder_id,purchaseorder_date,
                      id_employee,publishing_name,purchaseorder_status
                      FROM purchaseorder
                      INNER JOIN publishing ON id_publishing = publishing_id
                      WHERE ";

        foreach ($arr['selectmessage'] as $value)
        {
          if($value == 'id')
              $sqlselect .= "purchaseorder_id LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'date')
              $sqlselect .= "purchaseorder_date = '".$arr['inputsearch']."'";
          else if($value == 'idemp')
              $sqlselect .= "id_employee LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'publishing')
              $sqlselect .= "publishing_name LIKE '%".$arr['inputsearch']."%'";
          else
              $sqlselect .= "purchaseorder_id LIKE '%".$arr['inputsearch']."%'";

            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= '';
            else
                $sqlselect .= ' OR ';

            $i++;
        }

        $sqlselect .= " LIMIT ".$arr['offset'].",10";

        return $this->db->query($sqlselect)->result();
    }

    public function countsearchpurchaseorder_model($arr=array())
    {
        $i=0;

        $sqlselect = "SELECT COUNT(1) AS count_purchaseorder
                      FROM purchaseorder
                      INNER JOIN publishing ON id_publishing = publishing_id
                      WHERE ";

        foreach ($arr['selectmessage'] as $value)
        {
          if($value == 'id')
              $sqlselect .= "purchaseorder_id LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'date')
              $sqlselect .= "purchaseorder_date = '".$arr['inputsearch']."'";
          else if($value == 'idemp')
              $sqlselect .= "id_employee LIKE '%".$arr['inputsearch']."%'";
          else if($value == 'publishing')
              $sqlselect .= "publishing_name LIKE '%".$arr['inputsearch']."%'";
          else
              $sqlselect .= "purchaseorder_id LIKE '%".$arr['inputsearch']."%'";

            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= '';
            else
                $sqlselect .= ' OR ';

            $i++;
        }

        return $this->db->query($sqlselect)->result();
    }

}
