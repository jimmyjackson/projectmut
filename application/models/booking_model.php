<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking_Model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getbook($param)
    {
        $sqlselect = "SELECT
                        *
                        FROM borrowingdetial a
                        LEFT JOIN bookcopy b ON a.id_bookcopy=b.bookcopy_id
                        LEFT JOIN book c ON b.id_book=c.book_id
                        WHERE a.is_return='0' AND a.id_bookcopy NOT IN
                        (SELECT id_bookcopy FROM booking INNER JOIN bookingdetial
                        ON booking_id = id_booking WHERE bookingdetial_status = 'จอง')
                        ";
        if(!empty($param['bookcall'])){
            $sqlselect .= 'AND b.bookcopy_callno LIKE "%'.$param['bookcall'].'%" ';
        }
        if(!empty($param['bookname'])){
            $sqlselect .= 'AND c.book_name LIKE "%'.$param['bookname'].'%" ';
        }
        if(!empty($param['publishing'])){
            $sqlselect .= 'AND c.id_publishing="'.$param['publishing'].'" ';
        }
        if(!empty($param['bookcopy'])){
            $sqlselect .= 'AND b.bookcopy_id="'.$param['bookcopy'].'" ';
        }
        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }

    public function getbooking($param)
    {
        $sqlselect = "SELECT
                        *
                        FROM booking a
                        INNER JOIN bookingdetial b ON a.booking_id=b.id_booking
                        INNER JOIN bookcopy c ON a.id_bookcopy=c.bookcopy_id
                        INNER JOIN book d ON d.book_id=c.id_book
                        WHERE b.bookingdetial_status='จอง'
                        ";

        if(!empty($param['bookcall'])){
            $sqlselect .= 'AND c.bookcopy_callno="'.$param['bookcall'].'" ';
        }
        if(!empty($param['bookname'])){
            $sqlselect .= "AND d.book_name LIKE '%".$param['bookname']."%' ";
        }
        if(!empty($param['member'])){
            $sqlselect .= 'AND a.id_member="'.$param['member'].'" ';
        }
        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }


    public function borrowing($param)
    {
        $sqlselect = "SELECT
                        *
                        FROM borrowing a
                        INNER JOIN borrowingdetial b ON a.borrowing_id=b.id_borrowing
                        INNER JOIN bookcopy c ON b.id_bookcopy=c.bookcopy_id
                        INNER JOIN book d ON d.book_id=c.id_book
                        WHERE b.is_return='0' AND a.id_member='".$param['member']."'
                        ";
        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }

    public function updatebooking($param)
    {
        if($param['type'] == 'delete'){
            $sqlUpdate = "UPDATE bookingdetial SET
            bookingdetial_status = 'ยกเลิก'
            WHERE id_booking='".$param['save_id']."'";
            $this->db->query($sqlUpdate);
        }elseif($param['type'] == 'save'){


            $sqlselect = "SELECT
                            *
                            FROM booking a
                            INNER JOIN bookingdetial b ON a.booking_id=b.id_booking
                            INNER JOIN bookcopy c ON a.id_bookcopy=c.bookcopy_id
                            INNER JOIN book d ON d.book_id=c.id_book
                            WHERE a.booking_id='".$param['save_id']."'
                            ";
            $queryselect = $this->db->query($sqlselect)->result();

            $check['add_member'] = $queryselect[0]->id_member;
            $dataMember = $this->booking_model->member($check);

            $check['member'] = $queryselect[0]->id_member;
            $dataBorrowing = $this->booking_model->borrowing($check);

            if($dataMember[0]->membertype_unit < (count($dataBorrowing) + 1)){
                return 'unit';
                exit;
            }


            $sqlautoid = "SELECT CONVERT(IFNULL(CONCAT('BOR',LPAD(SUBSTRING(MAX(borrowing_id),4,7)+1,7,'0')),'BOR0000001') USING utf8) AS autoid FROM borrowing";
            $autoId = $this->db->query($sqlautoid)->result();

            $sqlBorrowing = "INSERT INTO borrowing
                  (borrowing_id, borrowing_date, id_employee, id_member)
                VALUES('".$autoId[0]->autoid."', '".date('Y-m-d')."', '".$this->session->userdata('idemp')."', '".$queryselect[0]->id_member."')";
            $this->db->query($sqlBorrowing);


            $date = $dataMember[0]->membertype_date;
            $dayReturn = date('Y-m-d', strtotime("+$date days", strtotime(date("Y-m-d"))));


            $sqlBorrowingDetail = "INSERT INTO borrowingdetial
              (id_borrowing, borrowingdetial_no, borrowingdetial_datereturn, borrowingdetial_status, id_bookcopy, id_book, is_return)
            VALUES ('".$autoId[0]->autoid."', '1', '".$dayReturn."', 'ยืม', '".$queryselect[0]->id_bookcopy."', '".$queryselect[0]->id_book."', '0')";
            $this->db->query($sqlBorrowingDetail);

            $sqlBorrowingBooking = "INSERT INTO borrowingbooking
              (id_borrowing, no_borrowingdetial, id_booking, no_bookingdetial)
            VALUES ('".$autoId[0]->autoid."', '1', '".$param['save_id']."', '1')";
            $this->db->query($sqlBorrowingBooking);


            $sqlUpdate = "UPDATE bookcopy SET
            bookcopy_status = 'ยืม'
            WHERE bookcopy_id='".$queryselect[0]->id_bookcopy."'";
            $this->db->query($sqlUpdate);

            $sqlUpdate = "UPDATE bookingdetial SET
            bookingdetial_status = 'ยืม'
            WHERE id_booking='".$param['save_id']."'";
            $this->db->query($sqlUpdate);
        }

    }

    public function getdatabook($param)
    {
        $sqlselect = "SELECT
                        *
                        FROM bookcopy a
                        LEFT JOIN book b ON a.id_book=b.book_id
                        WHERE a.bookcopy_id='".$param."'
                        ";
        $queryselect = $this->db->query($sqlselect)->result();

        return $queryselect;
    }


    public function member($param)
    {
        $sqlselect = "SELECT
                        *
                        FROM member a
                        INNER JOIN membertype b ON a.id_membertype=b.membertype_id
                        WHERE a.member_status='1' AND a.member_id='".$param['add_member']."'
                        ";
        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }

    public function save($param)
    {

        $dataMember = $this->booking_model->member($param);
        $dataBook = $this->booking_model->getdatabook($param['add_id']);

        $params['bookcopy'] = $param['add_id'];
        $dataBook = $this->booking_model->getbook($params);

        $sqlautoid = "SELECT CONVERT(IFNULL(CONCAT('BKT',LPAD(SUBSTRING(MAX(bookingtype_id),4,7)+1,7,'0')),'BKT0000001') USING utf8) AS autoid FROM bookingtype";
        $autoId = $this->db->query($sqlautoid)->result();

        $sqlBookingtype = "INSERT INTO bookingtype
              (bookingtype_id, bookingtype_name, bookingtype_status)
            VALUES('".$autoId[0]->autoid."', 'จอง', 'จอง')";
        $this->db->query($sqlBookingtype);


        $sqlautoid = "SELECT CONVERT(IFNULL(CONCAT('BOI',LPAD(SUBSTRING(MAX(booking_id),4,7)+1,7,'0')),'BOI0000001') USING utf8) AS autoid FROM booking";
        $autoId2 = $this->db->query($sqlautoid)->result();

        $sqlBooking = "INSERT INTO booking
              (booking_id, booking_date, id_booktype, id_book, id_bookcopy, id_member)
            VALUES('".$autoId2[0]->autoid."', '".date('Y-m-d')."', '".$autoId[0]->autoid."', '".$dataBook[0]->id_book."', '".$param['add_id']."', '".$param['add_member']."')";
        $this->db->query($sqlBooking);


        $sqlBookingcounter = "INSERT INTO bookingcounter
              (id_booking, id_employee)
            VALUES('".$autoId2[0]->autoid."', '".$this->session->userdata('idemp')."')";
        $this->db->query($sqlBookingcounter);

        $sqlBookingdetial = "INSERT INTO bookingdetial
              (id_booking, bookingdetial_no, bookingdetial_datereceiptbook, bookingdetial_status)
            VALUES('".$autoId2[0]->autoid."', '1', '".$dataBook[0]->borrowingdetial_datereturn."', 'จอง')";
        $this->db->query($sqlBookingdetial);
    }

    public function checkBook($param)
    {
        $sql = "SELECT COUNT(1) AS checkbook
                FROM borrowing d
                INNER JOIN borrowingdetial a ON d.borrowing_id = a.id_borrowing
                WHERE a.is_return='0' AND d.id_member = '".$param['add_member']."' AND a.id_bookcopy = '".$param['add_id']."'";

        return $this->db->query($sql)->row();
    }

    public function checkBorrow($param)
    {
        $sql = "SELECT COUNT(1) AS countborrow
                FROM borrowing d
                INNER JOIN borrowingdetial a ON d.borrowing_id = a.id_borrowing
                WHERE a.is_return='0' AND d.id_member = '".$param['add_member']."'";

        return $this->db->query($sql)->row();
    }

    public function memberUnit($param)
    {
        $sql = "SELECT b.membertype_unit
                FROM member a
                INNER JOIN membertype b ON a.id_membertype=b.membertype_id
                WHERE a.member_status='1' AND a.member_id='".$param['add_member']."'";

        return $this->db->query($sql)->row();
    }

    public function checkbooking($id_booking)
    {
      $sqlselect="SELECT COUNT(1) AS countbooking
                  FROM booking
                  INNER JOIN bookingdetial
                  ON booking_id = id_booking
                  WHERE bookingdetial_status = 'จอง' AND id_bookcopy = '".$id_booking['add_id']."'";

      return $this->db->query($sqlselect)->row();
    }
}
