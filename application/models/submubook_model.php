<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Submubook_Model extends CI_Model
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function selectallsubmubook_model($ar=array())
    {
        $sqlselect="SELECT submubook_no,submubook_name
                    FROM submubook
                    WHERE no_mubook=?";
        $query = $this->db->query($sqlselect,$ar)->result();
        return $query;
    }
}