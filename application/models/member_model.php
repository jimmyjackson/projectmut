<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("yearstudy_model");
    }

    public function deletemember_model($idmember='')
    {
        $result['status']=false;
        $result['image']=$this->get_image_member_model($idmember)->member_image;
        $sqldelete = "DELETE FROM member WHERE member_id=?";
        $result['data']=$this->db->query($sqldelete,$idmember);
        if(!$result['data'])
        {
            $sqldelete = "UPDATE member SET member_status = '0' WHERE member_id=?";
            $this->db->query($sqldelete,$idmember);
            $result['status']=true;
        }

        return $result;
    }

    public function editmember_model($ar=array())
    {

        $this->db->trans_begin();

        $yearstudy_result = $this->get_yearmemberstudent_model($ar['id'])->result();

        $sqledit = "UPDATE member SET member_name=?,member_lname=?,
        member_brithday=?,member_address=?,member_image=?,
        member_email=?,member_sex=?,member_registerdate=?,
        member_status=?,id_membertype=?,id_postcode=?,
        id_province=?,id_zone=?,id_district=?
        WHERE member_id=?";
        $this->db->query($sqledit,$ar);

        $this->del_student_model($ar['id']);
        $this->del_personal_model($ar['id']);

        if($ar['typemember']=='MEMTY00001'){

            $this->add_student_model(array(
                'id_student'=>$ar['id'],
                'student_id'=>$ar['idcardmember']
            ));

            if($yearstudy_result[0]->id_yearstudy != null){
                $this->add_yearmemberstudent_model(array(
                    'id_memberstudent'=>$ar['id'],
                    'id_yearstudy'=>$yearstudy_result[0]->id_yearstudy,
                    'semester'=>$yearstudy_result[0]->semester
                ));
            }else{
                $yearstudy_rest = $this->yearstudy_model->get_last_semester_and_year_model();

                $this->add_yearmemberstudent_model(array(
                    'id_memberstudent'=>$ar['id'],
                    'id_yearstudy'=>$yearstudy_rest->yearstudy,
                    'semester'=>$yearstudy_rest->semester
                ));
            }

        }
        else{
            $this->add_personal_model(array(
                'id_personal'=>$ar['id'],
                'personal_id'=>$ar['idcardmember']
            ));

            $this->add_tel_personal_model(
                $ar['id'],
                $ar['tels']
            );
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $this->db->trans_commit();
            return $ar['id'];
        }
    }

    public function addmember_model($ar=array())
    {
        $this->db->trans_begin();

        $sqladd = "INSERT INTO member (member_id, member_name, member_lname,
            member_brithday, member_address, member_image, member_email,
            member_sex, member_username, member_password, member_registerdate,
            member_status, id_membertype, id_postcode, id_province, id_zone, id_district)
            values(?,?,?,?,?,?,?,?,?,SHA1(?),?,?,?,?,?,?,?);";

        $this->db->query($sqladd,$ar);

        if($ar['typemember']=="MEMTY00001"){
            $yearstudy_result = $this->yearstudy_model->get_last_semester_and_year_model();

            $this->add_student_model(array(
                'id_student'=>$ar['id'],
                'student_id'=>$ar['idcardmember']
            ));
            $this->add_yearmemberstudent_model(array(
                'id_memberstudent'=>$ar['id'],
                'id_yearstudy'=>$yearstudy_result->yearstudy,
                'semester'=>$yearstudy_result->semester
            ));
        }
        else{
            $this->add_personal_model(array(
                'id_personal'=>$ar['id'],
                'personal_id'=>$ar['idcardmember']
            ));

            $this->add_tel_personal_model(
                $ar['id'],
                $ar['tels']
            );

        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $this->db->trans_commit();
            return $ar['id'];
        }
    }

    public function add_student_model($ar=array()){
        $sql="insert into student (id_student,student_id) values(?,?)";
        $this->db->query($sql,$ar);
    }

    public function del_student_model($id){
        $sql="DELETE FROM student WHERE id_student = ?";
        $this->db->query($sql,$id);
    }

    public function add_personal_model($ar=array()){
        $sql="insert into personnel (id_member,personnel_idcard) values(?,?)";
        $this->db->query($sql,$ar);
    }

    public function del_personal_model($id){
        $sql="DELETE FROM personnel WHERE id_member = ?";
        $this->db->query($sql,$id);
    }

    public function add_tel_personal_model($id,$ar=array()){
        foreach($ar as $value){
            $sql="insert into telpersonnel (id_member,telpersonnel_tel) values(?,?)";
            $this->db->query($sql,array('id'=>$id,'tel'=>$value));
        }
    }

    public function del_tel_personal_model($idmember){
        $sql="DELETE FROM telpersonnel WHERE id_member = ?";
        $this->db->query($sql,$idmember);
    }

    public function get_tel_personal_model($idmember){
        $sql="SELECT telpersonnel_tel FROM telpersonnel WHERE id_member = ?";
        $this->db->query($sql,$idmember);
    }

    public function get_member_type_model($idmember){
        $sql="SELECT id_membertype FROM member WHERE member_id=?";
        $result = $this->db->query($sql,$idmember)->row();
        return $result;
    }

    public function manage_yearmemberstudent_model($id){
        $this->db->trans_begin();

        $this->del_yearmemberstudent_model($id);
        $yearstudy_result = $this->yearstudy_model->get_last_semester_and_year_model();
        $this->add_yearmemberstudent_model(array(
            'id_memberstudent'=>$id,
            'id_yearstudy'=>$yearstudy_result->yearstudy,
            'semester'=>$yearstudy_result->semester
        ));

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $this->db->trans_commit();
            return $yearstudy_result->expiration_date;
        }
    }

    public function add_yearmemberstudent_model($ar=array()){
        $sql="insert into yearmemberstudent (id_memberstudent,id_yearstudy,semester)
        values(?,?,?)";
        $this->db->query($sql,$ar);
    }

    public function del_yearmemberstudent_model($id){
        $sql="DELETE FROM yearmemberstudent WHERE id_memberstudent = ?";
        $this->db->query($sql,$id);
    }

    public function get_yearmemberstudent_model($id){
        $sql="SELECT id_yearstudy,semester FROM  yearmemberstudent WHERE id_memberstudent=?";
        return $this->db->query($sql,$id);
    }

    public function selectmember_model($offset=0)
    {
        $sqlselect = "SELECT member_id,member_name,
        member_lname,member_email,
        member_username,id_membertype,membertype_name,
        IFNULL(student_id,personnel_idcard) AS idcard
        FROM member
        INNER JOIN membertype ON id_membertype=membertype_id
        LEFT JOIN student ON member_id=id_student
        LEFT JOIN personnel ON member_id=id_member
        WHERE member_status='1' ORDER BY member_id
        LIMIT ".$offset.",10";
        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }

    public function selectdatamember_model($idmember)
    {
        $sqlselectdatamember = "SELECT member_id,member_name,member_lname,member_brithday,
        member_address,member_image,member_email,member_sex,member_username,
        member_registerdate,id_membertype,id_postcode,id_province,
        id_zone,id_district,ys.expiration_date,
        IFNULL(student_id,p.personnel_idcard) id_card,tp.telpersonnel_tel
        FROM member
        LEFT JOIN student ON member_id=id_student
        LEFT JOIN yearmemberstudent yms ON id_student=yms.id_memberstudent
        LEFT JOIN personnel p ON member_id=p.id_member
        LEFT JOIN telpersonnel tp ON p.id_member=tp.id_member
        LEFT JOIN yearstudy ys ON yms.id_yearstudy=ys.yearstudy AND yms.semester=ys.semester
        WHERE member_id=? AND member_status='1'";
        $dataselectmember = $this->db->query($sqlselectdatamember,$idmember)->result();
        return $dataselectmember;
    }

    public function countmember_model()
    {
        $sqlcount = "SELECT COUNT(1) AS count_member
        FROM member
        WHERE member_status='1'";
        $querycount = $this->db->query($sqlcount)->row();
        return $querycount;
    }

    public function countmember_student_model()
    {
        $sqlcount ="SELECT COUNT(1) AS count
        FROM member
        WHERE member_status='1'
        AND id_membertype='MEMTY00001'";
        $querycount = $this->db->query($sqlcount)->row();
        return $querycount;
    }

    public function countmember_personnel_model()
    {
        $sqlcount ="SELECT COUNT(1) AS count
        FROM member
        WHERE member_status='1'
        AND id_membertype='MEMTY00002'";
        $querycount = $this->db->query($sqlcount)->row();
        return $querycount;
    }

    public function autoidmember_model()
    {
        $sqlautoid = "SELECT
        CONVERT(IFNULL(CONCAT('MEM',LPAD(SUBSTRING(MAX(member_id),4,5)+1,5,'0')),'MEM00001')
        USING utf8) AS autoid FROM member";
        $autoid = $this->db->query($sqlautoid)->result();
        return $autoid;
    }

    public function selectusernamemember_model($user)
    {
        $sqlusernane = "SELECT member_username
        FROM member
        WHERE member_username = ?
        UNION ALL
        SELECT employee_username
        FROM employee
        WHERE employee_username = ?";
        $username_check = $this->db->query($sqlusernane,array($user,$user))->result();
        return $username_check;
    }

    public function searchmember_model($arr=array())
    {
        $i=0;
        $j=0;
        $k=0;

        $sqlselect = "SELECT member_id,member_name,
        member_lname,member_email,
        member_username,id_membertype,membertype_name,
        IFNULL(student_id,personnel_idcard) AS idcard
        FROM member
        INNER JOIN membertype ON id_membertype=membertype_id
        LEFT JOIN student ON member_id=id_student
        LEFT JOIN personnel ON member_id=id_member
        WHERE ";

        foreach ($arr['selectmessage'] as $value)
        {
            if($value == 'id')
                $sqlselect .= "member_id LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'name')
                $sqlselect .= "member_name LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'lname')
                $sqlselect .= "member_lname LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'username')
                $sqlselect .= "member_username LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'email')
                $sqlselect .= "member_email LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'idcard'){
                foreach ($arr['selectshowtype'] as $value)
                {
                    if($k>0)
                        $sqlselect .= " OR ";

                    if($value =='student')
                        $sqlselect .= "student_id LIKE '%".$arr['inputsearch']."%'";
                    else if($value =='personnel')
                        $sqlselect .= "personnel_idcard LIKE '%".$arr['inputsearch']."%'";
                    else
                        $sqlselect .= "student_id LIKE '%".$arr['inputsearch'].
                        "%' OR personnel_idcard LIKE '%".$arr['inputsearch']."%'";

                    $k++;
                }
            }

            else
                $sqlselect .= "member_name LIKE '%".$arr['inputsearch']."%' OR member_username LIKE '%".$arr['inputsearch']."%' OR member_lname LIKE '%".$arr['inputsearch']."%'";

            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= ' AND ';
            else
                $sqlselect .= ' OR ';

            $i++;
        }

        $sqlselect .= '(';

        foreach ($arr['selectshowtype'] as $value)
        {
            if($value =='student')
                $sqlselect .="id_membertype='MEMTY00001'";
            else if($value =='personnel')
                $sqlselect .="id_membertype='MEMTY00002'";
            else
                $sqlselect .="(id_membertype='MEMTY00001' OR id_membertype='MEMTY00002')";

            if(($j==sizeof($arr['selectshowtype'])-1))
                $sqlselect .= ') AND ';
            else
                $sqlselect .= ' OR ';

            $j++;
        }

        $sqlselect .= "member_status='1' ORDER BY member_id LIMIT ".$arr['offset'].",10";

        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }

    public function get_image_member_model($id)
    {
        $sql = "SELECT SUBSTRING(member_image,28) AS member_image
        FROM member
        WHERE member_id = ?";

        $query = $this->db->query($sql,$id)->row();
        return $query;
    }

    public function countsearchmember_model($arr=array())
    {
        $i=0;
        $j=0;
        $k=0;

        $sqlselect = "SELECT COUNT(1) AS count
        FROM member
        INNER JOIN membertype ON id_membertype=membertype_id
        LEFT JOIN student ON member_id=id_student
        LEFT JOIN personnel ON member_id=id_member
        WHERE ";

        foreach ($arr['selectmessage'] as $value)
        {
            if($value == 'id')
                $sqlselect .= "member_id LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'name')
                $sqlselect .= "member_name LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'lname')
                $sqlselect .= "member_lname LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'username')
                $sqlselect .= "member_username LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'email')
                $sqlselect .= "member_email LIKE '%".$arr['inputsearch']."%'";
            else if($value == 'idcard'){
                foreach ($arr['selectshowtype'] as $value)
                {
                    if($k>0)
                        $sqlselect .= " OR ";

                    if($value =='student')
                        $sqlselect .= "student_id LIKE '%".$arr['inputsearch']."%'";
                    else if($value =='personnel')
                        $sqlselect .= "personnel_idcard LIKE '%".$arr['inputsearch']."%'";
                    else
                        $sqlselect .= "student_id LIKE '%".$arr['inputsearch'].
                        "%' OR personnel_idcard LIKE '%".$arr['inputsearch']."%'";

                    $k++;
                }
            }

            else
                $sqlselect .= "member_name LIKE '%".$arr['inputsearch']."%' OR member_username LIKE '%".$arr['inputsearch']."%' OR member_lname LIKE '%".$arr['inputsearch']."%'";

            if(($i==sizeof($arr['selectmessage'])-1))
                $sqlselect .= ' AND ';
            else
                $sqlselect .= ' OR ';

            $i++;
        }

        $sqlselect .= '(';

        foreach ($arr['selectshowtype'] as $value)
        {
            if($value =='student')
                $sqlselect .="id_membertype='MEMTY00001'";
            else if($value =='personnel')
                $sqlselect .="id_membertype='MEMTY00002'";
            else
                $sqlselect .="(id_membertype='MEMTY00001' OR id_membertype='MEMTY00002')";

            if(($j==sizeof($arr['selectshowtype'])-1))
                $sqlselect .= ') AND ';
            else
                $sqlselect .= ' OR ';

            $j++;
        }

        $sqlselect .= "member_status='1' LIMIT ".$arr['offset'].",10";

        $queryselect = $this->db->query($sqlselect)->result();
        return $queryselect;
    }

    public function edit_password_member_model($data = array())
    {
        $this->db->trans_begin();

        $sql = "UPDATE member SET
                member_password = SHA1(?)
                WHERE member_id = ?";

        $this->db->query($sql,$data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $this->db->trans_commit();
            return true;
        }
    }

    public function selectmember_where_id_model($idmember)
    {
        $sqlselect="SELECT member_id
                    FROM member
                    WHERE member_id=? AND member_status='1'";
        $queryselect = $this->db->query($sqlselect,$idmember)->result();

        return $queryselect!=null?true:false;
    }

}
