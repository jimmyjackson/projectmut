<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mubook_Model extends CI_Model
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function selectallmubook_model()
    {
        $sqlselect="SELECT mubook_no,mubook_name
                    FROM mubook";
        $query = $this->db->query($sqlselect)->result();
        return $query;
    }
}