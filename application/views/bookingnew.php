<div id="content">
    <div class="inner">
        <div class="row has-success" >
            <div class="col-lg-5" style="padding-top: 30px; ">
            <h1 class="page-header">การจองหนังสือ</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">ค้นหาข้อมูลหนังสือ</div>
                    <div class="panel-body">
                        <form action="<?php echo site_url("booking_controller/newbooking"); ?>" method="post">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">เลขเรียกหนังสือ</label>
                                            <input type="text" class="form-control" id="bookcall" name="bookcall" value="<?php echo (!empty($_POST['bookcall']) ? $_POST['bookcall'] : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">ชื่อหนังสือ</label>
                                            <input type="text" class="form-control" id="bookname" name="bookname" value="<?php echo (!empty($_POST['bookname']) ? $_POST['bookname'] : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">สำนักพิมพ์</label>
                                            <select name="publishing" id="publishing" class="form-control" tabindex="2">
                                                <option value="">สำนักพิมพ์</option>
                                                <?php
                                                if(count($dataPublishing) > 0){
                                                    foreach ($dataPublishing as $value) {

                                                    ?>
                                                    <option value="<?php echo $value->publishing_id; ?>" <?php echo (!empty($_POST['publishing']) && $_POST['publishing']==$value->publishing_id ? 'selected="selected"' : ''); ?>><?php echo $value->publishing_name; ?></option>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2">
                                        <div class="form-group" style="margin-top: 25px;">
                                            <button type="submit" name="btadddetial" id="btadddetial" class="btn btn-info" tabindex="3">
                                                <i class="glyphicon glyphicon glyphicon-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if(!empty($error) && ($error=='checkBook' || $error=='checkBorrow')){
        ?>
            <div class="col-lg-12">
              <?php if($error=='checkBook') { ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <strong>เกิดข้อผิดพลาด!</strong>ไม่สามารถจองหนังสือได้ เนื่องจากคุณยืมหนังสือเล่มนี้อยู่แล้ว
                </div>
              <?php } elseif ($error=='checkBorrow') { ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <strong>เกิดข้อผิดพลาด!</strong>ไม่สามารถจองหนังสือได้ เนื่องจากคุณมีการยืมเกินจำนวนแล้ว
                </div>
              <?php } ?>
            </div>
        <?php
        }
        ?>
        <?php
        if(!empty($status) && ($status=='save' || $status=='countbooking')){
          if($status=='save') {
        ?>
            <div class="col-lg-12">
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <strong>สำเร็จ!</strong>ทำการบันทึกการจองหนังสือเรียบร้อยแล้ว
                </div>
            </div>
        <?php
          }
          if($status=='countbooking') {
        ?>
            <div class="col-lg-12">
              <div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <strong>ผิดพลาด!</strong>เนื่องจากมีรายการหนังสือบางรายการได้ถูกจองไปก่อนแล้ว กรุณาลองใหม่อีกครั้ง
              </div>
            </div>
        <?php
          }
        }
        if(isset($_POST['btadddetial'])){
            if(!empty($dataBooking) && count($dataBooking)>0){
            ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">ข้อมูลหนังสือ</div>
                            <div class="panel-body">
                                <form id="save_receipt" action="<?php echo site_url("booking_controller/newbooking"); ?>" method="post">
                                    <div class="row">
                                        <table id="example" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th><center>ลำดับ</center></th>
                                                    <th><center>เลขเรียกหนังสือ</center></th>
                                                    <th><center>ชื่อหนังสือ</center></th>
                                                    <th><center>วันที่คืน</center></th>
                                                    <th><center>จอง</center></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $aa = 0;
                                                foreach ($dataBooking as $key=>$value) {
                                                ?>
                                                    <tr id="row_<?php echo $value->bookcopy_id; ?>">
                                                        <td style="text-align: center;"><?php echo ($aa+1); ?></td>
                                                        <td style="text-align: center;"><?php echo $value->bookcopy_callno; ?></td>
                                                        <td><?php echo $value->book_name; ?></td>
                                                        <td style="text-align: center;"><?php echo $value->borrowingdetial_datereturn; ?></td>
                                                        <td style="text-align: center;">
                                                            <button type="button" name="savereceipt" id="<?php echo $value->bookcopy_id; ?>" class="btn btn-success savereceipt">
                                                                <i class="glyphicon glyphicon glyphicon-plus"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                <?php
                                                    $aa++;
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-lg-4 col-md-4 pull-right">
                                        <div class="form-group field-member">
                                            <label class="control-label">รหัสสมาชิก</label>
                                            <input type="text" class="form-control" id="member" name="member" value="<?php echo (!empty($_POST['member']) ? $_POST['member'] : ''); ?>">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }else{
            ?>
                <div class="row" style="text-align: center; padding-top: 50px;">
                    <h1>ไม่พบข้อมูลหนังสือที่ค้นหา</h1>
                </div>
            <?php
            }
        }
        if(!empty($status)){
            if($status == 'member'){
        ?>
                <div class="row" style="text-align: center; padding-top: 50px;">
                    <h1>ไม่พบรหัสสมาชิก</h1>
                </div>
        <?php
            }
        }
        ?>

    </div>
</div>

<div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ยืนยันการจองหนังสือ</h4>
            </div>
            <div class="modal-body">
                คุณต้องการจองหนังสือใช่หรือไม่?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-primary" id="btnConfirm">ยืนยัน</button>
            </div>
        </div>
    </div>
</div>

<form id="send_form" action="<?php echo site_url("booking_controller/newbooking"); ?>" method="post">
    <input type="hidden" id="add_id" name="add_id">
    <input type="hidden" id="add_member" name="add_member">
</form>
<script>
$(document).ready(function(){
    $('.savereceipt').on('click',function(){
        var id = this.id;
        var member = $('#member').val();
        if(member === ''){
            $('.field-member').addClass('has-error');
            $('.field-member .help-block').html('กรุณากรอกรหัสสมาชิก');
        }else{
            $('.field-member').removeClass('has-error');
            $('.field-member .help-block').html('');
            $('#modalConfirm').modal('show');
            $('#add_id').val(id);
            $('#add_member').val(member);
            return false;
        }
    });

    $('#btnConfirm').on('click',function(){
        $('#send_form').submit();
    });




});
</script>
