<style>
.table-responsive {
    width: 100%;
    margin-bottom: 15px;
    overflow-x: auto;
    overflow-y: hidden;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
    border: 1px solid #DDD;
}
.nowrap {
        white-space:nowrap;
    }
</style>
<div id="content">
    <div class="inner">
        <div class="row has-success" >
            <div class="col-lg-5" style="padding-top: 30px; ">
            <h1 class="page-header">ค้นหาข้อมูลหนังสือ</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">ค้นหาข้อมูลหนังสือ</div>
                    <div class="panel-body">
                        <form action="<?php echo site_url("searching_controller/index"); ?>" method="post">
                            <div class="row">
                                <div class="col-lg-12">


                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group field-member">
                                            <label class="control-label">รหัสหนังสือ</label>
                                            <input type="text" class="form-control" id="book_id" name="book_id" value="<?php echo (!empty($_POST['book_id']) ? $_POST['book_id'] : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group field-member">
                                            <label class="control-label">ชื่อหนังสือ</label>
                                            <input type="text" class="form-control" id="book_name" name="book_name" value="<?php echo (!empty($_POST['book_name']) ? $_POST['book_name'] : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group field-member">
                                            <label class="control-label">หมายเลข ISBN</label>
                                            <input type="text" class="form-control" id="book_isbn" name="book_isbn" value="<?php echo (!empty($_POST['book_isbn']) ? $_POST['book_isbn'] : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group field-member">
                                            <label class="control-label">ชื่อผู้แต่ง</label>
                                            <input type="text" class="form-control" id="author_name" name="author_name" value="<?php echo (!empty($_POST['author_name']) ? $_POST['author_name'] : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group field-member">
                                            <label class="control-label">สำนักพิมพ์</label>
                                            <select name="id_publishing" id="id_publishing" class="form-control" tabindex="2">
                                            <option value="">สำนักพิมพ์</option>
                                            <?php
                                            if(count($dataPublishing) > 0){
                                                foreach ($dataPublishing as $value) {

                                                ?>
                                                <option value="<?php echo $value->publishing_id; ?>" <?php echo (!empty($_POST['id_publishing']) && $_POST['id_publishing']==$value->publishing_id ? 'selected="selected"' : ''); ?>><?php echo $value->publishing_name; ?></option>
                                                <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group field-member">
                                            <label class="control-label">หัวเรื่อง</label>
                                            <input type="text" class="form-control" id="title_name" name="title_name" value="<?php echo (!empty($_POST['title_name']) ? $_POST['title_name'] : ''); ?>">
                                        </div>
                                    </div>
                                   <div class="col-lg-3 col-md-3">
                                        <div class="form-group field-member">
                                            <label class="control-label">รหัสสำเนาหนังสือ</label>
                                            <input type="text" class="form-control" id="bookcopy_id" name="bookcopy_id" value="<?php echo (!empty($_POST['bookcopy_id']) ? $_POST['bookcopy_id'] : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group field-member">
                                            <label class="control-label">เลขเรียกหนังสือ</label>
                                            <input type="text" class="form-control" id="bookcopy_callno" name="bookcopy_callno" value="<?php echo (!empty($_POST['bookcopy_callno']) ? $_POST['bookcopy_callno'] : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2">
                                        <div class="form-group" style="margin-top: 25px;">
                                            <button type="submit" name="btadddetial" id="btadddetial" class="btn btn-info" tabindex="3">
                                                <i class="glyphicon glyphicon glyphicon-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php
        if(isset($_POST['btadddetial'])){
            if(!empty($databookcopy) && count($databookcopy)>0){


        ?>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                      <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                  <th class="col-md-3"><center><span class="nowrap">ภาพหนังสือ</span></center></th>
                                  <th class="col-md-3"><center><span class="nowrap">รหัสสำเนาหนังสือ</span></center></th>
                                  <th class="col-md-3"><center><span class="nowrap">เลขเรียกสำเนาหนังสือ</span></center></th>
                                  <th class="col-md-3"><center><span class="nowrap">รหัสหนังสือ</span></center></th>
                                  <th class="col-md-3"><center><span class="nowrap">ชื่อหนังสือ</span></center></th>
                                  <th class="col-md-3"><center><span class="nowrap">ISBN</span></center></th>
                                  <th class="col-md-3"><center><span class="nowrap">สำนักพิมพ์</span></center></th>
                                  <th class="col-md-3"><center><span class="nowrap">หัวเรื่อง</span></center></th>
                                  <th class="col-md-3"><center><span class="nowrap">ชื่อผู้แต่ง</span></center></th>
                                  <th class="col-md-3"><center><span class="nowrap">ชื่อแปล</span></center></th>
                                  <th class="col-md-3"><center><span class="nowrap">ปี่ที่พิมพ์/ครั้งที่พิมพ์</span></center></th>
                                  <th class="col-md-3"><center><span class="nowrap">ตู้ที่:ชั้นที่:ช่องที่</span></center></th>
                                  <th class="col-md-3"><center><span class="nowrap">สถานะ</span></center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $aa = 0;
                                foreach ($databookcopy as $value){
                                ?>
                                    <tr>
                                        <td style="text-align: center;"><img src="<?php echo $value['dataBook']->book_image; ?>" height="145" width="125"></td>
                                        <td style="text-align: center;"><span class="nowrap"><?php echo $value['dataBook']->bookcopy_id; ?></span></td>
                                        <td style="text-align: center;"><span class="nowrap"><?php echo $value['dataBook']->bookcopy_callno; ?></span></td>
                                        <td style="text-align: center;"><span class="nowrap"><?php echo $value['dataBook']->book_id; ?></span></td>
                                        <td style="text-align: center;"><span class="nowrap"><?php echo $value['dataBook']->book_name; ?></span></td>
                                        <td style="text-align: center;"><span class="nowrap"><?php echo $value['dataBook']->book_isbn; ?></span></td>
                                        <td style="text-align: center;"><span class="nowrap"><?php echo $value['dataBook']->publishing_name; ?></span></td>
                                        <td style="text-align: center;"><span class="nowrap"><?php foreach ($value['title'] as $valtitle){
                                          if($valtitle->title_name != '')
                                            echo $valtitle->title_name."<br>";
                                          else
                                            echo "ไม่พบข้อมูล";
                                        } ?></span></td>
                                        <td style="text-align: center;"><span class="nowrap"><?php foreach ($value['author'] as $valauthor){
                                          if($valauthor->author != '')
                                            echo $valauthor->author."<br>";
                                          else
                                            echo "ไม่พบข้อมูล";
                                        } ?></span></td>
                                        <td style="text-align: center;"><span class="nowrap"><?php foreach ($value['translator'] as $valtranslator){
                                          if($valtranslator->translator != '')
                                            echo $valtranslator->translator."<br>";
                                          else
                                            echo "ไม่พบข้อมูล";
                                        } ?></span></td>
                                        <td style="text-align: center;"><span class="nowrap"><?php echo $value['dataBook']->edition; ?></span></td>
                                        <td style="text-align: center;"><span class="nowrap"><?php echo $value['dataBook']->positionbook; ?></span></td>
                                        <td style="text-align: center;"><span class="nowrap"><?php echo $value['dataBook']->bookcopy_status; ?></span></td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            <?php
            }else{
            ?>
                <div class="row" style="text-align: center; padding-top: 50px;">
                    <h1>ไม่พบข้อมูลที่ค้นหา</h1>
                </div>
            <?php
            }
        }
        ?>
    </div>
</div>
