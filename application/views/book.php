<!--PAGE CONTENT -->
<div id="content">
    <div class="inner">
        <div class="row has-success" >
            <div class="col-lg-5" style="padding-top: 30px; ">
            <h1 class="page-header">หนังสือห้องสมุด</h1>
            </div>
        </div>
        <input type="hidden" id="base" />
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">ข้อมูลของหนังสือ</div>
                    <div class="panel-body">
                        <form id="bookForm" action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-6">
                                <span role="form">
                                    <div class="form-group">    
                                        <label class="control-label col-lg-4">รูปประจำตัว</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="<?php echo base_url()?>assets/images/demoUpload.jpg" id="imageprofile"/>
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">

                                            </div>
                                            <div>
                                                <span class="btn btn-file btn-primary">
                                                    <span class="fileupload-new">
                                                        <i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;Select image
                                                    </span>
                                                    <input type="file" name="file" id="file"/>
                                                </span>
                                            </div>  
                                        </div>
                                    </div>
                                    <label class="control-label col-lg-4">หมายเลขหนังสือ</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" class="form-control" value="Auto_Id" disabled id="id" name="id">
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">ชื่อหนังสือ</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="กรอกชื่อหนังสือ" tabindex="1">
                                        <span class="input-group-addon add-on">
                                            <font color="red">&#42;</font>
                                        </span>
                                    </div>   
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">ISBNหนังสือ</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" class="form-control" id="isbn" name="isbn" placeholder="กรอกISBNหนังสือ" tabindex="2">
                                        <span class="input-group-addon add-on">
                                            <font color="red">&#42;</font>
                                        </span>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">ขนาดหนังสือ</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" class="form-control" id="size" name="size" placeholder="กรอกขนาดหนังสือ" tabindex="3">
                                        <span class="input-group-addon add-on">
                                            <font color="red">&#42;</font>
                                        </span>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">จำนวนหน้า</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" class="form-control" id="page" name="page" placeholder="กรอกจำนวนหน้า" tabindex="4">
                                        <span class="input-group-addon add-on">
                                            <font color="red">&#42;</font>
                                        </span>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">ราคาปก</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" class="form-control" id="price" name="price" placeholder="กรอกราคาปก" tabindex="5"> 
                                        <span class="input-group-addon add-on">
                                            <font color="red">&#42;</font>
                                        </span>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">ราคาปัจจุบัน</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" class="form-control" id="priceupdate" name="priceupdate" placeholder="กรอกราคาปัจจุบัน" tabindex="6">
                                        <span class="input-group-addon add-on">
                                            <font color="red">&#42;</font>
                                        </span>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">สำนักพิมพ์</label>
                                    <div class="input-group col-lg-6">
                                        <select name="publishing" id="publishing" class="validate[required] form-control" tabindex="7">
                                            <option value="">สำนักพิมพ์</option>
                                        </select>
                                        <span class="input-group-addon add-on">
                                            <font color="red">&#42;</font>
                                        </span>
                                    </div>
                                </span>
                            </div>

                            <div class="col-lg-6">
                                <span role="form">
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">หมู่หนังสือ</label>
                                    <div class="input-group col-lg-6">
                                        <select name="category" id="category" class="validate[required] form-control" tabindex="8">
                                                <option value="">หมู่หนังสือ</option>
                                        </select>
                                        <span class="input-group-addon add-on">
                                            <font color="red">&#42;</font>
                                        </span>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">หมู่ย่อยหนังสือ</label>
                                    <div class="input-group col-lg-6">
                                        <select name="subcategory" id="subcategory" class="validate[required] form-control" tabindex="9">
                                            <option value="">หมู่ย่อยหนังสือ</option>
                                        </select>
                                        <span class="input-group-addon add-on">
                                            <font color="red">&#42;</font>
                                        </span>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">หัวเรื่องหนังสือ</label>
                                    <div class="input-group col-lg-5">
                                        <input type="text" id="titlebook" class="form-control col-lg-4" placeholder="กรอกหัวเรื่องหนังสือ" tabindex="10">
                                    </div>
                                    <button type="button" id="btaddtitlebook" class="btn btn-primary" tabindex="6"><i class="icon-pencil icon-white"></i> เพิ่ม
                                    </button>
                                    <button type="button" id="btdeltitlebook" class="btn btn-danger" tabindex="7"><i class="icon-remove icon-white"></i> ลบ
                                    </button>	
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4"></label>
                                    <div class="input-group col-lg-6">
                                        <select id="titlebooklist" multiple class="form-control" rows="3"></select>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">ผู้แต่งหนังสือ</label>
                                    <div class="input-group col-lg-5">
                                        <input type="text" id="author" class="form-control col-lg-4" placeholder="กรอกผู้แต่งหนังสือ" tabindex="11">
                                    </div>
                                    <button type="button" id="btaddauthor" class="btn btn-primary" tabindex="6"><i class="icon-pencil icon-white"></i> เพิ่ม
                                    </button>
                                    <button type="button" id="btdelauthor" class="btn btn-danger" tabindex="7"><i class="icon-remove icon-white"></i> ลบ
                                    </button>	
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4"></label>
                                    <div class="input-group col-lg-6">
                                        <select id="authorlist" multiple class="form-control" rows="3"></select>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">ผู้แปลหนังสือ</label>
                                    <div class="input-group col-lg-5">
                                        <input type="text" id="translator" class="form-control col-lg-4" placeholder="กรอกผู้แปลหนังสือ" tabindex="12">
                                    </div>
                                    <button type="button" id="btaddtranslator" class="btn btn-primary" tabindex="6"><i class="icon-pencil icon-white"></i> เพิ่ม
                                    </button>
                                    <button type="button" id="btdeltranslator" class="btn btn-danger" tabindex="7"><i class="icon-remove icon-white"></i> ลบ
                                    </button>	
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4"></label>
                                    <div class="input-group col-lg-6">
                                        <select id="translatorlist" multiple class="form-control" rows="3"></select>
                                    </div>
                                </span>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <span role="form">
                                    <label class="control-label col-lg-5"></label>
                                    <div class="col-lg-4">
                                        <button type="button" name="btsave" id="btsave" class="btn btn-primary" tabindex="21"><i class="glyphicon glyphicon-floppy-saved"></i> บันทึก</button>
                                        <input type="hidden" id="statuspage" value="add">
                                        <button type="button" id="btcancel" class="btn btn-danger" tabindex="22"><i class="icon-remove icon-white"></i> ยกเลิก</button>
                                    </div>
                                </span>
                            </div>
                        </div>
                        </form>   
                    </div>
                </div>
            </div>
        </div>
    </div>                                   
</div>
<!--END PAGE CONTENT -->
</div>
<!--END MAIN WRAPPER -->
<!-- GLOBAL SCRIPTS -->

<script src="<?php echo base_url()?>assets/js/validatebookfrom.js"></script>
<script src="<?php echo base_url()?>assets/js/bookfrom.js"></script>
<script>
validate_book.init_validate_book();
book.book_init_add();    
</script>
<!-- END GLOBAL SCRIPTS -->