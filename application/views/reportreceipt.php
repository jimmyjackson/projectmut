<!--PAGE CONTENT -->
<div id="content">
    <div class="inner">
        <div class="row has-success" >
            <div class="col-lg-5" style="padding-top: 30px; ">
                <h1 class="page-header">ออกรายงานการรับจากสั่งซื้อ</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        ออกรายงานการรับจากสั่งซื้อ
                    </div>

                    <div class="col-lg-12">
                        <div class="panel-body">
                            <div class="form-inline">
                                <label class="control-label col-lg-2">วันเริ่มต้น</label>
                                <div class="col-lg-2">
                                    <input type="text" class="form-control" id="from" name="from">
                                </div>
                                <label class="control-label col-lg-2">วันสิ้นสุด</label>
                                <div class="col-lg-2">
                                    <input type="text" class="form-control" id="to" name="to">
                                </div>
                                <div class="col-lg-4">
                                        <button type="button" name="btreport" id="btreport" class="btn btn-primary" tabindex="21"><i class="glyphicon glyphicon glyphicon-print"></i> ออกรายงาน</button>
                                        <button type="button" id="btcancel" class="btn btn-danger" tabindex="22"><i class="icon-remove icon-white"></i> ยกเลิก</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END PAGE CONTENT -->
</div>
<!--END MAIN WRAPPER -->
<!-- GLOBAL SCRIPTS -->
<script>
$(function() {
var base_url = window.location.origin+"/CI";

$( "#from" ).datepicker({
  changeMonth: true,
  changeYear: true,
  dateFormat : "yy-mm-dd",
  onClose: function( selectedDate ) {
    $( "#to" ).datepicker( "option", "minDate", selectedDate );
  }
});
$( "#to" ).datepicker({
  changeMonth: true,
  changeYear: true,
  dateFormat : "yy-mm-dd",
  onClose: function( selectedDate ) {
    $( "#from" ).datepicker( "option", "maxDate", selectedDate );
  }
});


$("#btreport").click(function () {
  if($( "#from" ).val() == ""){
    alert("กรุณากรอกวันเริ่มต้น");
    $( "#from" ).focus();
  }
  else if ($( "#to" ).val() == "") {
    alert("กรุณากรอกวันสิ้นสุด");
    $( "#to" ).focus();
  }
  else {
    window.open(base_url+"/reportreceipt_controller/selectreport/"+$( "#from" ).val()+":"+$( "#to" ).val());
  }
});

$("#btcancel").click(function () {
  window.location.href = base_url+"/reportreceipt_controller";
});

});
</script>
<!-- END GLOBAL SCRIPTS -->
