<!--PAGE CONTENT -->
<style>
.table-responsive {
    width: 100%;
    margin-bottom: 15px;
    overflow-x: auto;
    overflow-y: hidden;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
    border: 1px solid #DDD;
}
.nowrap {
        white-space:nowrap;
    }
</style>
<div id="content">

    <div class="inner">
            <div class="row has-success" >
                <div class="col-lg-5" style="padding-top: 30px; ">
                    <h1 class="page-header">หนังสือห้องสมุด</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            ข้อมูลของหนังสือ
                        </div>
                        <div class="col-lg-12">
                            <div class="panel-heading">
                                <div class="form-inline">
                                <label class="control-label col-lg-4"></label>
                                <button type="button" class="btn btn-primary" id="btaddbook"><i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;เพิ่มข้อมูลใหม่</button>
                                <label class="control-label col-lg-2"></label>
                                <select id="selectsearch" class="selectpicker" multiple>
                                    <option value="idcopy">รหัสสำเนาหนังสือ</option>
                                    <option value="nocopy">เลขเรียกสำเนาหนังสือ</option>
                                    <option value="id">รหัส</option>
                                    <option value="name">ชื่อหนังสือ</option>
                                    <option value="isbn">ISBN</option>
                                    <option value="publishing">สำนักพิมพ์</option>
                                </select>
                                <input id="inputsearch" type="text" style="width: 21.75%" class="form-control" placeholder="กรอกข้อมูลที่ต้องการค้นหา">
                                <button id="btsearch" type="button" class="btn btn-info"><i class="icon-search icon-white"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
        <div class="table-responsive">
        <table id="example" class="table table-responsive table-striped table-bordered table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="col-md-3"><center><span class="nowrap">รหัสสำเนาหนังสือ</span></center></th>
                <th class="col-md-3"><center><span class="nowrap">เลขเรียกสำเนาหนังสือ</span></center></th>
                <th class="col-md-3"><center><span class="nowrap">รหัสหนังสือ</span></center></th>
                <th class="col-md-3"><center><span class="nowrap">ชื่อหนังสือ</span></center></th>
                <th class="col-md-3"><center><span class="nowrap">ISBN</span></center></th>
                <th class="col-md-3"><center><span class="nowrap">สำนักพิมพ์</span></center></th>
                <th class="col-md-3"><center><span class="nowrap">หมู่</span></center></th>
                <th class="col-md-3"><center><span class="nowrap">หมู่ย่อย</span></center></th>
                <th class="col-md-3"><center><span class="nowrap">หัวเรื่อง</span></center></th>
                <th class="col-md-3"><center><span class="nowrap">ชื่อผู้แต่ง</span></center></th>
                <th class="col-md-3"><center><span class="nowrap">ชื่อแปล</span></center></th>
                <th class="col-md-3"><center><span class="nowrap">ปี่ที่พิมพ์/ครั้งที่พิมพ์</span></center></th>
                <th class="col-md-3"><center><span class="nowrap">ตู้ที่:ชั้นที่:ช่องที่</span></center></th>
                <th class="col-md-3"><center><span class="nowrap">สถานะ</span></center></th>
                <th class="col-md-3"><center><span class="nowrap">เลือก</span></center></th>
            </tr>
        </thead>

        <tfoot>
            <tr>
              <th class="col-md-3"><center><span class="nowrap">รหัสสำเนาหนังสือ</span></center></th>
              <th class="col-md-3"><center><span class="nowrap">เลขเรียกสำเนาหนังสือ</span></center></th>
              <th class="col-md-3"><center><span class="nowrap">รหัสหนังสือ</span></center></th>
              <th class="col-md-3"><center><span class="nowrap">ชื่อหนังสือ</span></center></th>
              <th class="col-md-3"><center><span class="nowrap">ISBN</span></center></th>
              <th class="col-md-3"><center><span class="nowrap">สำนักพิมพ์</span></center></th>
              <th class="col-md-3"><center><span class="nowrap">หมู่</span></center></th>
              <th class="col-md-3"><center><span class="nowrap">หมู่ย่อย</span></center></th>
              <th class="col-md-3"><center><span class="nowrap">หัวเรื่อง</span></center></th>
              <th class="col-md-3"><center><span class="nowrap">ชื่อผู้แต่ง</span></center></th>
              <th class="col-md-3"><center><span class="nowrap">ชื่อแปล</span></center></th>
              <th class="col-md-3"><center><span class="nowrap">ปี่ที่พิมพ์/ครั้งที่พิมพ์</span></center></th>
              <th class="col-md-3"><center><span class="nowrap">ตู้ที่:ชั้นที่:ช่องที่</span></center></th>
              <th class="col-md-3"><center><span class="nowrap">สถานะ</span></center></th>
              <th class="col-md-3"><center><span class="nowrap">เลือก</span></center></th>
            </tr>
        </tfoot>

        <tbody id="bodyshowdata">

        </tbody>
    </table>
  </div>
            <nav>
                <ul class="pagination" id="paging">

                </ul>
            </nav>

            </div>

                        <div class="col-lg-12">
                                &nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <!--END PAGE CONTENT -->
    </div>
</div>
<!--END MAIN WRAPPER -->
<!-- GLOBAL SCRIPTS -->
<script src="<?php echo base_url()?>assets/js/validatefromall.js"></script>
<script src="<?php echo base_url()?>assets/js/bookfrom.js"></script>
<script>

$("nav").on('click','#paging li a',function(event){
    getdatarows(event.target.id);
});

    $('#selectsearch').multiselect({
        nonSelectedText: 'ค้นหาจาก',
        includeSelectAllOption: true,
        selectAllText: 'เลือกทั้งหมด',
         /*enableFiltering: true,*/
        buttonWidth: '100px'
    });

    $("#btaddbook").click(function(){
        window.location.href=base_url+"/bookcopy_controller/bookcopy/";
    });

    $("#btsearch").click(function(){
        getdatarows(1);
    });

    getdatarows(1);

    function getdatarows(pageselect){

    var offset = (pageselect-1)*10;
    var numpageshow =5;
    var lengthshow = Math.floor(numpageshow/2);
    //console.log(offset);

    if($('#inputsearch').val() == ""){

        $.ajax({
            type : "POST",
            url: base_url+"/bookcopy_controller/selectbookcopy",
            data: {'offsetsend':offset},
            dataType: "json",
            success: function(data)
            {
                $("#bodyshowdata").empty();
                $.each(data,function(ID,book){
                  var title = "", author = "", translator = "";
                    book.title.forEach(function (t,i) {
                      if(t.title_name != null) {
                        if(i < (book.title.length - 1) && book.title.length > 1)
                          title += t.title_name + ", <br>";
                        else
                          title += t.title_name;
                      } else
                          title += "ไม่พบข้อมูล";
                    });

                    book.author.forEach(function (a,i) {
                      if(a.author != null) {
                        if(i < (book.author.length - 1) && book.author.length > 1)
                          author += a.author + ", <br>";
                        else
                          author += a.author;
                      } else
                          author += "ไม่พบข้อมูล";

                    });

                    book.translator.forEach(function (tl,i) {
                      if(tl.translator != null) {
                        if(i < (book.translator.length - 1) && book.translator.length > 1)
                          translator += tl.translator + ", <br>";
                        else
                          translator += tl.translator;
                      } else
                          translator += "ไม่พบข้อมูล";
                    });

                    $("#bodyshowdata").append(
                        '<tr><td class="col-md-3"><center>'+book.bookcopy.bookcopy_id+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.bookcopy_callno+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.book_id+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.book_name+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.book_isbn+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.publishing_name+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.no_mubook+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.no_submubook+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+title+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+author+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+translator+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.edition+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.positionbook+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.bookcopy_status+
                        '</span></center></td><td class="col-md-6"><center><a href='+base_url+
                        '/bookcopy_controller/bookcopy/'+book.bookcopy.bookcopy_id+
                        '><button type="button" class="btn btn-success" id='+book.bookcopy.bookcopy_id+
                        '><i class="glyphicon glyphicon-eye-open"></i>&nbsp;view</button></a></center></td></tr>');
                    });
                }
    });


        $.ajax({
            type : "POST",
            url: base_url+"/bookcopy_controller/countbookcopy",
            //data: {usernamecheck:'basadultindy0'},
            dataType: "json",
            success: function(data)
            {
                $("#paging").empty();
                $.each(data,function(i,count){
                    $("#paging").append("<li id='row' class='disabled'><span>rows "+(offset+1)+" to "+(offset+10)+"</span></li>");
                    $("#paging").append("<li id='first'><a href='#' id='1'>&laquo;</a></li>");

                    var page = Math.ceil(count/10);
                    if(page < 6)
                    {
                        for(var num=1;num<=page;num++)
                            if(num==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+num+"'>"+num+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+num+"'>"+num+"</a></li>");
                    }
                    else{
                    if(pageselect==page)
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++){
                            if((m+1)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                        }
                    }
                    else if(pageselect==(page-1))
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++)
                            if((m+2)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                    }
                    else{
                        for(var j=pageselect-1,k=0;k<numpageshow;j++,k++)
                        {
                            if((pageselect-1)<(numpageshow-lengthshow))
                                if((k+1)>page)
                                    break;
                                else
                                    if((k+1)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                            else
                                if((j+1)>(page+lengthshow))
                                    break;
                                else
                                    if(((j+1)-lengthshow)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                        }
                    }
                    }

                    $("#paging").append("<li id='last'><a href='#' id='"+page+"'>&raquo;</a></li>");
                    $("#paging").append("<li id='total' class='disabled'><span>page "+pageselect+" of "+page+"</span></li>");
                });

            }
    });

    }else{

        var selectmessage=[];
        var selectshowtype=[];
        $("#selectsearch option:selected").each(function (){
            selectmessage.push($(this).val());
        });

        if(selectmessage.length === 0)
            selectmessage.push('none');

        $("#selectshowtype option:selected").each(function (){
            selectshowtype.push($(this).val());
        });

        if(selectshowtype.length === 0)
            selectshowtype.push('none');

        $.ajax({
            type : "POST",
            url: base_url+"/bookcopy_controller/searchbookcopy",
            data: {datasearch:{'selectmessage':selectmessage,'selectshowtype':selectshowtype,
            'inputsearch':$('#inputsearch').val(),'offset':0}},
            dataType: "json",
            success: function(data)
            {
                $("#bodyshowdata").empty();

                $.each(data,function(ID,book){
                  var title = "", author = "", translator = "";
                    book.title.forEach(function (t,i) {
                      if(t.title_name != null) {
                        if(i < (book.title.length - 1) && book.title.length > 1)
                          title += t.title_name + ", <br>";
                        else
                          title += t.title_name;
                      } else
                          title += "ไม่พบข้อมูล";
                    });

                    book.author.forEach(function (a,i) {
                      if(a.author != null) {
                        if(i < (book.author.length - 1) && book.author.length > 1)
                          author += a.author + ", <br>";
                        else
                          author += a.author;
                      } else
                          author += "ไม่พบข้อมูล";

                    });

                    book.translator.forEach(function (tl,i) {
                      if(tl.translator != null) {
                        if(i < (book.translator.length - 1) && book.translator.length > 1)
                          translator += tl.translator + ", <br>";
                        else
                          translator += tl.translator;
                      } else
                          translator += "ไม่พบข้อมูล";
                    });

                    $("#bodyshowdata").append(
                        '<tr><td class="col-md-3"><center>'+book.bookcopy.bookcopy_id+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.bookcopy_callno+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.book_id+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.book_name+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.book_isbn+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.publishing_name+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.no_mubook+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.no_submubook+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+title+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+author+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+translator+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.edition+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.positionbook+
                        '</span></center></td><td class="col-md-6"><center><span class="nowrap">'+book.bookcopy.bookcopy_status+
                        '</span></center></td><td class="col-md-6"><center><a href='+base_url+
                        '/bookcopy_controller/bookcopy/'+book.bookcopy.bookcopy_id+
                        '><button type="button" class="btn btn-success" id='+book.bookcopy.bookcopy_id+
                        '><i class="glyphicon glyphicon-eye-open"></i>&nbsp;view</button></a></center></td></tr>');
                    });

            }
        });

        $.ajax({
            type : "POST",
            url: base_url+"/bookcopy_controller/countsearchbookcopy",
            data: {datasearch:{'selectmessage':selectmessage,'selectshowtype':selectshowtype,
            'inputsearch':$('#inputsearch').val()}},
            dataType: "json",
            success: function(data)
            {
                $.each(data,function(i,count){
                    $("#paging").empty();
                    //console.log(count.countsearchemp);
                    $("#paging").append("<li id='row' class='disabled'><span>rows "+(offset+1)+" to "+(offset+10)+"</span></li>");
                    $("#paging").append("<li id='first'><a href='#' id='1'>&laquo;</a></li>");

                    var page = Math.ceil(count.count/10);

                    //console.log(page);

                    if(page < 6)
                    {
                        for(var num=1;num<=page;num++)
                            if(num==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+num+"'>"+num+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+num+"'>"+num+"</a></li>");
                    }
                    else{
                    if(pageselect==page)
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++){
                            if((m+1)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                        }
                    }
                    else if(pageselect==(page-1))
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++)
                            if((m+2)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                    }
                    else{
                        for(var j=pageselect-1,k=0;k<numpageshow;j++,k++)
                        {
                            if((pageselect-1)<(numpageshow-lengthshow))
                                if((k+1)>page)
                                    break;
                                else
                                    if((k+1)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                            else
                                if((j+1)>(page+lengthshow))
                                    break;
                                else
                                    if(((j+1)-lengthshow)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");

                        }
                    }
                    }

                    $("#paging").append("<li id='last'><a href='#' id='"+page+"'>&raquo;</a></li>");
                    $("#paging").append("<li id='total' class='disabled'><span>page "+pageselect+" of "+page+"</span></li>");

                });

            }
    });

    }
}
</script>
<!-- END GLOBAL SCRIPTS -->
