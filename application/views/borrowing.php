<div id="content">
    <div class="inner">
        <div class="row has-success" >
            <div class="col-lg-5" style="padding-top: 30px; ">
            <h1 class="page-header">การยืมหนังสือ</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">ค้นหาข้อมูลหนังสือ</div>
                    <div class="panel-body">
                        <form action="<?php echo site_url("borrowing_controller/index"); ?>" method="post">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">เลขเรียกหนังสือ</label>
                                            <input type="text" class="form-control" id="bookcall" name="bookcall" value="<?php echo (!empty($_POST['bookcall']) ? $_POST['bookcall'] : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">ชื่อหนังสือ</label>
                                            <input type="text" class="form-control" id="bookname" name="bookname" value="<?php echo (!empty($_POST['bookname']) ? $_POST['bookname'] : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">สำนักพิมพ์</label>
                                            <select name="publishing" id="publishing" class="form-control" tabindex="2">
                                                <option value="">สำนักพิมพ์</option>
                                                <?php
                                                if(count($dataPublishing) > 0){
                                                    foreach ($dataPublishing as $value) {

                                                    ?>
                                                    <option value="<?php echo $value->publishing_id; ?>" <?php echo (!empty($_POST['publishing']) && $_POST['publishing']==$value->publishing_id ? 'selected="selected"' : ''); ?>><?php echo $value->publishing_name; ?></option>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2">
                                        <div class="form-group" style="margin-top: 25px;">
                                            <button type="submit" name="btadddetial" id="btadddetial" class="btn btn-info" tabindex="3">
                                                <i class="glyphicon glyphicon glyphicon-search"></i>
                                            </button>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group field-member">
                                            <label class="control-label">รหัสสมาชิก</label>
                                            <input type="text" class="form-control" id="member" name="member" value="<?php echo (!empty($_POST['member']) ? $_POST['member'] : ''); ?>">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if(!empty($sendError) && ($sendError=='save' || $sendError=='datastatus')){
        ?>
            <div class="col-lg-12">
              <?php if($sendError=='save') { ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <strong>สำเร็จ!</strong>ทำการบันทึกการยืมหนังสือเรียบร้อยแล้ว
                </div>
              <?php } elseif ($sendError=='datastatus') { ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <strong>เกิดข้อผิดพลาด!</strong>ไม่สามารถยืมหนังสือได้ เนื่องจากรายการที่ยืมมีบางเล่มถูกยืมไปแล้ว
                </div>
              <?php } ?>
            </div>
        <?php
        }
        if(isset($_POST['btadddetial']) || !empty($_POST['bookcopy']) || isset($_POST['delete_id'])){
            if(count($dataMember)>0){
        ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">ข้อมูลการยืมหนังสือ</div>
                            <div class="panel-body">
                                <form id="save_receipt" action="<?php echo site_url("receiptorder_controller/index"); ?>" method="post">
                                    <div class="row">
                                        <?php
                                        if(count($dataBorrowing)>0){
                                        ?>

                                            <table id="example" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th><center>ลำดับ</center></th>
                                                        <th><center>เลขเรียกหนังสือ</center></th>
                                                        <th><center>ชื่อหนังสือ</center></th>
                                                        <th><center>วันที่ยืม</center></th>
                                                        <th><center>กำหนดวันที่คืน</center></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($dataBorrowing as $key=>$value) {
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center;"><?php echo ($key+1); ?></td>
                                                        <td style="text-align: center;"><?php echo $value->bookcopy_callno; ?></td>
                                                        <td><?php echo $value->book_name; ?></td>
                                                        <td style="text-align: center;"><?php echo $value->borrowing_date; ?></td>
                                                        <td style="text-align: center;"><?php echo $value->datereturn; ?></td>
                                                    </tr>
                                                    <?php
                                                    }
                                                    ?>

                                                </tbody>
                                            </table>
                                        <?php
                                        }else{
                                        ?>
                                            <div class="row" style="text-align: center;">
                                                <h1>ไม่พบข้อมูลการยืมหนังสือ</h1>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                if(!empty($sendError) && ($sendError=='datastatus' || $sendError=='unit')){
                ?>
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <strong>เกิดข้อผิดพลาด!</strong>ไม่สามารถเลือกหนังสือเพิ่มได้ เนื่องจาก<?php
                            if ($sendError=='duplicate')
                              echo "มีรายการที่ยืมซ้ำ";
                            else
                              echo "จำนวนการยืมเกินจำนวนที่กำหนด";
                            ?>
                        </div>
                    </div>
                <?php
                }
                $arrDataBorrowing = $this->session->userdata('borrowing');
                if(count($dataBooking)>0){
                ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">ข้อมูลหนังสือ</div>
                                <div class="panel-body">
                                    <form id="save_receipt" action="<?php echo site_url("borrowing_controller/index"); ?>" method="post">
                                        <div class="row">
                                            <table id="example" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th><center>ลำดับ</center></th>
                                                        <th><center>เลขเรียกหนังสือ</center></th>
                                                        <th><center>ชื่อหนังสือ</center></th>
                                                        <th><center>สถานะ</center></th>
                                                        <th><center>ยืม</center></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $arrCheck = array();
                                                    if(!empty($arrDataBorrowing[$_POST['member']])){
                                                        foreach ($arrDataBorrowing[$_POST['member']] as $key=>$value) {
                                                            $arrCheck[] = $value['bookcopy_callno'];
                                                        }
                                                    }
                                                    $aa = 0;
                                                    foreach ($dataBooking as $key=>$value) {
                                                        if (!in_array($value->bookcopy_callno, $arrCheck)){
                                                        ?>
                                                            <tr id="row_<?php echo $value->bookcopy_id; ?>">
                                                                <td style="text-align: center;"><?php echo ($aa+1); ?></td>
                                                                <td style="text-align: center;"><?php echo $value->bookcopy_callno; ?></td>
                                                                <td><?php echo $value->book_name; ?></td>
                                                                <td style="text-align: center;"><?php echo $value->bookcopy_status; ?></td>
                                                                <td style="text-align: center;">
                                                                    <?php
                                                                    if($dataMember[0]->membertype_unit > count($dataBorrowing)){
                                                                        if($value->bookcopy_status == 'ว่าง'){
                                                                        ?>
                                                                            <input type="checkbox" class="receipt" id="<?php echo $value->bookcopy_id; ?>" value="<?php echo $value->bookcopy_id; ?>" name="bookcopy[<?php echo $aa; ?>]">
                                                                        <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                            $aa++;
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                            <?php if($aa > 0 && $dataMember[0]->membertype_unit > count($dataBorrowing)){ ?>
                                            <div class="col-lg-12" style="text-align: right;">
                                                <button type="submit" name="savereceipt" id="savereceipt" class="btn btn-success">
                                                    <i class="glyphicon glyphicon glyphicon-plus"></i>
                                                </button>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <input type="hidden" name="bookcall" value="<?php echo (!empty($_POST['bookcall']) ? $_POST['bookcall'] : ''); ?>">
                                        <input type="hidden" name="bookname" value="<?php echo (!empty($_POST['bookname']) ? $_POST['bookname'] : ''); ?>">
                                        <input type="hidden" name="publishing" value="<?php echo (!empty($_POST['publishing']) ? $_POST['publishing'] : ''); ?>">
                                        <input type="hidden" name="member" value="<?php echo (!empty($_POST['member']) ? $_POST['member'] : ''); ?>">
                                        <?php
                                        if(!empty($arrDataBorrowing[$_POST['member']])){
                                          foreach ($arrDataBorrowing[$_POST['member']] as $key=>$value) { ?>
                                            <input type="hidden" name="databookcopy_id[]" value="<?php echo $value['bookcopy_id']; ?>">
                                        <?php
                                          }
                                        } ?>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    if(!empty($arrDataBorrowing[$_POST['member']])){
                    ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">ข้อมูลหนังสือที่เลือก</div>
                                    <div class="panel-body">
                                        <form id="save_borrow" action="<?php echo site_url("borrowing_controller/index"); ?>" method="post">
                                            <div class="row">
                                                <table id="example" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th><center>ลำดับ</center></th>
                                                            <th><center>เลขเรียกหนังสือ</center></th>
                                                            <th><center>ชื่อหนังสือ</center></th>
                                                            <th><center>กำหนดวันที่คืน</center></th>
                                                            <th><center>ลบ</center></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        foreach ($arrDataBorrowing[$_POST['member']] as $key=>$value) {
                                                        ?>
                                                        <tr>
                                                            <td style="text-align: center;"><?php echo ($key+1); ?></td>
                                                            <td style="text-align: center;"><?php echo $value['bookcopy_callno']; ?></td>
                                                            <td style="text-align: center;"><?php echo $value['book_name']; ?></td>
                                                            <td style="text-align: center;"><?php echo $value['date']; ?></td>
                                                            <td style="text-align: center;">
                                                                <button type="button" name="delete" id="<?php echo $value['bookcopy_id']; ?>" class="btn btn-danger delete">
                                                                    <i class="glyphicon glyphicon glyphicon-trash"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                                <div class="col-lg-12" style="text-align: right;">
                                                    <button type="submit" name="saveborrow" id="saveborrow" class="btn btn-primary">
                                                        <i class="glyphicon glyphicon glyphicon-floppy-disk"></i> บันทึก
                                                    </button>
                                                </div>
                                            </div>
                                            <input type="hidden" name="member" value="<?php echo (!empty($_POST['member']) ? $_POST['member'] : ''); ?>">
                                            <input type="hidden" name="type" value="save">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


                    <?php
                    }
                }else{
                ?>
                    <div class="row" style="text-align: center; padding-top: 50px;">
                        <h1>ไม่พบข้อมูลหนังสือที่ค้นหา</h1>
                    </div>
                <?php
                }
            }else{
            ?>
                <div class="row" style="text-align: center; padding-top: 50px;">
                    <h1>ไม่พบรหัสสมาชิก</h1>
                </div>
            <?php
            }
        }
        ?>
    </div>
</div>

<div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ยืนยันการบันทึกข้อมูล</h4>
            </div>
            <div class="modal-body">
                คุณต้องการบันทึกข้อมูลใช่หรือไม่?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-primary" id="btnConfirm">ยืนยัน</button>
            </div>
        </div>
    </div>
</div>

<form id="send_form" action="<?php echo site_url("borrowing_controller/index"); ?>" method="post">
    <input type="hidden" id="delete_id" name="delete_id">
    <input type="hidden" name="bookcall" value="<?php echo (!empty($_POST['bookcall']) ? $_POST['bookcall'] : ''); ?>">
    <input type="hidden" name="bookname" value="<?php echo (!empty($_POST['bookname']) ? $_POST['bookname'] : ''); ?>">
    <input type="hidden" name="publishing" value="<?php echo (!empty($_POST['publishing']) ? $_POST['publishing'] : ''); ?>">
    <input type="hidden" name="member" value="<?php echo (!empty($_POST['member']) ? $_POST['member'] : ''); ?>">
</form>
<script>
$(document).ready(function(){
    $('#btadddetial').on('click',function(){
        var member = $('#member').val();
        var status = '';
        if(member === ''){
            $('.field-member').addClass('has-error');
            $('.field-member .help-block').html('กรุณากรอกรหัสสมาชิก');
            status = 'error';
        }else{
            $('.field-member').removeClass('has-error');
            $('.field-member .help-block').html('');
        }
        if(status !== ''){
            return false;
        }
    });
    $('.delete').on('click',function(){
        var id = this.id;
        $('#delete_id').val(id);
        $('#send_form').submit();

    });
    $('#saveborrow').on('click',function(){
        $('#modalConfirm').modal('show');
        return false;
    });

    $('#btnConfirm').on('click',function(){
        $('#save_borrow').submit();
    });




});
</script>
