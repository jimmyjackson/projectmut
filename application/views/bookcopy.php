<div id="content">
    <div class="inner">
        <div class="row has-success" >
            <div class="col-lg-5" style="padding-top: 30px; ">
            <h1 class="page-header">จัดการข้อมูลสำเนาหนังสือ</h1>
            </div>
        </div>
        <?php
        if(count($dataReceiptorderdetial)>0){
        ?>
            <div class="col-lg-12">
                <form id="save_receipt" action="" method="post">
                    <div class="row">
                        <table id="example" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th><center>ลำดับ</center></th>
                                    <th><center>เลขที่ใบรับ</center></th>
                                    <th><center>วันที่รับ</center></th>
                                    <th><center>สำนักพิมพ์</center></th>
                                    <th><center>ชื่อหนังสือ</center></th>
                                    <th><center>จำนวน</center></th>
                                    <th><center>ทำสำเนา</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($dataReceiptorderdetial as $key=>$value) {
                                ?>
                                <tr>
                                    <td style="text-align: center;"><?php echo ($key+1); ?></td>
                                    <td style="text-align: center;"><?php echo $value->id_receiptorder; ?></td>
                                    <td style="text-align: center;"><?php echo $value->receiptorder_date; ?></td>
                                    <td><?php echo $value->publishing_name; ?></td>
                                    <td><?php echo $value->book_name; ?></td>
                                    <td style="text-align: center;"><?php echo $value->receiptorderdetial_unit; ?></td>
                                    <td style="text-align: center;"><span id="<?php echo $value->id_receiptorder.'_'.$value->receiptorderdetial_no; ?>" class="glyphicon glyphicon-file bookcopy" aria-hidden="true" style="cursor: pointer;"></span></td>
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <?php
                if($statusSave == 'true'){
                ?>
                    <div class="col-lg-12">
                        <div class="alert alert-success alert-dismissible fade in" role="alert">
                            <strong>สำเร็จ!</strong> ทำการจัดการข้อมูลสำเนาหนังสือเรียบร้อยแล้ว
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
        <?php
        }else{
        ?>
            <div class="row" style="text-align: center; padding-top: 50px;">
                <h1>ไม่พบข้อมูลรายการรับหนังสือใหม่</h1>
            </div>
        <?php
        }
        ?>
    </div>
</div>
<form id="send_form" action="<?php echo site_url("bookcopy_controller/form"); ?>" method="post">
    <input type="hidden" id="receiptorder_id" name="receiptorder_id">
</form>
<script>
$(document).ready(function(){
    $('.bookcopy').on('click',function(){
        var id = this.id;
        $('#receiptorder_id').val(id);
        $('#send_form').submit();
    });
});
</script>
