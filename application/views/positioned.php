<style>
tr {
width: 100%;
display: inline-table;
}
table{
 height:400px; 
}
tbody{
  overflow-y: scroll;
  height: 360px;
  width: 98.2%;
  position: absolute;
}
</style>
    <!--PAGE CONTENT -->
    <div id="content">
        <div class="inner">
            <div class="row has-success" >
                <div class="col-lg-5" style="padding-top: 30px; ">
                    <h1 class="page-header">ตำแหน่งพนักงานห้องสมุด</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <div class="panel-heading">
                            ข้อมูลตำแหน่งพนักงาน
                        </div>
                         
                        <div class="col-lg-12">
                            <div class="panel-heading">
                                <div class="form-inline">
                                    <label class="control-label col-lg-2">รหัสตำแหน่ง</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" value="<?php echo $idposition ?>" disabled id="id">
                                    </div>
                                    <label class="control-label col-lg-2">ชื่อตำแหน่ง</label>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" id="nameposition" value="<?php echo $nameposition[0]->position_name ?>">
                                    </div>
                                    
                                    <div class="col-lg-3">
                                            <button type="button" name="btsave" id="btsave" class="btn btn-primary" tabindex="19"><i class="glyphicon glyphicon-floppy-saved"></i> บันทึก</button>
                                            <button type="button" id="btdelete" class="btn btn-danger" tabindex="20"><i class="glyphicon glyphicon-floppy-remove"></i> ลบ</button>
                                            <input type="hidden" id="statuspage" value="edit">
                                            <button type="button" id="btcancel" class="btn btn-warning" tabindex="21"><i class="icon-remove icon-white"></i> ยกเลิก</button>	
                                             
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="statuspage" value="edit">
                        <div class="col-lg-12">
                            <label class="control-label">&nbsp;</label>
                        </div>
                        
                        <div class="col-lg-9">
                            <label class="control-label">สิทธิ์การเข้าใช้งาน</label>
                        </div>
                        
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="col-lg-12">
                                            <th class="col-lg-1">
                                                <center>SELECT</center>
                                            </th>
                                            <th class="col-lg-2"><center>ID</center></th>
                                            <th class="col-lg-3"><center>Name</center></th>
                                            <th class="col-lg-6"><center>URL</center></th>
                                        </tr>
                                    </thead>

                                    <tbody id="bodyshowdataposition">
                                        <?php foreach ($access_data as $value){ ?>
                                        <tr class="col-lg-12">
                                            <td class="col-lg-1">
                                                <center>
                                                    <input type="checkbox" id="<?php echo $value->access_id ?>">
                                                </center>
                                            </td>
                                            <td class="col-lg-2">
                                                <center>
                                                    <?php 
                                                        echo $value->access_id;
                                                    ?>
                                                </center>
                                            </td>
                                            <td class="col-lg-3">
                                                <center>
                                                    <?php
                                                        echo $value->access_name; 
                                                    ?>
                                                </center>
                                            </td>
                                            <td class="col-lg-6">
                                                <center>
                                                    <?php 
                                                        echo $value->access_url;
                                                    ?>
                                                </center>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END PAGE CONTENT -->
</div>
<!--END MAIN WRAPPER -->
<!-- GLOBAL SCRIPTS -->
<script src="<?php echo base_url()?>assets/js/positionfrom.js"></script>
<script>
    var base_url = window.location.origin+"/CI";
    
    var init=function(){
        $.ajax({
                type : "GET",
                url: base_url+"/position_controller/select_access_id_where_id_position/<?php echo $idposition ?>",
                dataType: "json",
                success: function(data)
                {
                    $.each(data,function(ID,pos){
                        $("input[type='checkbox']").each(function(){
                            if(this.id==pos.id_access)
                                $(this).attr('checked',true);
                        });
                    });
                }
        });
    }
    window.onload = init;
    
</script>
<!-- END GLOBAL SCRIPTS -->