<!--PAGE CONTENT -->
<div id="content">

    <div class="inner">
            <div class="row has-success" >
                <div class="col-lg-5" style="padding-top: 30px; ">
                    <h1 class="page-header">หนังสือห้องสมุด</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            ข้อมูลของหนังสือ
                        </div>
                        <div class="col-lg-12">
                            <div class="panel-heading">
                                <div class="form-inline">
                                <label class="control-label col-lg-4"></label>
                                <button type="button" class="btn btn-primary" id="btaddbook"><i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;เพิ่มข้อมูลใหม่</button>
                                <label class="control-label col-lg-2"></label>
                                <select id="selectsearch" class="selectpicker" multiple>
                                    <option value="id">รหัส</option>
                                    <option value="name">ชื่อหนังสือ</option>
                                    <option value="isbn">ISBN</option>
                                    <option value="publishing">สำนักพิมพ์</option>
                                    <option value="mubook">หมู่หนังสือ</option>
                                    <option value="submubook">หมู่ย่อยหนังสือ</option>
                                </select>
                                <input id="inputsearch" type="text" style="width: 21.75%" class="form-control" placeholder="กรอกข้อมูลที่ต้องการค้นหา">
                                <button id="btsearch" type="button" class="btn btn-info"><i class="icon-search icon-white"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
        <table id="example" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th><center>รหัส</center></th>
                <th><center>ชื่อหนังสือ</center></th>
                <th><center>ISBN</center></th>
                <th><center>สำนักพิมพ์</center></th>
                <th><center>หมู่</center></th>
                <th><center>หมู่ย่อย</center></th>
                <th><center>หัวเรื่อง</center></th>
                <th><center>ชื่อผู้แต่ง</center></th>
                <th><center>เลือก</center></th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th><center>รหัส</center></th>
                <th><center>ชื่อหนังสือ</center></th>
                <th><center>ISBN</center></th>
                <th><center>สำนักพิมพ์</center></th>
                <th><center>หมู่</center></th>
                <th><center>หมู่ย่อย</center></th>
                <th><center>หัวเรื่อง</center></th>
                <th><center>ชื่อผู้แต่ง</center></th>
                <th><center>เลือก</center></th>
            </tr>
        </tfoot>

        <tbody id="bodyshowdata">

        </tbody>
    </table>
            <nav>
                <ul class="pagination" id="paging">

                </ul>
            </nav>

            </div>

                        <div class="col-lg-12">
                                &nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <!--END PAGE CONTENT -->
    </div>
</div>
<!--END MAIN WRAPPER -->
<!-- GLOBAL SCRIPTS -->
<script src="<?php echo base_url()?>assets/js/validatefromall.js"></script>
<script src="<?php echo base_url()?>assets/js/bookfrom.js"></script>
<script>

$("nav").on('click','#paging li a',function(event){
    getdatarows(event.target.id);
});

    book.book_init_list();
    getdatarows(1);
    function getdatarows(pageselect){

    var offset = (pageselect-1)*10;
    var numpageshow =5;
    var lengthshow = Math.floor(numpageshow/2);
    //console.log(offset);

    if($('#inputsearch').val() == ""){

        $.ajax({
            type : "POST",
            url: base_url+"/book_controller/selectbook",
            data: {'offsetsend':offset},
            dataType: "json",
            success: function(data)
            {
                $("#bodyshowdata").empty();

                $.each(data.bookselect,function(ID,book){

                    $("#bodyshowdata").append(
                        '<tr><td><center>'+book.book_id+'</center></td><td><center>'+book.book_name+'</center></td><td><center>'+book.book_isbn+'</center></td><td><center>'+book.publishing_name+'</center></td><td><center>'+book.no_mubook+'</center></td><td><center>'+book.no_submubook+'</center></td><td><center>'+book.title_name+'</center></td><td><center>'+book.author_name_n_lname+'</center></td><td><center><a href='+base_url+'/book_controller/book/'+book.book_id+'><button type="button" class="btn btn-success" id='+book.book_id+'><i class="glyphicon glyphicon-eye-open"></i>&nbsp;view</button></a></center></td></tr>');
                    });
                }
    });


        $.ajax({
            type : "POST",
            url: base_url+"/book_controller/countbook",
            //data: {usernamecheck:'basadultindy0'},
            dataType: "json",
            success: function(data)
            {
                $("#paging").empty();
                $.each(data,function(i,count){
                    $("#paging").append("<li id='row' class='disabled'><span>rows "+(offset+1)+" to "+(offset+10)+"</span></li>");
                    $("#paging").append("<li id='first'><a href='#' id='1'>&laquo;</a></li>");

                    var page = Math.ceil(count/10);
                    if(page < 6)
                    {
                        for(var num=1;num<=page;num++)
                            if(num==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+num+"'>"+num+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+num+"'>"+num+"</a></li>");
                    }
                    else{
                    if(pageselect==page)
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++){
                            if((m+1)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                        }
                    }
                    else if(pageselect==(page-1))
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++)
                            if((m+2)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                    }
                    else{
                        for(var j=pageselect-1,k=0;k<numpageshow;j++,k++)
                        {
                            if((pageselect-1)<(numpageshow-lengthshow))
                                if((k+1)>page)
                                    break;
                                else
                                    if((k+1)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                            else
                                if((j+1)>(page+lengthshow))
                                    break;
                                else
                                    if(((j+1)-lengthshow)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                        }
                    }
                    }

                    $("#paging").append("<li id='last'><a href='#' id='"+page+"'>&raquo;</a></li>");
                    $("#paging").append("<li id='total' class='disabled'><span>page "+pageselect+" of "+page+"</span></li>");
                });

            }
    });

    }else{

        var selectmessage=[];
        var selectshowtype=[];
        $("#selectsearch option:selected").each(function (){
            selectmessage.push($(this).val());
        });

        if(selectmessage.length === 0)
            selectmessage.push('none');

        $("#selectshowtype option:selected").each(function (){
            selectshowtype.push($(this).val());
        });

        if(selectshowtype.length === 0)
            selectshowtype.push('none');

        $.ajax({
            type : "POST",
            url: base_url+"/book_controller/searchbook",
            data: {datasearch:{'selectmessage':selectmessage,'selectshowtype':selectshowtype,
            'inputsearch':$('#inputsearch').val(),'offset':0}},
            dataType: "json",
            success: function(data)
            {
                $("#bodyshowdata").empty();

                $.each(data,function(ID,book){

                    $("#bodyshowdata").append('<tr><td><center>'+book.book_id+'</center></td><td><center>'+book.book_name+'</center></td><td><center>'+book.book_isbn+'</center></td><td><center>'+book.publishing_name+'</center></td><td><center>'+book.no_mubook+'</center></td><td><center>'+book.no_submubook+'</center></td><td><center>'+book.title_name+'</center></td><td><center>'+book.author_name_n_lname+'</center></td><td><center><a href='+base_url+'/book_controller/book/'+book.book_id+'><button type="button" class="btn btn-success" id='+book.book_id+'><i class="glyphicon glyphicon-eye-open"></i>&nbsp;view</button></a></center></td></tr>');
                });

            }
        });

        $.ajax({
            type : "POST",
            url: base_url+"/book_controller/countsearchbook",
            data: {datasearch:{'selectmessage':selectmessage,'selectshowtype':selectshowtype,
            'inputsearch':$('#inputsearch').val()}},
            dataType: "json",
            success: function(data)
            {
                $.each(data,function(i,count){
                    $("#paging").empty();
                    //console.log(count.countsearchemp);
                    $("#paging").append("<li id='row' class='disabled'><span>rows "+(offset+1)+" to "+(offset+10)+"</span></li>");
                    $("#paging").append("<li id='first'><a href='#' id='1'>&laquo;</a></li>");

                    var page = Math.ceil(count.count/10);

                    //console.log(page);

                    if(page < 6)
                    {
                        for(var num=1;num<=page;num++)
                            if(num==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+num+"'>"+num+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+num+"'>"+num+"</a></li>");
                    }
                    else{
                    if(pageselect==page)
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++){
                            if((m+1)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                        }
                    }
                    else if(pageselect==(page-1))
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++)
                            if((m+2)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                    }
                    else{
                        for(var j=pageselect-1,k=0;k<numpageshow;j++,k++)
                        {
                            if((pageselect-1)<(numpageshow-lengthshow))
                                if((k+1)>page)
                                    break;
                                else
                                    if((k+1)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                            else
                                if((j+1)>(page+lengthshow))
                                    break;
                                else
                                    if(((j+1)-lengthshow)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");

                        }
                    }
                    }

                    $("#paging").append("<li id='last'><a href='#' id='"+page+"'>&raquo;</a></li>");
                    $("#paging").append("<li id='total' class='disabled'><span>page "+pageselect+" of "+page+"</span></li>");

                });

            }
    });

    }
}
</script>
<!-- END GLOBAL SCRIPTS -->
