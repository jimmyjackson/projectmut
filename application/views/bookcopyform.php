<!--PAGE CONTENT -->
<div id="content">
    <div class="inner">
        <div class="row has-success" >
            <div class="col-lg-5" style="padding-top: 30px; ">
            <h1 class="page-header">จัดการข้อมูลสำเนาหนังสือ</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">ข้อมูลการรับหนังสือ</div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td style="width: 20%; text-align: right; background-color: #EEEEEE; font-weight: bold;">รหัสใบรับสินค้า</td>
                                    <td style="width: 30%;"><?php echo $dataReceiptorderdetial[0]->receiptorder_id; ?></td>
                                    <td style="width: 20%; text-align: right; background-color: #EEEEEE; font-weight: bold;">วันที่รับ</td>
                                    <td style="width: 30%;"><?php echo $dataReceiptorderdetial[0]->receiptorder_date; ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right; background-color: #EEEEEE; font-weight: bold;">ชื่อหนังสือ</td>
                                    <td><?php echo $dataReceiptorderdetial[0]->publishing_name; ?></td>
                                    <td style="text-align: right; background-color: #EEEEEE; font-weight: bold;">สำนักพิมพ์</td>
                                    <td><?php echo $dataReceiptorderdetial[0]->book_name; ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right; background-color: #EEEEEE; font-weight: bold;">จำนวน</td>
                                    <td><?php echo $dataReceiptorderdetial[0]->receiptorderdetial_unit; ?></td>
                                    <td style="text-align: right; background-color: #EEEEEE; font-weight: bold;">ราคารวม</td>
                                    <td><?php echo number_format($dataReceiptorderdetial[0]->receiptorderdetial_sumprice); ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right; background-color: #EEEEEE; font-weight: bold;">รับโดย</td>
                                    <td colspan="3"><?php echo $dataEmployee[0]->employee_name.' '.$dataEmployee[0]->employee_lname; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">แบบฟอร์มข้อมูลสำเนาหนังสือ</div>
                    <div class="panel-body">
                        <form id="search_purchaseorder" action="<?php echo site_url("bookcopy_controller/bookcopy"); ?>" method="post" class="form-inline">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-2 col-md-2">
                                        <div class="form-group field-positionbook_tutee">
                                            <label class="control-label">ตู้ที่</label>
                                            <input type="text" class="form-control" id="positionbook-positionbook_tutee" name="positionbook_tutee">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2">
                                        <div class="form-group field-positionbook_chantee">
                                            <label class="control-label">ชั้นที่</label>
                                            <input type="text" class="form-control" id="positionbook-positionbook_chantee" name="positionbook_chantee">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2">
                                        <div class="form-group field-positionbook_chongtee">
                                            <label class="control-label">ช่องที่</label>
                                            <input type="text" class="form-control" id="positionbook-positionbook_chongtee" name="positionbook_chongtee">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>


                                    <div class="col-lg-2 col-md-2">
                                        <div class="form-group field-bookcopy_year">
                                            <label class="control-label">ปีที่พิมพ์</label>
                                            <input type="text" class="form-control" id="bookcopy-year" name="bookcopy_year">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2">
                                        <div class="form-group field-bookcopy_number">
                                            <label class="control-label">ครั้งที่พิมพ์</label>
                                            <input type="text" class="form-control" id="bookcopy-number" name="bookcopy_number">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-group field-bookcopy_callno">
                                            <label class="control-label">เลขเรียกหนังสือ</label>
                                            <input type="text" class="form-control" id="bookcopy-bookcopy_callno" name="bookcopy_callno">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2">
                                        <div class="form-group" style="margin-top: 25px;">
                                            <button type="submit" name="savebookcopy" id="savebookcopy" class="btn btn-primary">
                                                <i class="glyphicon glyphicon glyphicon-floppy-disk"></i> บันทึก
                                            </button>
                                            <input type="hidden" name="receiptorder_id" value="<?php echo $receiptorder_id; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ยืนยันการบันทึกข้อมูล</h4>
            </div>
            <div class="modal-body">
                คุณต้องการบันทึกข้อมูลใช่หรือไม่?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-primary" id="btnConfirm">ยืนยัน</button>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $('#savebookcopy').on('click',function(){

        var tutee = $('#positionbook-positionbook_tutee').val();
        var chantee = $('#positionbook-positionbook_chantee').val();
        var chongtee = $('#positionbook-positionbook_chongtee').val();
        var callno = $('#bookcopy-bookcopy_callno').val();

        var year = $('#bookcopy-year').val();
        var number = $('#bookcopy-number').val();



        var status = '';
        if(tutee === ''){
            $('.field-positionbook_tutee').addClass('has-error');
            $('.field-positionbook_tutee .help-block').html('กรุณากรอกตู้ที่');
            status = 'error';
        }else{
            $('.field-positionbook_tutee').removeClass('has-error');
            $('.field-positionbook_tutee .help-block').html('');
        }
        if(chantee === ''){
            $('.field-positionbook_chantee').addClass('has-error');
            $('.field-positionbook_chantee .help-block').html('กรุณากรอกชั้นที่');
            status = 'error';
        }else{
            $('.field-positionbook_chantee').removeClass('has-error');
            $('.field-positionbook_chantee .help-block').html('');
        }
        if(chongtee === ''){
            $('.field-positionbook_chongtee').addClass('has-error');
            $('.field-positionbook_chongtee .help-block').html('กรุณากรอกช่องที่');
            status = 'error';
        }else{
            $('.field-positionbook_chongtee').removeClass('has-error');
            $('.field-positionbook_chongtee .help-block').html('');
        }
        if(callno === ''){
            $('.field-bookcopy_callno').addClass('has-error');
            $('.field-bookcopy_callno .help-block').html('กรุณากรอกเลขเรียกหนังสือ');
            status = 'error';
        }else{
            $('.field-bookcopy_callno').removeClass('has-error');
            $('.field-bookcopy_callno .help-block').html('');
        }
        if(year === ''){
            $('.field-bookcopy_year').addClass('has-error');
            $('.field-bookcopy_year .help-block').html('กรุณากรอกปีที่พิมพ์');
            status = 'error';
        }else{
            $('.field-bookcopy_year').removeClass('has-error');
            $('.field-bookcopy_year .help-block').html('');
        }
        if(number === ''){
            $('.field-bookcopy_number').addClass('has-error');
            $('.field-bookcopy_number .help-block').html('กรุณากรอกครั้งที่พิมพ์');
            status = 'error';
        }else{
            $('.field-bookcopy_number').removeClass('has-error');
            $('.field-bookcopy_number .help-block').html('');
        }

        if(status !== ''){
            return false;
        }else{
            $('#modalConfirm').modal('show');
            return false;
        }

    });

    $('#btnConfirm').on('click',function(){
        $('#search_purchaseorder').submit();
    });

});
</script>
