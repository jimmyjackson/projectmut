  <!--PAGE CONTENT -->
      <div id="content">
          
                <div class="inner">
                    <div class="row has-success" >
                <div class="col-lg-5" style="padding-top: 30px; ">
                    <h1 class="page-header">พนักงานห้องสมุด</h1>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            ข้อมูลของพนักงาน
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <span role="form">
                                    
                                        <div class="form-group">    
                                        
                                         <label class="control-label col-lg-4">รูปประจำตัว</label>
                        <div class="col-lg-8">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="<?php echo base_url()?>assets/images/demoUpload.jpg" id="imageprofile"/></div>
                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                        <div>
        
<form id="empForm" action="<?php echo base_url()?>emp_controller/doupload" method="post" enctype="multipart/form-data">   
<span class="btn btn-file btn-primary"><span class="fileupload-new"><i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;Select image</span>
<input type="file" name="file" id="file"/></span>
                                </div>
                            </div>
                        </div>
                       </div>                           
                        </span>

                       
                                            <div class="form-group">                            
                                            <br><label class="control-label col-lg-6">หมายเลขประจำตัวพนักงาน<br></label>
                                            <input type="text"  style="width: 30%" class="form-control" value="<?php echo $idemp ?>" disabled id="id">
                                            <br>
                                            
                                            <label class="control-label col-lg-4">ชื่อ</label>
                                            <input type="text" style="width: 47%" class="form-control" id="name" tabindex="1">
                                            <br>
                                            
                                            <label class="control-label col-lg-4">นามสกุล</label>
                                            <input type="text" style="width: 47%" class="form-control" id="lname" tabindex="2">
                                            <br>
                                                                                        
                                            <div class="form-group">
                                            <label class="control-label col-lg-4">เพศ</label>
                                            <div class="col-lg-5">
                                            <select name="sex" id="sex" class="validate[required] form-control" style="width: 47%" tabindex="3">
                                                    <option value="ชาย">ชาย</option>
                                                    <option value="หญิง">หญิง</option>
                                                    <option value="ไม่ระบุ">ไม่ระบุ</option>
                                                   </select><br>

                                            </div>
                                        </div>
                                        <div class="form-group">
                        <label class="control-label col-lg-4" >วันเกิด</label>

                        <div class="col-lg-5">
                           <div class="input-group" id="dp3">
                                <input class="form-control" type="text" id="birthdate" placeholder="วันเกิด dd/mm/yyyy" tabindex="4"/>
                                <span class="input-group-addon">
                                    <span class="icon-calendar"></span>
                                </span>
                            </div>                             
                        </div>                           
                    </div>
                                        </div>
                                        
                                        </div> 
                                        
                                        	<label class="control-label col-lg-2">รหัสบัตรประชาชน</label>
                                            <input type="text" style="width: 23%" class="form-control" id="idcard" maxlength="13" tabindex="9">
                                            <br>
                                            
                                            <label class="control-label col-lg-2">E-mail</label>
                                            <input type="text" style="width: 23%" class="form-control" id="email" tabindex="10">
                                            <br>
                                            
                                            <label class="control-label col-lg-2">Username</label>
                                    <input type="text" style="width: 23%" class="form-control" id="user" maxlength="16" tabindex="11" disabled>
                          
                            
                                            <br>
                                            
                                            <label class="control-label col-lg-2">Password</label>
                                            <button type="button" class="btn btn-info" id="btchangepassword" name="checkpass" tabindex="12"><i class="glyphicon glyphicon-lock"></i>&nbsp;&nbsp;Change Password</button>
                                          
                                            <br>
                                            <div class="form-group">
                                            <label class="control-label col-lg-2">ตำแหน่ง</label>
                                            <div class="col-lg-4">
                                      <select name="position" id="position" class="validate[required] form-control" style="width: 70%" tabindex="13">
                                                    <option value="">ตำแหน่ง</option>
                                            
                                              </select>
                                            </div>
                                        
                                        	<div class="form-group">
                                            <label class="control-label col-lg-2"><br>ที่อยู่</label>
                                            <div class="col-lg-4">
                                            <br><textarea class="form-control" rows="3" style="width: 70%" id="address" tabindex="14"></textarea><br>
                                            </div>
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-4">
                                                <select name="province" id="province" class="validate[required] form-control" style="width: 70%" tabindex="15">
                                                    <option value="">จังหวัด</option>
                                              </select>
                                            </div>
                                            <div class="col-lg-6">&nbsp;</div>
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-4">
                                              <select name="zone" id="zone" class="validate[required] form-control" style="width: 70%" tabindex="16">     
                                                    <option value="">อำเภอ</option>
                                              </select>
                                            </div>
                                            <div class="col-lg-8">&nbsp;</div>
                                            <div class="col-lg-8">
                                            <label class="control-label col-lg-3">โทร</label>
                                            <div class="input-group col-lg-4">      
                                            <input type="text" id="tel" class="form-control col-lg-4" maxlength="10" tabindex="5">
                                            <span class="input-group-addon add-on"><i id="icondate" class="glyphicon glyphicon-earphone"></i></span></div><div class="col-lg-1"></div>
                                                
                                            <button type="button" id="btadd" class="btn btn-primary" tabindex="6"><i class="icon-pencil icon-white"></i> เพิ่ม</button>
                                            <button type="button" id="btdel" class="btn btn-danger" tabindex="7"><i class="icon-remove icon-white"></i> ลบ</button>
                                            </div>
                                            
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-4">
                                              <select name="district" id="district" class="validate[required] form-control" style="width: 70%" tabindex="17">
                                                    <option value="">ตำบล</option>
                                               </select>
                                            </div>
 
                                            <div class="form-group">
                                                
                                            <div class="form-group">    
                                            <div class="col-lg-8">
                                                                                
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>                                            

                                            <div class="col-lg-3">
                                            </div>
                                            
                                            <div class="col-lg-6">
                        
                                                <select id="tellist" multiple class="form-control" rows="3" style="width: 72%" tabindex="8"></select>
                                            </div>
                                              
                                            </div> 
                                            
                                                
                                            
                                            </div>
                                                  
                                            </div>
                                            <div class="col-lg-3">&nbsp;
                                            </div>
                                            <div class="col-lg-4">
                                             <select name="postcode" id="postcode" class="validate[required] form-control" style="width: 50%" tabindex="18">
                                                    <option value="">รหัสไปรษณีย์</option>
                                              </select>
                                              </div> 
                                            
                                            </div>    
                                            
                                                <label class="control-label col-lg-5"></label>
                                            <div class="col-lg-4">
                                            <button type="submit" name="submit" id="btsave" class="btn btn-primary" tabindex="19"><i class="glyphicon glyphicon-floppy-saved"></i> บันทึก</button>
                                            <button type="button" id="btdelete" class="btn btn-danger" tabindex="20"><i class="glyphicon glyphicon-floppy-remove"></i> ลบ</button>
                                            <input type="hidden" id="statuspage" value="edit">
                                            <button type="button" id="btcancel" class="btn btn-warning" tabindex="21"><i class="icon-remove icon-white"></i> ยกเลิก</button>	
                                             
                                            </div>
                                                
                                            </div>
                                            </div>
                                            
                                            </div>
                          
                                            </div>
                                            </span>  
                                           </form>
                                            </div>
                                            
    <div class="bgCover">&nbsp;</div> 
    <div class="overlayBox">
        <div class="overlayContent">
            <div class="row">
                
                    <div class="input-group col-lg-12">
                        <h4 class="text-center" style="color: black" id="show_status_pass">Change Password</h4>
                    </div> 
                    
                    <div class="input-group col-lg-12">
                        &nbsp;
                    </div>
                    
                    <label class="control-label col-lg-4">Password</label>
                    <div class="input-group col-lg-8">
                        <input type="password" class="form-control" id="pass_ed" maxlength="16" placeholder="กรอก Password">
                        <span class="input-group-addon add-on">
                            <font color="red">&#42;</font>
                        </span>
                    </div>
                    
                    <br>
                    
                    <div class="input-group col-lg-12">
                        &nbsp;
                    </div>
                                    
                    <label class="control-label col-lg-4">Confirm Password</label>
                    <div class="input-group col-lg-8">
                        <input type="password" class="form-control" id="conpass_ed" maxlength="16" autocomplete="off" placeholder="กรอก Confirm Password">
                        <span class="input-group-addon add-on">
                            <font color="red">&#42;</font>
                        </span>
                    </div>
                    
                    <div class="input-group col-lg-12">
                        &nbsp;
                    </div> 

                    <div class="input-group col-lg-12">
                        <center>
                            <button class="btn btn-success" style="display: block; width: 100%;" id="bt_edit_pass">ยืนยัน</button>
                        </center>
                    </div>
                   

            </div>
        </div>
    </div>
                
                </div>
            <!--END PAGE CONTENT -->
    </div>
    <!--END MAIN WRAPPER -->
<!-- GLOBAL SCRIPTS -->
<script src="<?php echo base_url()?>assets/js/validatefromall.js"></script>
<script src="<?php echo base_url()?>assets/js/empfrom.js"></script>
<!-- END GLOBAL SCRIPTS -->
<script type="text/javascript">
    var base_url = window.location.origin+"/CI";
    var password_edit;
    var init=function(){   
        
        $.ajax({
                type : "GET",
                url: base_url+"/emp_controller/selectdataemp/<?php echo $idemp ?>",
                dataType: "json",
                success: function(data)
                {
                    $("#name").val(data[0].employee_name);
                    $("#lname").val(data[0].employee_lname);
                    $("#birthdate").val(data[0].employee_brithday.substring(8)+'/'
                    +data[0].employee_brithday.substring(5,7)+'/'
                    +data[0].employee_brithday.substring(0,4));
                    $("#idcard").val(data[0].employee_idcard);
                    $("#email").val(data[0].employee_email);
                    $("#user").val(data[0].employee_username);
                    $("#address").val(data[0].employee_address);
                    
                    if(data[0].employee_image!=null)
                        $("#imageprofile").attr('src',data[0].employee_image);
                    
                    if(data[0].employee_sex=="ชาย")
                        $("#sex option[value='ชาย']").attr('selected', 'selected');
                    else
                        $("#sex option[value='หญิง']").attr('selected', 'selected');
                    
                    if(data[0].telemployee_tel!=null)
                        for(var i=0;i<data.length;i++)
                        {
                            $("#tellist").append("<option value='"+data[i].telemployee_tel+
                            "'>"+data[i].telemployee_tel+"</option>");
                        }
                    
                    $("#position option").each(function()
                    {
                        if(data[0].id_position==this.value)
                            $(this).attr('selected', 'selected');
                    });
                    
                    $("#province option").each(function()
                    {
                        if(data[0].id_province==this.value)
                            $(this).attr('selected', 'selected');
                    });
                    
                    showallzone_edit(data[0].id_province,data[0].id_zone);
                        showalldistrict_edit(data[0].id_province,data[0].id_zone,data[0].id_district); 
                    showallpostcode_edit(data[0].id_province,data[0].id_zone,data[0].id_district,
                    data[0].id_postcode);
                }
        });
    }
    window.onload = init;
    
    function showOverlayBox() {
        //if box is not set to open then don't do anything
        //if( isOpen == false ) return;
        // set the properties of the overlay box, the left and top positions
        
        $('.overlayBox').css({
            display:'block',
            left:( $(window).width() - $('.overlayBox').width() )/2,
            top:(( $(window).height() - $('.overlayBox').height() )/2 -20)+20,
            position:'fixed'
        });
        // set the window background for the overlay. i.e the body becomes darker
        $('.bgCover').css({
            display:'block'
        });
        
    }
    function doOverlayOpen() {
        
        showOverlayBox();
        $('.bgCover').css({opacity:0}).animate( {opacity:0.5, backgroundColor:'#000'} );
        // dont follow the link : so return false.
        return false;
        
    }
    function doOverlayClose() {
        //set status to closed
        //isOpen = false;
        $('.overlayBox').css( 'display', 'none' );
        // now animate the background to fade out to opacity 0
        // and then hide it after the animation is complete.
        $('.bgCover').animate( {opacity:0}, null, null, function() { $(this).hide(); } );
    }
    // if window is resized then reposition the overlay box
    $(window).bind('resize',showOverlayBox);
    // activate when the link with class launchLink is clicked
    $('#btchangepassword').click( doOverlayOpen );
    // close it when bgCover is clicked
    $('div.bgCover').click( doOverlayClose );
    
    $("#bt_edit_pass").click(function(){ 
    
    if($("#conpass_ed").val() != $("#pass_ed").val()){
        $("#show_status_pass").html("กรอก Password กับ Confirm Password ให้ตรงกัน");
        $("#pass_ed").focus();
    }
    
    else if($("#pass_ed").val().length < 6 || $("#pass_ed").val().length > 16){
        $("#show_status_pass").html("กรอก Password ความยาวตั้งแต่ 6 ถึง 15 ตัวอักษร");
        $("#pass_ed").focus();
    }
    
    else{
        $("#show_status_pass").html("Password สามารถใช้งานได้");
        var a = confirm("ต้องการเปลี่ยน Password ใช่หรือไม่");
        if(a){
            $.ajax({
                type : "POST",
                url: base_url+"/emp_controller/edit_password_emp",
                data: {'objemp':[{
                        password_edit:$("#pass_ed").val(),id_emp:$("#id").val()}]},
                dataType: "json",
                success: function(data)
                {
                    if(data === true){
                        alert("เปลี่ยน Password สำเร็จ");
                        $("#pass_ed").val("");
                        $("#conpass_ed").val("");
                        doOverlayClose();
                    }else{
                        alert("เปลี่ยน Password ไม่สำเร็จ");
                    }
                }
            });
            
        }
    }
    
});
    
</script>