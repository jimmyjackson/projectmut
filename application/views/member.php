  <!--PAGE CONTENT -->
      
      <div id="content">
          
                <div class="inner">
                    <div class="row has-success" >
                <div class="col-lg-5" style="padding-top: 30px; ">
                    <h1 class="page-header">สมาชิกห้องสมุด</h1>
                </div>
            </div>
            <input type="hidden" id="base" />
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            ข้อมูลของสมาชิก
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <span role="form">
                                    
                                        <div class="form-group">    
                                        
                                         <label class="control-label col-lg-4">รูปประจำตัว</label>
                        <div class="col-lg-8">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="<?php echo base_url()?>assets/images/demoUpload.jpg" id="imageprofile"/></div>
                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                        <div>
        
<form id="memForm" action="" method="post" enctype="multipart/form-data">   
<span class="btn btn-file btn-primary"><span class="fileupload-new"><i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;Select image</span>
<input type="file" name="file" id="file"/></span>
                                </div>
                            </div>
                        </div>
                       </div>                           
                        </span>

                       
                                            <div class="form-group">                            
                                            <br><label class="control-label col-lg-6">หมายเลขประจำตัวสมาชิก<br></label>
                                            <input type="text" style="width: 30%" class="form-control" value="Auto_Id" disabled id="id" name="id">
                                            <br>
                                            
                                            <label class="control-label col-lg-4">ชื่อ</label>
                                            <div class="input-group col-lg-6">
                                            <input type="text" class="form-control" id="name" name="name" placeholder="กรอกชื่อ" tabindex="1">
                                            <span class="input-group-addon add-on">
                                                <font color="red">&#42;</font>
                                            </span>
                                            </div>
                                            <br>
                                            <div class="input-group col-lg-9">
                                            &nbsp;
                                            </div>
                                            
                                            <label class="control-label col-lg-4">นามสกุล</label>
                                            <div class="input-group col-lg-6">
                                            <input type="text" class="form-control" id="lname" name="lname" placeholder="กรอกนามสกุล" tabindex="2">
                                            <span class="input-group-addon add-on">
                                                <font color="red">&#42;</font>
                                            </span>
                                            </div>
                                            <br>
                                            <div class="input-group col-lg-9">
                                            &nbsp;
                                            </div>
                                                                                        
                                            <div class="form-group">
                                            <label class="control-label col-lg-4">เพศ</label>
                                            <div class="input-group col-lg-4">
                                            <select name="sex" id="sex" class="validate[required] form-control" tabindex="3">
                                                    <option value="ชาย">ชาย</option>
                                                    <option value="หญิง">หญิง</option>
                                            </select>
                                            <span class="input-group-addon add-on">
                                                <font color="red">&#42;</font>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="input-group col-lg-9">
                                            &nbsp;
                                            </div>
                                        
                                        <div class="form-group">
                        <label class="control-label col-lg-4" >วันเกิด</label>

                        <div class="col-lg-5">
                           <div class="input-group input-append date" id="dp3">
                                <input class="form-control" type="text" value="" id="birthdate" name="birthdate" placeholder="วันเกิด dd/mm/yyyy" tabindex="4"/>
                                <span class="input-group-addon add-on"><i id="icondate" class="icon-calendar"></i></span>
                            </div> 
                                                       
                        </div>                           
                    </div>
                                       
                           
                    <div class="input-group col-lg-12">
                                            &nbsp;
                    </div>                   
                    
                                                                           
                    <div class="form-group">
                        <label class="control-label col-lg-4" >วันสมัครสมาชิก</label>

                        <div class="col-lg-6">
                           <div class="input-group input-append date" id="dp3">
                                <input class="form-control" type="text" value="" id="regisdate" name="regisdate" placeholder="วันที่สมัครสมาชิก dd/mm/yyyy" tabindex="4"/>
                                <span class="input-group-addon add-on"><i id="icondate" class="icon-calendar"></i></span>
                            </div> 
                                                       
                        </div>                           
                    </div>   
                                       
                    <div class="input-group col-lg-12">
                                            &nbsp;
                    </div>  
                                        </div>
                                        
                                        </div> 
                                        
                                        
                                        
                                        	<label class="control-label col-lg-2">ประเภทสมาชิก</label>
                                            <div class="input-group col-lg-3">
                                            <select name="typemember" id="typemember" class="validate[required] form-control" tabindex="17">
                                                    <option value="1">นักเรียน</option>
                                                    <option value="2">บุคลากร</option>
                                            </select>
                                            <span class="input-group-addon add-on">
                                                <font color="red">&#42;</font>
                                            </span>
                                            </div>
                                            <br>
                                            <div class="input-group col-lg-5">
                                            &nbsp;
                                            </div>
                                            
                                            <label class="control-label col-lg-2">E-mail</label>
                                            <div class="input-group col-lg-3">
                                            <input type="text" class="form-control" id="email" name="email" placeholder="กรอก Email" tabindex="10">
                                            <span class="input-group-addon add-on">
                                                <font color="red">&#42;</font>
                                            </span>
                                            </div>
                                            <br>
                                            <div class="input-group col-lg-5">
                                            &nbsp;
                                            </div>
                                            
                                            <label class="control-label col-lg-2">Username</label>
                                    <div class="input-group col-lg-3">
                                            <input type="text" class="form-control" id="user"  name="user" maxlength="16" placeholder="กรอก Username 8 - 16 ตัว" tabindex="11">
                                            <span class="input-group-addon add-on">
                                                <font color="red">&#42;</font>
                                            </span>
                                            </div>
                                            <br> 
                                            <div class="input-group col-lg-5">
                                            &nbsp;
                                            </div>
                                                
                                        <div>
                                          <label class="control-label col-lg-2">Check Username</label>
                                          <div class="input-group col-lg-3">
                                           <button type="button" class="btn btn-info" id="btcheckuser" name="checkuser"><i class="glyphicon glyphicon-hand-right" tabindex="12"></i>&nbsp;&nbsp;Check Username
                                           </button>&nbsp;&nbsp;&nbsp;&nbsp;
                                           <span id="showstatususer" class="btn" style='background-color: white;'></span>
                                           </div>
                                        </div>
                                            <div class="input-group col-lg-5">
                                            &nbsp;
                                            </div>
                                            
                                            <label class="control-label col-lg-2">Password</label>
                                              <div class="input-group col-lg-3">
                                            <input type="password" class="form-control" id="pass" name="pass" maxlength="16" placeholder="กรอก Password" tabindex="13">
                                            <span class="input-group-addon add-on">
                                                <font color="red">&#42;</font>
                                            </span>
                                            </div>
                                            <br>
                                            <div class="input-group col-lg-5">
                                            &nbsp;
                                            </div>
                                    
                                            <label class="control-label col-lg-2">Confirm Password</label>
                                          <div class="input-group col-lg-3">
                                            <input type="password" class="form-control" id="conpass" maxlength="16" autocomplete="off" placeholder="กรอก Confirm Password" tabindex="14">
                                            <span class="input-group-addon add-on">
                                                <font color="red">&#42;</font>
                                            </span>
                                            </div>
                                            <br>
                                            <div class="input-group col-lg-5">
                                            &nbsp;
                                            </div>
                                            
                                            <div class="form-group">
                                            <label class="control-label col-lg-2">รหัสบัตรประชาชน</label>
                                            <div class="input-group col-lg-3">
                                            <input type="text" class="form-control" id="idcard" name="idcard" maxlength="13" placeholder="กรอกรหัสบัตรประชาชน 13 หลัก" tabindex="9" disabled>
                                            <span class="input-group-addon add-on">
                                                <font color="red">&#42;</font>
                                            </span>
                                            </div>
                                            
                                            <div class="input-group col-lg-5">
                                            &nbsp;
                                            </div>
                                            
                                            <div class="form-group">
                                            <label class="control-label col-lg-2">รหัสนักเรียน</label>
                                            <div class="input-group col-lg-3">
                                            <input type="text" class="form-control" id="idstudentcard" name="idstudentcard" maxlength="10" placeholder="กรอกรหัสนักเรียน 10 หลัก" tabindex="9">
                                            <span class="input-group-addon add-on">
                                                <font color="red">&#42;</font>
                                            </span>
                                            </div>
                                            </div>

                                            <div class="input-group col-lg-5">
                                            &nbsp;
                                            </div>

                                            <div class="input-group col-lg-5">
                                            &nbsp;
                                            </div>

                                        	<div class="form-group">
                                            <label class="control-label col-lg-2"><br>ที่อยู่</label>
                                            <div class="input-group col-lg-3">
                                            <textarea class="form-control" rows="3" id="address" name="address" placeholder="กรอก ที่อยู่ บ้านเลขที่ หมู่ที่ ซอย ถนน" tabindex="16"></textarea>
                                            <span class="input-group-addon add-on">
                                                <font color="red">&#42;</font>
                                            </span>
                                            </div>
                                            <div class="input-group col-lg-5">
                                            &nbsp;
                                            </div>
                                            
                                            
                                            <div><label class="control-label col-lg-6"></label></div>
                                            <div class="form-group">
                                                
                                            <div class="form-group">    
                                            <div class="col-lg-8">
                                            <label class="control-label col-lg-3">โทร</label><div class="input-group col-lg-4">
                                            <input type="text" id="tel" class="form-control col-lg-4" maxlength="10" tabindex="5" disabled><span class="input-group-addon add-on"><i id="icondate" class="glyphicon glyphicon-earphone"></i></span></div><div class="col-lg-1"></div> 
                                            <button type="button" id="btadd" class="btn btn-primary" tabindex="6"><i class="icon-pencil icon-white"></i> เพิ่ม</button>
                                            <button type="button" id="btdel" class="btn btn-danger" tabindex="7"><i class="icon-remove icon-white"></i> ลบ</button>	
                                            </div>
                                                
                                            <div class="input-group col-lg-3">
                                                <select name="province" id="province"  class="validate[required] form-control" tabindex="17">
                                                    <option value="">จังหวัด</option>
                                              </select>
                                              <span class="input-group-addon add-on">
                                                  <font color="red">&#42;</font>
                                              </span>
                                            </div>
                                            </div>
                                                <br>
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>
                                            
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>
                                                
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>
                                                
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>
                                                
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>
                                    
                                                
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>
                                                
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>
                                                
                                                
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>
                                                
                                                
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>
                                                
                                                
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>
                                                
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>
                                                
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>
                                                
                                                
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>
                                                
                                                
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>
                                                
                                                
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>
                                                
                                                
                                            
                                                
                                                
                                            <div class="form-group">
                                            <div class="col-lg-2"></div>       
                                            <div class="col-lg-4">
                                            <select id="tellist" multiple class="form-control" rows="3" style="width: 72%" tabindex="8" disabled></select></div> 
                                                <div class="col-lg-2"></div>
                                            <div class="input-group col-lg-3">
                                      <select name="zone" id="zone" class="validate[required] form-control" tabindex="18">     
                                            <option value="">อำเภอ</option>
                                              </select>
                                              <span class="input-group-addon add-on">
                                                  <font color="red">&#42;</font>
                                              </span>
                                            </div>
                                            </div>
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            
                                        
                                        <label class="control-label col-lg-6"></label>
                                        <label class="control-label col-lg-2"></label>
                                            <div class="input-group col-lg-3">
                                              <select name="district" id="district" class="validate[required] form-control" tabindex="19">
                                                    <option value="">ตำบล</option>
                                              </select>
                                              <span class="input-group-addon add-on">
                                                  <font color="red">&#42;</font>
                                              </span>
                                            </div>
                                                <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>   
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div> 
                                            <div class="form-group">
                                            <div class="col-lg-6"></div>
                                            <div class="col-lg-6"></div>
                                            </div>                                       
                                            <label class="control-label col-lg-8"></label>
                                            <div class="input-group col-lg-3">
                                             <select name="postcode" id="postcode" class="validate[required] form-control" tabindex="20">
                                                    <option value="">รหัสไปรษณีย์</option>
                                              </select>
                                              <span class="input-group-addon add-on">
                                                  <font color="red">&#42;</font>
                                              </span>
                                            </div>
                                            	
                                            
                                            </div>    

                                            <label class="control-label col-lg-5"></label>
                                            <div class="col-lg-4">
                                            <button type="button" name="btsave" id="btsave" class="btn btn-primary" tabindex="21"><i class="glyphicon glyphicon-floppy-saved"></i> บันทึก</button>
                                            <input type="hidden" id="statuspage" value="add">
                                            <button type="button" id="btcancel" class="btn btn-danger" tabindex="22"><i class="icon-remove icon-white"></i> ยกเลิก</button>	
                                              
                                            </div>
                                                
                                            </div>
                                            </div>
                                            
                                            </div>
                          
                                            </div>
                                            </span>  
                                           </form>
                                            </div>
                </div>
            <!--END PAGE CONTENT -->
    </div>
     <!--END MAIN WRAPPER -->
<!-- GLOBAL SCRIPTS -->

<script src="<?php echo base_url()?>assets/js/validatememberfrom.js"></script>
<script src="<?php echo base_url()?>assets/js/memberfrom.js"></script>
<script>
validate_member.init_validate_member();
member.member_init_add();    
</script>
<!-- END GLOBAL SCRIPTS -->