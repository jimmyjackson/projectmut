<!--PAGE CONTENT -->
<div id="content">
    <div class="inner">
        <div class="row has-success" >
            <div class="col-lg-5" style="padding-top: 30px; ">
            <h1 class="page-header">ใบเสนอซื้อหนังสือ</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">ข้อมูลของใบเสนอซื้อหนังสือ</div>
                    <div class="panel-body">
                        <form id="offerorderForm" action="" method="post">
                        <div class="row">
                            <div class="col-lg-6">
                                <label class="control-label col-lg-5">รหัสใบเสนอซื้อหนังสือ</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" value="Auto_Id" disabled id="id" name="id">
                                </div>
                                <div class="input-group col-lg-12">
                                    &nbsp;
                                </div>
                                <label class="control-label col-lg-5">ปีการศึกษา</label>
                                <div class="input-group col-lg-6">
                                    <select name="year" id="year" class="validate[required] form-control" tabindex="1">
                                        <option value="">ปีการศึกษา</option>
                                    </select>
                                    <span class="input-group-addon add-on">
                                        <font color="red">&#42;</font>
                                    </span>
                                </div>
                                <div class="input-group col-lg-12">
                                    &nbsp;
                                </div>
                                <div class="col-lg-9">
                                    <button type="button" name="btadddetial" id="btadddetial" class="btn btn-info" tabindex="3">
                                        <i class="glyphicon glyphicon glyphicon-plus"></i> เพิ่มรายการหนังสือ
                                    </button>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <label class="control-label col-lg-4" >วันที่เสนอซื้อหนังสือ</label>
                                <div class="col-lg-6">
                                    <div class="input-group input-append date">
                                        <input class="form-control" type="text" id="dateoffer" name="dateoffer" disabled/>
                                        <span class="input-group-addon add-on">
                                            <i id="icondate" class="icon-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="input-group col-lg-12">
                                    &nbsp;
                                </div>
                                <label class="control-label col-lg-4">สำนักพิมพ์</label>
                                <div class="input-group col-lg-6">
                                    <select name="publishing" id="publishing" class="validate[required] form-control" tabindex="2">
                                        <option value="">สำนักพิมพ์</option>
                                    </select>
                                    <span class="input-group-addon add-on">
                                        <font color="red">&#42;</font>
                                    </span>
                                </div>
                                <div class="input-group col-lg-12">
                                    &nbsp;
                                </div>
                                <label class="control-label col-lg-4">รหัสผู้เสนอซื้อหนังสือ</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" disabled name="empid" id="empid" value="<?= $this->session->userdata('idemp');?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            &nbsp;
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <table id="example" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th><center>ลำดับ</center></th>
                                            <th><center>ชื่อหนังสือ</center></th>
                                            <th><center>ราคา</center></th>
                                            <th><center>จำนวน</center></th>
                                            <th><center>ราคารวม</center></th>
                                            <th><center>กระทำ</center></th>
                                        </tr>
                                    </thead>

                                    <tfoot>
                                        <tr>
                                            <th colspan="5" class="text-right">ราคารวมทั้งหมด</th>
                                            <th colspan="2"><center><span id="total"></span></center></th>
                                        </tr>
                                        <tr>
                                            <th colspan="5" class="text-right">
                                                <span class="pull-right">%</span>
                                                <input style="width:45px;" name="vat" id="vat" value="7" type="text" class="form-control pull-right">
                                                <span class="pull-right">ภาษีมูลค่าเพิ่ม</span>
                                            </th>
                                            <th colspan="2"><center><span id="vattotal"></span></center></th>
                                        </tr>
                                        <tr>
                                            <th colspan="5" class="text-right">ราคารวมสุทธิ</th>
                                            <th colspan="2"><center><span id="subtotal"></span></center></th>
                                        </tr>
                                    </tfoot>

                                    <tbody id="bodylisttable">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            &nbsp;
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <span role="form">
                                    <label class="control-label col-lg-4"></label>
                                    <div class="col-lg-5">
                                        <button type="button" name="btcon" id="btcon" class="btn btn-success" tabindex="5">
                                        <i class="glyphicon glyphicon-check
"></i> ขออนุมัติ
                                       </button>
                                        <button type="button" name="btsave" id="btsave" class="btn btn-primary" tabindex="6"><i class="glyphicon glyphicon-floppy-saved"></i> บันทึก</button>
                                        <input type="hidden" id="statuspage" value="add">
                                        <button type="button" id="btcancel" class="btn btn-danger" tabindex="6"><i class="icon-remove icon-white"></i> ยกเลิก</button>
                                    </div>
                                </span>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bgCover">&nbsp;</div>
    <div class="overlayBox">
        <div class="overlayContent">
            <div class="row">
                <div class="input-group col-lg-12">
                    <h4 class="text-center" style="color: black" id="show_status_pass">
                    รายการหนังสือ
                    </h4>
                </div>

                <div class="input-group col-lg-12">
                    &nbsp;
                </div>

                <label class="control-label col-lg-4">ชื่อหนังสือ</label>
                <div class="input-group col-lg-8">
                    <select name="bookname" style="width: 100%" id="bookname" class="validate[required] form-control">
                    </select>
                    <span class="input-group-addon add-on">
                        <font color="red">&#42;</font>
                    </span>
                </div>

                <div class="input-group col-lg-12">
                    &nbsp;
                </div>

                <label class="control-label col-lg-4">จำนวนหนังสือ</label>
                <div class="input-group col-lg-8">
                    <input type="text" class="form-control" id="bookunit" placeholder="กรอกจำนวน">
                    <span class="input-group-addon add-on">
                        <font color="red">&#42;</font>
                    </span>
                </div>

                <div class="input-group col-lg-12">
                    &nbsp;
                </div>

                <div class="input-group col-lg-12">
                    <center>
                        <button class="btn btn-success" style="display: block; width: 100%;" id="bt_ok">ยืนยัน</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
<!--END PAGE CONTENT -->
</div>
<!--END MAIN WRAPPER -->
<!-- GLOBAL SCRIPTS -->

<script src="<?php echo base_url()?>assets/js/validateofferorderfrom.js"></script>
<script src="<?php echo base_url()?>assets/js/offerorderfrom.js"></script>
<script>
validate_offerorder.init_validate_offerorder();
offerorder.offerorder_init_add();

function showOverlayBox() {
        $('.overlayBox').css({
            display:'block',
            left:( $(window).width() - $('.overlayBox').width() )/2,
            top:(( $(window).height() - $('.overlayBox').height() )/2 -20)+20,
            position:'fixed'
        });
        // set the window background for the overlay. i.e the body becomes darker
        $('.bgCover').css({
            display:'block'
        });

    }
    function doOverlayOpen() {
      if($("#publishing").val()==""){
          alert("กรุณาเลือกสำนักพิมพ์");
          $("#publishing").focus();
          return false;
      } else {
        showOverlayBox();
        $('.bgCover').css({opacity:0}).animate( {opacity:0.5, backgroundColor:'#000'} );
        // dont follow the link : so return false.
        return false;
      }
    }
    function doOverlayClose() {
        //set status to closed
        //isOpen = false;
        $('.overlayBox').css( 'display', 'none' );
        // now animate the background to fade out to opacity 0
        // and then hide it after the animation is complete.
        $('.bgCover').animate( {opacity:0}, null, null, function() { $(this).hide(); } );
        $('#bookname').val($('#bookname option:first-child').val()).trigger('change');
        $("#bookunit").val("");
    }
    // if window is resized then reposition the overlay box
    $(window).bind('resize',showOverlayBox);
    // activate when the link with class launchLink is clicked
    $('#btadddetial').click( doOverlayOpen );
    // close it when bgCover is clicked
    $('div.bgCover').click( doOverlayClose );
</script>
<!-- END GLOBAL SCRIPTS -->
