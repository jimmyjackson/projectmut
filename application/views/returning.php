<div id="content">
    <div class="inner">
        <div class="row has-success" >
            <div class="col-lg-5" style="padding-top: 30px; ">
            <h1 class="page-header">การคืนหนังสือ</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">ค้นหาข้อมูลการคืนหนังสือ</div>
                    <div class="panel-body">
                        <form action="<?php echo site_url("returning_controller/index"); ?>" method="post">
                            <div class="row">
                                <div class="col-lg-12">
                                  <div class="col-lg-3 col-md-3">
                                      <div class="form-group">
                                          <label class="control-label">เลขเรียกหนังสือ</label>
                                          <input type="text" class="form-control" id="bookcall" name="bookcall" value="<?php echo (!empty($_POST['bookcall']) ? $_POST['bookcall'] : ''); ?>">
                                      </div>
                                  </div>
                                  <div class="col-lg-3 col-md-3">
                                      <div class="form-group">
                                          <label class="control-label">ชื่อหนังสือ</label>
                                          <input type="text" class="form-control" id="bookname" name="bookname" value="<?php echo (!empty($_POST['bookname']) ? $_POST['bookname'] : ''); ?>">
                                      </div>
                                  </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group field-member">
                                            <label class="control-label">รหัสสมาชิก</label>
                                            <input type="text" class="form-control" id="member" name="member" value="<?php echo (!empty($_POST['member']) ? $_POST['member'] : ''); ?>">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2">
                                        <div class="form-group" style="margin-top: 25px;">
                                            <button type="submit" name="btadddetial" id="btadddetial" class="btn btn-info" tabindex="3">
                                                <i class="glyphicon glyphicon glyphicon-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php
        if(!empty($status)){
            if($status=='save'){
            ?>
                <input type="hidden" id="id_invoice" name="id_invoice" value="<?php echo (!empty($invoice) ? $invoice : ''); ?>">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <strong>สำเร็จ!</strong>ทำการคืนหนังสือเรียบร้อยแล้ว
                    </div>
                </div>
            <?php
            }
            if($status=='no'){
            ?>
              <div class="col-lg-12">
                  <div class="alert alert-danger alert-dismissible fade in" role="alert">
                      <strong>ผิดพลาด!</strong>กรุณาคลิกคืนหนังสือด้วย
                  </div>
              </div>
            <?php
            }
            if($status=='not'){
            ?>
              <div class="col-lg-12">
                  <div class="alert alert-danger alert-dismissible fade in" role="alert">
                      <strong>ผิดพลาด!</strong>กรุณาคลิกเลือก ชำรุด หรือ สูญหาย อย่างใดอย่างนึงเท่านั้น
                  </div>
              </div>
            <?php
            }
            if($status=='nomember'){
            ?>
              <div class="col-lg-12">
                  <div class="alert alert-danger alert-dismissible fade in" role="alert">
                      <strong>ผิดพลาด!</strong>กรุณากรอกรหัสสมาชิก
                  </div>
              </div>
        <?php
          }
          if($status=='countcheckreturnbook'){
        ?>
            <div class="col-lg-12">
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <strong>ผิดพลาด!</strong>เนื่องจากมีรายการหนังสือบางรายการได้ถูกคืนไปก่อนแล้ว กรุณาลองใหม่อีกครั้ง
                </div>
            </div>
        <?php
          }
        }
        if(!empty($_POST['member'])){
            if(!empty($dataBooking) && $dataBooking>0){
        ?>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                        <form id="save_receipt" action="<?php echo site_url("returning_controller/index"); ?>" method="post">
                            <table id="example" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th><center>ลำดับ</center></th>
                                        <th><center>เลขเรียกหนังสือ</center></th>
                                        <th><center>ชื่อหนังสือ</center></th>
                                        <th><center>วันที่ยืม</center></th>
                                        <th><center>เกินกำหนด(วัน)</center></th>
                                        <th><center>ค่าปรับ</center></th>
                                        <th><center>คืน</center></th>
                                        <th><center>สูญหาย</center></th>
                                        <th><center>ชำรุด</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $aa = 0;

                                    foreach ($dataBooking as $key=>$value) {

                                        $dStart = new DateTime($value->borrowing_date);
                                        $dEnd  = new DateTime(date("Y-m-d"));
                                        $dDiff = $dStart->diff($dEnd);
    //                                    echo $dDiff->format('%R'); // use for point out relation: smaller/greater


                                        $cal = $dDiff->days - $dataMember[0]->membertype_date;
                                        $cal2 = '';
                                        if($cal > 0){
                                            $cal2 = $dataMember[0]->membertype_price * $cal;
                                        }
                                    ?>
                                        <tr>
                                            <td style="text-align: center;"><?php echo ($aa+1); ?></td>
                                            <td style="text-align: center;"><?php echo $value->bookcopy_callno; ?></td>
                                            <td><?php echo $value->book_name; ?></td>
                                            <td style="text-align: center;"><?php echo $value->borrowing_date; ?></td>
                                            <td style="text-align: center;"><?php echo ($cal < 0 ? 0 : $cal); ?></td>
                                            <td style="text-align: center;" id="cal-<?php echo $value->borrowing_id; ?>"><?php echo $cal2; ?></td>
                                            <td style="text-align: center;">
                                                <input id="<?php echo $value->borrowing_id; ?>" type="checkbox" class="receipt" value="<?php echo $value->id_borrowing.'_'.$value->borrowingdetial_no; ?>" name="return[<?php echo $key; ?>]">
                                            </td>
                                            <td style="text-align: center;">
                                                <input id="<?php echo $value->borrowing_id; ?>" type="checkbox" class="receipt" value="<?php echo $value->id_borrowing.'_'.$value->borrowingdetial_no; ?>" name="lost[<?php echo $key; ?>]">
                                            </td>
                                            <td style="text-align: center;">
                                                <input id="<?php echo $value->borrowing_id; ?>" type="checkbox" class="receipt" value="<?php echo $value->id_borrowing.'_'.$value->borrowingdetial_no; ?>" name="fray[<?php echo $key; ?>]">
                                            </td>
                                        </tr>
                                    <?php
                                        $aa++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <input type="hidden" id="member_hidder" name="member_hidder">
                            <div class="col-lg-12" style="text-align: right;">
                                <button type="submit" name="savereceipt" id="savereceipt" class="btn btn-primary">
                                    <i class="glyphicon glyphicon glyphicon-floppy-disk"></i> บันทึก
                                </button>
                            </div>
                        </form>


                    </div>
                </div>
            <?php
            }else{
            ?>
                <div class="row" style="text-align: center; padding-top: 50px;">
                    <h1>ไม่พบข้อมูลที่ค้นหา</h1>
                </div>
            <?php
            }
        }
        ?>
    </div>
</div>


<div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ยืนยันการบันทึกข้อมูล</h4>
            </div>
            <div class="modal-body">
                คุณต้องการบันทึกข้อมูลใช่หรือไม่?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-primary" id="btnConfirm">ยืนยัน</button>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){

    if((typeof $("#id_invoice").val() !== "undefined") && ($("#id_invoice").val() !== "")){
      window.open(base_url+"/returning_controller/invoicepdf/"+$("#id_invoice").val());
    }

    $('#savereceipt').on('click',function(){
        $('#modalConfirm').modal('show');
        $("#member_hidder").val($("#member").val());
        return false;
    });
    $('#btnConfirm').on('click',function(){
        $('#save_receipt').submit();
    });

});
</script>
