<!--PAGE CONTENT -->
<div id="content">
    <div class="inner">
        <div class="row has-success" >
            <div class="col-lg-5" style="padding-top: 30px; ">
            <h1 class="page-header">การรับหนังสือจากการสั่งซื้อ</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">ค้นหาข้อมูลใบสั่งซื้อหนังสือ</div>
                    <div class="panel-body">
                        <form id="search_purchaseorder" action="" method="post">
                        <div class="row">
                            <div class="col-lg-12">
                                <label class="control-label col-lg-2">รหัสใบสั่งซื้อหนังสือ</label>
                                <div class="col-lg-2">
                                    <input type="text" class="form-control" id="purchaseId" name="purchaseId" value="<?php echo (!empty($_POST['purchaseId']) ? $_POST['purchaseId'] : ''); ?>">
                                </div>
                                <label class="control-label col-lg-2" style="text-align: right;">สำนักพิมพ์</label>
                                <div class="input-group col-lg-4">
                                    <select name="publishing" id="publishing" class="form-control" tabindex="2">
                                        <option value="">สำนักพิมพ์</option>
                                        <?php
                                        if(count($dataPublishing) > 0){
                                            foreach ($dataPublishing as $value) {

                                            ?>
                                            <option value="<?php echo $value->publishing_id; ?>" <?php echo (!empty($_POST['publishing']) && $_POST['publishing']==$value->publishing_id ? 'selected="selected"' : ''); ?>><?php echo $value->publishing_name; ?></option>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    <button type="submit" name="btadddetial" id="btadddetial" class="btn btn-info" tabindex="3">
                                        <i class="glyphicon glyphicon glyphicon-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if(!empty($_POST['purchaseId']) || !empty($_POST['publishing'])){
            if(count($dataPurchase)>0){
        ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">ข้อมูลใบสั่งซื้อหนังสือ</div>
                            <div class="panel-body">
                                <form id="save_receipt" action="<?php echo site_url("receiptorder_controller/receiptorder"); ?>" method="post">
                                    <div class="row">
                                        <table id="example" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th><center>ลำดับ</center></th>
                                                    <th><center>เลขที่สั่งซื้อ</center></th>
                                                    <th><center>วันที่สั่งซื้อ</center></th>
                                                    <th><center>ชื่อหนังสือ</center></th>
                                                    <th><center>ราคา</center></th>
                                                    <th><center>จำนวน</center></th>
                                                    <th><center>ราคารวม</center></th>
                                                    <th><center>ยังไม่ได้รับ</center></th>
                                                    <th><center>รับหนังสือ</center></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($dataPurchase as $key=>$value) {
                                                    $idHtml = $value->purchaseorder_id.$value->purchaseorderdetial_no;
                                                    $idSend = $value->purchaseorder_id.'-'.$value->purchaseorderdetial_no;
                                                ?>
                                                <tr>
                                                    <td style="text-align: center;"><?php echo ($key+1); ?></td>
                                                    <td style="text-align: center;"><?php echo $value->purchaseorder_id; ?></td>
                                                    <td style="text-align: center;"><?php echo $value->purchaseorder_date; ?></td>
                                                    <td><?php echo $value->book_name; ?></td>
                                                    <td style="text-align: center;"><?php echo number_format($value->purchaseorderdetial_bookprice); ?></td>
                                                    <td style="text-align: center;"><?php echo $value->purchaseorderdetial_unit; ?></td>
                                                    <td style="text-align: center;"><?php echo number_format($value->purchaseorderdetial_sumprice); ?></td>
                                                    <td style="text-align: center;"><?php echo $value->purchaseorderdetial_balanceunit; ?></td>

                                                    <td style="text-align: center;">
                                                        <input id="receipt_balanceunit_<?php echo $idHtml; ?>" type="hidden" value="<?php echo $value->purchaseorderdetial_balanceunit; ?>">
                                                        <input id="receipt_check_<?php echo $idHtml; ?>" type="checkbox" class="receipt" value="<?php echo $idSend; ?>" name="receipt[<?php echo $key; ?>]"> รับ
                                                        <div class="form-group field-receipt_<?php echo $idHtml; ?>" style="width: 50%; display: inline;">
                                                            <input id="receipt_<?php echo $idHtml; ?>" class="form-control receipt_val" type="text" style="width: 50%; display: inline;" maxlength="5" name="receipt_<?php echo $idHtml; ?>" disabled="disabled">
                                                            <div class="help-block"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                        <div class="col-lg-12 hide" id="alertError">
                                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                                <strong>เกิดข้อผิดพลาด!</strong> กรุณาเลือกรายการหนังสือที่ต้องการรับ
                                            </div>
                                        </div>
                                        <div class="col-lg-12" style="text-align: right;">
                                            <button type="submit" name="savereceipt" id="savereceipt" class="btn btn-primary">
                                                <i class="glyphicon glyphicon glyphicon-floppy-disk"></i> บันทึก
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        <?php
            }else{
            ?>
                <div class="row" style="text-align: center; padding-top: 50px;">
                    <h1>ไม่พบข้อมูลที่ค้นหา</h1>
                </div>
            <?php
            }
        }elseif($statusReceipt == 'receipt'){
            ?>

                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <strong>สำเร็จ!</strong> ทำการรับหนังสือเรียบร้อยแล้ว
                    </div>
                </div>
            <?php
        }
        ?>
        <input type="hidden" id="idreceipt" value="<?php echo $id; ?>">
    </div>
</div>
<div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ยืนยันการบันทึกข้อมูล</h4>
            </div>
            <div class="modal-body">
                คุณต้องการบันทึกข้อมูลใช่หรือไม่?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-primary" id="btnConfirm">ยืนยัน</button>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){

    $(".receipt_val").bind('keypress',function(e){
        return (e.which!=8&&e.which!=0&&(e.which<48||e.which>57))?false:true;
    });

    $('.receipt').on('click',function(){
        var id = this.id;
        var arrId = id.split('_');
        var id = arrId[2];
        if($('#receipt_check_' + id).prop("checked") === true){
            $('#receipt_' + id).prop('disabled', false);
            $('#alertError').addClass('hide');
        }else{
            $('#receipt_' + id).prop('disabled', true);
            $('#receipt_' + id).val('');
            $('.field-receipt_' + id).removeClass('has-error');
            $('.field-receipt_' + id + ' .help-block').html('');
        }
    });

    $('.receipt_val').on('keyup',function(){
        var id = this.id;
        var arrId = id.split('_');
        var id = arrId[1];
        var balanceunit = $('#receipt_balanceunit_' + id).val();
        var receipt_val = $('#receipt_' + id).val();
        balanceunit = parseInt(balanceunit);
        receipt_val = parseInt(receipt_val);
        if(receipt_val > balanceunit){
            $('.field-receipt_' + id).addClass('has-error');
            $('.field-receipt_' + id + ' .help-block').html('จำนวนรับมากกว่าจำนวนคงเหลือ');
        }else{
            $('.field-receipt_' + id).removeClass('has-error');
            $('.field-receipt_' + id + ' .help-block').html('');
        }
    });

    $('#savereceipt').on('click',function(){

        var checkChecked = '';
        var checkData = '';
        $('#example .receipt').each(function( index ) {
            if($( this ).prop("checked") === true){
                checkChecked = 'true';
                var id = this.id;
                var arrId = id.split('_');
                var id = arrId[2];

                var balanceunit = $('#receipt_balanceunit_' + id).val();
                var receipt_val = $('#receipt_' + id).val();

                balanceunit = (balanceunit !== '' ? parseInt(balanceunit) : balanceunit);
                receipt_val = (receipt_val !== '' ? parseInt(receipt_val) : receipt_val);

                if($('#receipt_check_' + id).prop("checked") === true && receipt_val === ''){
                    $('.field-receipt_' + id).addClass('has-error');
                    $('.field-receipt_' + id + ' .help-block').html('กรุณากรอกจำนวนรับ');
                    checkData = 'false';
                }else if(receipt_val > balanceunit){
                    $('.field-receipt_' + id).addClass('has-error');
                    $('.field-receipt_' + id + ' .help-block').html('จำนวนรับมากกว่าจำนวนคงเหลือ');
                    checkData = 'false';
                }else{
                    $('.field-receipt_' + id).removeClass('has-error');
                    $('.field-receipt_' + id + ' .help-block').html('');
                }
            }
        });

        if(checkChecked === ''){
            $('#alertError').removeClass('hide');
            return false;
        }
        if(checkData !== ''){
            return false;
        }else{
            $('#modalConfirm').modal('show');
            return false;
        }
    });


    $('#btnConfirm').on('click',function(){
        $('#save_receipt').submit();
    });

    if($("#idreceipt").val() != "")
      window.open(base_url+"/receiptorder_controller/receiptorderpdf/"+$("#idreceipt").val());

});
</script>
