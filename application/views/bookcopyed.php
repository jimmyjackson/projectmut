<!--PAGE CONTENT -->
<div id="content">
    <div class="inner">
        <div class="row has-success" >
            <div class="col-lg-5" style="padding-top: 30px; ">
            <h1 class="page-header">สำเนาหนังสือห้องสมุด</h1>
            </div>
        </div>
        <input type="hidden" id="base" />
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">ข้อมูลของสำเนาหนังสือ</div>
                    <div class="panel-body">
                        <form id="bookForm" action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-6">
                                <span role="form">
                                    <div class="form-group">
                                        <label class="control-label col-lg-4">รูปประจำหนังสือ</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="<?php echo base_url().substr($book_image, 5); ?>" id="imageprofile"/>
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">

                                            </div>
                                        </div>
                                    </div>
                                    <label class="control-label col-lg-4">หมายเลขหนังสือ</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" value="<?php echo $book_id; ?>" class="form-control" value="" disabled id="id" name="id">
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">ชื่อหนังสือ</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" value="<?php echo $book_name; ?>" class="form-control" id="name" name="name" tabindex="1" disabled>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">ISBNหนังสือ</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" value="<?php echo $book_isbn; ?>" class="form-control" id="isbn" name="isbn" tabindex="2" disabled>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">ขนาดหนังสือ</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" value="<?php echo $book_size; ?>" class="form-control" id="size" name="size" tabindex="3" disabled>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">จำนวนหน้า</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" value="<?php echo $book_page; ?>" class="form-control" id="page" name="page" tabindex="4" disabled>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">ราคาปก</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" value="<?php echo $book_price; ?>" class="form-control" id="price" name="price" tabindex="5" disabled>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">ราคาปัจจุบัน</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" value="<?php echo $bookcopy_status; ?>" class="form-control" id="priceupdate" name="priceupdate" tabindex="6" disabled>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">สำนักพิมพ์</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" value="<?php echo $publishing_name; ?>" class="form-control" id="publishing" name="publishing" tabindex="7" disabled>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">ตู้ที่:ชั้นที่:ช่องที่</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" value="<?php echo $positionbook; ?>" class="form-control" id="positionbook" name="positionbook" tabindex="7" disabled>
                                    </div>
                                </span>
                            </div>

                            <div class="col-lg-6">
                                <span role="form">
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">หมู่หนังสือ</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" value="<?php echo $mubook_name; ?>" class="form-control" id="category" name="category" tabindex="8" disabled>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">หมู่ย่อยหนังสือ</label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" value="<?php echo $submubook_name; ?>" class="form-control" id="subcategory" name="subcategory" tabindex="9" disabled>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">หัวเรื่องหนังสือ</label>
                                    <div class="input-group col-lg-6">
                                        <select id="titlebooklist" multiple class="form-control" rows="3" disabled>
                                          <?php foreach ($title as $value) { ?>
                                            <option><?php echo $value; ?></option>
                                          <?php } ?>
                                        </select>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">ผู้แต่งหนังสือ</label>
                                    <label class="control-label col-lg-4"></label>
                                    <div class="input-group col-lg-6">
                                        <select id="authorlist" multiple class="form-control" rows="3" disabled>
                                          <?php foreach ($author as $value) { ?>
                                            <option><?php echo $value; ?></option>
                                          <?php } ?>
                                        </select>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">ผู้แปลหนังสือ</label>
                                    <label class="control-label col-lg-4"></label>
                                    <div class="input-group col-lg-6">
                                        <select id="translatorlist" multiple class="form-control" rows="3" disabled>
                                          <?php foreach ($translator as $value) { ?>
                                            <option><?php echo $value; ?></option>
                                          <?php } ?>
                                        </select>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">รหัสสำเนา</label>
                                    <label class="control-label col-lg-4"></label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" value="<?php echo $bookcopy_id; ?>" class="form-control" id="bookcopyid" name="bookcopyid" tabindex="9" disabled>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">เลขเรียกสำเนา</label>
                                    <label class="control-label col-lg-4"></label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" value="<?php echo $bookcopy_callno; ?>" class="form-control" id="bookcopyno" name="bookcopyno" tabindex="9" disabled>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">ปีที่พิมพ์/ครั้งที่พิมพ์</label>
                                    <label class="control-label col-lg-4"></label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" value="<?php echo $edition; ?>" class="form-control" id="edition" name="edition" tabindex="9" disabled>
                                    </div>
                                    <label class="control-label col-lg-12"></label>
                                    <label class="control-label col-lg-4">สถานะสำเนา</label>
                                    <label class="control-label col-lg-4"></label>
                                    <div class="input-group col-lg-6">
                                        <input type="text" value="<?php echo $bookcopy_status; ?>" class="form-control" id="bookcopystatus" name="bookcopystatus" tabindex="9" disabled>
                                    </div>
                                </span>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END PAGE CONTENT -->
</div>
<!--END MAIN WRAPPER -->
<!-- GLOBAL SCRIPTS -->
<!-- END GLOBAL SCRIPTS -->
