    <!--PAGE CONTENT -->
    <div id="content" style="padding-top: 100px; ">
        <div class="inner">   
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-8 col-lg-offset-2 text-center">
                        <div class="logo">
                            <h1>ไม่พบข้อมูลที่ต้องการค้นหา</h1>          
                        </div>
                        <p class="lead text-muted">
                            ไม่พบข้อมูลที่ต้องการค้นหา กรุณาตรวจสอบข้อมูลใหม่อีกครั้ง 
                            <br>ข้อมูลที่กรอกเข้ามาอาจไม่ถูกต้องหรือไม่มีในระบบ
                        </p>
                    </div>                           
                </div>
            </div>
        </div>
    </div>
    <!--END PAGE CONTENT -->
</div>
<!--END MAIN WRAPPER -->
