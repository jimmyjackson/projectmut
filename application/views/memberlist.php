<!--PAGE CONTENT -->
<div id="content">

    <div class="inner">
            <div class="row has-success" >
                <div class="col-lg-5" style="padding-top: 30px; ">
                    <h1 class="page-header">สมาชิกห้องสมุด</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            ข้อมูลของสมาชิก
                        </div>
                        <div class="col-lg-12">
                            <div class="panel-heading">
                                <div class="form-inline">
                                <label class="control-label col-lg-1"></label>
                                <button type="button" class="btn btn-primary" id="btaddmem"><i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;เพิ่มข้อมูลใหม่</button>
                                <label class="control-label col-lg-2"></label>
                                <select id="selectshowtype" class="selectpicker" multiple>
                                    <option value="student">Student</option>
                                    <option value="personnel">Personnel</option>
                                </select>
                                <label class="control-label col-lg-2"></label>
                                <select id="selectsearch" class="selectpicker" multiple>
                                    <option value="id">ID</option>
                                    <option value="name">Name</option>
                                    <option value="lname">Lastname</option>
                                    <option value="idcard">ID Card</option>
                                    <option value="email">Email</option>
                                    <option value="username">Username</option>
                                </select>
                                <input id="inputsearch" type="text" style="width: 20%" class="form-control" placeholder="กรอกข้อมูลที่ต้องการค้นหา">
                                <button id="btsearch" type="button" class="btn btn-info"><i class="icon-search icon-white"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
        <table id="example" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th><center>ID</center></th>
                <th><center>Name</center></th>
                <th><center>ID Card</center></th>
                <th><center>Email</center></th>
                <th><center>Username</center></th>
                <th><center>Type</center></th>
                <th><center>Select</center></th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th><center>ID</center></th>
                <th><center>Name</center></th>
                <th><center>ID Card</center></th>
                <th><center>Email</center></th>
                <th><center>Username</center></th>
                <th><center>Tppe</center></th>
                <th><center>Select</center></th>
            </tr>
        </tfoot>

        <tbody id="bodyshowdataemp">

        </tbody>
    </table>
            <nav>
                <ul class="pagination" id="paging">

                </ul>
            </nav>

            </div>

                        <div class="col-lg-12">
                                &nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <!--END PAGE CONTENT -->
    </div>
</div>
<!--END MAIN WRAPPER -->
<!-- GLOBAL SCRIPTS -->
<script src="<?php echo base_url()?>assets/js/validatefromall.js"></script>
<script src="<?php echo base_url()?>assets/js/memberfrom.js"></script>
<script>

$("nav").on('click','#paging li a',function(event){
    getempdatarows(event.target.id);
});

    member.member_init_list();
    getempdatarows(1);
    function getempdatarows(pageselect){

    var offset = (pageselect-1)*10;
    var numpageshow =5;
    var lengthshow = Math.floor(numpageshow/2);
    //console.log(offset);

    if($('#inputsearch').val() == ""){

        $.ajax({
            type : "POST",
            url: base_url+"/member_controller/selectmember",
            data: {'offsetsend':offset},
            dataType: "json",
            success: function(data)
            {
                $("#bodyshowdataemp").empty();

                $.each(data.memberselect,function(ID,mem){

                    $("#bodyshowdataemp").append('<tr><td><center>'+mem.member_id+'</center></td><td><center>'+mem.member_name+
                    ' '+mem.member_lname+'</center></td><td><center>'+mem.idcard+'</center></td><td><center>'+mem.member_email+
                    '</center></td><td><center>'+mem.member_username+
                    '</center></td><td><center>'+mem.membertype_name+'</center></td><td><center><a href='+base_url+'/member_controller/member/'+mem.member_id+'><button type="button" class="btn btn-success" id='+mem.member_id+'><i class="glyphicon glyphicon-eye-open"></i>&nbsp;view</button></a></center></td></tr>');
                    });
                }
    });


        $.ajax({
            type : "POST",
            url: base_url+"/member_controller/countmember",
            //data: {usernamecheck:'basadultindy0'},
            dataType: "json",
            success: function(data)
            {
                $("#paging").empty();
                $.each(data,function(i,count){
                    //console.log(count);
                    $("#paging").append("<li id='row' class='disabled'><span>rows "+(offset+1)+" to "+(offset+10)+"</span></li>");
                    $("#paging").append("<li id='first'><a href='#' id='1'>&laquo;</a></li>");

                    var page = Math.ceil(count/10);
                    //console.log(page);
                    if(page < 6)
                    {
                        for(var num=1;num<=page;num++)
                            if(num==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+num+"'>"+num+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+num+"'>"+num+"</a></li>");
                    }
                    else{
                    if(pageselect==page)
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++){
                            if((m+1)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                        }
                    }
                    else if(pageselect==(page-1))
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++)
                            if((m+2)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                    }
                    else{
                        for(var j=pageselect-1,k=0;k<numpageshow;j++,k++)
                        {
                            if((pageselect-1)<(numpageshow-lengthshow))
                                if((k+1)>page)
                                    break;
                                else
                                    if((k+1)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                            else
                                if((j+1)>(page+lengthshow))
                                    break;
                                else
                                    if(((j+1)-lengthshow)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                //$("#next").before("<li><a href='#' id='"+(j+1)+"'>"+(j+1)+"</a></li>");
                        }
                    }
                    }

                    $("#paging").append("<li id='last'><a href='#' id='"+page+"'>&raquo;</a></li>");
                    $("#paging").append("<li id='total' class='disabled'><span>page "+pageselect+" of "+page+"</span></li>");
                });

            }
    });

    }else{

        var selectmessage=[];
        var selectshowtype=[];
        $("#selectsearch option:selected").each(function (){
            selectmessage.push($(this).val());
        });

        if(selectmessage.length === 0)
            selectmessage.push('none');

        $("#selectshowtype option:selected").each(function (){
            selectshowtype.push($(this).val());
        });

        if(selectshowtype.length === 0)
            selectshowtype.push('none');

        $.ajax({
            type : "POST",
            url: base_url+"/member_controller/searchmember",
            data: {datasearch:{'selectmessage':selectmessage,'selectshowtype':selectshowtype,
            'inputsearch':$('#inputsearch').val(),'offset':0}},
            dataType: "json",
            success: function(data)
            {
                //console.log(data);
                $("#bodyshowdataemp").empty();

                $.each(data,function(ID,mem){

                    $("#bodyshowdataemp").append('<tr><td><center>'+mem.member_id+'</center></td><td><center>'+mem.member_name+
                    ' '+mem.member_lname+'</center></td><td><center>'+mem.idcard+'</center></td><td><center>'+mem.member_email+
                    '</center></td><td><center>'+mem.member_username+
                    '</center></td><td><center>'+mem.membertype_name+'</center></td><td><center><a href='+base_url+'/member_controller/member/'+mem.member_id+'><button type="button" class="btn btn-success" id='+mem.member_id+'><i class="glyphicon glyphicon-eye-open"></i>&nbsp;view</button></a></center></td></tr>');
                });

            }
        });

        $.ajax({
            type : "POST",
            url: base_url+"/member_controller/countsearchmember",
            data: {datasearch:{'selectmessage':selectmessage,'selectshowtype':selectshowtype,
            'inputsearch':$('#inputsearch').val()}},
            dataType: "json",
            success: function(data)
            {
                $.each(data,function(i,count){
                    $("#paging").empty();
                    //console.log(count.countsearchemp);
                    $("#paging").append("<li id='row' class='disabled'><span>rows "+(offset+1)+" to "+(offset+10)+"</span></li>");
                    $("#paging").append("<li id='first'><a href='#' id='1'>&laquo;</a></li>");

                    var page = Math.ceil(count.count/10);

                    //console.log(page);

                    if(page < 6)
                    {
                        for(var num=1;num<=page;num++)
                            if(num==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+num+"'>"+num+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+num+"'>"+num+"</a></li>");
                    }
                    else{
                    if(pageselect==page)
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++){
                            if((m+1)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                        }
                    }
                    else if(pageselect==(page-1))
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++)
                            if((m+2)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                    }
                    else{
                        for(var j=pageselect-1,k=0;k<numpageshow;j++,k++)
                        {
                            if((pageselect-1)<(numpageshow-lengthshow))
                                if((k+1)>page)
                                    break;
                                else
                                    if((k+1)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                            else
                                if((j+1)>(page+lengthshow))
                                    break;
                                else
                                    if(((j+1)-lengthshow)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                //$("#next").before("<li><a href='#' id='"+(j+1)+"'>"+(j+1)+"</a></li>");
                        }
                    }
                    }

                    $("#paging").append("<li id='last'><a href='#' id='"+page+"'>&raquo;</a></li>");
                    $("#paging").append("<li id='total' class='disabled'><span>page "+pageselect+" of "+page+"</span></li>");

                });

            }
    });

    }
}
</script>
<!-- END GLOBAL SCRIPTS -->
