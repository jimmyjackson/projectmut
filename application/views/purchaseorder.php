<style>
tr {
width: 100%;
display: inline-table;
}
table{
 height:400px;
}
tbody{
  overflow-y: scroll;
  height: 360px;
  width: 98.2%;
  position: absolute;
}
</style>
    <!--PAGE CONTENT -->
    <div id="content">
        <div class="inner">
            <div class="row has-success" >
                <div class="col-lg-5" style="padding-top: 30px; ">
                    <h1 class="page-header">ใบสั่งซื้อหนังสือ</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <div class="panel-heading">
                            ข้อมูลใบสั่งซื้อหนังสือ
                        </div>

                        <div class="col-lg-12">
                            <div class="panel-heading">
                                <div class="form-inline">
                                    <label class="control-label col-lg-2">รหัสใบสั่งซื้อหนังสือ</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" value="Auto_Id" disabled id="id">
                                    </div>
                                    <label class="control-label col-lg-2">สำนักพิมพ์</label>
                                    <div class="input-group col-lg-3">
                                        <select name="publishing" id="publishing" class="validate[required] form-control" tabindex="2">
                                            <option value="">สำนักพิมพ์</option>
                                        </select>
                                        <span class="input-group-addon add-on">
                                            <font color="red">&#42;</font>
                                        </span>
                                    </div>

                                    <div class="col-lg-3">
                                            <button type="button" name="btsave" id="btsave" class="btn btn-primary" tabindex="21"><i class="glyphicon glyphicon-floppy-saved"></i> สั่งซื้อ</button>
                                            <button type="button" id="btcancel" class="btn btn-danger" tabindex="22"><i class="icon-remove icon-white"></i> ยกเลิก</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <label class="control-label">&nbsp;</label>
                        </div>

                        <div class="col-lg-9">
                            <label class="control-label">รายการใบเสนอซื้อหนังสือ</label>
                        </div>


                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="col-lg-12">
                                            <th class="col-lg-1">
                                                <center>SELECT</center>
                                            </th>
                                            <th class="col-lg-2"><center>รหัส</center></th>
                                            <th class="col-lg-3"><center>วันที่อนุมัติ</center></th>
                                            <th class="col-lg-3"><center>ภาษีมูลค่าเพิ่ม</center></th>
                                            <th class="col-lg-3"><center>ราคารวมสุทธิ</center></th>
                                        </tr>
                                    </thead>

                                    <tbody id="bodylisttable">
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END PAGE CONTENT -->
</div>
<!--END MAIN WRAPPER -->
<!-- GLOBAL SCRIPTS -->
<script src="<?php echo base_url()?>assets/js/validationpurchaseorder.js"></script>
<script src="<?php echo base_url()?>assets/js/purchaseorderfrom.js"></script>
<script>
purchaseorder.purchaseorder_init_add();
</script>
<!-- END GLOBAL SCRIPTS -->
