<div id="content">
    <div class="inner">
        <div class="row has-success" >
            <div class="col-lg-5" style="padding-top: 30px; ">
            <h1 class="page-header">การจองหนังสือ</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">ค้นหาข้อมูลการจองหนังสือ</div>
                    <div class="panel-body">
                        <form action="<?php echo site_url("booking_controller/index"); ?>" method="post">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">เลขเรียกหนังสือ</label>
                                            <input type="text" class="form-control" id="bookcall" name="bookcall" value="<?php echo (!empty($_POST['bookcall']) ? $_POST['bookcall'] : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">ชื่อหนังสือ</label>
                                            <input type="text" class="form-control" id="bookname" name="bookname" value="<?php echo (!empty($_POST['bookname']) ? $_POST['bookname'] : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group field-member">
                                            <label class="control-label">รหัสสมาชิก</label>
                                            <input type="text" class="form-control" id="member" name="member" value="<?php echo (!empty($_POST['member']) ? $_POST['member'] : ''); ?>">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2">
                                        <div class="form-group" style="margin-top: 25px;">
                                            <button type="submit" name="btadddetial" id="btadddetial" class="btn btn-info" tabindex="3">
                                                <i class="glyphicon glyphicon glyphicon-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <a href="<?php echo site_url("booking_controller/newbooking"); ?>" class="btn btn-success"><i class="glyphicon glyphicon glyphicon-plus"></i>  เพิ่ม</a>
            </div>
        </div>
        <br>
        <?php
        if(!empty($status)){
            if($status=='delete'){
            ?>
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <strong>สำเร็จ!</strong>ทำการยกเลิกการจองหนังสือเรียบร้อยแล้ว
                    </div>
                </div>
            <?php
            }elseif($status=='unit'){
            ?>
                <div class="col-lg-12">
                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <strong>เกิดข้อผิดพลาด!</strong>ไม่สามารถยืมหนังสือเพิ่มได้ เนื่องจากจำนวนการยืมเกินจำนวนที่กำหนด
                    </div>
                </div>
            <?php
            }elseif($status=='save'){
            ?>
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <strong>สำเร็จ!</strong>ทำการยืมหนังสือเรียบร้อยแล้ว
                    </div>
                </div>
            <?php
            }
        }
        if(isset($_POST['btadddetial'])){
            if(!empty($dataBooking) && $dataBooking>0){
        ?>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                        <table id="example" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th><center>ลำดับ</center></th>
                                    <th><center>เลขเรียกหนังสือ</center></th>
                                    <th><center>ชื่อหนังสือ</center></th>
                                    <th><center>วันที่จอง</center></th>
                                    <th><center>วันที่รับหนังสือ</center></th>
                                    <th><center>รหัสสมาชิก</center></th>
                                    <th><center>ยืม</center></th>
                                    <th><center>ยกเลิก</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $aa = 0;
                                foreach ($dataBooking as $key=>$value) {
                                ?>
                                    <tr>
                                        <td style="text-align: center;"><?php echo ($aa+1); ?></td>
                                        <td style="text-align: center;"><?php echo $value->bookcopy_callno; ?></td>
                                        <td><?php echo $value->book_name; ?></td>
                                        <td style="text-align: center;"><?php echo $value->booking_date; ?></td>
                                        <td style="text-align: center;"><?php echo $value->bookingdetial_datereceiptbook; ?></td>
                                        <td style="text-align: center;"><?php echo $value->id_member; ?></td>
                                        <td style="text-align: center;">
                                            <?php if($value->bookcopy_status != "ว่าง") { ?>
                                              <button type="button" name="savereceipt" id="<?php echo $value->booking_id; ?>" class="btn btn-primary savereceipt" disabled>
                                                  <i class="glyphicon glyphicon glyphicon-floppy-disk"></i>
                                              </button>
                                            <?php } else { ?>
                                              <button type="button" name="savereceipt" id="<?php echo $value->booking_id; ?>" class="btn btn-primary savereceipt">
                                                  <i class="glyphicon glyphicon glyphicon-floppy-disk"></i>
                                              </button>
                                            <?php } ?>
                                        </td>
                                        <td style="text-align: center;">
                                            <button type="button" name="deletereceipt" id="<?php echo $value->booking_id; ?>" class="btn btn-danger deletereceipt">
                                                <i class="glyphicon glyphicon glyphicon-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                <?php
                                    $aa++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php
            }else{
            ?>
                <div class="row" style="text-align: center; padding-top: 50px;">
                    <h1>ไม่พบข้อมูลหนังสือที่ค้นหา</h1>
                </div>
            <?php
            }
        }
        ?>
    </div>
</div>

<div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ยืนยันการยืมหนังสือ</h4>
            </div>
            <div class="modal-body">
                คุณต้องการยืมหนังสือใช่หรือไม่?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-primary" id="btnConfirm">ยืนยัน</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ยืนยันการยกเลิกการจอง</h4>
            </div>
            <div class="modal-body">
                คุณต้องการยกเลิกการจองใช่หรือไม่?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-primary" id="btnConfirmDelete">ยืนยัน</button>
            </div>
        </div>
    </div>
</div>

<form id="send_form" action="<?php echo site_url("booking_controller/index"); ?>" method="post">
    <input type="hidden" id="save_id" name="save_id">
    <input type="hidden" id="type" name="type">
</form>


<script>
$(document).ready(function(){

    $('.savereceipt').on('click',function(){
        var id = this.id;
        $('#save_id').val(id);
        $('#type').val('save');
        $('#modalConfirm').modal('show');
        return false;
    });

    $('.deletereceipt').on('click',function(){
        var id = this.id;
        $('#save_id').val(id);
        $('#type').val('delete');
        $('#modalConfirmDelete').modal('show');
        return false;
    });

    $('#btnConfirm, #btnConfirmDelete').on('click',function(){
        $('#send_form').submit();
    });



});
</script>
