<!--PAGE CONTENT -->
<div id="content">

    <div class="inner">
            <div class="row has-success" >
                <div class="col-lg-5" style="padding-top: 30px; ">
                    <h1 class="page-header">พนักงานห้องสมุด</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            ข้อมูลของพนักงาน
                        </div>
                        <div class="col-lg-12">
                            <div class="panel-heading">
                                <div class="form-inline">
                                <label class="control-label col-lg-4"></label>
                                <button type="button" class="btn btn-primary" id="btaddemp"><i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;เพิ่มข้อมูลใหม่</button>
                                <label class="control-label col-lg-2"></label>
                                <select id="selectsearch" class="selectpicker" multiple>
                                    <option value="id">ID</option>
                                    <option value="name">Name</option>
                                    <option value="lname">Lastname</option>
                                    <option value="username">Username</option>
                                </select>
                                <input id="inputsearch" type="text" style="width: 19%" class="form-control" placeholder="กรอกข้อมูลที่ต้องการค้นหา">
                                <button id="btsearch" type="button" class="btn btn-info"><i class="icon-search icon-white"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
        <table id="example" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th><center>ID</center></th>
                <th><center>Name</center></th>
                <th><center>Position</center></th>
                <th><center>Username</center></th>
                <th><center>Tel</center></th>
                <th><center>Select</center></th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th><center>ID</center></th>
                <th><center>Name</center></th>
                <th><center>Position</center></th>
                <th><center>Username</center></th>
                <th><center>Tel</center></th>
                <th><center>Select</center></th>
            </tr>
        </tfoot>

        <tbody id="bodyshowdataemp">

        </tbody>
    </table>
            <nav>
                <ul class="pagination" id="paging">

                </ul>
            </nav>

            </div>

                        <div class="col-lg-12">
                                &nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <!--END PAGE CONTENT -->
    </div>
</div>
<!--END MAIN WRAPPER -->
<!-- GLOBAL SCRIPTS -->
<script src="<?php echo base_url()?>assets/js/validatefromall.js"></script>
<script src="<?php echo base_url()?>assets/js/empfrom.js"></script>

<!-- END GLOBAL SCRIPTS -->
