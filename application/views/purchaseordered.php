<!--PAGE CONTENT -->
<div id="content">
    <div class="inner">
        <div class="row has-success" >
            <div class="col-lg-5" style="padding-top: 30px; ">
            <h1 class="page-header">ใบสั่งซื้อหนังสือ</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">ข้อมูลของใบสั่งซื้อหนังสือ</div>
                    <div class="panel-body">
                        <form id="offerorderForm" action="" method="post">
                        <div class="row">
                            <div class="col-lg-6">
                                <label class="control-label col-lg-5">รหัสใบสั่งซื้อหนังสือ</label>
                                <div class="col-lg-6">
                                    <input type="text" value="<?php echo $idpurchaseorder; ?>" class="form-control" value="Auto_Id" disabled id="id" name="id">
                                </div>
                                <div class="input-group col-lg-12">
                                    &nbsp;
                                </div>
                                <label class="control-label col-lg-5">รหัสผู้สั่งซื้อหนังสือ</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" disabled name="empid" id="empid">
                                </div>
                                <div class="input-group col-lg-12">
                                    &nbsp;
                                </div>
                                <div class="col-lg-9">
                                    <button type="button" name="btprint" id="btprint" class="btn btn-default" tabindex="4">
                                        <i class="glyphicon glyphicon glyphicon-print"></i> พิมพ์ใบสั่งซื้อ
                                    </button>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <label class="control-label col-lg-4" >วันที่สั่งซื้อหนังสือ</label>
                                <div class="col-lg-6">
                                    <div class="input-group input-append date">
                                        <input class="form-control" type="text" id="dateoffer" name="dateoffer" disabled/>
                                        <span class="input-group-addon add-on">
                                            <i id="icondate" class="icon-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="input-group col-lg-12">
                                    &nbsp;
                                </div>
                                <label class="control-label col-lg-4">สำนักพิมพ์</label>
                                <div class="input-group col-lg-6">
                                    <select name="publishing" id="publishing" class="validate[required] form-control" tabindex="2" disabled>
                                        <option value="">สำนักพิมพ์</option>
                                    </select>
                                    <span class="input-group-addon add-on">
                                        <font color="red">&#42;</font>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            &nbsp;
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <table id="example" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th><center>ลำดับ</center></th>
                                            <th><center>ชื่อหนังสือ</center></th>
                                            <th><center>ราคา</center></th>
                                            <th><center>จำนวน</center></th>
                                            <th><center>ราคารวม</center></th>
                                        </tr>
                                    </thead>

                                    <tfoot>
                                        <tr>
                                            <th colspan="4" class="text-right">ราคารวมทั้งหมด</th>
                                            <th colspan="2"><center><span id="total"></span></center></th>
                                        </tr>
                                        <tr>
                                            <th colspan="4" class="text-right">
                                                <span class="pull-right">%</span>
                                                <input style="width:45px;" disabled name="vat" id="vat" value="7" type="text" class="form-control pull-right">
                                                <span class="pull-right">ภาษีมูลค่าเพิ่ม</span>
                                            </th>
                                            <th colspan="2"><center><span id="vattotal"></span></center></th>
                                        </tr>
                                        <tr>
                                            <th colspan="4" class="text-right">ราคารวมสุทธิ</th>
                                            <th colspan="2"><center><span id="subtotal"></span></center></th>
                                        </tr>
                                    </tfoot>

                                    <tbody id="bodylisttable">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--END PAGE CONTENT -->
</div>
<!--END MAIN WRAPPER -->
<!-- GLOBAL SCRIPTS -->
<script src="<?php echo base_url()?>assets/js/purchaseorderfrom.js"></script>
<script>
purchaseorder.purchaseorder_init_edit();
$("#btprint").click(function () {
  window.open("<?php echo base_url()?>purchaseorder_controller/purchaseorderpdf/<?php echo $idpurchaseorder; ?>");
});
</script>
<!-- END GLOBAL SCRIPTS -->
