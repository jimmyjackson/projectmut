<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD-->
<head>

     <meta charset="UTF-8" />
    <title>Watbangphliyainai School Library</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-fileupload.min.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/select2-4.0.2/dist/css/select2.min.css" />
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
<link href="<?php echo base_url()?>assets/css/jquery-ui.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/uniform/themes/default/css/uniform.default.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/inputlimiter/jquery.inputlimiter.1.0.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/chosen/chosen.min.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/colorpicker/css/colorpicker.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/tagsinput/jquery.tagsinput.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/daterangepicker/daterangepicker-bs3.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/datepicker/css/datepicker.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-multiselect.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/switch/static/stylesheets/bootstrap-switch.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/DataTables/media/css/jquery.dataTables.css" />
<link href="<?php echo base_url()?>assets/css/jquery-ui.css" rel="stylesheet" />
<style type="text/css">
    .bgCover {
        background:#000;
        position:fixed;
        left:0;
        top:0;
        width: 100%;
        height: 100%;
        display:none;
        overflow:hidden;
    }
    .overlayBox {
        border:1px solid #ccc;
        box-shadow: 2px 2px 2px #ccc;
        border-radius: 3px;
        position:absolute;
        display:none;
        width:465px;
        height:218px;
        background:#fff;
    }
    .overlayContent {
        margin-top: 0px;
        margin-left: 5px;
        padding:10px;

    }
    h2 {
        padding:5px;
        margin:0;
    }
</style>
<!-- GLOBAL SCRIPTS -->
<script src="<?php echo base_url()?>assets/plugins/jquery-2.0.3.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-multiselect.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-typeahead.js"></script>
<script src="<?php echo base_url()?>assets/js/position.js"></script>
<script src="<?php echo base_url()?>assets/js/province.js"></script>
<script src="<?php echo base_url()?>assets/js/zone.js"></script>
<script src="<?php echo base_url()?>assets/js/district.js"></script>
<script src="<?php echo base_url()?>assets/js/postcode.js"></script>
<script src="<?php echo base_url()?>assets/js/publishing.js"></script>
<script src="<?php echo base_url()?>assets/js/mubook.js"></script>
<script src="<?php echo base_url()?>assets/js/submubook.js"></script>
<script src="<?php echo base_url()?>assets/js/year.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.form.js"></script>
<script src="<?php echo base_url()?>assets/select2-4.0.2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url()?>assets/js/notclickright.js"></script>
<!-- END GLOBAL SCRIPTS -->
</head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
<body class="padTop53 " >

     <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
        <div id="top">

            <nav class="navbar navbar-inverse navbar-fixed-top " style="padding-top: 1px;">
                <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
                    <i class="icon-align-justify"></i>
                </a>
                <!-- LOGO SECTION -->
                <header class="navbar-header">

                    <a href="index.html" class="navbar-brand">
                    <img src="<?php echo base_url()?>assets/images/banner-3.png" alt="" /></a>
                </header>
                <!-- END LOGO SECTION -->
                <ul class="nav navbar-top-links navbar-right"></ul>
            </nav>
        </div>
        <!-- END HEADER SECTION -->



        <!-- MENU SECTION -->
       <div id="left">
            <div class="media user-media well-small">
                <div>
                    <h5>
                        &nbsp;
                    </h5>
                </div>
                <br />
                <center>
                <h4 class="media-heading">
                    ยินดีต้อนรับ
                </h4>
                <img class="img-circle" src="<?php echo $this->session->userdata("imgemp")?>" height="130" width="130"/>
                <br/>
                <h4 class="media-heading">
                    <?php echo $this->session->userdata("username")?>
                </h4>
                </center>
            </div>

            <ul id="menu" class="collapse">
                <li class="panel">
                    <a href="<?php echo base_url()?>homeadmin_controller" onclick="javascript:void(0)">
                        <i class="icon-home"></i> หน้าแรก
                    </a>
                </li>
                <?php if($this->session->userdata("permission_constant")!=""){?>
                <li class="panel">
                    <a href="#" data-parent="#menu"  onclick="javascript:void(0)" data-toggle="collapse" class="accordion-toggle" data-target="#component-nav1">
                        <i class="icon-hdd"> </i> การจัดการค่าคงที่
                        <span class="pull-right">
                          <i class="icon-angle-left"></i>
                        </span>
                    </a>
                    <ul class="collapse" id="component-nav1">
                        <?php
                        foreach($this->session->userdata("permission_constant") as $value){ ?>
                            <li class="">
                                <a href="<?php echo base_url().$value ?>" onclick="javascript:void(0)">
                                    &nbsp;<i class="icon-caret-right"></i> <?php echo $this->session->userdata("permission_constant_name")[$value] ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>
                <?php if($this->session->userdata("permission_purchase")!=""){?>
                <li class="panel">
                    <a href="#" data-parent="#menu" onclick="javascript:void(0)" data-toggle="collapse" class="accordion-toggle" data-target="#component-nav2">
                        <i class="icon-usd"> </i> การจัดซื้อหนังสือ
                        <span class="pull-right">
                          <i class="icon-angle-left"></i>
                        </span>
                    </a>
                    <ul class="collapse" id="component-nav2">
                        <?php
                        foreach($this->session->userdata("permission_purchase") as $value){ ?>
                            <li class="">
                                <a href="<?php echo base_url().$value ?>" onclick="javascript:void(0)">
                                    &nbsp;<i class="icon-caret-right"></i> <?php echo $this->session->userdata("permission_purchase_name")[$value] ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>
                <?php if($this->session->userdata("permission_borrow")!=""){?>
                <li class="panel">
                    <a href="#" data-parent="#menu" onclick="javascript:void(0)" data-toggle="collapse" class="accordion-toggle" data-target="#component-nav3">
                        <i class="icon-book"> </i> การยืม/การคืนหนังสือ
                        <span class="pull-right">
                          <i class="icon-angle-left"></i>
                        </span>
                    </a>
                    <ul class="collapse" id="component-nav3">
                        <?php
                        foreach($this->session->userdata("permission_borrow") as $value){ ?>
                            <li class="">
                                <a href="<?php echo base_url().$value ?>" onclick="javascript:void(0)">
                                    &nbsp;<i class="icon-caret-right"></i> <?php echo $this->session->userdata("permission_borrow_name")[$value] ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>
                <?php if($this->session->userdata("permission_search")!=""){?>
                <li class="panel">
                    <a href="#" data-parent="#menu" onclick="javascript:void(0)" data-toggle="collapse" class="accordion-toggle" data-target="#component-nav4">
                        <i class="icon-search"> </i> การค้นหาหนังสือ
                        <span class="pull-right">
                          <i class="icon-angle-left"></i>
                        </span>
                    </a>
                    <ul class="collapse" id="component-nav4">
                        <?php
                        foreach($this->session->userdata("permission_search") as $value){ ?>
                            <li class="">
                                <a href="<?php echo base_url().$value ?>" onclick="javascript:void(0)">
                                    &nbsp;<i class="icon-caret-right"></i> <?php echo $this->session->userdata("permission_search_name")[$value] ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>
                <?php if($this->session->userdata("permission_report")!=""){?>
                <li class="panel">
                    <a href="#" data-parent="#menu" onclick="javascript:void(0)" data-toggle="collapse" class="accordion-toggle" data-target="#component-nav5">
                        <i class="icon-print"> </i> การออกรายงาน
                        <span class="pull-right">
                          <i class="icon-angle-left"></i>
                        </span>
                    </a>
                    <ul class="collapse" id="component-nav5">
                        <?php
                        foreach($this->session->userdata("permission_report") as $value){ ?>
                            <li class="">
                                <a href="<?php echo base_url().$value ?>" onclick="javascript:void(0)">
                                    &nbsp;<i class="icon-caret-right"></i> <?php echo $this->session->userdata("permission_report_name")[$value] ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>
                <li class="panel">
                    <a href="<?php echo base_url()?>logout_emp_controller/logout"  onclick="javascript:void(0)">
                        <i class="icon-off"></i> ออกจากระบบ
                    </a>
                </li>
            </ul>
        </div>
        <!--END MENU SECTION -->
