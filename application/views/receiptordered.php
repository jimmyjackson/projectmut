<!--PAGE CONTENT -->
<div id="content">
    <div class="inner">
        <div class="row has-success" >
            <div class="col-lg-5" style="padding-top: 30px; ">
            <h1 class="page-header">ใบรับหนังสือ</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">ข้อมูลของใบรับหนังสือ</div>
                    <div class="panel-body">
                        <form id="offerorderForm" action="" method="post">
                        <div class="row">
                            <div class="col-lg-6">
                                <label class="control-label col-lg-5">รหัสใบรับหนังสือ</label>
                                <div class="col-lg-6">
                                    <input type="text" value="<?php echo $data[0]->receiptorder_id; ?>" class="form-control" disabled id="id" name="id">
                                </div>
                                <div class="input-group col-lg-12">
                                    &nbsp;
                                </div>
                                <label class="control-label col-lg-5">รหัสผู้รับหนังสือ</label>
                                <div class="col-lg-6">
                                  <input type="text" value="<?php echo $data[0]->id_employee; ?>" class="form-control" disabled id="empid" name="empid">
                                </div>
                                <div class="input-group col-lg-12">
                                    &nbsp;
                                </div>
                                <div class="col-lg-9">
                                    <button type="button" name="btprint" id="btprint" class="btn btn-default" tabindex="4">
                                        <i class="glyphicon glyphicon glyphicon-print"></i> พิมพ์ใบรับ
                                    </button>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <label class="control-label col-lg-4" >วันที่รับหนังสือ</label>
                                <div class="col-lg-6">
                                    <div class="input-group input-append date">
                                        <input class="form-control" type="text" value="<?php echo $data[0]->receiptorder_date; ?>" id="dateoffer" name="dateoffer" disabled/>
                                    </div>
                                </div>
                                <div class="input-group col-lg-12">
                                    &nbsp;
                                </div>
                                <label class="control-label col-lg-4">สำนักพิมพ์</label>
                                <div class="input-group col-lg-6">
                                    <input type="text" value="<?php echo $data[0]->publishing_name; ?>" class="form-control" disabled id="publishing" name="publishing">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            &nbsp;
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <table id="example" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th><center>ลำดับ</center></th>
                                            <th><center>ชื่อหนังสือ</center></th>
                                            <th><center>ราคา</center></th>
                                            <th><center>จำนวน</center></th>
                                            <th><center>ราคารวม</center></th>
                                        </tr>
                                    </thead>

                                    <tfoot>
                                        <tr>
                                            <th colspan="4" class="text-right">ราคารวมทั้งหมด</th>
                                            <th colspan="2"><center><span id="total"><?php echo $data[0]->receiptorder_totalprice; ?></span></center></th>
                                        </tr>
                                        <tr>
                                            <th colspan="4" class="text-right">
                                                <span class="pull-right">ภาษีมูลค่าเพิ่ม</span>
                                            </th>
                                            <th colspan="2"><center><span id="vattotal"><?php echo $data[0]->receiptorder_vat; ?></span></center></th>
                                        </tr>
                                        <tr>
                                            <th colspan="4" class="text-right">ราคารวมสุทธิ</th>
                                            <th colspan="2"><center><span id="subtotal"><?php echo $data[0]->receiptorder_vattotal; ?></span></center></th>
                                        </tr>
                                    </tfoot>

                                    <tbody id="bodylisttable">
                                      <?php $i = 1; foreach ($data as $value) { ?>
                                        <tr>
                                          <td><center><?php echo $i; ?></center></td>
                                          <td><center><?php echo $value->book_name; ?></center></td>
                                          <td><center><?php echo $value->price; ?></center></td>
                                          <td><center><?php echo $value->unit; ?></center></td>
                                          <td><center><?php echo $value->sumprice; ?></center></td>
                                        </tr>
                                      <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--END PAGE CONTENT -->
</div>
<!--END MAIN WRAPPER -->
<!-- GLOBAL SCRIPTS -->
<script>
$("#btprint").click(function () {
  window.open("<?php echo base_url()?>receiptorder_controller/receiptorderpdf/<?php echo $data[0]->receiptorder_id; ?>");
});
</script>
<!-- END GLOBAL SCRIPTS -->
