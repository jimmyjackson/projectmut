<!--PAGE CONTENT -->
<div id="content">

    <div class="inner">
            <div class="row has-success" >
                <div class="col-lg-5" style="padding-top: 30px; ">
                    <h1 class="page-header">ใบรับหนังสือ</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            ข้อมูลของใบรับหนังสือ
                        </div>
                        <div class="col-lg-12">
                            <div class="panel-heading">
                                <div class="form-inline">
                                <label class="control-label col-lg-4"></label>
                                <button type="button" class="btn btn-primary" id="btaddbook"><i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;เพิ่มข้อมูลใหม่</button>
                                <label class="control-label col-lg-2"></label>
                                <select id="selectsearch" class="selectpicker" multiple>
                                    <option value="id">รหัสใบรับ</option>
                                    <option value="date">วันที่รับ</option>
                                    <option value="idemp">รหัสผู้รับ</option>
                                    <option value="publishing">สำนักพิมพ์</option>
                                </select>
                                <input id="inputsearch" type="text" style="width: 21.75%" class="form-control" placeholder="กรอกข้อมูลที่ต้องการค้นหา">
                                <button id="btsearch" type="button" class="btn btn-info"><i class="icon-search icon-white"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
        <table id="example" class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th><center>รหัสใบรับ</center></th>
                <th><center>วันที่รับ</center></th>
                <th><center>รหัสผู้รับ</center></th>
                <th><center>สำนักพิมพ์</center></th>
                <th><center>เลือก</center></th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th><center>รหัสใบรับ</center></th>
                <th><center>วันทรับ</center></th>
                <th><center>รหัสผู้รับ</center></th>
                <th><center>สำนักพิมพ์</center></th>
                <th><center>เลือก</center></th>
            </tr>
        </tfoot>

        <tbody id="bodyshowdata">

        </tbody>
    </table>
            <nav>
                <ul class="pagination" id="paging">

                </ul>
            </nav>

            </div>

                        <div class="col-lg-12">
                                &nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <!--END PAGE CONTENT -->
    </div>
</div>
<!--END MAIN WRAPPER -->
<!-- GLOBAL SCRIPTS -->
<script>

$("nav").on('click','#paging li a',function(event){
    getdatarows(event.target.id);
});

$('#selectsearch').multiselect({
  nonSelectedText: 'ค้นหาจาก',
  includeSelectAllOption: true,
  selectAllText: 'เลือกทั้งหมด',
  /*enableFiltering: true,*/
  buttonWidth: '100px'
});

$("#btaddbook").click(function() {
  window.location.href = base_url + "/receiptorder_controller/receiptorder/";
});

$("#btsearch").click(function() {
  getdatarows(1);
});

getdatarows(1);

function getdatarows(pageselect){

  var offset = (pageselect-1)*10;
  var numpageshow =5;
  var lengthshow = Math.floor(numpageshow/2);

  if($('#inputsearch').val() == ""){

    $.ajax({
      type : "POST",
      url: base_url+"/receiptorder_controller/selectreceiptorder",
      data: {'offsetsend':offset},
      dataType: "json",
      success: function(data)
      {
        $("#bodyshowdata").empty();
        $.each(data.purchaseorderselect,function(ID,purchaseorder){
          $("#bodyshowdata").append(
            '<tr><td><center>'+purchaseorder.receiptorder_id+
            '</center></td><td><center>'+purchaseorder.receiptorder_date+
            '</center></td><td><center>'+purchaseorder.id_employee+
            '</center></td><td><center>'+purchaseorder.publishing_name+
            '</center></td><td><center><a href='+base_url+
            '/receiptorder_controller/receiptorder/'+purchaseorder.receiptorder_id+
            '><button type="button" class="btn btn-success" id='+purchaseorder.receiptorder_id+
            '><i class="glyphicon glyphicon-eye-open"></i>&nbsp;view</button></a></center></td></tr>');
          });
        }
    });

    $.ajax({
        type : "POST",
        url: base_url+"/receiptorder_controller/countreceiptorder",
        dataType: "json",
        success: function(data)
        {
          $("#paging").empty();
          $.each(data,function(i,count){
            $("#paging").append("<li id='row' class='disabled'><span>rows "+(offset+1)+" to "+(offset+10)+"</span></li>");
            $("#paging").append("<li id='first'><a href='#' id='1'>&laquo;</a></li>");

            var page = Math.ceil(count/10);
            if(page < 6)
            {
              for(var num=1;num<=page;num++)
                if(num==pageselect)
                  $("#paging").append("<li class='disabled'><a href='#' id='"+num+"'>"+num+"</a></li>");
                else
                  $("#paging").append("<li><a href='#' id='"+num+"'>"+num+"</a></li>");
            }
            else{
                    if(pageselect==page)
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++){
                            if((m+1)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                        }
                    }
                    else if(pageselect==(page-1))
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++)
                            if((m+2)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                    }
                    else{
                        for(var j=pageselect-1,k=0;k<numpageshow;j++,k++)
                        {
                            if((pageselect-1)<(numpageshow-lengthshow))
                                if((k+1)>page)
                                    break;
                                else
                                    if((k+1)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                            else
                                if((j+1)>(page+lengthshow))
                                    break;
                                else
                                    if(((j+1)-lengthshow)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                        }
                    }
                    }

                    $("#paging").append("<li id='last'><a href='#' id='"+page+"'>&raquo;</a></li>");
                    $("#paging").append("<li id='total' class='disabled'><span>page "+pageselect+" of "+page+"</span></li>");
                });
          }
    });

    }else{

        var selectmessage=[];
        var selectshowtype=[];
        $("#selectsearch option:selected").each(function (){
            selectmessage.push($(this).val());
        });

        if(selectmessage.length === 0)
            selectmessage.push('none');

        $("#selectshowtype option:selected").each(function (){
            selectshowtype.push($(this).val());
        });

        if(selectshowtype.length === 0)
            selectshowtype.push('none');

        $.ajax({
            type : "POST",
            url: base_url+"/receiptorder_controller/searchreceiptorder",
            data: {datasearch:{'selectmessage':selectmessage,'selectshowtype':selectshowtype,
            'inputsearch':$('#inputsearch').val(),'offset':0}},
            dataType: "json",
            success: function(data)
            {
                $("#bodyshowdata").empty();
                $.each(data,function(ID,purchaseorder){
                  $("#bodyshowdata").append(
                    '<tr><td><center>'+purchaseorder.receiptorder_id+
                    '</center></td><td><center>'+purchaseorder.receiptorder_date+
                    '</center></td><td><center>'+purchaseorder.id_employee+
                    '</center></td><td><center>'+purchaseorder.publishing_name+
                    '</center></td><td><center><a href='+base_url+
                    '/receiptorder_controller/receiptorder/'+purchaseorder.receiptorder_id+
                    '><button type="button" class="btn btn-success" id='+purchaseorder.receiptorder_id+
                    '><i class="glyphicon glyphicon-eye-open"></i>&nbsp;view</button></a></center></td></tr>');
                  });
            }
        });

        $.ajax({
            type : "POST",
            url: base_url+"/receiptorder_controller/countsearchreceiptorder",
            data: {datasearch:{'selectmessage':selectmessage,'selectshowtype':selectshowtype,
            'inputsearch':$('#inputsearch').val(),'offsetsend':offset}},
            dataType: "json",
            success: function(data)
            {
                $.each(data,function(i,count){
                    $("#paging").empty();
                    //console.log(count.countsearchemp);
                    $("#paging").append("<li id='row' class='disabled'><span>rows "+(offset+1)+" to "+(offset+10)+"</span></li>");
                    $("#paging").append("<li id='first'><a href='#' id='1'>&laquo;</a></li>");

                    var page = Math.ceil(count.count_purchaseorder/10);

                    //console.log(page);

                    if(page < 6)
                    {
                        for(var num=1;num<=page;num++)
                            if(num==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+num+"'>"+num+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+num+"'>"+num+"</a></li>");
                    }
                    else{
                    if(pageselect==page)
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++){
                            if((m+1)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                        }
                    }
                    else if(pageselect==(page-1))
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++)
                            if((m+2)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                    }
                    else{
                        for(var j=pageselect-1,k=0;k<numpageshow;j++,k++)
                        {
                            if((pageselect-1)<(numpageshow-lengthshow))
                                if((k+1)>page)
                                    break;
                                else
                                    if((k+1)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                            else
                                if((j+1)>(page+lengthshow))
                                    break;
                                else
                                    if(((j+1)-lengthshow)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");

                        }
                    }
                    }

                    $("#paging").append("<li id='last'><a href='#' id='"+page+"'>&raquo;</a></li>");
                    $("#paging").append("<li id='total' class='disabled'><span>page "+pageselect+" of "+page+"</span></li>");

                });

            }
    });

    }
}
</script>
<!-- END GLOBAL SCRIPTS -->
