<!--PAGE CONTENT -->
<div id="content">
    <div class="inner">
        <div class="row has-success" >
            <div class="col-lg-7" style="padding-top: 30px; ">
                <h1 class="page-header">ออกรายงานการยืมหนังสือรายปี</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        ออกรายงานการยืมหนังสือรายปี
                    </div>

                    <div class="col-lg-12">
                        <div class="panel-body">
                            <div class="form-inline">
                                <label class="control-label col-lg-2">ปีการศึกษา</label>
                                <div class="col-lg-2">
                                    <select id="from" name="from" class="validate[required] form-control">
                                      <?php
                                      foreach ($year as $value) {
                                      ?>
                                        <option value="<?php echo $value->yearstudy; ?>"><?php echo $value->yearstudy; ?></option>
                                      <?php
                                      }
                                      ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                        <button type="button" name="btreport" id="btreport" class="btn btn-primary" tabindex="21"><i class="glyphicon glyphicon glyphicon-print"></i> ออกรายงาน</button>
                                        <button type="button" id="btcancel" class="btn btn-danger" tabindex="22"><i class="icon-remove icon-white"></i> ยกเลิก</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END PAGE CONTENT -->
</div>
<!--END MAIN WRAPPER -->
<!-- GLOBAL SCRIPTS -->
<script>
$(function() {
var base_url = window.location.origin+"/CI";

$("#btreport").click(function () {
  window.open(base_url+"/reportborrowing_controller/selectreport/"+$( "#from" ).val());
});

$("#btcancel").click(function () {
  window.location.href = base_url+"/reportborrowing_controller";
});

});
</script>
<!-- END GLOBAL SCRIPTS -->
