<?php
class CheckPermission
{
    private $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function checklogin()
    {
        $class = $this->ci->router->class;
        if($this->ci->session->userdata("username") == null){
            if($class!="login_emp_controller"){
                redirect("/","refresh");
                exit();
            }
        }
        else{
            if($class=="login_emp_controller")
            {
                redirect("homeadmin_controller","refresh");
                exit();
            }
        }
    }

    public function check_permission_path()
    {
        $class = $this->ci->router->class;
        $method = $this->ci->router->method;
        /*$parameter = $this->ci->uri->segment(3);
        $path = $this->ci->uri->uri_string();
        $this->ci->load->helper('url');
        //var_dump(base_url().$path);*/
        if($this->ci->session->userdata("permission_constant") != null ||
          $this->ci->session->userdata("permission_purchase") != null){
            $status = false;

            if($this->ci->session->userdata("permission_constant") != null){
                    foreach($this->ci->session->userdata("permission_constant") as $key => $value){
                    if($class==$value||
                       $class=="homeadmin_controller"||
                       $class=="logout_emp_controller"||
                       $class=="province_controller"||
                       $class=="district_controller"||
                       $class=="zone_controller"||
                       $class=="postcode_controller"||
                       $class=="publishing_controller"||
                       $class=="mubook_controller"||
                       $class=="submubook_controller"||
                       $class=="title_controller"||
                       $class=="author_controller"||
                       $class=="yearstudy_controller"||
                       $class=="report"||
                       ($class=="position_controller"&&$method=="selectposition")){
                        $status = true;
                    }
                }
            }

            if($this->ci->session->userdata("permission_purchase") != null){
                foreach($this->ci->session->userdata("permission_purchase") as $key => $value){
                    if($class==$value||
                       $class=="homeadmin_controller"||
                       $class=="logout_emp_controller"||
                       $class=="province_controller"||
                       $class=="district_controller"||
                       $class=="zone_controller"||
                       $class=="postcode_controller"||
                       $class=="publishing_controller"||
                       $class=="mubook_controller"||
                       $class=="submubook_controller"||
                       $class=="title_controller"||
                       $class=="author_controller"||
                       $class=="yearstudy_controller"||
                       ($class=="position_controller"&&$method=="selectposition")){
                        $status = true;
                    }
                }
            }

            if($this->ci->session->userdata("permission_borrow") != null){
                foreach($this->ci->session->userdata("permission_borrow") as $key => $value){
                    if($class==$value||
                       $class=="homeadmin_controller"||
                       $class=="logout_emp_controller"||
                       $class=="province_controller"||
                       $class=="district_controller"||
                       $class=="zone_controller"||
                       $class=="postcode_controller"||
                       $class=="publishing_controller"||
                       $class=="mubook_controller"||
                       $class=="submubook_controller"||
                       $class=="title_controller"||
                       $class=="author_controller"||
                       $class=="yearstudy_controller"||
                       ($class=="position_controller"&&$method=="selectposition")){
                        $status = true;
                    }
                }
            }

            if($this->ci->session->userdata("permission_search") != null){
                foreach($this->ci->session->userdata("permission_search") as $key => $value){
                    if($class==$value||
                       $class=="homeadmin_controller"||
                       $class=="logout_emp_controller"||
                       $class=="province_controller"||
                       $class=="district_controller"||
                       $class=="zone_controller"||
                       $class=="postcode_controller"||
                       $class=="publishing_controller"||
                       $class=="mubook_controller"||
                       $class=="submubook_controller"||
                       $class=="title_controller"||
                       $class=="author_controller"||
                       $class=="yearstudy_controller"||
                       ($class=="position_controller"&&$method=="selectposition")){
                        $status = true;
                    }
                }
            }

            if($this->ci->session->userdata("permission_report") != null){
                foreach($this->ci->session->userdata("permission_report") as $key => $value){
                    if($class==$value||
                       $class=="homeadmin_controller"||
                       $class=="logout_emp_controller"||
                       $class=="province_controller"||
                       $class=="district_controller"||
                       $class=="zone_controller"||
                       $class=="postcode_controller"||
                       $class=="publishing_controller"||
                       $class=="mubook_controller"||
                       $class=="submubook_controller"||
                       $class=="title_controller"||
                       $class=="author_controller"||
                       $class=="yearstudy_controller"||
                       ($class=="position_controller"&&$method=="selectposition")){
                        $status = true;
                    }
                }
            }

            if(!$status){
                redirect("homeadmin_controller","refresh");
                exit();
            }
        }
    }
}
?>
