var idinput = $("#id");
var nameinput = $("#name");
var Lnameinput = $("#lname");
var sexinput = $("#sex");
var birthdateinput = $("#birthdate");
var imageinput = $("#imgInp");
var idcardinput = $("#idcard");
var emailinput = $("#email");
var positioninput = $("#position");
var userinput = $("#user");
var passinput = $("#pass");
var conpassinput = $("#conpass");
var addressinput = $("#address");
var districtinput = $("#district");
var zoneinput = $("#zone");
var provinceinput = $("#province");
var postcodeinput = $("#postcode");
var telinput = $("#tel");
var tellistinput = $("#tellist");
var telobj=[];
var teldelobj=[];
var statususername = 0;
var filenamesend ="";
var statusimage=1;
var base_url = window.location.origin+"/CI";
function cleandata(){
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = (month)+"/"+(day)+"/"+now.getFullYear();
    nameinput.val("");
    Lnameinput.val("");
    birthdateinput.val(today);
    idcardinput.val("");
    emailinput.val("");
    userinput.val("");
    passinput.val("");
    addressinput.val("");
    telinput.val("");
    $("#tellist option").remove();
}
function checkEmpty(){

   if(nameinput.val()==""){
		alert("กรุณากรอกชื่อ");
		nameinput.focus();
        return false;
	}
    else if(Lnameinput.val()==""){
		alert("กรุณากรอกนามสกุล");
		Lnameinput.focus();
        return false;
	}
	else if(birthdateinput.val()==""){
		alert("กรุณาเลือกวัน/เดือน/ปีเกิด");
		birthdateinput.focus();
        return false;
	}
    else if(tellistinput.val()==""){
		alert("กรุณาเพิ่มเบอร์ศัพท์");
		telinput.focus();
        return false;
	}
	else if(idcardinput.val()==""){
		alert("กรุณากรอกรหัสบัตรประชาชน");
		idcardinput.focus();
        return false;
	}
	else if(idcardinput.val()!=""&&checkidcard()!=1){
		alert("กรุณากรอกรหัสบัตรประชาชน 13 หลักให้ถูกต้อง");
        idcardinput.focus();
        return false;
	}
	else if(emailinput.val()==""){
		alert("กรุณากรอกอีเมล์");
		emailinput.focus();
        return false;
	}
	
	else if(emailinput.val()!=""&&checkemail()!=1){
		alert("กรุณากรอกอีเมล์ให้ถูกต้อง");
		emailinput.focus();
        return false;
	}
	else if(userinput.val()==""){
		alert("กรุณากรอกUsername");
		userinput.focus();
        return false;
	}
	else if(userinput.val()!=""&&checkusername()!=1){
		alert("กรุณากรอกUsernameให้มีความยาวตั้งแต่ 6 ตัวอักษรขึ้นไปแต่ไม่เกิน 15 ตัวอักษร");
		userinput.focus();
        return false;
	}
    else if((!statususername)&&$('#statuspage').val()=='add'){
        alert("กรุณาตรวจสอบ Username อาจซ้ำ หรือ ยังไม่ได้ทำการคลิกตรวจสอบ");
        userinput.focus();
        return false;
    }
	else if(passinput.val()==""){
		alert("กรุณากรอกPassword");
		passinput.focus();
        return false;
	}
	else if($('#statuspage').val()!='edit'&&passinput.val()!=""&&checkpassword()!=1){
		alert("กรุณากรอกPasswordให้มีความยาวตั้งแต่ 6 ตัวอักษรขึ้นไปแต่ไม่เกิน 15 ตัวอักษร");
		passinput.focus();
        return false;
	}
    else if(passinput.val()!=conpassinput.val()&&$('#statuspage').val()=='add'){
		alert("กรุณากรอก Password กับ Confirm Password ให้ตรงกัน");
		conpassinput.focus();
        return false;
	}
    else if(positioninput.val()==""){
		alert("กรุณาเลือกตำแหน่ง");
		positioninput.focus();
        return false;
	}
	else if(addressinput.val()==""){
		alert("กรุณากรอกที่อยู่");
		addressinput.focus();
        return false;
	}
    else if(provinceinput.val()==""){
		alert("กรุณาเลือกจังหวัด");
		provinceinput.focus();
        return false;
	}
    else if(zoneinput.val()==""){
		alert("กรุณาเลือกอำเภอ");
		zoneinput.focus();
        return false;
	}
	else if(districtinput.val()==""){
		alert("กรุณาเลือกตำบล");
		districtinput.focus();
        return false;
	}		
	else if(postcodeinput.val()==""){
		alert("กรุณาเลือกรหัสไปรษณีย์");
		postcodeinput.focus();
        return false;
	}
    else{
        return true;
    }
    
}
function checkidcard(){
    if(idcardinput.val().length != 13)
	   return 0;
	else
	{
	   for(i=0,sum=0; i<12;i++) 
	       sum += parseFloat(idcardinput.val().charAt(i))*(13-i); 
	   if((11-sum%11)%10!=parseFloat(idcardinput.val().charAt(12))) 
	       return 0; 
	   else
	       return 1;
	}
}
function checkemail(){
    var filter = /(^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9\.]{2,6}))$/;
    if (!filter.test(emailinput.val()))
	   return 0;
	else
	   return 1;    
}
function checkTel() {
    var filter = /(^0+([0-9]{7,9}))$/;
    var tel = telinput.val();   
    if (!filter.test(telinput.val())||tel[1]=="0")
	    return 0;
	else
	    return 1;
}
function checkTellist() {
    var status=1;
    var tel = document.getElementById("tel");
    var tellist = document.getElementById("tellist");
    for(i=0;i<tellist.length;i++){
        if(tel.value.localeCompare(tellist.options[i].value)==0){
            status=0;
            break;
        }
        else
            status=1;
    }
    if(status==0)
        return 0;
    else
        return 1;
}
function checkpassword(){
    if(passinput.val().length < 6 || passinput.val().length > 16)
	   return 0;
	return 1;
}
function checkusername(){
    if(userinput.val().length < 6 || userinput.val().length > 16)
	   return 0;
	return 1;
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var ftype=new Array(".gif",".jpg",".jpeg",".png"); 
        var a=input.value;  
	    var FileName  = input.value;
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today = now.getFullYear()+(month)+(day);
        filenamesend = FileName.substring((FileName.lastIndexOf("\\")+1));
        //console.log(filenamesend);
        var FileSize = input.files[0].size;
	    var FileSizeMB = (FileSize/10485760).toFixed(6);    
        var permiss=0;  
        a=a.toLowerCase();      
        if(a !=""){  
                for(i=0;i<ftype.length;i++){    
                    if(a.lastIndexOf(ftype[i])>=0){     
                        permiss=1;  
                        break;  
                    }else
                        continue;  
                }    
            if(permiss==0){  
               alert("กรุณาเลือกเฉพาะไฟล์นามสกุล .gif .jpg .jpeg .png เท่านั้น");
               input.value="";
               return false;                 
            } 
            else if(FileSizeMB>1.0){
                alert("กรุณาเลือกไฟล์ให้มีขนาดต่ำกว่า 1 MB");
                input.value="";
                return false;
            }
            else{
                reader.onload = function (e) {
                    $('#imageprofile').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
                return true;
            }
       }
    }
}
$("#file").change(function(){
    readURL(this);
       
});
function notRightClick(){
        $(this).bind('contextmenu', function(e){
            e.preventDefault();
            return false;
        });
}
$("#btcancel").click(function(){
        cleandata();
});
$("#name,#lname").bind('keypress',function(e){
           return (e.which!=8&&e.which!=0&&(e.which<48||e.which>57))?true:false;
});
$("#tel,#idcard").bind('keypress',function(e){
           return (e.which!=8&&e.which!=0&&(e.which<48||e.which>57))?false:true;
});
$( "#birthdate").datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat : "dd/mm/yy",
      duration : "slow"
    });
$("#btadd").click(function(){
    if(telinput.val()!=""&&checkTel()==1){
        var check = checkTellist();
        if(check==0){
            alert("เบอร์โทรซ้ำกับที่มีอยู่แล้ว");
            telinput.focus(); 
        }
        else{
            tellistinput.append("<option value='"+telinput.val()+"'>"+telinput.val()+"</option>");
            telobj.push(telinput.val());
            telinput.val("");
            telinput.focus();
        }
    }
    else
    {
        alert("กรุณากรอกเบอร์โทรให้ถูกต้อง");
        telinput.focus();
    }       
});
$("#btdel").click(function(){
    teldelobj.push($("#tellist option:selected").val());
    telobj.splice(telobj.indexOf($("#tellist option:selected").val()), 1);
    $("#tellist option:selected").remove();
});