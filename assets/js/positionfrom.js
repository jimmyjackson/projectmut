var base_url = window.location.origin+"/CI";
$(document).ready(function(){
        notRightClick();
        //cleandata();
        $('#selectsearch').multiselect({
            nonSelectedText: 'ค้นหาจาก',
            includeSelectAllOption: true,
            selectAllText: 'เลือกทั้งหมด',
             /*enableFiltering: true,*/
            buttonWidth: '134px'
        });

        pageingpositionlist(1);
        
        $("#btsave").click(function() {
            if(!checkEmpty()){
                event.preventDefault(); 
                return false;
            }
            else{
                if($('#statuspage').val()=='edit')
                {
                    var a = confirm("ต้องการแก้ไขข้อมูลใช่หรือไม่");
                    if(a){
                        editdataposition();
                        window.location.href=base_url+"/position_controller/";
                    }
                    else{
                        event.preventDefault(); 
                        return false;
                    }
                }
                else{
                    var a = confirm("ต้องการเพิ่มข้อมูลใช่หรือไม่");
                    if(a){
                        adddataposition();
                        window.location.href=base_url+"/position_controller/";
                    }
                    else{
                        event.preventDefault(); 
                        return false;
                    }
                }
            }
        });
});

function checkEmpty(){

   if($('#nameposition').val()==""){
		alert("กรุณากรอกชื่อตำแหน่ง");
		$('#id').focus();
        return false;
	}
    else{
        return true;
    }
    
}

function notRightClick(){
        $(this).bind('contextmenu', function(e){
            e.preventDefault();
            return false;
        });
}

function adddataposition()
{
    var access_position = [];
    var name_position = $('#nameposition').val();
    
    $('#bodyshowdataposition > tr > td > center > input[type=checkbox]:checked')
    .each(function(index) {
        access_position.push(this.id);
    });
    
    if(access_position.length < 1)
        access_position = "";
    
    $.ajax({
            type : "POST",
            url: base_url+"/position_controller/addposition",
            data: {'objposi':
                   [{'name_position':name_position,'access_position':access_position}]},
            success: function(data)
            {
                if(data!="")
                {
                    alert("insert OK");
                }
                else
                {
                    alert("insert error");
                }
            }
    });
}

function editdataposition()
{
    var access_position = [];
    var name_position = $('#nameposition').val();
    
    $('#bodyshowdataposition > tr > td > center > input[type=checkbox]:checked')
    .each(function(index) {
        access_position.push(this.id);
    });
    
    if(access_position.length < 1)
        access_position = "";
    
    $.ajax({
            type : "POST",
            url: base_url+"/position_controller/editposition",
            data: {'objposi':[{'id_position':$("#id").val(),
            'name_position':name_position,'access_position':access_position}]},
            success: function(data)
            {
                if(data!="")
                {
                    alert("update OK");
                }
                else
                {
                    alert("update error");
                }
            }
    });
}

function pageingpositionlist(pageselect){
    
    var offset = (pageselect-1)*10;
    var numpageshow =5;
    var lengthshow = Math.floor(numpageshow/2);
    //console.log(offset);
    
    if($('#inputsearch').val() == ""){
        
        $.ajax({
            type : "POST",
            url: base_url+"/position_controller/selectallposition",
            data: {'offsetsend':offset},
            dataType: "json",
            success: function(data)
            {
                $("#bodyshowdata").empty();
                    
                $.each(data.positionselect,function(ID,pos){
              
                    $("#bodyshowdata").append('<tr><td><center>'+pos.position_id+'</center></td><td><center>'+pos.position_name+'</center></td><td><center><a href='+base_url+'/position_controller/position/'+pos.position_id+'><button type="button" class="btn btn-success" id='+pos.position_id+'><i class="glyphicon glyphicon-eye-open"></i>&nbsp;view</button></a></center></td></tr>');
                    
                });
            }
    });
    
    
        $.ajax({
            type : "POST",
            url: base_url+"/position_controller/countallposition",
            //data: {usernamecheck:'basadultindy0'},
            dataType: "json",
            success: function(data)
            {
                $("#paging").empty();
                $.each(data,function(i,count){ 
                    //console.log(count);
                    $("#paging").append("<li id='row' class='disabled'><span>rows "+(offset+1)+" to "+(offset+10)+"</span></li>");
                    $("#paging").append("<li id='first'><a href='#' id='1'>&laquo;</a></li>");
                        
                    var page = Math.ceil(count/10);
                    //console.log(page);
                    if(page < 6)
                    {
                        for(var num=1;num<=page;num++)
                            if(num==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+num+"'>"+num+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+num+"'>"+num+"</a></li>");
                    }
                    else{
                    if(pageselect==page)
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++){
                            if((m+1)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                        }
                    }
                    else if(pageselect==(page-1))
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++)
                            if((m+2)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                    }
                    else{
                        for(var j=pageselect-1,k=0;k<numpageshow;j++,k++)
                        {
                            if((pageselect-1)<(numpageshow-lengthshow))
                                if((k+1)>page)
                                    break;
                                else 
                                    if((k+1)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                            else
                                if((j+1)>(page+lengthshow))
                                    break;
                                else
                                    if(((j+1)-lengthshow)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                //$("#next").before("<li><a href='#' id='"+(j+1)+"'>"+(j+1)+"</a></li>");
                        }
                    }
                    }
                    
                    $("#paging").append("<li id='last'><a href='#' id='"+page+"'>&raquo;</a></li>");
                    $("#paging").append("<li id='total' class='disabled'><span>page "+pageselect+" of "+page+"</span></li>");
                });
        
            }
    });
        
    }else{
        
        var selectmessage=[];
        $("#selectsearch option:selected").each(function (){
            selectmessage.push($(this).val());
        });

        if(selectmessage.length === 0)
            selectmessage.push('none');

        $.ajax({
            type : "POST",
            url: base_url+"/position_controller/searchposition",
            data: {datasearch:{'selectmessage':selectmessage,
            'inputsearch':$('#inputsearch').val(),'offset':0}},
            dataType: "json",
            success: function(data)
            {
                //console.log(data);
                $("#bodyshowdata").empty();
                    
                $.each(data,function(ID,pos){
              
                    $("#bodyshowdata").append('<tr><td><center>'+pos.position_id+'</center></td><td><center>'+pos.position_name+'</center></td><td><center><a href='+base_url+'/position_controller/position/'+pos.position_id+'><button type="button" class="btn btn-success" id='+pos.position_id+'><i class="glyphicon glyphicon-eye-open"></i>&nbsp;view</button></a></center></td></tr>');
                    
                }); 
                
            }
        });
        
        $.ajax({
            type : "POST",
            url: base_url+"/position_controller/countsearchposition",
            data: {datasearch:{'selectmessage':selectmessage,
            'inputsearch':$('#inputsearch').val()}},
            dataType: "json",
            success: function(data)
            {
                $.each(data,function(i,count){
                    $("#paging").empty();
                    //console.log(count.countsearchemp);
                    $("#paging").append("<li id='row' class='disabled'><span>rows "+(offset+1)+" to "+(offset+10)+"</span></li>");
                    $("#paging").append("<li id='first'><a href='#' id='1'>&laquo;</a></li>");
                    
                    var page = Math.ceil(count.countsearchposition/10);
                    
                    //console.log(page);
                    
                    if(page < 6)
                    {
                        for(var num=1;num<=page;num++)
                            if(num==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+num+"'>"+num+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+num+"'>"+num+"</a></li>");
                    }
                    else{
                    if(pageselect==page)
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++){
                            if((m+1)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                        }
                    }
                    else if(pageselect==(page-1))
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++)
                            if((m+2)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                    }
                    else{
                        for(var j=pageselect-1,k=0;k<numpageshow;j++,k++)
                        {
                            if((pageselect-1)<(numpageshow-lengthshow))
                                if((k+1)>page)
                                    break;
                                else 
                                    if((k+1)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                            else
                                if((j+1)>(page+lengthshow))
                                    break;
                                else
                                    if(((j+1)-lengthshow)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                //$("#next").before("<li><a href='#' id='"+(j+1)+"'>"+(j+1)+"</a></li>");
                        }
                    }
                    }
                    
                    $("#paging").append("<li id='last'><a href='#' id='"+page+"'>&raquo;</a></li>");
                    $("#paging").append("<li id='total' class='disabled'><span>page "+pageselect+" of "+page+"</span></li>");
                    
                });
                    
            }
    });
        
    }
}

$("nav").on('click','#paging li a',function(event){
    pageingpositionlist(event.target.id);
});

$("#btsearch").click(function(){ 
    pageingpositionlist(1);
});

$("#btaddposition").click(function(){ 
    window.location.href=base_url+"/position_controller/position/";
});

$("#btdelete").click(function(){ 
    //alert("delect");
    var a = confirm("ต้องการลบข้อมูลใช่หรือไม่");
    if(a){
        $.post(base_url+'/position_controller/deleteposition',
        {idposition:$('#id').val()},function(data){});

        window.location.href=base_url+"/position_controller";
    }
});

$("#btcancel").click(function(){ 
   
    if($('#id').val()=='Auto_Id')
        window.location.href=base_url+"/position_controller/position/";
    else
    window.location.href=base_url+"/position_controller/position/"+$('#id').val();
    
    
});
