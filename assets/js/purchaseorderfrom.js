var base_url = window.location.origin + "/CI";

var purchaseorder = function() {
  var purchaseorderlist = [];
  return {

    purchaseorder_init_list: function() {

      $('#selectsearch').multiselect({
        nonSelectedText: 'ค้นหาจาก',
        includeSelectAllOption: true,
        selectAllText: 'เลือกทั้งหมด',
        /*enableFiltering: true,*/
        buttonWidth: '100px'
      });

      $("#btaddbook").click(function() {
        window.location.href = base_url + "/purchaseorder_controller/purchaseorder/";
      });

      $("#btsearch").click(function() {
        getdatarows(1);
      });

    },

    purchaseorder_init_add: function() {
      purchaseorder.load_purchaseorder_detial(function (status) {});
      purchaseorder.change_publishing();
      purchaseorder.purchaseorder_add();
      purchaseorder.purchaseorder_reset_add();
    },

    purchaseorder_add: function() {
      $("#btsave").click(function() {
        purchaseorderlist = [];
        $('#bodylisttable > tr > td > center > input[type=checkbox]:checked')
        .each(function() {
            purchaseorderlist.push(this.value);
        });
        purchaseorder.purchaseorder_submit('save');
      });
    },

    purchaseorder_reset_add: function() {

      $("#btcancel").click(function() {
        if ($('#id').val() == 'Auto_Id')
          window.location.href = base_url + "/purchaseorder_controller/purchaseorder/";
        else
          window.location.href = base_url +
          "/purchaseorder_controller/purchaseorder/" + $('#id').val();
      });

    },

    purchaseorder_init_edit: function() {
      purchaseorder.load_purchaseorder_detial(function (status) {
        $.ajax({
            type : "GET",
            url: base_url+"/purchaseorder_controller/selectdatapurchaseorder/"+$("#id").val(),
            dataType: "json",
            success: function(datas)
            {
              $("#dateoffer").val(
                datas[0].purchaseorder_date.substring(5,7)+'/'+
                datas[0].purchaseorder_date.substring(8)+'/'+
                datas[0].purchaseorder_date.substring(0,4)
              );

              $("#empid").val(datas[0].id_employee);
              $("#vat").val(datas[0].purchaseorder_vat);
              $("#publishing").val(datas[0].id_publishing);
              $("#total").text(
                parseFloat(datas[0].purchaseorder_pricetotal).toFixed(2)
              );
              $("#vattotal").text(
                parseFloat(datas[0].purchaseorder_vattotal).toFixed(2)
              );
              $("#subtotal").text(
                parseFloat(datas[0].purchaseorder_subpricetotal).toFixed(2)
              );

              datas.forEach(function (purchaseorder, index) {
                var newRowContent = "<tr><td><center>" + (index + 1) +
                    "</center></td><td><center>" + purchaseorder.book_name +
                    "</center></td><td><center>" + purchaseorder.purchaseorderdetial_bookprice +
                    "</center></td><td><center>" + purchaseorder.purchaseorderdetial_unit +
                    "</center></td><td><center>" + purchaseorder.purchaseorderdetial_sumprice +
                    "</center></td></tr>";

                $("#bodylisttable").append(newRowContent);
              });
            }
        });
      });
    },

    load_purchaseorder_detial: function(callback) {
      showallpublishing();

      if($("#publishing > option").length > 1)
        callback(true);
      else {
        setTimeout(function () {
          callback(true);
        },700);
      }
    },

    change_publishing: function () {
      $("#publishing").change(function() {
        $.ajax({
            type : "POST",
            url: base_url+"/offerorder_controller/selectapproveorderpurchaseorder/",
            dataType: "json",
            data: {
              'idpublishing': $("#publishing").val()
            },
            success: function(datas)
            {
              $("#bodylisttable").empty();

              datas.forEach(function(data, index) {
                var newRowContent = "<tr><td class='col-lg-1'><center><input type='checkbox' value='" + data.offerorder_id +
                      "' /></center></td><td class='col-lg-2'><center>" + data.offerorder_id +
                      "</center></td><td class='col-lg-3'><center>" + data.offerorder_approvedate +
                      "</center></td><td class='col-lg-3'><center>" + data.offerorder_offervat + " % " +
                      "</center></td><td class='col-lg-3'><center>" + data.offerorder_approvesubtotal +
                      "</center></td></tr>";

                $("#bodylisttable").append(newRowContent);
              });
            }
        });
      });
    },

    purchaseorder_submit: function(status) {
      if (!validate_purchaseorder.checkEmpty()) {
        event.preventDefault();
        return false;
      }
      else if(purchaseorderlist.length == 0) {
        alert("กรุณาเลือกรายการที่จะสั่งซื้อ");
        $("input[type=checkbox]:first").focus();
        return false;
      }
      else {

        $.ajax({
          url: base_url + "/purchaseorder_controller/selectvatapproveorder",
          type: 'POST',
          data: {
            'objpurchaseorder': [{
              'purchaseorderlist': purchaseorderlist
            }]
          },
          success: function(data) {
            if(data == true){
              var a = confirm("ต้องการบันทึกข้อมูลใช่หรือไม่");
              if (a) {
                if (purchaseorderlist.length == 0)
                  purchaseorderlist.push("none");

                $.ajax({
                  url: base_url + "/purchaseorder_controller/save_purchaseorder",
                  type: 'POST',
                  data: {
                    'objpurchaseorder': [{
                      'publishing': $("#publishing").val(),
                      'purchaseorderlist': purchaseorderlist
                    }]
                  },
                  dataType: "json",
                  success: function(data) {
                    window.open(base_url+"/purchaseorder_controller/purchaseorderpdf/"+data.idpurchaseorder);
                    window.location.href = base_url + "/purchaseorder_controller";
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                    window.location.href = base_url + "/purchaseorder_controller";
                  }
                });
              } else {
                event.preventDefault();
                return false;
              }
            } else {
              alert("ภาษีมูลค่าเพิ่มไม่เท่ากันกรุณาเลือกรายการใหม่")
              event.preventDefault();
              return false;
            }
          }
        });
      }
    },

  };

}();
