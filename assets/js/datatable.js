function datatable(url_selectdata,url_editdata,url_countdata,url_searchdata,url_countsearchdata,pageselect){
    
    var offset = (pageselect-1)*10;
    var numpageshow =5;
    var lengthshow = Math.floor(numpageshow/2);
    //console.log(offset);
    
    if($('#inputsearch').val() == ""){
        
        $.ajax({
            type : "POST",
            url: base_url+"/"+url_selectdata,
            data: {'offsetsend':offset},
            dataType: "json",
            success: function(data)
            {
                $("#bodyshowdataemp").empty();
                    
                $.each(data.empselect,function(ID,emp){
                    if(emp.telemployee_tel==null)
                        var telemployee_tel = "ไม่มี";
                    else
                        var telemployee_tel = emp.telemployee_tel;
                            
                    $("#bodyshowdataemp").append('<tr><td><center>'+emp.employee_id+'</center></td><td><center>'+emp.employee_name+
                    ' '+emp.employee_lname+'</center></td><td><center>'+emp.position_name+'</center></td><td><center>'+emp.employee_username+
                    '</center></td><td><center>'+telemployee_tel+'</center></td><td><center><a href='+base_url+'/'+url_editdata+'/'+emp.employee_id+'><button type="button" class="btn btn-success" id='+emp.employee_id+'><i class="glyphicon glyphicon-eye-open"></i>&nbsp;view</button></a></center></td></tr>');
                    });
                }
    });
    
    
        $.ajax({
            type : "POST",
            url: base_url+"/"+url_countdata,
            //data: {usernamecheck:'basadultindy0'},
            dataType: "json",
            success: function(data)
            {
                $("#paging").empty();
                $.each(data,function(i,count){ 
                    //console.log(count);
                    $("#paging").append("<li id='row' class='disabled'><span>rows "+(offset+1)+" to "+(offset+10)+"</span></li>");
                    $("#paging").append("<li id='first'><a href='#' id='1'>&laquo;</a></li>");
                        
                    var page = Math.ceil(count/10);
                    //console.log(page);
                    if(page < 6)
                    {
                        for(var num=1;num<=page;num++)
                            if(num==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+num+"'>"+num+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+num+"'>"+num+"</a></li>");
                    }
                    else{
                    if(pageselect==page)
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++){
                            if((m+1)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                        }
                    }
                    else if(pageselect==(page-1))
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++)
                            if((m+2)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                    }
                    else{
                        for(var j=pageselect-1,k=0;k<numpageshow;j++,k++)
                        {
                            if((pageselect-1)<(numpageshow-lengthshow))
                                if((k+1)>page)
                                    break;
                                else 
                                    if((k+1)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                            else
                                if((j+1)>(page+lengthshow))
                                    break;
                                else
                                    if(((j+1)-lengthshow)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                //$("#next").before("<li><a href='#' id='"+(j+1)+"'>"+(j+1)+"</a></li>");
                        }
                    }
                    }
                    
                    $("#paging").append("<li id='last'><a href='#' id='"+page+"'>&raquo;</a></li>");
                    $("#paging").append("<li id='total' class='disabled'><span>page "+pageselect+" of "+page+"</span></li>");
                });
        
            }
    });
        
    }else{
        
        var selectmessage=[];
        $("#selectsearch option:selected").each(function (){
            selectmessage.push($(this).val());
        });

        if(selectmessage.length === 0)
            selectmessage.push('none');

        $.ajax({
            type : "POST",
            url: base_url+"/"+url_searchdata,
            data: {datasearch:{'selectmessage':selectmessage,
            'inputsearch':$('#inputsearch').val(),'offset':0}},
            dataType: "json",
            success: function(data)
            {
                //console.log(data);
                $("#bodyshowdataemp").empty();
                    
                $.each(data,function(ID,emp){
                    if(emp.telemployee_tel==null)
                        var telemployee_tel = "ไม่มี";
                    else
                        var telemployee_tel = emp.telemployee_tel;
                            
                    $("#bodyshowdataemp").append('<tr><td><center>'+emp.employee_id+'</center></td><td><center>'+emp.employee_name+
                    ' '+emp.employee_lname+'</center></td><td><center>'+emp.position_name+'</center></td><td><center>'+emp.employee_username+
                    '</center></td><td><center>'+telemployee_tel+'</center></td><td><center><a href='+base_url+'/'+url_editdata+'/'+emp.employee_id+'><button type="button" class="btn btn-success" id='+emp.employee_id+'><i class="glyphicon glyphicon-eye-open"></i>&nbsp;view</button></a></center></td></tr>');
                }); 
                
            }
        });
        
        $.ajax({
            type : "POST",
            url: base_url+"/"+url_countsearchdata,
            data: {datasearch:{'selectmessage':selectmessage,
            'inputsearch':$('#inputsearch').val()}},
            dataType: "json",
            success: function(data)
            {
                $.each(data,function(i,count){
                    $("#paging").empty();
                    //console.log(count.countsearchemp);
                    $("#paging").append("<li id='row' class='disabled'><span>rows "+(offset+1)+" to "+(offset+10)+"</span></li>");
                    $("#paging").append("<li id='first'><a href='#' id='1'>&laquo;</a></li>");
                    
                    var page = Math.ceil(count.countsearchemp/10);
                    
                    //console.log(page);
                    
                    if(page < 6)
                    {
                        for(var num=1;num<=page;num++)
                            if(num==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+num+"'>"+num+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+num+"'>"+num+"</a></li>");
                    }
                    else{
                    if(pageselect==page)
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++){
                            if((m+1)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                        }
                    }
                    else if(pageselect==(page-1))
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++)
                            if((m+2)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                    }
                    else{
                        for(var j=pageselect-1,k=0;k<numpageshow;j++,k++)
                        {
                            if((pageselect-1)<(numpageshow-lengthshow))
                                if((k+1)>page)
                                    break;
                                else 
                                    if((k+1)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                            else
                                if((j+1)>(page+lengthshow))
                                    break;
                                else
                                    if(((j+1)-lengthshow)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                //$("#next").before("<li><a href='#' id='"+(j+1)+"'>"+(j+1)+"</a></li>");
                        }
                    }
                    }
                    
                    $("#paging").append("<li id='last'><a href='#' id='"+page+"'>&raquo;</a></li>");
                    $("#paging").append("<li id='total' class='disabled'><span>page "+pageselect+" of "+page+"</span></li>");
                    
                });
                    
            }
    });
        
    }
}