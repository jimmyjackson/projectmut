var base_url = window.location.origin+"/CI";
$(document).ready(function(){
        notRightClick();
        //cleandata();
        $('#selectsearch').multiselect({
            nonSelectedText: 'ค้นหาจาก',
            includeSelectAllOption: true,
            selectAllText: 'เลือกทั้งหมด',
             /*enableFiltering: true,*/
            buttonWidth: '134px'
        });
        showallposition();
        showallprovince();
        getempdatarows(1);
        provinceinput.change(function(){
            $("#zone option").remove();
            $("#district option").remove();
            $("#postcode option").remove();
            zoneinput.append("<option value=''>อำเภอ</option>");
            districtinput.append("<option value=''>ตำบล</option>");
            postcodeinput.append("<option value=''>รหัสไปรษณีย์</option>");
            showallzone(provinceinput.val());
        });
        zoneinput.change(function(){
            $("#district option").remove();
            $("#postcode option").remove();
            districtinput.append("<option value=''>ตำบล</option>");
            postcodeinput.append("<option value=''>รหัสไปรษณีย์</option>");
            showalldistrict(provinceinput.val(),zoneinput.val());
        });
        districtinput.change(function(){
            $("#postcode option").remove();
            postcodeinput.append("<option value=''>รหัสไปรษณีย์</option>");
            showallpostcode(provinceinput.val(),zoneinput.val(),districtinput.val());
        });
        $("#empForm").submit(function(event) {
            if(!checkEmpty()){
                event.preventDefault(); 
                return false;
            }
            else{
                if($('#statuspage').val()=='edit')
                {
                    /*alert('edit');*/
                    var a = confirm("ต้องการแก้ไขข้อมูลใช่หรือไม่");
                    if(a){
                        editdataemp();
                    }
                    else{
                        event.preventDefault(); 
                        return false;
                    }
                }
                else{
                    //console.log('add');
                    var a = confirm("ต้องการเพิ่มข้อมูลใช่หรือไม่");
                    if(a){
                        adddataemp();
                    }
                    else{
                        event.preventDefault(); 
                        return false;
                    }
                }
            }
        });
});

function adddataemp()
{
    if(filenamesend=="")
    {
        filenamesend = "demoUpload.jpg";
        statusimage = 0;
    }
    
    var birthday = birthdateinput.val().substring(6,10)+'-'+birthdateinput.val().substring(0,2)
                +'-'+birthdateinput.val().substring(3,5); 
    $.post(base_url+'/emp_controller/addemp',{'objemp':           
    [{idemp:idinput.val(),nameemp:nameinput.val(),lnameemp:Lnameinput.val(),
    sexemp:sexinput.val(),birthdayemp:birthday,filename:filenamesend,
    imagestatus:statusimage,addressemp:addressinput.val(),emailemp:emailinput.val(),
    idcardemp:idcardinput.val(),useremp:userinput.val(),passemp:passinput.val(),
    postcemp:postcodeinput.val(),provemp:provinceinput.val(),zoneemp:zoneinput.val(),
    distemp:districtinput.val(),positemp:positioninput.val(),tel:telobj}]},
    function(data){
        if(data!="")
        {
            alert("insert ok"); 
            cleandata();
        }
        else
        {
            alert("insert error");
        }
    });
    
}
function editdataemp(){
    var tel,deltel;
    
    if(filenamesend=="")
    {
        filenamesend = $('#imageprofile').attr('src');
        statusimage = 0;
    }
    var birthday = birthdateinput.val().substring(6,10)+'-'+birthdateinput.val().substring(0,2)
                +'-'+birthdateinput.val().substring(3,5);
    
    if(telobj.length == 0)
        tel = "";
    else
        tel = telobj;
        
    if(teldelobj.length == 0)
        deltel = "";
    else
        deltel = teldelobj;
    
    $.post(base_url+'/emp_controller/editemp',{'objemp':           
    [{idemp:idinput.val(),nameemp:nameinput.val(),lnameemp:Lnameinput.val(),
    sexemp:sexinput.val(),birthdayemp:birthday,filename:filenamesend,
    imagestatus:statusimage,addressemp:addressinput.val(),emailemp:emailinput.val(),
    idcardemp:idcardinput.val(),useremp:userinput.val(),postcemp:postcodeinput.val(),
    provemp:provinceinput.val(),zoneemp:zoneinput.val(),distemp:districtinput.val(),
    positemp:positioninput.val(),tel:tel,teldel:deltel}]},
    function(data){
        if(data!="")
        {
            alert("update OK");
            //cleandata();
        }
        else
        {
            alert("update error");
        }
    });
}
function getempdatarows(pageselect){
    
    var offset = (pageselect-1)*10;
    var numpageshow =5;
    var lengthshow = Math.floor(numpageshow/2);
    //console.log(offset);
    
    if($('#inputsearch').val() == ""){
        
        $.ajax({
            type : "POST",
            url: base_url+"/emp_controller/selectemp",
            data: {'offsetsend':offset},
            dataType: "json",
            success: function(data)
            {
                $("#bodyshowdataemp").empty();
                    
                $.each(data.empselect,function(ID,emp){
                    if(emp.telemployee_tel==null)
                        var telemployee_tel = "ไม่มี";
                    else
                        var telemployee_tel = emp.telemployee_tel;
                            
                    $("#bodyshowdataemp").append('<tr><td><center>'+emp.employee_id+'</center></td><td><center>'+emp.employee_name+
                    ' '+emp.employee_lname+'</center></td><td><center>'+emp.position_name+'</center></td><td><center>'+emp.employee_username+
                    '</center></td><td><center>'+telemployee_tel+'</center></td><td><center><a href='+base_url+'/emp_controller/emp/'+emp.employee_id+'><button type="button" class="btn btn-success" id='+emp.employee_id+'><i class="glyphicon glyphicon-eye-open"></i>&nbsp;view</button></a></center></td></tr>');
                    });
                }
    });
    
    
        $.ajax({
            type : "POST",
            url: base_url+"/emp_controller/countemp",
            //data: {usernamecheck:'basadultindy0'},
            dataType: "json",
            success: function(data)
            {
                $("#paging").empty();
                $.each(data,function(i,count){ 
                    //console.log(count);
                    $("#paging").append("<li id='row' class='disabled'><span>rows "+(offset+1)+" to "+(offset+10)+"</span></li>");
                    $("#paging").append("<li id='first'><a href='#' id='1'>&laquo;</a></li>");
                        
                    var page = Math.ceil(count/10);
                    //console.log(page);
                    if(page < 6)
                    {
                        for(var num=1;num<=page;num++)
                            if(num==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+num+"'>"+num+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+num+"'>"+num+"</a></li>");
                    }
                    else{
                    if(pageselect==page)
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++){
                            if((m+1)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                        }
                    }
                    else if(pageselect==(page-1))
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++)
                            if((m+2)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                    }
                    else{
                        for(var j=pageselect-1,k=0;k<numpageshow;j++,k++)
                        {
                            if((pageselect-1)<(numpageshow-lengthshow))
                                if((k+1)>page)
                                    break;
                                else 
                                    if((k+1)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                            else
                                if((j+1)>(page+lengthshow))
                                    break;
                                else
                                    if(((j+1)-lengthshow)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                //$("#next").before("<li><a href='#' id='"+(j+1)+"'>"+(j+1)+"</a></li>");
                        }
                    }
                    }
                    
                    $("#paging").append("<li id='last'><a href='#' id='"+page+"'>&raquo;</a></li>");
                    $("#paging").append("<li id='total' class='disabled'><span>page "+pageselect+" of "+page+"</span></li>");
                });
        
            }
    });
        
    }else{
        
        var selectmessage=[];
        $("#selectsearch option:selected").each(function (){
            selectmessage.push($(this).val());
        });

        if(selectmessage.length === 0)
            selectmessage.push('none');

        $.ajax({
            type : "POST",
            url: base_url+"/emp_controller/searchemp",
            data: {datasearch:{'selectmessage':selectmessage,
            'inputsearch':$('#inputsearch').val(),'offset':0}},
            dataType: "json",
            success: function(data)
            {
                //console.log(data);
                $("#bodyshowdataemp").empty();
                    
                $.each(data,function(ID,emp){
                    if(emp.telemployee_tel==null)
                        var telemployee_tel = "ไม่มี";
                    else
                        var telemployee_tel = emp.telemployee_tel;
                            
                    $("#bodyshowdataemp").append('<tr><td><center>'+emp.employee_id+'</center></td><td><center>'+emp.employee_name+
                    ' '+emp.employee_lname+'</center></td><td><center>'+emp.position_name+'</center></td><td><center>'+emp.employee_username+
                    '</center></td><td><center>'+telemployee_tel+'</center></td><td><center><a href='+base_url+'/emp_controller/emp/'+emp.employee_id+'><button type="button" class="btn btn-success" id='+emp.employee_id+'><i class="glyphicon glyphicon-eye-open"></i>&nbsp;view</button></a></center></td></tr>');
                }); 
                
            }
        });
        
        $.ajax({
            type : "POST",
            url: base_url+"/emp_controller/countsearchemp",
            data: {datasearch:{'selectmessage':selectmessage,
            'inputsearch':$('#inputsearch').val()}},
            dataType: "json",
            success: function(data)
            {
                $.each(data,function(i,count){
                    $("#paging").empty();
                    //console.log(count.countsearchemp);
                    $("#paging").append("<li id='row' class='disabled'><span>rows "+(offset+1)+" to "+(offset+10)+"</span></li>");
                    $("#paging").append("<li id='first'><a href='#' id='1'>&laquo;</a></li>");
                    
                    var page = Math.ceil(count.countsearchemp/10);
                    
                    //console.log(page);
                    
                    if(page < 6)
                    {
                        for(var num=1;num<=page;num++)
                            if(num==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+num+"'>"+num+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+num+"'>"+num+"</a></li>");
                    }
                    else{
                    if(pageselect==page)
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++){
                            if((m+1)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+1)+"'>"+(m+1)+"</a></li>");
                        }
                    }
                    else if(pageselect==(page-1))
                    {
                        for(var m=pageselect-numpageshow,n=0;n<numpageshow;m++,n++)
                            if((m+2)==pageselect)
                                $("#paging").append("<li class='disabled'><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                            else
                                $("#paging").append("<li><a href='#' id='"+(m+2)+"'>"+(m+2)+"</a></li>");
                    }
                    else{
                        for(var j=pageselect-1,k=0;k<numpageshow;j++,k++)
                        {
                            if((pageselect-1)<(numpageshow-lengthshow))
                                if((k+1)>page)
                                    break;
                                else 
                                    if((k+1)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+(k+1)+"'>"+(k+1)+"</a></li>");
                            else
                                if((j+1)>(page+lengthshow))
                                    break;
                                else
                                    if(((j+1)-lengthshow)==pageselect)
                                        $("#paging").append("<li class='disabled'><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                    else
                                        $("#paging").append("<li><a href='#' id='"+((j+1)-lengthshow)+"'>"+((j+1)-lengthshow)+"</a></li>");
                                //$("#next").before("<li><a href='#' id='"+(j+1)+"'>"+(j+1)+"</a></li>");
                        }
                    }
                    }
                    
                    $("#paging").append("<li id='last'><a href='#' id='"+page+"'>&raquo;</a></li>");
                    $("#paging").append("<li id='total' class='disabled'><span>page "+pageselect+" of "+page+"</span></li>");
                    
                });
                    
            }
    });
        
    }
}
$("nav").on('click','#paging li a',function(event){
    getempdatarows(event.target.id);
});

$("#btcheckuser").click(function(){ 
    if(userinput.val()!="" && (userinput.val().length >= 6 && userinput.val().length <= 16)){
    $.post(base_url+'/emp_controller/selectusernameemp',
               {usernamecheck:userinput.val()},function(data){
                        
                    if(data.uc!=""||userinput.val()=="")
                    {
                        statususername=0;
                        $("#showstatususer").html("");
                        $("#showstatususer").append("<i class='glyphicon glyphicon-remove text-danger' style='font-size:25px;'></i>");
                        userinput.focus();
                    }
                    else{
                        statususername=1;
                        $("#showstatususer").html("");
                        $("#showstatususer").append("<i class='glyphicon glyphicon-ok text-success' style='font-size:25px;'></i>");
                    }

    },"json");
    }
    else{
        statususername=0;
        $("#showstatususer").html("");
        $("#showstatususer").append("<i class='glyphicon glyphicon-remove text-danger' style='font-size:25px;'></i>");
        userinput.focus();
    }
    
});

$("#btaddemp").click(function(){ 
    window.location.href=base_url+"/emp_controller/emp/";
});

$("#btcancel").click(function(){ 
    if($('#id').val()=='Auto_Id')
        window.location.href=base_url+"/emp_controller/emp/";
    else
        window.location.href=base_url+"/emp_controller/emp/"+$('#id').val();
});

$("#btdelete").click(function(){ 
    //alert("delect");
    var a = confirm("ต้องการลบข้อมูลใช่หรือไม่");
    if(a){
        $.post(base_url+'/emp_controller/deleteemp',
        {idemp:$('#id').val()},function(data){});

        window.location.href=base_url+"/emp_controller";
    }
});
$("#btsearch").click(function(){ 
    getempdatarows(1);
});



/*$("#btsave").click(function(){ 
    $.post(base_url+'/emp_controller/delete_image_emp',{'objemp':           
    [{idemp:idinput.val()}]},
    function(data){
        if(data!="")
        {
            alert(data);
        }
        else
        {
            alert("update error");
        }
    });
    alert("update");
    editdataemp();
});*/

/*function getempdata(idemp){
    $.ajax({
                type : "GET",
                url: base_url+'/emp_controller/emp/'+idemp,
                dataType: "json",
                success: function(data)
                {
                    alert(data);
                }
    });
}*/
/*$("#bodyshowdataemp").on('click','td center > button',function(event){
    console.log(event.target.id);
    //getempdata(event.target.id);
    $.ajax({
                type : "GET",
                url: base_url+'/emp_controller/emp/EMP00001',
                dataType: "json",
                success: function(data)
                {
                    console.log(data);
                }
    });
});*/

    /*$.ajax({
                type : "POST",
                url: "emp_controller/selectusernameemp",
                data: {usernamecheck:'basadultindy0'},
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data)
                {
                    if(data!="[]"){
                        statususername=0;
                        $("#showstatususer").html("");
                        $("#showstatususer").append("User นี้ซ้ำไม่สามารถใช้งานได้");
                    }
                    else{
                        statususername=1;
                        $("#showstatususer").html("");
                        $("#showstatususer").append("User นี้สามารถใช้งานได้");
                    }
                }
    });*/


/*$.post('emp_controller/selectemp',function(data){
                $.each(data.empselect,function(ID,emp){
                    /*console.log(emp.employee_id+" "+emp.employee_name+" "+emp.employee_lname+
                    " "+emp.employee_username+" "+emp.position_name+" "+emp.employee_idcard+" "+
                    emp.telemployee_tel+" "+emp.employee_email);*/
                    /*$("#bodyshowdataemp").append('<tr><td>'+emp.employee_id+'</td><td>'+emp.employee_name+
                    ' '+emp.employee_lname+'</td><td>'+emp.position_name+'</td><td>'+emp.employee_username+
                    '</td><td>'+emp.telemployee_tel+'</td><td><button type="button" class="btn btn-success" value='+emp.employee_id+'>เลือก</button></td></tr>');
                });
                        //console.log(data.empselect);
        },"json");
    
    $('#paging').jqPagination({
    // Set the max number of pages
    	max_page : 6,
    // set the call back function for when a page change has been requested
    	paged : function(page) {
    		alert(page);
    	}
    });*/