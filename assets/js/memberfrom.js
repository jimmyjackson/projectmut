var base_url = window.location.origin+"/CI";

var member = function(){

    return {

        member_init_list : function(){

            $('#selectshowtype').multiselect({
                nonSelectedText: 'แสดงจาก',
                includeSelectAllOption: true,
                selectAllText: 'แสดงทั้งหมด',
                 /*enableFiltering: true,*/
                buttonWidth: '100px'
            });

            $('#selectsearch').multiselect({
                nonSelectedText: 'ค้นหาจาก',
                includeSelectAllOption: true,
                selectAllText: 'เลือกทั้งหมด',
                 /*enableFiltering: true,*/
                buttonWidth: '100px'
            });

            $("#btaddmem").click(function(){
                window.location.href=base_url+"/member_controller/member/";
            });

            $("#btsearch").click(function(){
                getempdatarows(1);
            });

        },

        member_init_add : function(){

            member.member_hide_show();
            member.load_address_position();
            member.member_reset_add();
            member.member_check_username();
            $("#btsave").click(function(){
                member.member_submit();
            });
        },

        member_check_username : function(){

            $("#btcheckuser").click(function(){
                if($("#user").val()!=""&&
                ($("#user").val().length>=6&&
                $("#user").val().length <= 16)
                ){
                    $.post(base_url+'/emp_controller/selectusernameemp',
                    {usernamecheck:$("#user").val()},function(data){

                        if(data.uc!=""||$("#user").val()=="")
                        {
                            statususername=0;
                            $("#showstatususer").html("");
                            $("#showstatususer").append("<i class='glyphicon glyphicon-remove text-danger' style='font-size:25px;'></i>");
                            $("#user").focus();
                        }
                        else{
                            statususername=1;
                            $("#showstatususer").html("");
                            $("#showstatususer").append("<i class='glyphicon glyphicon-ok text-success' style='font-size:25px;'></i>");
                        }

                    },"json");
                }
                else{
                    statususername=0;
                    $("#showstatususer").html("");
                    $("#showstatususer").append("<i class='glyphicon glyphicon-remove text-danger' style='font-size:25px;'></i>");
                    userinput.focus();
                }

            });

        },

        member_reset_add : function(){

            $("#btcancel").click(function(){
                if($('#id').val()=='Auto_Id')
                    window.location.href=base_url+"/member_controller/member/";
                else
                    window.location.href=base_url+
                        "/member_controller/member/"+$('#id').val();
            });

        },

        member_init_edit : function(){

            member.member_hide_show();
            member.load_address_position();
            member.member_reset_edit();
            member.member_reset_password();
            member.member_expiration_date();
            member.member_edit();
            member.member_delete();
            $.ajax({
                type : "GET",
                url: base_url+"/member_controller/selectdatamember/"+$("#id").val(),
                dataType: "json",
                success: function(data)
                {
                    /*console.log(data);*/
                    /*$("#id").val(data[0].member_id);*/
                    $("#name").val(data[0].member_name);
                    $("#lname").val(data[0].member_lname);
                    $("#birthdate").val(data[0].member_brithday.substring(8)+'/'
                    +data[0].member_brithday.substring(5,7)+'/'
                    +data[0].member_brithday.substring(0,4));
                    $("#regisdate").val(data[0].member_registerdate.substring(8)+'/'
                    +data[0].member_registerdate.substring(5,7)+'/'
                    +data[0].member_registerdate.substring(0,4));
                    $("#email").val(data[0].member_email);
                    $("#user").val(data[0].member_username);
                    $("#address").val(data[0].member_address);

                    if(data[0].expiration_date!=null){
                        $("#expirationdate").val(data[0].expiration_date.substring(8)+'/'
                        +data[0].expiration_date.substring(5,7)+'/'
                        +data[0].expiration_date.substring(0,4));
                    }

                    if(data[0].id_membertype=='MEMTY00001'){
                        $("#typemember option[value='1']").attr('selected', 'selected');
                        $("#idstudentcard").val(data[0].id_card);
                        $("#tel").val("");
                        $("#tellist option").remove();
                        $("#idcard").val("");
                        $("#idcard").prop("disabled",true);
                        $("#tel").prop("disabled",true);
                        $("#tellist").prop("disabled",true);
                        $("#btadd").hide();
                        $("#btdel").hide();
                        $("#idstudentcard").prop("disabled",false);
                    }
                    else{
                        $("#typemember option[value='2']").attr('selected', 'selected');
                        $("#idcard").val(data[0].id_card);
                        $("#idstudentcard").val("");
                        $("#idcard").prop("disabled",false);
                        $("#tel").prop("disabled",false);
                        $("#tellist").prop("disabled",false);
                        $("#btadd").show();
                        $("#btdel").show();
                        $("#idstudentcard").prop("disabled",true);
                    }


                   if(data[0].member_image!=null)
                        $("#imageprofile").attr('src',data[0].member_image);

                    if(data[0].member_sex=="ชาย")
                        $("#sex option[value='ชาย']").attr('selected', 'selected');
                    else
                        $("#sex option[value='หญิง']").attr('selected', 'selected');


                    $("#province > option").each(function()
                    {
                        if(data[0].id_province==this.value)
                            $(this).attr('selected', 'selected');
                    });

                    showallzone_edit(data[0].id_province,data[0].id_zone);
                    showalldistrict_edit(data[0].id_province,data[0].id_zone,
                    data[0].id_district);
                    showallpostcode_edit(data[0].id_province,data[0].id_zone,data[0].id_district,
                    data[0].id_postcode);

                    if(data[0].telpersonnel_tel!=null)
                        for(var i=0;i<data.length;i++)
                        {
                            $("#tellist").append("<option value='"+data[i].telpersonnel_tel+
                            "'>"+data[i].telpersonnel_tel+"</option>");
                        }

                }
        });
        },

        member_reset_password : function(){

        $("#bt_edit_pass").click(function(){

            if($("#conpass_ed").val() != $("#pass_ed").val()){
                $("#show_status_pass").html("กรอก Password กับ Confirm Password ให้ตรงกัน");
                $("#pass_ed").focus();
            }

            else if($("#pass_ed").val().length < 6 || $("#pass_ed").val().length > 16){
                $("#show_status_pass").html("กรอก Password ความยาวตั้งแต่ 6 ถึง 15 ตัวอักษร");
                $("#pass_ed").focus();
            }

            else{
                $("#show_status_pass").html("Password สามารถใช้งานได้");
                var a = confirm("ต้องการเปลี่ยน Password ใช่หรือไม่");
                if(a){
                    $.ajax({
                        type : "POST",
                        url: base_url+"/member_controller/edit_password_member",
                        data: {'objmember':[{
                                password_edit:$("#pass_ed").val(),id_member:$("#id").val()}]},
                        dataType: "json",
                        success: function(data)
                        {
                            if(data === true){
                                alert("เปลี่ยน Password สำเร็จ");
                                $("#pass_ed").val("");
                                $("#conpass_ed").val("");
                                doOverlayClose();
                            }else{
                                alert("เปลี่ยน Password ไม่สำเร็จ");
                            }
                        }
                    });

                }
            }

        });

        },

        member_edit : function(){
            $("#btsave").click(function(){
                member.member_submit();
            });
        },

        member_reset_edit : function(){
            member.member_reset_add();
        },

        member_delete : function(){

            $("#btdelete").click(function(){
                //alert("delect");
                var a = confirm("ต้องการลบข้อมูลใช่หรือไม่");
                if(a){
                    $.post(base_url+'/member_controller/deletemember',
                    {idmember:$('#id').val()},function(data){});

                    window.location.href=base_url+"/member_controller";
                }
            });

        },

        load_address_position : function(){

            showallposition();
            showallprovince();

            $("#province").change(function(){
                $("#zone option").remove();
                $("#district option").remove();
                $("#postcode option").remove();
                $("#zone").append("<option value=''>อำเภอ</option>");
                $("#district").append("<option value=''>ตำบล</option>");
                $("#postcode").append("<option value=''>รหัสไปรษณีย์</option>");
                showallzone($("#province").val());
            });
            $("#zone").change(function(){
                $("#district option").remove();
                $("#postcode option").remove();
                $("#district").append("<option value=''>ตำบล</option>");
                $("#postcode").append("<option value=''>รหัสไปรษณีย์</option>");
                showalldistrict($("#province").val(),$("#zone").val());
            });
            $("#district").change(function(){
                $("#postcode option").remove();
                $("#postcode").append("<option value=''>รหัสไปรษณีย์</option>");
                showallpostcode($("#province").val(),$("#zone").val(),$("#district").val());
            });

        },

        member_submit : function(){

            if(!validate_member.checkEmpty()){
                event.preventDefault();
                return false;
            }
            else{
                var msg="";

                if($('#statuspage').val()=='edit')
                {
                    msg = "ต้องการแก้ไขข้อมูลใช่หรือไม่";
                }
                else{
                    msg = "ต้องการเพิ่มข้อมูลใช่หรือไม่";
                }

                var a = confirm(msg);

                if(a){
                    $("#id").prop('disabled',false);
                    $("#memForm").submit();
                }
                else{
                    event.preventDefault();
                    return false;
                }

            }

        },

        member_hide_show : function(){

            $("#btadd").hide();
            $("#btdel").hide();

            $("#typemember").change(function() {

                if($("#typemember").val()=="1"){

                    $("#tel").val("");
                    $("#tellist option").remove();
                    telobj = [];
                    teldelobj = [];
                    $("#idcard").val("");
                    $("#idcard").prop("disabled",true);
                    $("#tel").prop("disabled",true);
                    $("#tellist").prop("disabled",true);
                    $("#btadd").hide();
                    $("#btdel").hide();
                    $("#idstudentcard").prop("disabled",false);

                }
                else{

                    $("#idstudentcard").val("");
                    $("#idcard").prop("disabled",false);
                    $("#tel").prop("disabled",false);
                    $("#tellist").prop("disabled",false);
                    $("#btadd").show();
                    $("#btdel").show();
                    $("#idstudentcard").prop("disabled",true);

                }

            });

        },

        member_expiration_date : function(){
            $("#btexdate").click(function(){
                if($("#typemember").val()!='2'){
                    a = confirm("ต้องการต่ออายุสมาชิกใช่หรือไม่");
                    if(a){
                        $.post(
                            base_url+'/member_controller/edit_expiration_date_member',
                            {idmember:$('#id').val()},
                            function(data){
                                if(data == 'false')
                                    alert("ไม่สามารถต่ออายุสมาชิกได้");
                                else{
                                    $("#expirationdate").val(
                                    data.substring(8)+'/'+
                                    data.substring(5,7)+'/'+
                                    data.substring(0,4)
                                    );
                                    alert("การต่ออายุสมาชิกเสร็จสิ้น");
                                }
                            }
                        );
                    }else{
                        event.preventDefault();
                        return false;
                    }

                }
                else{
                    alert("ไม่สามารถต่ออายุสมาชิกได้");
                }
            });
        },

    };

}();

$("#memForm").submit(function(e){
    var imagebase64 = $('#base').val();
    var tels = [];

    var formData = {};
    $.each($("#memForm").serializeArray(), function(index, value) {
      formData[value.name] = value.value;
    });

    console.log(formData);

    $('#tellist > option').each(function() {
        tels.push($(this).text());
    });

    if(tels.length==0)
        tels.push("none");

    $.ajax({
        url: base_url+"/member_controller/save_member",
        type: 'POST',
        data: {'objmem':[{'tels':tels,
                          'fileimage':imagebase64,
                          'filename':$('input[type=file]').val().substring(
                            $('input[type=file]').val().lastIndexOf("\\")+1),
                          'formdata':formData}]},
        dataType: "json",
        /*mimeType:"multipart/form-data",
        contentType: false,
        cache: false,
        processData:false,*/
        success: function()/*data, textStatus, jqXHR)*/
        {
            /*if($("#typemember").val()=='2' && $("#statuspage").val()=='add' && data==''){
                var tels = [];
                $('#tellist > option').each(function() {
                    tels.push($(this).text());
                });
                $.ajax({
                    url: base_url+"/member_controller/manage_tel_personal",
                    type: 'POST',
                    data: {obj:[{id:data,tels:tels}]},
                    success: function(data_tel, textStatus, jqXHR)
                    {
                        console.log(data_tel);
                    }
                });
            }*/

            window.location.href=base_url+"/member_controller";
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            window.location.href=base_url+"/member_controller";
        }
    });
    e.preventDefault();
    return false;
});
