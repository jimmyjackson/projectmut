var statusimage=1;
var base_url = window.location.origin+"/CI";
var validate_book = function(){

    return {

        init_validate_book : function(){

            $("#file").change(function(){
                validate_book.readURL(this);
            });

            $("#lname").bind('keypress',function(e){
                return (e.which!=8&&e.which!=0&&(e.which<48||e.which>57))?true:false;
            });

            $("#size").bind('keypress',function(e){
                return (e.which!=8&&e.which!=0&&e.which!=42&&(e.which<48||e.which>57))?false:true;
            });

            $("#page,#price,#priceupdate").bind('keypress',function(e){
                return (e.which!=8&&e.which!=0&&(e.which<48||e.which>57))?false:true;
            });

        },

        checkEmpty : function(){

            if($("#name").val()==""){
                alert("กรุณากรอกชื่อ");
                $("#name").focus();
                return false;
            }
            else if($("#isbn").val()==""){
                alert("กรุณากรอกISBN");
                $("#isbn").focus();
                return false;
            }
            else if($("#size").val()==""){
                alert("กรุณากรอกขนาด");
                $("#size").focus();
                return false;
            }
            else if(!validate_book.checksizeformat()){
                alert("กรุณากรอกขนาดให้ครบและถูกต้อง");
                $("#size").focus();
                return false;
            }
            else if($("#page").val()==""){
                alert("กรุณากรอกจำนวนหน้า");
                $("#page").focus();
                return false;
            }
            else if($("#price").val()==""){
                alert("กรุณากรอกราคาปก");
                $("#price").focus();
                return false;
            }
            else if($("#priceupdate").val()==""){
                alert("กรุณากรอกราคาปัจจุบัน");
                $("#priceupdate").focus();
                return false;
            }
            else if($("#publishing").val()==""){
                alert("กรุณาเลือกสำนักพิมพ์");
                $("#publishing").focus();
                return false;
            }
            else if($("#category").val()==""){
                alert("กรุณาเลือกหมู่หนังสือ");
                $("#category").focus();
                return false;
            }
            else if($("#subcategory").val()==""){
                alert("กรุณาเลือกหมู่ย่อยหนังสือ");
                $("#subcategory").focus();
                return false;
            }
            else if(typeof($("#titlebooklist > option").val())==='undefined'){
                alert("กรุณาเพิ่มหัวเรื่องหนังสือ");
                $("#titlebook").focus();
                return false;
            }
            else if(typeof($("#authorlist > option").val())==='undefined'){
                alert("กรุณาเพิ่มผู้แต่ง");
                $("#author").focus();
                return false;
            }
            else{
                return true;
            }

        },

        readURL : function(input){

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var ftype=new Array(".gif",".jpg",".jpeg",".png");
                var a=input.value;
                var FileSize = input.files[0].size;
                var FileSizeMB = (FileSize/10485760).toFixed(6);
                var permiss=0;
                a=a.toLowerCase();
                if(a !=""){
                        for(i=0;i<ftype.length;i++){
                            if(a.lastIndexOf(ftype[i])>=0){
                                permiss=1;
                                break;
                            }else
                                continue;
                        }
                    if(permiss==0){
                       alert("กรุณาเลือกเฉพาะไฟล์นามสกุล .gif .jpg .jpeg .png เท่านั้น");
                       input.value="";
                       return false;
                    }
                    else if(FileSizeMB>1.0){
                        alert("กรุณาเลือกไฟล์ให้มีขนาดต่ำกว่า 1 MB");
                        input.value="";
                        return false;
                    }
                    else{
                        reader.onload = function (e) {
                            $('#imageprofile').attr('src', e.target.result);
                            $('#base').val(e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                        return true;
                    }
               }
            }

        },

        checksizeformat : function(){

            var filter = /(^([0-9]{1,3})+\*([0-9]{1,3})+\*([0-9]{1,3}))$/;
            return filter.test($("#size").val());

        },

    }

}();
