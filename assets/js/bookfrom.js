var base_url = window.location.origin+"/CI";

var book = function(){

    return {

        book_init_list : function(){

            $('#selectsearch').multiselect({
                nonSelectedText: 'ค้นหาจาก',
                includeSelectAllOption: true,
                selectAllText: 'เลือกทั้งหมด',
                 /*enableFiltering: true,*/
                buttonWidth: '100px'
            });

            $("#btaddbook").click(function(){
                window.location.href=base_url+"/book_controller/book/";
            });

            $("#btsearch").click(function(){
                getdatarows(1);
            });

        },

        book_init_add : function(){

            book.load_book_detial();
            book.book_add();
            book.book_reset_add();

        },

        book_add : function(){
            $("#btsave").click(function(){
                book.book_submit();
            });
        },

        book_reset_add : function(){

            $("#btcancel").click(function(){
                if($('#id').val()=='Auto_Id')
                    window.location.href=base_url+"/book_controller/book/";
                else
                    window.location.href=base_url+
                        "/book_controller/book/"+$('#id').val();
            });

        },

        book_init_edit : function(){

            book.load_book_detial();
            book.book_reset_edit();
            book.book_edit();
            book.book_delete();

            $.ajax({
                type : "GET",
                url: base_url+"/book_controller/selectdatabook/"+$("#id").val(),
                dataType: "json",
                success: function(datas)
                {
                    var authors=[],titles=[],translators=[];

                    $.each(datas, function (i, data) {
                        if (!book.checkAvailability(authors, data.id_author)){
                            authors.push({id:data.id_author,
                                          name:data.author_name});
                        }

                        if (!book.checkAvailability(titles, data.id_title)){
                            titles.push({id:data.id_title,
                                         name:data.title_name});
                        }

                        if (!book.checkAvailability(translators, data.id_translator)){
                            translators.push({id:data.id_translator,
                                              name:data.translator_name});
                        }
                    });

                    $("#name").val(datas[0].book_name);
                    $("#isbn").val(datas[0].book_isbn);
                    $("#size").val(datas[0].book_size);
                    $("#page").val(datas[0].book_page);
                    $("#price").val(datas[0].book_price);
                    $("#priceupdate").val(datas[0].pricebook_price);

                   if(datas[0].book_image!=null)
                        $("#imageprofile").attr('src',base_url+'/'+datas[0].book_image.substring(5));

                    $("#publishing > option").each(function()
                    {
                        if(datas[0].id_publishing==this.value)
                            $(this).attr('selected', 'selected');
                    });

                    $("#category > option").each(function()
                    {
                        if(datas[0].no_mubook==this.value)
                            $(this).attr('selected', 'selected');
                    });

                    showallsubmubook_edit(datas[0].no_mubook,datas[0].no_submubook);

                    if(authors[0].id!=null)
                        for(var i=0;i<authors.length;i++)
                            $("#authorlist").append("<option value='"+authors[i].id+"'>"+authors[i].name+"</option>");

                    if(titles[0].id!=null)
                        for(var i=0;i<titles.length;i++)
                            $("#titlebooklist").append("<option value='"+titles[i].id+"'>"+titles[i].name+"</option>");

                    if(translators[0].id!=null)
                        for(var i=0;i<translators.length;i++)
                            $("#translatorlist").append("<option value='"+translators[i].id+"'>"+translators[i].name+"</option>");

                }
            });
        },

        book_edit : function(){
            book.book_add();
        },

        book_reset_edit : function(){
            book.book_reset_add();
        },

        book_delete : function(){

            $("#btdelete").click(function(){
                //alert("delect");
                var a = confirm("ต้องการลบข้อมูลใช่หรือไม่");
                if(a){
                    $.post(base_url+'/book_controller/deletebook',
                    {idbook:$('#id').val()},function(data){});

                    window.location.href=base_url+"/book_controller";
                }
            });

        },

        load_book_detial : function(){

            showallpublishing();
            showallmubook();
            book.initTitlebook();
            book.initTranslator();
            book.initAuthor();

            $("#category").change(function(){
                $("#subcategory option").remove();
                $("#subcategory").append("<option value=''>หมู่ย่อยหนังสือ</option>");
                showallsubmubook($("#category").val());
            });

        },

        book_submit : function(){
            if(!validate_book.checkEmpty()){
                event.preventDefault();
                return false;
            }
            else{
                var a = confirm("ต้องการบันทึกข้อมูลใช่หรือไม่");
                if(a)
                {
                    $("#id").prop('disabled',false);
                    var imagebase64 = $('#base').val();
                    var translators = [];
                    var titles = [];
                    var authors = [];

                    var formData = {};
                    $.each($("#bookForm").serializeArray(), function(index, value) {
                    formData[value.name] = value.value;
                    });

                    $('#translatorlist > option').each(function() {
                        translators.push($(this).val());
                    });

                    if(translators.length==0)
                        translators.push("none");

                    $('#titlebooklist > option').each(function() {
                        titles.push($(this).val());
                    });

                    if(titles.length==0)
                        titles.push("none");

                    $('#authorlist > option').each(function() {
                        authors.push($(this).val());
                    });

                    if(authors.length==0)
                        authors.push("none");

                    $.ajax({
                        url: base_url+"/book_controller/save_book",
                        type: 'POST',
                        data: {'objbook':[{'translators':translators,
                                        'titles':titles,
                                        'authors':authors,
                                        'fileimage':imagebase64,
                                        'filename':$('input[type=file]').val().substring(
                                        $('input[type=file]').val().lastIndexOf("\\")+1),
                                        'formdata':formData}]},
                        dataType: "json",
                        success: function(){
                            window.location.href=base_url+"/book_controller";
                        },
                        error: function(jqXHR, textStatus, errorThrown)
                        {
                            window.location.href=base_url+"/book_controller";
                        }
                    });
                }
                else{
                    event.preventDefault();
                    return false;
                }

            }
        },

        checkAuthorList : function(authorid){
            var status=1;
            var authorlist = document.getElementById("authorlist");
            for(i=0;i<authorlist.length;i++){
                if(authorid === authorlist.options[i].value || authorid === "-21"){
                    status=0;
                    break;
                }
                else
                    status=1;
            }
            if(status==0)
                return 0;
            else
                return 1;

        },

        checkTranslatorList : function(translatorid){
            var status=1;
            var translatorlist = document.getElementById("translatorlist");
            for(i=0;i<translatorlist.length;i++){
                if(translatorid === translatorlist.options[i].value || translatorid === "-21"){
                    status=0;
                    break;
                }
                else
                    status=1;
            }
            if(status==0)
                return 0;
            else
                return 1;

        },

        checkTitlebookList : function(titlebookid){
            var status=1;
            var titlebooklist = document.getElementById("titlebooklist");
            for(i=0;i<titlebooklist.length;i++){
                if(titlebookid === titlebooklist.options[i].value || titlebookid === "-21"){
                    status=0;
                    break;
                }
                else
                    status=1;
            }
            if(status==0)
                return 0;
            else
                return 1;

        },

        initTitlebook : function(){
            var titlebookselect;

            $.ajax({
                type : "GET",
                url: base_url+'/title_controller/selectalltitle',
                dataType: "json",
                success: function(data)
                {
                    $('#titlebook').typeahead({
                        source: data,
                        triggerLength: 1,
                        onSelect: function(item){
                            titlebookselect = {'id':item.value,'value':item.text};
                        }
                    });
                }
            });

            $("#btaddtitlebook").click(function(){
                if($('#titlebook').val()!=""){
                    var check = book.checkTitlebookList(titlebookselect.id);
                    if(check==0 || titlebookselect.id==-21){
                        alert("ชื่อซ้ำกับที่มีอยู่แล้ว หรือ ไม่สามารถเพิ่มข้อมูลได้");
                        $('#titlebook').focus();
                    }
                    else{
                        $("#titlebooklist").append("<option value='"+titlebookselect.id+"'>"+titlebookselect.value+"</option>");
                        $('#titlebook').val("");
                        $('#titlebook').focus();
                    }
                }
                else
                {
                    alert("กรุณาเลือกหัวเรื่อง");
                    $('#titlebook').focus();
                }

            });

            $("#btdeltitlebook").click(function(){
                $("#titlebooklist option:selected").remove();
            });

        },

        initTranslator : function(){
            var translatorselect;

            $.ajax({
                type : "GET",
                url: base_url+'/author_controller/selectallauthor',
                dataType: "json",
                success: function(data)
                {
                    $('#translator').typeahead({
                        source: data,
                        triggerLength: 1,
                        onSelect: function(item){
                            translatorselect = {'id':item.value,'value':item.text};
                        }
                    });
                }
            });

            $("#btaddtranslator").click(function(){
                if($('#translator').val()!=""){
                    var check = book.checkTranslatorList(translatorselect.id);
                    if(check==0 || translatorselect.id==-21){
                        alert("ชื่อซ้ำกับที่มีอยู่แล้ว หรือ ไม่สามารถเพิ่มข้อมูลได้");
                        $('#translator').focus();
                    }
                    else{
                        $("#translatorlist").append("<option value='"+translatorselect.id+"'>"+translatorselect.value+"</option>");
                        $('#translator').val("");
                        $('#translator').focus();
                    }
                }
                else
                {
                    alert("กรุณาเลือกผู้แปล");
                    $('#translator').focus();
                }
            });

            $("#btdeltranslator").click(function(){
                $("#translatorlist option:selected").remove();
            });

        },

        initAuthor : function(){
            var authorselect;

            $.ajax({
                type : "GET",
                url: base_url+'/author_controller/selectallauthor',
                dataType: "json",
                success: function(data)
                {
                    $('#author').typeahead({
                        source: data,
                        triggerLength: 1,
                        onSelect: function(item){
                            authorselect = {'id':item.value,'value':item.text};
                        }
                    });
                }
            });

            $("#btaddauthor").click(function(){
                if($('#author').val()!=""){
                    var check = book.checkAuthorList(authorselect.id);
                    if(check==0 || authorselect.id==-21){
                        alert("ชื่อซ้ำกับที่มีอยู่แล้ว หรือ ไม่สามารถเพิ่มข้อมูลได้");
                        $('#author').focus();
                    }
                    else{
                        $("#authorlist").append("<option value='"+authorselect.id+"'>"+authorselect.value+"</option>");
                        $('#author').val("");
                        $('#author').focus();
                    }
                }
                else
                {
                    alert("กรุณาเลือกผู้แต่ง");
                    $('#author').focus();
                }
            });

            $("#btdelauthor").click(function(){
                $("#authorlist option:selected").remove();
            });

        },

        checkAvailability : function(arr, val){
            return arr.some(function(arrVal) {
                return val === arrVal.id;
            });
        },
    };

}();
