var statusimage=1;
var base_url = window.location.origin+"/CI";
var validate_offerorder = function(){

    return {

        init_validate_offerorder : function(){

            $("#bookunit, #vat").bind('keypress',function(e){
                return (e.which!=8&&e.which!=0&&(e.which<48||e.which>57))?false:true;
            });

        },

        checkEmpty : function(){

            if($("#year").val()==""){
                alert("กรุณาเลือกปีการศึกษา");
                $("#year").focus();
                return false;
            }
            else if($("#publishing").val()==""){
                alert("กรุณาเลือกสำนักพิมพ์");
                $("#publishing").focus();
                return false;
            }
            else if($("#vat").val()==""){
                alert("กรุณากรอกภาษีมูลค่าเพิ่ม");
                $("#vat").focus();
                return false;
            }
            else{
                return true;
            }

        },

    }

}();
