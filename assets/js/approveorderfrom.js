var base_url = window.location.origin + "/CI";

var offerorder = function() {
  var offerorderlist = [];
  return {

    offerorder_init_list: function() {

      $('#selectsearch').multiselect({
        nonSelectedText: 'ค้นหาจาก',
        includeSelectAllOption: true,
        selectAllText: 'เลือกทั้งหมด',
        /*enableFiltering: true,*/
        buttonWidth: '100px'
      });

      $("#btsearch").click(function() {
        getdatarows(1);
      });

    },

    click_OK: function() {
      $("#bt_ok").click(function() {
        if ($("#bookunit").val() == "") {
          alert("กรุณากรอกจำนวน");
          $("#bookunit").focus();
        } else {
          var unit = $("#bookunit").val();
          $.ajax({
            url: base_url + "/book_controller/get_book_and_price",
            type: 'POST',
            data: {
              'obj': [{
                id_publishing: $("#publishing").val(),
                id_book: $("#bookname").val()
              }]
            },
            dataType: "json",
            success: function(data) {
              var loop = offerorderlist.filter(function(offerorder) {
                return offerorder.id_book === data.books[0].book_id;
              }).some(function(offerorder1) {
                offerorder.edit_offerorder_detial_list(
                  offerorder1,
                  data.books[0].pricebook_price,
                  unit
                );
                return true;
              });

              if (!loop) {
                offerorder.add_offerorder_detial_list(
                  data.books[0].book_name,
                  data.books[0].pricebook_price,
                  parseInt(data.books[0].pricebook_price) * parseInt($("#bookunit").val())
                );
              }

              $('#bookname').val($('#bookname option:first-child').val()).trigger('change');
              $("#bookunit").val("");

              offerorder.load_table_list();
            }
          });
        }
      });
    },

    add_offerorder_detial_list: function(name, price, total) {
      offerorderlist.push({
        id_book: $("#bookname").val(),
        id_publishing: $("#publishing").val(),
        name: name,
        price: price,
        unit: $("#bookunit").val(),
        unitapprove: $("#bookunit").val(),
        total: total,
        totalapprove: total,
      });
    },

    edit_offerorder_detial_list: function(offerorder, price, unit) {
      offerorder.unitapprove = unit;
      offerorder.totalapprove = parseInt(price) * parseInt(unit);
    },

    edit_offerorder_detial: function(obj) {
      var id = obj.id;
      var unit = obj.parentElement.parentElement
        .parentElement.getElementsByTagName("center")[4].innerHTML;

      $("#bookunit").val(unit);
      $('#bookname').val(id).trigger('change');

      doOverlayOpen();
    },

    remove_offerorder_detial_list: function(obj) {
      var index = offerorderlist.findIndex(function(offerorder) {
        return offerorder.id_book == obj.id;
      });

      offerorderlist.splice(index, 1);
      offerorder.load_table_list();
    },

    load_table_list: function(status = "add") {
      var total = "",
        vat = "",
        subtotal = "",
        totalapprove = "",
        vatapprove = "",
        subtotalapprove = "";

      $("#bodylisttable").empty();

      offerorderlist.forEach(function(offerorder, index) {
        var newRowContent;

        if(status == "add"){
          newRowContent = "<tr><td><center>" + (index + 1) +
            "</center></td><td><center>" + offerorder.name +
            "</center></td><td><center>" + offerorder.price +
            "</center></td><td><center>" + offerorder.unit +
            "</center></td><td><center>" + offerorder.unitapprove +
            "</center></td><td><center>" + offerorder.total +
            "</center></td><td><center>" + offerorder.totalapprove +
            "</center></td><td><center>" +
            "<button type='button' id='" + offerorder.id_book +
            "' class='btn btn-primary' " +
            "onClick='JavaScript:offerorder.edit_offerorder_detial(this);'>" +
            "<i class='glyphicon glyphicon-wrench'></i></button>" +
            "<button type='button' id='" + offerorder.id_book +
            "' class='btn btn-danger' " +
            "onClick='JavaScript:offerorder.remove_offerorder_detial_list(this);'>" +
            "<i class='glyphicon glyphicon-trash'></i></button>" +
            "</center></td></tr>";
        }
        else {
          newRowContent = "<tr><td><center>" + (index + 1) +
            "</center></td><td><center>" + offerorder.name +
            "</center></td><td><center>" + offerorder.price +
            "</center></td><td><center>" + offerorder.unit +
            "</center></td><td><center>" + offerorder.unitapprove +
            "</center></td><td><center>" + offerorder.total +
            "</center></td><td><center>" + offerorder.totalapprove +
            "</center></td><td><center>" +
            "<button type='button' id='" + offerorder.id_book +
            "' class='btn btn-primary' disabled " +
            "onClick='JavaScript:offerorder.edit_offerorder_detial(this);'>" +
            "<i class='glyphicon glyphicon-wrench'></i></button>" +
            "<button type='button' id='" + offerorder.id_book +
            "' class='btn btn-danger' disabled " +
            "onClick='JavaScript:offerorder.remove_offerorder_detial_list(this);'>" +
            "<i class='glyphicon glyphicon-trash'></i></button>" +
            "</center></td></tr>";
        }

        $("#bodylisttable").append(newRowContent);
      });

      if (offerorderlist.length > 0) {
        var tot = offerorderlist.reduce(function(prev, curr) {
          return {
            total: parseInt(prev.total) + parseInt(curr.total),
            totalapprove: parseInt(prev.totalapprove) + parseInt(curr.totalapprove)
          };
        });

        total = parseInt(tot.total);
        vat = (parseInt(tot.total) * parseInt($('#vat').val())) / 100;
        subtotal = parseInt(tot.total) + parseFloat(vat);

        totalapprove = parseInt(tot.totalapprove);
        vatapprove = (parseInt(tot.totalapprove) * parseInt($('#vat').val())) / 100;
        subtotalapprove = parseInt(tot.totalapprove) + parseFloat(vatapprove);
      }

      $('#total').text(total);
      $('#vattotal').text(vat);
      $('#subtotal').text(subtotal);

      $('#totalapprove').text(totalapprove);
      $('#vattotalapprove').text(vatapprove);
      $('#subtotalapprove').text(subtotalapprove);
    },

    add_offerorder_detial: function() {
      offerorder.click_OK();
    },

    offerorder_reset_add: function() {

      $("#btcancel").click(function() {
          window.location.href = base_url +
          "/approveorder_controller/approveorder/" + $('#id').val();
      });

    },

    load_book: function (idpublishing) {
      $.ajax({
        url: base_url + "/book_controller/getbook",
        type: 'POST',
        data: {
          'id_publishing': idpublishing
        },
        dataType: "json",
        success: function(data) {
          var result = data.books.map(function(book) {
            return {
              id: book.book_id,
              text: book.book_name
            };
          });

          $("#bookname").select2({
            placeholder: "กรอกชื่อหนังสือ",
            data: result
          });
        }
      });
    },

    offerorder_init_edit: function() {
      offerorder.load_offerorder_detial(function (status) {
        $.ajax({
            type : "GET",
            url: base_url+"/approveorder_controller/selectdataapproveorder/"+$("#id").val(),
            dataType: "json",
            success: function(datas)
            {
              var id_employeeapprove,offerorder_approvedate;

              $("#dateoffer").val(
                datas[0].offerorder_offerdate.substring(5,7)+'/'+
                datas[0].offerorder_offerdate.substring(8)+'/'+
                datas[0].offerorder_offerdate.substring(0,4)
              );

              if(datas[0].id_employeeapprove == null) {
                id_employeeapprove = $("#emp").val();
              }
              else {
                id_employeeapprove = datas[0].id_employeeapprove;
              }

              if(datas[0].offerorder_approvedate == null) {
                var now = new Date();
                var day = ("0" + now.getDate()).slice(-2);
                var month = ("0" + (now.getMonth() + 1)).slice(-2);
                offerorder_approvedate = (month) + "/" + (day) + "/" + now.getFullYear();
              }
              else {
                offerorder_approvedate = datas[0].offerorder_approvedate.substring(5,7)+'/'+
                datas[0].offerorder_approvedate.substring(8)+'/'+
                datas[0].offerorder_approvedate.substring(0,4);
              }

              $("#empidapprove").val(id_employeeapprove);
              $("#dateapprove").val(offerorder_approvedate);

              $("#empid").val(datas[0].id_employeeoffer);
              $("#vat").val(datas[0].offerorder_offervat);
              $("#vat2").val(datas[0].offerorder_offervat);

              $("#year").val(
                (datas[0].offerorder_year+'/'+datas[0].offerorder_semester)
              );

              $("#publishing").val(datas[0].id_publishing);

              offerorder.load_book($("#publishing").val());

              datas.forEach(function (data) {
                var unitapprove,totalapprove;

                if(data.offerorderdetial_approveunit == null) {
                  unitapprove = data.offerorderdetial_offerunit;
                } else {
                  unitapprove = data.offerorderdetial_approveunit;
                }

                if(data.offerorderdetial_approveprice == null) {
                  totalapprove = data.offerorderdetial_offerprice;
                } else {
                  totalapprove = data.offerorderdetial_approveprice;
                }

                offerorderlist.push({
                  id_book: data.id_book,
                  id_publishing: data.id_publishing,
                  name: data.book_name,
                  price: data.pricebook,
                  unit: data.offerorderdetial_offerunit,
                  unitapprove: unitapprove,
                  total: data.offerorderdetial_offerprice,
                  totalapprove: totalapprove,
                });
              });

              $("#year").prop("disabled",true);
              $("#publishing").prop("disabled",true);
              $("#vat").prop("disabled",true);
              $("#vat2").prop("disabled",true);

              $("#status").val(datas[0].offerorder_status);

              if(datas[0].offerorder_status=="รออนุมัติ" || datas[0].offerorder_status=="ไม่อนุมัติ"){
                $("#btprint").hide();
                offerorder.load_table_list();
              }
              else {
                $("#btadddetial").hide();
                $("#btcon").hide();
                $("#btsave").hide();
                $("#btdelete").hide();
                $("#btcancel").hide();
                offerorder.load_table_list("edit");
              }
            }
        });
      });
      offerorder.offerorder_reset_edit();
      offerorder.add_offerorder_detial();
      offerorder.offerorder_delete();
      offerorder.offerorder_edit();
    },

    offerorder_edit: function() {
      $("#btsave").click(function() {
        offerorder.offerorder_submit('save');
      });
      $("#btcon").click(function() {
        offerorder.offerorder_submit('con');
      });
    },

    offerorder_reset_edit: function() {
      offerorder.offerorder_reset_add();
    },

    offerorder_delete: function() {

      $("#btdelete").click(function() {
        var a = confirm("ไม่อนุมัติใช่หรือไม่");
        if (a) {
          $.post(base_url + '/approveorder_controller/deleteapproveorder', {
            idofferorder: $('#id').val()
          }, function(data) {});

          window.location.href = base_url + "/approveorder_controller";
        }
      });

    },

    load_offerorder_detial: function(callback) {
      showallpublishing();
      showallyear();

      if($("#publishing > option").length > 1 && $("#year > option").length > 1)
        callback(true);
      else {
        setTimeout(function () {
          callback(true);
        },700);
      }
    },

    offerorder_submit: function(status) {
      if (!validate_offerorder.checkEmpty()) {
        event.preventDefault();
        return false;
      } else {
        var a = confirm("ต้องการบันทึกข้อมูลใช่หรือไม่");
        if (a) {
          $("#id").prop('disabled', false);
          $("#dateapprove").prop('disabled', false);
          $("#empidapprove").prop('disabled', false);

          var formData = {};
          $.each($("#offerorderForm").serializeArray(), function(index, value) {
            formData[value.name] = value.value;
          });

          var checkallzero = offerorderlist.every(function (v) {
              return v.unitapprove == 0;
          });

          if(checkallzero){
            alert("ไม่สามารถอนุมัติได้เมื่อทุกรายการมีจำนวนอนุมัติเป็น 0");
          } else {
            if (offerorderlist.length == 0)
              offerorderlist.push("none");
            $.ajax({
              url: base_url + "/approveorder_controller/save_approveorder",
              type: 'POST',
              data: {
                'objapproveorder': [{
                  'total': $("#total").text(),
                  'vattotal': $("#vattotal").text(),
                  'subtotal': $("#subtotal").text(),
                  'totalapprove': $("#totalapprove").text(),
                  'vattotalapprove': $("#vattotalapprove").text(),
                  'subtotalapprove': $("#subtotalapprove").text(),
                  'status': status,
                  'formdata': formData,
                  'offerorderlist': offerorderlist
                }]
              },
              dataType: "json",
              success: function(data) {
                if(data.status == "อนุมัติ")
                  window.open(base_url+"/approveorder_controller/approveorderpdf/"+data.idofferorder);
                window.location.href = base_url + "/approveorder_controller";
              },
              error: function(jqXHR, textStatus, errorThrown) {
                window.location.href = base_url + "/approveorder_controller";
              }
            });
          }
        } else {
          event.preventDefault();
          return false;
        }

      }
    },

  };

}();
